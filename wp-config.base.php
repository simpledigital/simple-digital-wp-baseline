<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dbname');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'At6ikwxAUu-hn{$zo5&o3>a|}f]Lht|^z$H|SJC~^B/0ljnG5v`._Vd?Vw?^p.@I');
define('SECURE_AUTH_KEY',  'lX!bu;,JMsUb|j3&Pnr0mV4nG-NV`-!$k.S{dfnGB]wBf%$Q,~;.pkb$Kx2:@&?q');
define('LOGGED_IN_KEY',    'xkKAQ*aZ7bu,EqBX927:E?|RZM. oCTl#j?$)aK1yB7* vZgRmraT>$CvXt`_e):');
define('NONCE_KEY',        'zK^If. NIy/IV;$<MU{x2z`.*/PQ0Rca22Wz@-h*7D~mW 1R5^-J |h*}Dfcgr::');
define('AUTH_SALT',        '}^Ilr(6/7`b|<zB~K1e:L2UG*oe%$Y:~iz/5HNrW2k]p*!Riwl4h;LXXv4s+z6p$');
define('SECURE_AUTH_SALT', '(T9QWFe.~-tg3UwY3EN6pt*r2&V H+NhxJ*+7IILRhp+ys,Hy9sZkdIj{?v4,D.-');
define('LOGGED_IN_SALT',   'p52=:Q!JZB~wbNk@Cf-URm1]6Q-AL.480Z50`p[r+]?E4_v>G|&pQyZ`8.&vLGU|');
define('NONCE_SALT',       'l|,kd%Ja}Kmn28a;|7z-z5vfE;Uf^O(?COww.E(TzS#`?TOsZ-DvKmrG!%K$o5!W');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_intra_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
// error_reporting(E_ALL | E_WARNING | E_NOTICE);
// ini_set('display_errors', TRUE);
define('WP_DEBUG', true);
// ini_set('display_errors', 'On');
// error_reporting(-1);


/* That's all, stop editing! Happy blogging. */

/** License Keys */
define( 'ACF_PRO_KEY', 'b3JkZXJfaWQ9MTI5ODcxfHR5cGU9cGVyc29uYWx8ZGF0ZT0yMDE4LTA0LTIzIDE2OjEwOjE0' );
define( 'ACF_5_KEY', 'b3JkZXJfaWQ9MTI5ODcxfHR5cGU9cGVyc29uYWx8ZGF0ZT0yMDE4LTA0LTIzIDE2OjEwOjE0' );


/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/** ACF Options */
if ( ! defined( 'USE_LOCAL_ACF_CONFIGURATION' ) ):
    define('USE_LOCAL_ACF_CONFIGURATION', true);
endif;

/** Log variables */
$GLOBALS['user_log_file'] = dirname(__FILE__) . '/dev.log';
$GLOBALS['user_log_email'] = 'dev@itssimpledigital.com';

/** Miscellaneous options */
define( 'WP_HTTP_BLOCK_EXTERNAL', false );

/** Disallow theme editing */
define( 'DISALLOW_FILE_MODS', true );
define( 'DISALLOW_FILE_EDIT', true );

/** Prevent WordPress home page redirect after WP 4.4.1 */
# remove_filter('template_redirect', 'redirect_canonical');

/** Manually update site URLs */
# define( 'WP_SITEURL', 'http://website.local' );
# define( 'WP_HOME', 'http://website.local' );
