<?php
/*
 Plugin Name: Auto-Activate "Must-Use" Plugins
 Description: Loop through and activate all plugins in "wp-content/mu-plugins" folder
 Version: 1.0.0
 Author: Ben Wagner (Cement Marketing)
 Author URI: https://cementmarketing.com
 Text Domain: cementmarketing

 @package Auto-Activate "Must-Use" Plugins
 @author Cement Marketing
*/

function cmnt_is_mu_plugin_active( $plugin ) {
    $plugin_substr = explode('mu-plugins/', $plugin);
    if ( is_array($plugin_substr) ) {
        $plugin_str = $plugin_substr[count($plugin_substr)-1];
        return (in_array( 'wp-plugins/' . $plugin_str, (array) get_option( 'active_plugins', array() ) ) || (in_array( $plugin_str, (array) get_option( 'active_plugins', array() ) ) ));
    }
}

function cmnt_activate_mu_plugins() {
    $dir = new DirectoryIterator(dirname(__FILE__) );
    foreach ($dir as $fileinfo) :
        if ( !$fileinfo->isDot() && strpos($fileinfo->getFilename(), '.') !== 0 ) :
            $file_name = $fileinfo->getFilename();
            $base_path = dirname(__FILE__) . '/' . $file_name;
            $file_include_path = $base_path . '/' . $file_name . '.php';
            if ( is_file($file_include_path)):
                /* Check if plugin already active */
                if ( !cmnt_is_mu_plugin_active( $file_include_path ) ) :
                    /* Check for default plugin PHP initializer (plugin-name/plugin-name.php) */
                    $mu_plugin_fpath = $file_include_path;
                endif;
            else:
                /* Check directory for single PHP file (no more, no less) */
                $current_path = $base_path;
                $files =  glob($base_path . '/*.php');
                if ( is_array($files) && count($files) === 1 ) :
                    $plugin_file = $files[0];
                    /* Check if plugin already active */
                    if ( !cmnt_is_mu_plugin_active( $plugin_file ) ) :
                        /* Activate if not already active */
                        $mu_plugin_fpath = $plugin_file;
                        /* Copy Plugin Header Info to MU Plugins directory */
                    endif;
                endif;
            endif;
            if (isset($mu_plugin_fpath)):
                require_once($mu_plugin_fpath);
                $file_name = cmnt_split_string_custom($mu_plugin_fpath, '/', true);
                $mu_plugin_def_file = dirname(__FILE__) . '/' . $file_name;
                if (!is_file($mu_plugin_def_file)) :
                    $fd = fopen($mu_plugin_def_file, "wb");
                    fwrite($fd, '<?php ' . cmnt_get_file_comment_block($file_include_path, '/**' . "\n\t" . ' * Plugin Name: ' . str_replace('.php', '', $file_name) . "\n\t" . ' */'));
                    fclose($fd);
                endif;
            endif;
        endif;
    endforeach;
}

function cmnt_get_file_comment_block($file_name, $backup_string = '/* */ ') {
    if (!is_file($file_name)) {
        return $backup_string;
    } else {
        $comments = array_filter(
            token_get_all( file_get_contents($file_name) ),function($entry) { return $entry[0] == T_DOC_COMMENT; }
        );
        $file_comment = array_shift($comments);

        if (isset($file_comment[1])) {
            $return_str = $file_comment[1];
        }
        if (!isset($return_str) || is_null($return_str)) {
            $return_str = $backup_string;
        }
        return $return_str;
    }
}

if ( !function_exists('cmnt_split_string_custom') ):
    function cmnt_split_string_custom($string, $split, $last = false) {
        $array = explode($split, $string);
        if ($last) {
            if (is_array($array) && isset($array[count($array) - 1])) {
                return $array[count($array) - 1];
            }
        } else {
            return $array;
        }
    }
endif;

/* Add actions & filters */
add_action('plugins_loaded', 'cmnt_activate_mu_plugins', -10);
