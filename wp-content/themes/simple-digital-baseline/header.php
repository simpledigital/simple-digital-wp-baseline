<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package sdg
 * @subpackage  sdg
 * @since  sdg 1.0
 */

global $sdg;

?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/html5.js" type="text/javascript"></script>
    <![endif]--> <?php
    require_once(dirname( __FILE__ ) . '/inc/redirects.php');
    wp_head();
    global $sdg;
    $sdg['logged-in'] = is_user_logged_in();
    $sdg['login-page'] = is_login_page();
    if ( !$sdg['login-page'] && !$sdg['logged-in'] ):
        echo '<script>document.location="/login/";</script>';
    endif;
    $bclass = $sdg['logged-in'] ? array(clean_slug(get_current_page_path_last()), 'intranet-body') : array(clean_slug(get_current_page_path_last()), 'login-page'); ?>
</head>

<body <?php body_class($bclass); ?>> <?php

    get_template_part('parts/site-header'); ?>

    <main> <?php

        if ( !$sdg['logged-in'] && $sdg['login-page'] ):
            get_template_part('parts/login-view');
        endif;

        if ( !is_search() && !is_single() && !is_home() && (sdg_get_field('show_breadcrumbs')) ):
            get_template_part('parts/breadcrumbs');
        endif;
