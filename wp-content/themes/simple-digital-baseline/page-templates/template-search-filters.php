<?php
/**
 * Template Name: Filter & Search Directory
 *
 * @author sdg
 */


get_header();

global $sdg_opts, $sdg, $AppGlobals, $sdg_id;


$sdg['current_post_type'] = sdg_get_field('choose_post_type', $sdg_id);


while ( have_posts() ) : the_post(); ?>
    <div id="filter-filter" class="wrapper"> <?php
        get_template_part('parts/page-header-filters');

        echo sdg_get_the_content(null, '<section><article class="wrapper"><div class="row"><div class="col">', '</div></div></article></section>');

        wp_reset_query();

        $args = array(
            'post_type' => sdg_get_field('choose_post_type', $sdg_id),
            'posts_per_page' => -1,
            'display_type' => 'paged',
            'orderby' => 'post_title',
            'order' => 'ASC'
        );

        $GLOBALS['current_loop'] = new WP_Query( $args );

        get_template_part('inc/views/filter-archive-loop'); ?>
    </div> <?php

endwhile; ?>
<!-- <script src="/scripts/masonry.pkgd.min.js"></script>
<script src="/scripts/isotope.min.js"></script> -->
<script>
    window.onload=function() {
        sdg.Filters.initiateTextSearchFilter();
};</script> <?php

get_footer();