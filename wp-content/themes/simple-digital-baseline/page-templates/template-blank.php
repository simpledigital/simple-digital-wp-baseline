<?php
/**
 * Template Name: Blank Page
 *
 * @author sdg
 */


get_header();

global $sdg_opts, $sdg, $AppGlobals;

while ( have_posts() ) : the_post();

    the_content();

endwhile;

get_footer();