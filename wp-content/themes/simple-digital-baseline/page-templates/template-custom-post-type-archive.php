<?php
/**
 * Template Name: Custom Post Type Archive
 *
 * @author sdg
 */

get_header();

global $sdg_opts, $sdg, $AppGlobals, $sdg_id;

$PT = sdg_get_field('choose_post_type');
if ( !post_type_exists($PT) ) {$PT = toggle_word_singular_plural($PT); }
$sdg['current_post_type'] = $PT;

if ( file_exists(get_stylesheet_directory() . '/parts/page-header-' . str_replace_custom('_', '-', $PT) . '.php' )):
    get_template_part('parts/page-header-' . str_replace_custom('_', '-', $PT));
else:
    get_template_part('parts/page-header');
endif;

// var_log(grabfield('post_limit', $sdg_id, -1));
// var_log(grabfield('orderby', $sdg_id, -1));
wp_reset_query();
$args = array(
    'post_type' => $PT,
    'posts_per_page' => grabfield('post_limit', $sdg_id, -1),
    'display_type' => 'paged',
    'orderby' => grabfield('orderby', $sdg_id, 'menu_order'),
    'order' => 'ASC'
);

$GLOBALS['current_loop'] = new WP_Query( $args );

get_template_part('inc/views/generic-archive-loop'); ?> <?php

while ( have_posts() ) : the_post();

    get_template_part('parts/acf/flexible-content-loop');

endwhile;

get_footer();

