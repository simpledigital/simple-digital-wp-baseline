<?php
/**
 * Template Name: Discussion Board Page
 *
 * @author sdg
 */


get_header();

global $sdg_opts, $sdg, $AppGlobals;

get_template_part('parts/page-header-discussion-board');

get_footer();