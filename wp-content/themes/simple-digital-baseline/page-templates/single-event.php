<?php
/**
 * Template Name: Single Event Template
 *
 * @package sdg
 */

get_header();

global $sdg_opts, $sdg, $AppGlobals, $sdg_id;

$sdg['bread_class'] = 'block transparent-bg flush';

while ( have_posts() ) : the_post();
	$id = $sdg_id;
	$event_meta = get_post_meta($id);
	// var_log($event_meta);
	$EID = get_array_item($event_meta, 'ID');
	$vid = get_array_item($event_meta, '_EventVenueID');
	$date = split_string_custom(sdg_get_event_start_date_meta($event_meta, 'M j'), ' ');
	$time = sdg_get_event_start_date_meta($event_meta, 'ga'); ?>

    <section class="row wrapper-wrap condense more alternate-bg alt equal-heights">
        <article class="col medium-8 equal">
            <nav id="breadcrumb-nav" class="breadcrumb-nav breadcrumbs bar no-pad <?php echo grab($sdg, 'bread_class'); ?>">
            	<ul>
            		<li><a href="/events/">Events</a></li>
            		<li><a class="current" href="<?php sdg_get_the_permalink($sdg_id); ?>"><?php sdg_the_title($sdg_id); ?></a></li>
            	</ul>
    		</nav>
            <div class="row pad-bottom equal-heights-inner">
                <div class="col small-2 medium-2 pad less">
					<figure>
						<a href="<?php the_permalink($EID); ?>" class="thumbnail post-thumbnail">
						    <span class="emblem block">
						        <time>
						            <strong class="uppercase h5">
						                <?php echo get_array_item($date, 0); ?><br/><?php echo get_array_item($date, 1); ?>
						            </strong>
						        </time>
						    </span>
						</a>
					</figure>
                </div>
                <div class="col small-10 medium-9">
                	<h3 class="black-color normal-weight min-push"><?php sdg_the_title($id); ?></h3>
                	<h5 class="primary-color light-weight italic pull-up">
                	    <?php echo trim($time); ?> <span class="pipe">&nbsp;|&nbsp;</span> <?php echo tribe_get_venue($id) ?>
                	</h5>
                	<div class="row">
                	    <div class="col">
                	        <figure>
                	            <?php get_the_post_thumbnail(get_the_id(), 'full'); ?>
                	        </figure>
                	    </div>
                	</div>
                	<div class="row">
                	    <div class="col">
							<?php the_content(); ?>
                	    </div>
                	</div>
                </div>
            </div>
        </article>

        <aside class="col medium-4 sidebar equal">
            <?php get_template_part('parts/snippets/back-to-parent-btn'); ?>

            <?php get_template_part('parts/upcoming-events'); ?>
        </aside>
    </section> <?php

endwhile; ?>

<script></script> <?php

get_footer();
