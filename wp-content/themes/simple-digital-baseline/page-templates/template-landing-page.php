<?php
/**
 * Template Name: Landing Page
 *
 * @author sdg
 */


get_header();

global $sdg_opts, $sdg, $AppGlobals;

get_template_part('parts/page-header');

while ( have_posts() ) : the_post();

    echo sdg_get_the_content(null, '<section><article class="wrapper"><div class="row"><div class="col">', '</div></div></article></section>');

    get_template_part('parts/loops/page-sections-loop');

    get_template_part('parts/acf/flexible-content-loop');

endwhile;

get_footer();