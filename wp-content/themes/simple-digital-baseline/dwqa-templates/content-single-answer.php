<?php
/**
 * The template for displaying single answers
 *
 * @package DW Question & Answer
 * @since DW Question & Answer 1.4.3
 */
?>
<div class="container">
	<div class="row answer">
		<div class="col medium-12">
			<div class="<?php echo dwqa_post_class() ?>">
				<?php if ( dwqa_current_user_can( 'edit_question', dwqa_get_question_from_answer_id() ) ) : ?>
				<?php elseif ( dwqa_is_the_best_answer() ) : ?>
				<?php endif; ?>
				<div class="container">
					<div class="row">
						<div class="col medium-1">
							<?php $user_id = get_the_author_meta('ID'); ?>
							<span>
								<a href="<?php echo dwqa_get_author_link( $user_id ); ?>"><?php echo get_avatar( $user_id, 48 ); ?></a>
								
							</span>
						</div>
						<div class="col medium-11">
							<div class="dwqa-answer-meta">
								<a href="<?php echo dwqa_get_author_link( $user_id ); ?>"><?php echo get_the_author()?></a><?php echo dwqa_print_user_badge( $user_id ); ?> replied <?php echo human_time_diff( get_post_time( 'U', true ) ) ?> ago
								<?php if ( 'private' == get_post_status() ) : ?>
									<span><?php _e( '&nbsp;&bull;&nbsp;', 'dwqa' ); ?></span>
									<span><?php _e( 'Private', 'dwqa' ) ?></span>
								<?php endif; ?>
								<span class="dwqa-answer-actions"><?php dwqa_answer_button_action(); ?></span>
							</div>
							<div class="dwqa-answer-content"><?php the_content(); ?></div>
						</div>
					</div>
				</div>
				<?php do_action('dwqa_after_show_content_answer', get_the_ID()); ?>
				<?php comments_template(); ?>
			</div>
		</div>
	</div>
</div>