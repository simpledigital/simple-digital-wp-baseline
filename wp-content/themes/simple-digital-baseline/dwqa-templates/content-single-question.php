<?php
/**
 * The template for displaying single questions
 *
 * @package DW Question & Answer
 * @since DW Question & Answer 1.4.3
 */


$category = wp_get_post_terms( get_the_ID(), 'dwqa-question_category' );
$catid = get_term_by('name', $category[0]->name, 'dwqa-question_category')->term_id;
$dwqacats = get_categories( array( 	'taxonomy' => 'dwqa-question_category', "hide_empty" => 0, "orderby"   => "menu_order") );
?>

<?php do_action( 'dwqa_before_single_question_content' ); ?>
	<div class="container">
		<div class="row">
			<div class="col medium-12">
				<div class="dwqa-question-item">
					<div class="container">
						<div class="row">
							<div class="col medium-2">
								<div class="dwqa-question-meta">
									<?php $user_id = get_the_author_meta('ID'); ?>
									<div class="dwqa-question-meta">
										<span>
											<a href="<?php echo site_url(); ?>/discussion-board/?user=<?php echo get_the_author(); ?>" class="internal-link image-link">
												<?php echo get_avatar( $user_id, 48 ); ?>
											</a>
											<p>Posted by:</p>
											<div class="author"><a href="<?php echo site_url(); ?>/discussion-board/?user=<?php echo get_the_author(); ?>" class="internal-link image-link"><?php echo get_the_author_meta('display_name'); ?></a></div>
											<?php echo dwqa_print_user_badge( $user_id ); ?>
										</span>
									</div>
									<span class="dwqa-question-actions"><?php dwqa_question_button_action() ?></span>
								</div>
							</div>
							<div class="col medium-10">
								<div class="dwqa-question-content">
									<h2><?php sdg_the_title(); ?></h2>
									<?php the_content(); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php do_action('dwqa_after_show_content_question', get_the_ID()); ?>
		<div class="container question-footer">
			<div class="row">
				<div class="col medium-12">
					<div class="dwqa-question-footer">
						<div class="dwqa-question-meta">
							<div class="posted-on">Posted on <?php echo date('m/j/Y',strtotime(get_the_date())); ?></div>
							<div class="dwqa-answers-title"><?php printf( __( 'Responses: %s', 'dwqa' ), dwqa_question_answers_count( get_the_ID() ) ) ?></div>
							<?php echo get_the_term_list( get_the_ID(), 'dwqa-question_tag', '<span class="dwqa-question-tag">' . __( 'Question Tags: ', 'dwqa' ), ', ', '</span>' ); ?>
							<?php if ( dwqa_current_user_can( 'edit_question', get_the_ID() ) ) : ?>
								<?php if ( dwqa_is_enable_status() ) : ?>
								<div class="dwqa-question-category">
									<?php _e ( 'The Topic Type is:', 'dwqa' ) ?>
									<select id="dwqa-question-category" data-nonce="<?php echo wp_create_nonce( '_dwqa_update_category_nonce' ) ?>" data-post="<?php the_ID(); ?>">
										<?php
											foreach($dwqacats as $dwqacat):
												?><option <?php selected( $dwqacat->term_id, $catid )?> value="<?php echo $dwqacat->term_id ?>"><?php echo $dwqacat->name ?></optio><?php
											endforeach;
										?>

									</select>
								</div>
								<?php endif; ?>
							<?php endif; ?>
							<?php if ( dwqa_current_user_can( 'edit_question', get_the_ID() ) ) : ?>
								<?php if ( dwqa_is_enable_status() ) : ?>
								<div class="dwqa-question-status">
									<?php _e( 'This thread is:', 'dwqa' ) ?>
									<select id="dwqa-question-status" data-nonce="<?php echo wp_create_nonce( '_dwqa_update_privacy_nonce' ) ?>" data-post="<?php the_ID(); ?>">
										<optgroup label="<?php _e( 'Status', 'dwqa' ); ?>">
											<option <?php selected( dwqa_question_status(), 'open' ) ?> value="open"><?php _e( 'Open', 'dwqa' ) ?></option>
											<option <?php selected( dwqa_question_status(), 'close' ) ?> value="close"><?php _e( 'Closed', 'dwqa' ) ?></option>
											<option <?php selected( dwqa_question_status(), 'resolved' ) ?> value="resolved"><?php _e( 'Solved', 'dwqa' ) ?></option>
										</optgroup>
									</select>
								</div>
								<?php endif; ?>
							<?php endif; ?>
						</div>
					</div>
					<?php do_action( 'dwqa_before_single_question_comment' ) ?>
					<?php comments_template(); ?>
					<?php do_action( 'dwqa_after_single_question_comment' ) ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php do_action( 'dwqa_after_single_question_content' ); ?>
