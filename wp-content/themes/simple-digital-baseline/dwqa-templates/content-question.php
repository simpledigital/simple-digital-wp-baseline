<?php
/**
 * The template for displaying question content
 *
 * @package DW Question & Answer
 * @since DW Question & Answer 1.4.3
 */

global $dwqa;
?>
<div class="row dwqa-question-row row-eq-height">
	<div class="col medium-8">
		<div class="<?php echo dwqa_post_class(); ?>">
			<?php
					global $post;
					$user_id = get_post_field( 'post_author', get_the_ID() ) ? get_post_field( 'post_author', get_the_ID() ) : false;
					$time = human_time_diff( get_post_time( 'U', true ) );
					$text = __( 'asked', 'dwqa' );
					$latest_answer = dwqa_get_latest_answer();
					if ( $latest_answer ) {
						$time = human_time_diff( strtotime( $latest_answer->post_date_gmt ) );
						$text = __( 'answered', 'dwqa' );
					}
			?>
			<div class="dwqa-question-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>&nbsp;&nbsp;<?php echo dwqa_print_user_badge($user_id) ?></div>
		
			<div class="dwqa-question-snip"><?php echo get_the_excerpt(); ?></div>
		</div>
	</div>
	<div class="col medium-1 center">
			<?php 
				echo get_post_time( 'n/j/y', true );
			?>
	</div>
	<div class="col medium-1 center">
		<div class="dwqa-responses">
			<?php echo dwqa_question_answers_count(); ?>
		</div>
	</div>
	<div class="col medium-1 center">
		<div class="dwqa-category-img">
		<?php 
					$catid = (get_the_terms( get_the_ID(), 'dwqa-question_category'));
					$img =  get_field('image', 'dwqa-question_category_' . $catid[0]->term_id);
					$img = ( $img == '' ? get_stylesheet_directory_uri() . '/assets/img/Status_CompanyUpdate.png' : $img );
				$cat_title = $catid[0]->name ?>
			<img src="<?php echo $img ?>" />
			<?php echo $cat_title; ?>
		</div>
	</div>
	<div class="col medium-1 center dwqa-status-response">
			<?php
			$this_dwqa_status = get_post_meta(get_the_ID(), "_dwqa_status")[0];
			switch( $this_dwqa_status ):
				case 'open':
					$img = get_stylesheet_directory_uri() . '/assets/img/Status_Open.png';
					$text = 'open';
				break;
				case 'resolved':
					$img = get_stylesheet_directory_uri() . '/assets/img/Status_Solved.png';
					$text = 'solved';
				break;
				default:
					$img = get_stylesheet_directory_uri() . '/assets/img/Status_CompanyUpdate.png';
					$text = '';
				break;
			endswitch; ?>
		<div class="dwqa-category-img <?php echo $text; ?>">
			<img src="<?php echo $img; ?>" />
			<?php echo $text; ?>
		</div>
	</div>
</div> <!-- .row -->