<?php
/**
 * The template for displaying question archive pages
 *
 * @package DW Question & Answer
 * @since DW Question & Answer 1.4.3
 */

global $dwqa, $script_version, $dwqa_sript_vars,$post;

$col1text = "Topics";
$col2text = "Date Posted";
$col3text = "Responses";
$col4text = "Type";
$col5text = "Status";
?>
<div class="<?php echo $colorclass ?>">
	<div class="dwqa-questions-archive">
		<div class="archive-header">
			<div class="row no-gutters">
				<div class="col large-4">
					<a href="/new-topic" class="button outline">Start a Thread or Discussion</a>
				</div>
				<div class="col large-8">
					<?php dwqa_search_form(); ?>
				</div>
			</div>
			<div class="filters">
				<div class="row no-gutters"
					<div class="col medium-12">
						<?php do_action( 'dwqa_before_questions_archive' ) ?>
					</div>
				</div>
			</div>
		</div>
		<div class="dwqa-header">
			<h3><?php echo $headertext; ?></h3>
		</div>
		<div class="container">
			<div class="questions-list">
			<div class="dwqa-questions-list">
			<?php do_action( 'dwqa_before_questions_list' ) ?>
			<?php if ( dwqa_has_question() ): ?>
				<div class="row dwqa-questions-list-head">
					<div class="col medium-8 dwqa-question-item-head">
						Topics
					</div>
					<div class="col medium-1 dwqa-question-item-head center">
						Date Posted
					</div>
					<div class="col medium-1 dwqa-question-item-head center">
						Responses
					</div>
					<div class="col medium-1 dwqa-question-item-head center">
						Type
					</div>
					<div class="col medium-1 dwqa-status-response dwqa-question-item-head  center">
						Status
					</div>
				</div>
				<?php while ( dwqa_has_question() ) : dwqa_the_question(); ?>
					<?php if ( get_post_status() == 'publish' || ( get_post_status() == 'private' && dwqa_current_user_can( 'edit_question', get_the_ID() ) ) ) : ?>
						<?php dwqa_load_template( 'content', 'question' ) ?>
					<?php endif; ?>
				<?php endwhile; ?>
			<?php else : ?>
				<?php dwqa_load_template( 'content', 'none' ) ?>
			<?php endif; ?>
			<?php do_action( 'dwqa_after_questions_list' ) ?>
			</div>
			<div class="dwqa-questions-footer">
			<?php if( $post->post_name == 'dashboard' ): ?>
				<a href="<?php echo $seeall ?>"class="dwqa-see-all">See All »</a>
			<?php endif; ?>
			</div>
		</div>
		<?php do_action( 'dwqa_after_questions_archive' ); ?>
	</div>
</div>
<?php wp_reset_postdata() ?>