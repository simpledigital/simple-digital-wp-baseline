<?php
/**
 * The template for displaying answers
 *
 * @package DW Question & Answer
 * @since DW Question & Answer 1.4.3
 */

global $dwqa_general_settings;
$sort = isset( $_GET['sort'] ) ? esc_html( $_GET['sort'] ) : '';
$filter = isset( $_GET['filter'] ) ? esc_html( $_GET['filter'] ) : 'all';
$filter_category = isset( $_GET['dwqa-question_category']) ? esc_html( $_GET['dwqa-question_category'] ) : '';

?>
<div class="dwqa-question-filter">
	<span><?php _e( 'Hot Topics:', 'dwqa' ); ?></span>
	<?php if ( !isset( $_GET['user'] ) ) : ?>
		<a href="<?php echo esc_url( add_query_arg( array( 'dwqa-question_category' => '' ) ) ) ?>" class="<?php echo ( 'all' == $filter && $filter_category == '' ) ? 'active' : '' ?>"><?php _e( 'ALL', 'dwqa' ); ?></a>
		<?php if ( dwqa_is_enable_status() ) : ?>
			<?php $cats = get_categories( array( 'taxonomy' => 'dwqa-question_category') );?>
			<?php foreach($cats as $cat): ?>
				<a href="<?php echo esc_url ( add_query_arg( array("dwqa-question_category" => $cat->slug) ) ) ?>" class="<?php echo $filter_category == $cat->slug ? 'active' : '' ?>"><?php echo $cat->name ?></a>
			<?php endforeach; ?>
		<?php endif; ?>
	<?php endif; ?>
	<select id="dwqa-sort-by" class="dwqa-sort-by" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
		<option selected disabled><?php _e( 'Sort by', 'dwqa' ); ?></option>
		<option <?php selected( $sort, 'views' ) ?> value="<?php echo esc_url( add_query_arg( array( 'sort' => 'views' ) ) ) ?>"><?php _e( 'Views', 'dwqa' ) ?></option>
		<option <?php selected( $sort, 'answers' ) ?> value="<?php echo esc_url( add_query_arg( array( 'sort' => 'answers' ) ) ) ?>"><?php _e( 'Answers', 'dwqa' ); ?></option>
		<option <?php selected( $sort, 'votes' ) ?> value="<?php echo esc_url( add_query_arg( array( 'sort' => 'votes' ) ) ) ?>"><?php _e( 'Votes', 'dwqa' ) ?></option>
	</select>
</div>