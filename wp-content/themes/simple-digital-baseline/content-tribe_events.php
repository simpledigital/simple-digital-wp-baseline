<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package sdg
 * @subpackage sdg
 * @since sdg 1.0
 */
?><article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
	<div class="featured-post">
		<?php _e( 'Featured post' ); ?>
	</div>
	<?php endif; ?>

	<?php if( has_post_thumbnail() ): ?>
	<div class="featured-image">
		<?php the_post_thumbnail( 'large' ); ?>
	</div>
	<?php endif; ?>
	<div class="post-content">
		<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<div class="post-date"><?php echo get_the_date(); ?></div>

		<?php if ( !is_single() ) : // Only display Excerpts for Search and Archive ?>
		<div class="entry-summary">
			<?php the_excerpt(); ?>
			<a href="<?php the_permalink(); ?>" class="blue-button button">Read More</a>
		</div><!-- .entry-summary -->
		<?php endif; ?>

<?php /*
		<footer class="entry-meta">
			<?php sdg_entry_meta(); ?>
			<?php edit_post_link( __( 'Edit' ), '<span class="edit-link">', '</span>' ); ?>
		</footer><!-- .entry-meta --> */ ?>

		<?php if ( is_single() ) : ?>
		<div class="entry-content">
			<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>' ) ); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:' ), 'after' => '</div>' ) ); ?>
		</div><!-- .entry-content -->
		<?php endif; ?>
	</div> <!-- .post-content -->
	<div class="article-lines"></div>
</article><!-- #post -->
