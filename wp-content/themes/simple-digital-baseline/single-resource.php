<?php
/**
 * Single Team Member Page
 *
 * @author sdg
 */

ob_start();

get_header();

global $sdg_id;

$linkhref = sdg_get_field('file_download', $sdg_id, sdg_get_field('resource_link', $sdg_id));

/* Redirect to a different page in the current directory that was requested */
if (sdg_validate_url($linkhref)):
    wp_redirect(clean_url($linkhref), 301);
    exit;
endif;