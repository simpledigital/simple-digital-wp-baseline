<?php
/**
 * Sector Custom Post Type
 *
 * @author   sdg Branding
 * @package sdg Branding
 * @subpackage sdg-child
 * @since    1.0
 */

/* Sectors */
// $labels = array(
//     'name' => __('Sectors', 'framework'),
//     'singular_name' => __('Sector', 'framework'),
//     'add_new' => __('Add a Sector', 'framework'),
//     'add_new_item' => __('Add a Sector', 'framework'),
//     'edit_item' => __('Edit Sector', 'framework'),
//     'new_item' => __('New Sector', 'framework'),
//     'view_item' => __('View Sector', 'framework'),
//     'search_items' => __('Search Sectors', 'framework'),
//     'not_found' =>  __('No Sector found', 'framework'),
//     'not_found_in_trash' => __('No Sectors found in Trash', 'framework'),
//     'parent_item_colon' => ''
// );

// $args = array(
//     'labels' => $labels,
//     'menu_icon' => 'dashicons-clipboard',
//     'public' => true,
//     'publicly_queryable' => false,
//     'show_ui' => true,
//     'query_var' => false,
//     'capability_type' => 'post',
//     'show_in_nav_menus' => false,
//     'hierarchical' => true,
//     'exclude_from_search' => true,
//     'menu_position' => 5,
//     'has_archive' => false,
//     'rewrite' => array( 'slug' => 'sector' ),
//     'supports' => array('title','editor','excerpt','thumbnail'),
// );

// register_post_type('sector',$args);
