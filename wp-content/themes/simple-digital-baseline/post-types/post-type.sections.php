<?php
/**
 * Sections Custom Post Type
 *
 * @author   sdg Branding
 * @package sdg Branding
 * @subpackage sdg-child
 * @since    1.0
 */

/* Sections */
$labels = array(
    'name'               => __('Sections', 'framework'),
    'singular_name'      => __('Section', 'framework'),
    'add_new'            => __('Add a Section', 'framework'),
    'add_new_item'       => __('Add a Section', 'framework'),
    'edit_item'          => __('Edit Section', 'framework'),
    'new_item'           => __('New Section', 'framework'),
    'view_item'          => __('View Section', 'framework'),
    'search_items'       => __('Search Sections', 'framework'),
    'not_found'          => __('No Section found', 'framework'),
    'not_found_in_trash' => __('No Sections found in Trash', 'framework'),
    'parent_item_colon'  => '',
);

$rewrite = array(
    'slug' => 'section',
);

$args = array(
    'labels'              => $labels,
    'menu_icon'           => 'dashicons-exerpt-view',
    'public'              => true,
    'publicly_queryable'  => false,
    'show_ui'             => true,
    'query_var'           => false,
    'capability_type'     => 'post',
    'show_in_nav_menus'   => false,
    'hierarchical'        => true,
    'exclude_from_search' => true,
    'menu_position'       => 20,
    'has_archive'         => false,
    'rewrite'             => $rewrite,
    'supports'            => array('title', 'editor', 'excerpt', 'thumbnail', 'post-attributes'),
    'show_in_menu'        => 'edit.php?post_type=page',
);

register_post_type('section', $args);
