<?php
/**
 * Sections Custom Post Type
 *
 * @author   sdg Branding
 * @package sdg Branding
 * @subpackage sdg-child
 * @since    1.0
 */

function location_post_type() {
    $labels = array(
        'name'               => _x('Locations', 'Locations', 'text_domain'),
        'singular_name'      => _x('Location', 'Location', 'text_domain'),
        'menu_name'          => __('Locations', 'text_domain'),
        'name_admin_bar'     => __('Location', 'text_domain'),
        'parent_item_colon'  => __('Parent Location:', 'text_domain'),
        'all_items'          => __('All Locations', 'text_domain'),
        'add_new_item'       => __('Add New Locations', 'text_domain'),
        'add_new'            => __('Add New', 'text_domain'),
        'new_item'           => __('New Location', 'text_domain'),
        'edit_item'          => __('Edit Location', 'text_domain'),
        'update_item'        => __('Update Locations', 'text_domain'),
        'view_item'          => __('View Location', 'text_domain'),
        'search_items'       => __('Search Locations', 'text_domain'),
        'not_found'          => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
    );

    $rewrite = array('slug' => 'locations', 'with_front' => false);

    $args = array(
        'label'                 => __('Locations', 'text_domain'),
        'description'           => __('Locations', 'text_domain'),
        'labels'                => $labels,
        'taxonomies'            => array('service_area', 'studio', 'work_sector'),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'show_in_rest'          => true,
        'rest_base'             => 'locations',
        'rest_controller_class' => 'WP_REST_Posts_Controller',
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-location',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'rewrite'               => $rewrite,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
        'query_var'             => true,
        'supports'              => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'page-attributes'),
    );
    register_post_type('location', $args);
}

/** Initialize custom post type */
add_action('init', 'location_post_type', 0);
