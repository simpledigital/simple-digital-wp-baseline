<?php
/**
 * Resources Custom Post Type
 *
 * @author   sdg Branding
 * @package sdg Branding
 * @subpackage sdg-child
 * @since    1.0
 */

function sdg_register_resources_cpt() {
    $labels = array(
        'name'               => __( 'Resources', 'framework' ),
        'singular_name'      => __( 'Resource', 'framework' ),
        'add_new'            => __( 'Add a Resource', 'framework' ),
        'add_new_item'       => __( 'Add a Resource', 'framework' ),
        'edit_item'          => __( 'Edit Resource', 'framework' ),
        'new_item'           => __( 'New Resource', 'framework' ),
        'view_item'          => __( 'View Resource', 'framework' ),
        'search_items'       => __( 'Search Resources', 'framework' ),
        'not_found'          => __( 'No Resource found', 'framework' ),
        'not_found_in_trash' => __( 'No Resources found in Trash', 'framework' ),
        'parent_item_colon'  => 'Parent',
    );

    $rewrite = array(
        'slug'         => 'resource',
        'hierarchical' => true,
        'with_front'   => false,
    );

    $args = array(
        'labels'              => $labels,
        'menu_icon'           => 'dashicons-exerpt-view',
        'public'              => true,
        'publicly_queryable'  => true,
        'show_ui'             => true,
        'query_var'           => true,
        'capability_type'     => 'page',
        'show_in_nav_menus'   => true,
        'hierarchical'        => true,
        'exclude_from_search' => false,
        'menu_position'       => 10,
        'has_archive'         => true,
        'rest_base'           => 'resources',
        'rewrite'             => $rewrite,
        'supports'            => array( 'title', 'custom-fields', 'excerpt', 'page-attributes' ),
    );

    register_post_type('resource', $args);
}

add_action( 'init', 'sdg_register_resources_cpt' );
