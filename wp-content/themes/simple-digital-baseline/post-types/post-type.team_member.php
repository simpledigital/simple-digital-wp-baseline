<?php
/**
 * Team Member Custom Post Type
 *
 * @author sdg
 */

$labels = array(
    'name' => __('Team Members', 'framework'),
    'singular_name' => __('Team Member', 'framework'),
    'add_new' => __('Add a Team Member', 'framework'),
    'add_new_item' => __('Add a Team Member', 'framework'),
    'edit_item' => __('Edit Team Member', 'framework'),
    'new_item' => __('New Team Member', 'framework'),
    'view_item' => __('View Team Member', 'framework'),
    'search_items' => __('Search Team Members', 'framework'),
    'not_found' =>  __('No Team Member found', 'framework'),
    'not_found_in_trash' => __('No Team Members found in Trash', 'framework'),
    'parent_item_colon' => ''
);

$rewrite = array(
    'slug' => 'working-at-ma/employee-directory',
    'hierarchical' => true,
    'with_front' => false,
    'query_var'    => true,
);

$args = array(
    'labels' => $labels,
    'menu_icon' => 'dashicons-groups',
    'public' => true,
    'taxonomies' => array('work_sector', 'studio', 'service_area'),
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'capability_type' => 'post',
    'show_in_nav_menus' => true,
    'hierarchical' => true,
    'exclude_from_search' => false,
    'menu_position' => 5,
    'has_archive' => false,
    'rest_base' => 'employee-directory',
    'rewrite' => $rewrite,
    'supports' => array( 'excerpt', 'thumbnail', 'author', 'title', 'page-attributes'),
);

register_post_type('team_member', $args);
