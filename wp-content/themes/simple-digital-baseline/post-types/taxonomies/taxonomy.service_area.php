<?php
/**
 * Area of Expertise Custom Taxonomy
 *
 * @author sdg
 */

$labels = array(
    'name'                       => _x('Areas of Expertise', 'Taxonomy General Name', 'text_domain'),
    'singular_name'              => _x('Area of Expertise', 'Taxonomy Singular Name', 'text_domain'),
    'menu_name'                  => __('Areas of Expertise', 'text_domain'),
    'all_items'                  => __('All Areas of Expertise', 'text_domain'),
    'parent_item'                => __('Parent Area of Expertise', 'text_domain'),
    'parent_item_colon'          => __('Parent Area of Expertise:', 'text_domain'),
    'new_item_name'              => __('New Area of Expertise', 'text_domain'),
    'add_new_item'               => __('Add New Area of Expertise', 'text_domain'),
    'edit_item'                  => __('Edit Area of Expertise', 'text_domain'),
    'update_item'                => __('Update Area of Expertise', 'text_domain'),
    'view_item'                  => __('View Area of Expertise', 'text_domain'),
    'separate_items_with_commas' => __('Separate areas with commas', 'text_domain'),
    'add_or_remove_items'        => __('Add or remove areas', 'text_domain'),
    'choose_from_most_used'      => __('Choose from most used areas', 'text_domain'),
    'popular_items'              => __('Popular areas', 'text_domain'),
    'search_items'               => __('Search areas', 'text_domain'),
    'not_found'                  => __('No areas found', 'text_domain'),
);

$rewrite = array(
    'slug'         => 'service-areas',
    'hierarchical' => true,
    'with_front'   => true,
    'query_var'    => true,
);

$args = array(
    'labels'            => $labels,
    'hierarchical'      => false,
    'public'            => true,
    'show_ui'           => true,
    'show_admin_column' => true,
    'show_in_nav_menus' => true,
    'enable_filters'    => false,
    'rewrite'           => $rewrite,
);

register_taxonomy('service_area', array('team_member', 'location'), $args);
