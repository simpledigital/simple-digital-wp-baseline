<?php
/**
 * Sector Taxonomy Custom Post Type
 *
 * @author sdg
 */

$labels = array(
    'name'                       => _x('Sectors', 'Taxonomy General Name', 'text_domain'),
    'singular_name'              => _x('Sector', 'Taxonomy Singular Name', 'text_domain'),
    'menu_name'                  => __('Sectors', 'text_domain'),
    'all_items'                  => __('All Sectors', 'text_domain'),
    'parent_item'                => __('Parent Sector', 'text_domain'),
    'parent_item_colon'          => __('Parent Sector:', 'text_domain'),
    'new_item_name'              => __('New Sector', 'text_domain'),
    'add_new_item'               => __('Add New Sector', 'text_domain'),
    'edit_item'                  => __('Edit Sector', 'text_domain'),
    'update_item'                => __('Update Sector', 'text_domain'),
    'view_item'                  => __('View Sector', 'text_domain'),
    'separate_items_with_commas' => __('Separate sectors w/ commas', 'text_domain'),
    'add_or_remove_items'        => __('Add/Remove Sectors', 'text_domain'),
    'choose_from_most_used'      => __('Commonly Used Sectors', 'text_domain'),
    'popular_items'              => __('Popular Sectors', 'text_domain'),
    'search_items'               => __('Search Sectors', 'text_domain'),
    'not_found'                  => __('Not Found', 'text_domain'),
);

$rewrite = array(
    'slug'         => 'sectors',
    'hierarchical' => true,
    'with_front'   => true,
    'query_var'    => true,
);

$args = array(
    'labels'            => $labels,
    'hierarchical'      => false,
    'public'            => true,
    'show_ui'           => true,
    'show_admin_column' => true,
    'show_in_nav_menus' => true,
    'query_var'         => true,
    'rewrite'           => $rewrite,
);

register_taxonomy('work_sector', array('team_member', 'location'), $args);
