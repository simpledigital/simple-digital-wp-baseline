<?php
/**
* Resource Category Custom Taxonomy
*
* @author sdg
*/

function sdg_register_resources_ctx() {
    $labels = array(
        'name'                       => _x( 'Resource Categories', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( 'Resource Category', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( 'Resource Categories', 'text_domain' ),
        'all_items'                  => __( 'All Resource Categories', 'text_domain' ),
        'parent_item'                => __( 'Parent Resource Category', 'text_domain' ),
        'parent_item_colon'          => __( 'Resource Category:', 'text_domain' ),
        'new_item_name'              => __( 'New Resource Category', 'text_domain' ),
        'add_new_item'               => __( 'Add Resource Category', 'text_domain' ),
        'edit_item'                  => __( 'Edit Resource Category', 'text_domain' ),
        'update_item'                => __( 'Update Resource Category', 'text_domain' ),
        'view_item'                  => __( 'View Resource Category', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate w/ commas', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add/Remove Categories', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose From Most Used Categories', 'text_domain' ),
        'popular_items'              => __( 'Popular Resource Categories', 'text_domain' ),
        'search_items'               => __( 'Search Resource Categories', 'text_domain' ),
        'not_found'                  => __( 'No Category Matches', 'text_domain' ),
    );

    $rewrite = array(
        'slug'         => 'resources',
        'hierarchical' => true,
        'with_front'   => false,
    );

    $args = array(
        'labels'                => $labels,
        'hierarchical'          => true,
        'public'                => true,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'show_in_nav_menus'     => true,
        'show_in_rest'          => true,
        'rest_base'             => 'resource-categories',
        'query_var'             => 'resource_category',
        'show_in_quick_edit' => true,
        'description'           => 'Categorize resources for better organization',
        'update_count_callback' => '_update_generic_term_count',
        'rewrite'               => $rewrite,
    );

    register_taxonomy( 'resource_category', array( 'resource' ), $args );
}

add_action('init', 'sdg_register_resources_ctx', 0);
