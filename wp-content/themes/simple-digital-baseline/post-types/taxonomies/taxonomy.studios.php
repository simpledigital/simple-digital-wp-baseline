<?php
/**
 * Studio Custom Taxonomy
 *
 * @author   sdg
 */

$labels = array(
    'name'              => _x('Studios', 'taxonomy general name'),
    'singular_name'     => _x('Studio', 'taxonomy singular name'),
    'search_items'      => __('Search Studios'),
    'all_items'         => __('All Studios'),
    'parent_item'       => __('Parent Studio'),
    'parent_item_colon' => __('Parent Studio:'),
    'edit_item'         => __('Edit Studio'),
    'update_item'       => __('Update Studio'),
    'add_new_item'      => __('Add New Studio'),
    'new_item_name'     => __('New Studio Name'),
    'menu_name'         => __('Studios'),
);

$rewrite = array(
    'slug'         => 'studios',
    'hierarchical' => true,
    'with_front'   => true,
    'query_var'    => true,
);

$args = array(
    'hierarchical'      => false,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'enable_filters'    => false,
    'rewrite'           => $rewrite,
);

register_taxonomy('studio', array('team_member', 'location'), $args);
