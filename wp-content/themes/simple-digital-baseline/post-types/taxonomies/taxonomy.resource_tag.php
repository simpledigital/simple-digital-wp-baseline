<?php
/**
 * Resources Custom Post Type
 *
 * @author   sdg Branding
 * @package sdg Branding
 * @subpackage sdg-child
 * @since    1.0
 */

$labels = array(
    'name'              => _x( 'Resource Tags', 'taxonomy general name' ),
    'singular_name'     => _x( 'Resource Tag', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Resource Tags' ),
    'all_items'         => __( 'All Resource Tags' ),
    'parent_item'       => __( 'Parent Resource Tag' ),
    'parent_item_colon' => __( 'Parent Resource Tag:' ),
    'edit_item'         => __( 'Edit Resource Tag' ),
    'update_item'       => __( 'Update Resource Tag' ),
    'add_new_item'      => __( 'Add New Resource Tag' ),
    'new_item_name'     => __( 'New Resource Tag Name' ),
    'menu_name'         => __( 'Resource Tags' ),
);

$args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'resource_tag' ),
);

register_taxonomy( 'resource_tag', array( 'resource' ), $args );