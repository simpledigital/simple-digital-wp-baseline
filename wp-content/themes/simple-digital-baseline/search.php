<?php
/**
 * Search Results
 *
 * @package sdg
 */

get_header();

$search_term = get_qv('s');
$results_count = grab($wp_query, 'found_posts');

global $sdg, $wp_query; ?>

<section class="wrapper condense more hero-section">
    <div class="row">
        <div class="col">
            <h2>Search Results</h2> <?php
            if (have_posts()): ?>
                <p> Showing <strong><?php echo $results_count; ?></strong> results for <span class="primary-color">"<strong class="strong"><?php echo $search_term; ?></strong>"</span>. </p>

                <!-- <p class="secondary-color">viewing page <u><?php echo get_query_var('page'); ?></u> of <?php echo $results_count; ?>
                </p> --> <?php
            endif; ?>
        </div>
    </div>
</section>

<section class="wrapper begin-section alt-bg"><?php
    while ( have_posts() ) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class('row wrapper condense more'); ?>>
            <div class="row">
                <div class="col medium-8 medium-centered">
                    <h4 class="post-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h4>
                    <p><?php echo strip_tags(sdg_get_first_paragraph()); ?></p>
                    <a class="cta" href="<?php the_permalink(); ?>">Read More</a><br/><br/>
                    <hr>
                </div>
            </div>
        </article> <?php
    endwhile; ?>
</section>

<?php get_template_part('parts/prev-next-links'); ?>
    </div>
    </div>
</section> <?php

get_footer();
