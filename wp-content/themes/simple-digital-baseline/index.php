<?php
/**
 * Main Template File
 *
 * @author sdg
 */

get_header();

global $sdg, $sdg_id;

$sdg['post_id'] = get_id_by_slug(get_current_page_path_first());

$sdg['current_post_type'] = sdg_get_field('choose_post_type', $sdg_id);
$sdg_id = grab($sdg, 'post_id');
get_template_part('parts/page-header-filters');
// $sdg['current_filter_terms']
$suffix='-default';

    echo sdg_get_the_content($sdg_id, '<section><article class="wrapper"><div class="row"><div class="col">', '</div></div></article></section>');

    wp_reset_query();

    $args = array(
        'post_type' => grab($sdg, 'current_post_type', toggle_word_singular_plural(get_current_page_path_last())),
        'posts_per_page' => -1,
        'display_type' => 'paged',
        'orderby' => 'term_order',
        'order' => 'ASC'
    );
    $GLOBALS['current_loop'] = new WP_Query( $args );
?>
<section id="filter-filter" class="wrapper alt-bg begin-section"> <?php

    // get_template_part('parts/search-filter-form');

    get_template_part('parts/search-filter-nav');

    get_template_part('parts/loops/blog-module'); ?>

    </section> <?php

    get_comments(); ?>

<script>
    window.onload=function() {
        $('body').addClass('page-template-template-search-filters');
        // $('main').attr('id', 'filter-filter');
        sdg.Filters.initiateTextSearchFilter();
};</script><?php
get_footer();
