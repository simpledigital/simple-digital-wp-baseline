<?php

ob_start();

if( is_archive() ): ?>
<h2><?php printf( __( 'Category Archives: %s' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></h2><?php
endif;

if( is_search() ): ?>
<h2><?php printf( __( 'Search Results: %s' ), '<span>' . get_search_query() . '</span>' ); ?></h2><?php
endif;

// Get our posts
if ( have_posts() ) :
	// Start the Loop
	while ( have_posts() ) : the_post();
		get_template_part( 'content', get_post_format() );
	endwhile;
	// Show numeric pagination vs. default prev/next
	sdg_numeric_posts_nav();
else : ?>

	<article id="post-0" class="post no-results not-found">

		<header class="entry-header">
			<h1 class="entry-title"><?php _e( 'Nothing Found' ); ?></h1>
		</header>

		<div class="entry-content">
			<p><?php _e( 'Apologies, but no results were found. Perhaps searching will help find a related post.' ); ?></p>
			<?php get_search_form(); ?>
		</div><!-- .entry-content -->

	</article><!-- #post-0 -->

<?php endif; // have_posts()?

global $post;

$loop = ob_get_contents();
ob_end_clean();

$page_for_posts_id = get_option( 'page_for_posts' );

if( $page_for_posts_id != 0 ):
	$post = get_page($page_for_posts_id);
	setup_postdata($post);
	$page_css = get_vc_css($page_for_posts_id);

	// output our page with styles with loop in place
	echo $page_css;
	echo str_replace('[[content]]', $loop, apply_filters( 'the_content', get_the_content() ) );
	rewind_posts();
else:
	echo $loop;
endif;