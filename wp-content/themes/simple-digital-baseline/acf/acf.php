<?php
/**
 * All ACF Theme Customizations
 *
 * @author sdg Branding
 */

/** Directory is relative to theme root */
// if ( ! defined( 'ACF_CUSTOM_EXPORT_DIRECTORY' ) || ! USE_LOCAL_ACF_CONFIGURATION ) :
//     define('ACF_CUSTOM_EXPORT_DIRECTORY', '/acf');
// endif;

// if ( defined('ACF_CUSTOM_EXPORT_DIRECTORY') && !file_exists( get_template_directory() . ACF_CUSTOM_EXPORT_DIRECTORY ) && defined('WP_DEBUG') && WP_DEBUG ):
//     var_log('PHP Constant ACF_CUSTOM_EXPORT_DIRECTORY does not exist.  Define in wp-config.php');
// endif;

/**
 * Load the PHP export of your ACF config in non-development environments
 *
 * To prevent loading this file (e.g. in development), add the following to your wp-config.php:
 * define('USE_LOCAL_ACF_CONFIGURATION', true);
 */
// if ( ! defined( 'USE_LOCAL_ACF_CONFIGURATION' ) || ! USE_LOCAL_ACF_CONFIGURATION ) :
// require_once dirname( __FILE__ ) . '/acf-exports.php';
// endif;

/** Load custom ACF functions */
require_once dirname( __FILE__ ) . '/inc/acf-functions.php';

/** Load shortcut/alias functions */
require_once dirname( __FILE__ ) . '/inc/acf-function-aliases.php';

/** Load custom ACF options */
require_once dirname( __FILE__ ) . '/inc/acf-options.php';

/** Load ACF extras */
require_once dirname( __FILE__ ) . '/inc/acf-extras.php';
