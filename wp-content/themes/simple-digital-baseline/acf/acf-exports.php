<?php
/**
 * Advanced Custom Fields Exports Loop
 *
 * @author sdg Branding
 */

/** Define the field group directory */
$fields_dir = dirname(__FILE__) . '/fields';

/** Loop through/register all ACF field groups in specified directory */
if(file_exists($fields_dir)) :
    $dir = new DirectoryIterator($fields_dir);
    foreach ($dir as $fileinfo) :
        if (!$fileinfo->isDot()) :
            $filename = $fileinfo->getFilename();
            if (strpos($filename, '.') !== 0 && strpos($filename, '.php') !== false) :
                require_once $fileinfo->getPath() . '/' . $fileinfo->getFilename();
            endif;
        endif;
    endforeach;
endif;
