<?php
/**
 * ACF Extras
 *
 * @author sdg Branding
 */

/**
 * Add ACF "Options" page to WP admin menu
 */
// if (function_exists('acf_add_options_page')) {
//     // global $my_mu_plugins_dir;
//     // require_once($my_mu_plugins_dir . '/validated-field-for-acf/validated-field-for-acf.php');
//     acf_add_options_page(array(
//         'page_title' => 'Options',
//         'menu_title' => 'Options',
//         'menu_slug'  => 'acf-options',
//         'capability' => 'edit_posts',
//         'redirect'   => false,
//     ));
// }

/**
 * Change ACF auto-export directory
 *
 * @return strin
 */
function sdg_acf_auto_export_dir($path) {
    /* Check if ACF_CUSTOM_EXPORT_DIRECTORY is defined (this can be set in wp-config.php) */
    if ( defined('ACF_CUSTOM_EXPORT_DIRECTORY')) {
        $acf_export_dir = get_stylesheet_directory() . ACF_CUSTOM_EXPORT_DIRECTORY . '/acf-json';
    } elseif (file_exists(get_stylesheet_directory() . '/acf/acf-json')) {
        $acf_export_dir = get_stylesheet_directory() . '/acf/acf-json';
    } elseif (file_exists(get_stylesheet_directory() . '/acf-json')) {
        $acf_export_dir = get_stylesheet_directory() . '/acf-json';
    } else {
        if (!file_exists(get_stylesheet_directory() . '/acf')) {
            mkdir(get_stylesheet_directory() . '/acf');
            mkdir(get_stylesheet_directory() . '/acf/acf-json');
        }
        $acf_export_dir = get_stylesheet_directory() . '/acf/acf-json';
    }
    if (isset($acf_export_dir) && !file_exists($acf_export_dir)) {
        mkdir($acf_export_dir);
    }
    if ( isset($acf_export_dir) && file_exists($acf_export_dir) ) {
        $path = $acf_export_dir;
        return $path;
    }
}

/**
 * Set ACF JSON load point
 *
 * @param  $paths global value
 * @return void
 */
function sdg_acf_json_load_point($paths) {
    /* Check if ACF_CUSTOM_EXPORT_DIRECTORY is defined (this can be set in wp-config.php) */
    if ( defined('ACF_CUSTOM_EXPORT_DIRECTORY')) {
        $acf_export_dir = get_stylesheet_directory() . ACF_CUSTOM_EXPORT_DIRECTORY . '/acf-json';
    } elseif (file_exists(get_stylesheet_directory() . '/acf/acf-json')) {
        $acf_export_dir = get_stylesheet_directory() . '/acf/acf-json';
    } elseif (file_exists(get_stylesheet_directory() . '/acf-json')) {
        $acf_export_dir = get_stylesheet_directory() . '/acf-json';
    } else {
      if (!file_exists(get_stylesheet_directory() . '/acf')) {
          mkdir(get_stylesheet_directory() . '/acf');
          mkdir(get_stylesheet_directory() . '/acf/acf-json');
      }
      $acf_export_dir = get_stylesheet_directory() . '/acf/acf-json';
    }
    if (isset($acf_export_dir) && !file_exists($acf_export_dir)) {
        mkdir($acf_export_dir);
    }
    /* Check if acf-json folder exists */
    if ( isset($acf_export_dir) && file_exists($acf_export_dir) ) {
        /* Remove original path (optional) */
        unset($paths);
        /* Append path */
        $paths[] = $acf_export_dir;
    }
    return $paths;
}

/**
 * Customize ACF plugin path
 *
 * @param  $paths global value
 * @return void
 */
function sdg_acf_add_plugin_path($paths) {
    $paths['my_plugin'] = get_stylesheet_directory() . '/acf/acf-json';
    return $paths;
}

/**
 * Update a specific post's custom field value
 *
 * @param  $value
 * @param  [int] $post_id
 * @param  [str] $field
 * @return void
 */
function sdg_acf_update_value_title( $value, $post_id, $field  ) {
    // override value
    $value = get_the_title($post_id);
    // do something else to the $post object via the $post_id
    // return
    return $value;
}

/**
 * Update a specific post's custom field value (Featured Image)
 *
 * @param  $value
 * @param  [int] $post_id
 * @param  [str] $field
 * @return void
 */
function sdg_acf_update_posts_per_page_count( $value, $post_id, $field  ) {
    // override value
    if ( $post_id == get_id_by_slug('news') ) {
        update_option( 'posts_per_page', $value );
    }
    return $value;
}



/**
 * Reset all meta box & ACF positions in WP admin page editor
 *
 * @return void
 */
function sdg_prefix_reset_metabox_positions(){
    $users = array(1,2,3);
    foreach ( $users as $uid) {
        delete_user_meta($uid, 'meta-box-order_post');
        delete_user_meta($uid, 'meta-box-order_page');
        delete_user_meta($uid, 'meta-box-order_custom_post_type');
    }
}


/**
 * ACF Color Picker Custom Pallete
 *
 * @return void
 */
function my_acf_collor_pallete_script() { ?>
    <script type="text/javascript">
        (function($){

            acf.add_filter('color_picker_args', function( args, $field ){

                // do something to args
                args.palettes = ['#FFFFFF', '#D54B2A', '#F8F8F8', '#333333', '#5f6383', '#93cc84', '#363b64'];

                console.log(args);
                // return
                return args;
            });

            // acf.add_action('append', function( $el ){

            //     // $el will be equivalent to the new element being appended $('tr.row')


            //     // find a specific field
            //    args.palettes = ['#1379c9', '#fc4d27', '#122463', '#a7a7a7', '#393939', '#5e5e5e'];


            //     // do something to $field

            // });


        })(jQuery);
    </script> <?php
}

function my_acf_collor_pallete_css() { ?>
    <style>
        .acf-color_picker .iris-picker.iris-border{
            width: 200px !important;
            height: 10px !important;
        }
        .acf-color_picker .wp-picker-input-wrap,
        .acf-color_picker .iris-picker .iris-slider,
        .acf-color_picker .iris-picker .iris-square{
            display:none !important;
        }
    </style> <?php
}

function multi_array_search_with_condition($array, $condition)
{
    $foundItems = array();

    foreach($array as $item)
    {
        $find = TRUE;
        foreach($condition as $key => $value)
        {
            if(isset($item[$key]) && $item[$key] == $value)
            {
                $find = TRUE;
            } else {
                $find = FALSE;
            }
        }
        if($find)
        {
            array_push($foundItems, $item);
        }
    }
    return $foundItems;
}

// function my_acf_acf_instructions_mod () {
    // Global object containing current admin page
    // global $pagenow;

    // If current page is post.php and post isset than query for its post type
    // if the post type is 'event' do something
    // if ( 'post.php' === $pagenow && isset($_GET['post']) ) {
    //     $pid = get_qv('post');
    //     $acf_repeaters = get_field( 'dynamic_content_repeater', $pid );
        // $acf_repeaters = get_field( 'dynamic_content_repeater', $pid );
        // $field['instructions'] = '<strong>Your location is: ' . $acf_repeaters['cityNameAscii'] . ' (' . $acf_repeaters['countryCode'] . ')</strong>';
        // var_dump($acf_repeaters);
        // multi_array_search_with_condition($acf_repeaters, array( 'name' => 'CarEnquiry'));
    // }
// }

// add_action( 'admin_init', 'my_acf_acf_instructions_mod' );

/**
 * Customize various ACF options below
 */

// acf/update_value - filter for every field
// add_filter('acf/update_value', 'sdg_acf_update_value', 10, 3);

// acf/update_value/type={$field_type} - filter for a specific field based on it's type
//add_filter('acf/update_value/type=select', 'sdg_acf_update_value', 10, 3);

// acf/update_value/name={$field_name} - filter for a specific field based on it's name
add_filter('acf/update_value/name=member_name', 'sdg_acf_update_value_title', 10, 3);
add_filter('acf/update_value/name=post_limit', 'sdg_acf_update_posts_per_page_count', 10, 3);
// add_filter('acf/update_value/name=member_name', 'sdg_acf_update_value', 10, 3);

// acf/update_value/key={$field_key} - filter for a specific field based on it's name
//add_filter('acf/update_value/key=field_508a263b40457', 'sdg_acf_update_value', 10, 3);

/** Register/Add all Filters/Actions */
add_filter('acf/settings/load_json', 'sdg_acf_json_load_point');
add_action('admin_init', 'sdg_prefix_reset_metabox_positions');
// remove_action('plugins_loaded', 'acf_wpcli_register_groups');
// add_filter( 'acfwpcli_fieldgroup_paths', 'sdg_acf_add_plugin_path' );
add_filter('acf/settings/save_json', 'sdg_acf_auto_export_dir');


/** Register/add filters & actions */
add_action('acf/input/admin_footer', 'my_acf_collor_pallete_script');
add_action('acf/input/admin_head', 'my_acf_collor_pallete_css');
