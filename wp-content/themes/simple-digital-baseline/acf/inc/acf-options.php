<?php
/**
 * Customized ACF Options
 *
 * @author sdg
 */

function sdg_my_acf_options_init() {
    if( function_exists('acf_add_options_page') ) {
       acf_add_options_page(array(
           'page_title' => 'Global Options',
           'menu_title' => 'Global Options',
           'menu_slug'  => 'global-settings',
           'capability' => 'edit_posts',
           'redirect'   => false,
       ));
    }
}

/** Register/Add all Filters/Actions */
add_action('acf/init', 'sdg_my_acf_options_init');
