<?php
/**
 * Functionality related to Advanced Custom Fields
 *
 * @author sdg Branding
 */

/**
 * Shortcut for `echo sdg_get_field( ... )`, accepts the same arguments
 *
 * @param str $key the custom field key
 * @param int $id the post id
 * @param mixed $default what to return if there's no custom field value
 *
 * @return void
 */
function sdg_field( $key, $id=false, $default='' ) {
    echo sdg_get_field( $key, $id, $default );
}

/**
 * Get a custom field stored in the Advanced Custom Fields plugin - by
 * running it through this function, we ensure that we don't die if the
 * plugin is uninstalled/disabled (and thus the function is undefined)
 *
 * @global $post
 * @param str $key the key to look for
 * @param int $id the post id
 * @param mixed $default what to return if there's nothing
 *
 * @return mixed (dependent upon $echo)
 */
function sdg_get_field($key, $id = false, $default = '') {
    global $post;
    $key = trim(filter_var($key, FILTER_SANITIZE_STRING));
    $result = '';

    if (function_exists('get_field')) {
        $result = (isset($post->ID) && !$id ? get_field($key) : get_field($key, $id));

        if ( function_exists('get_sub_field') && '' == $result ) {
            $result = (isset($post->ID) && !$id ? get_sub_field($key) : get_sub_field($key, $id));
        }
        if ( '' == $result ) {
            $result = get_field($key, 'option');
        }
        if ('' == $result) {
            $result = $default;
        }
    } else {
        /* get_field() is undefined, most likely due to the plugin being inactive */
        $result = $default;
    }
    return $result;
}

/**
 * Extension of `sdg_get_field( ... )`, allows additional options
 *
 * @param str $key the custom field key
 * @param str $tag optional tag name to wrap return value in
 * @param str $after optional content to append to return value
 * @param arr $extra_attrs attributes as an array to add to html element/tag
 * @param int $id the post id
 * @return str
 */
function sdg_get_field_wrap($key, $tag = '', $after = '', $extra_attrs = array(), $id = false, $default = '') {
    $field_val = sdg_get_field($key, $id, $default);
    if ('' !== $tag && '' !== $field_val) {
        $attrs = '';
        foreach ($extra_attrs as $attr => $val) {
            $attrs .= ' ' . $attr . '="' . $val . '"';
        }
        return '<' . $tag . $attrs . '>' . $field_val . '</' . $tag . '>' . $after;
    } else {
        return $field_val;
    }
}

/**
 * Shotcut for `echo sdg_get_field_wrap( ... )`
 *
 * @param str $key the custom field key
 * @param str $tag optional tag name to wrap return value in
 * @param str $after optional content to append to return value
 * @param arr $extra_attrs attributes as an array to add to html element/tag
 * @param int $id the post id
 * @param mixed $default what to return if there's no custom field value
 * @return void
 */
function sdg_field_wrap($key, $tag = '', $after = '', $extra_attrs = array(), $id = false, $default = '') {
    echo sdg_get_field_wrap($key, $tag, $after, $extra_attrs, $id, $default);
}

/**
 * Get specified $fields from the repeater with slug $key
 *
 * @global $post
 * @param str $key the custom field slug of the repeater
 * @param int $id the post id (will use global $post if not specified)
 * @param array $fields the sub-fields to retrieve
 *
 * @return array
 */
function sdg_get_repeater( $key, $id=null, $fields=array() ) {
    global $post;
    if ( ! $id ) $id =$post->ID;
    $values = array();

    if ( sdg_get_field( $key, $id, false ) && function_exists( 'has_sub_field' ) && function_exists( 'get_sub_field' ) ) {

        while ( has_sub_field( $key, $id ) ) {
            $value = array();
            foreach ( $fields as $field ){
                $value[$field] = get_sub_field( $field );
            }
            if( ! empty( $value ) ) {
                $values[] = $value;
            }
        }
    }
    return $values;
}

/**
 * Return site option value
 *
 * @param str $option the option name
 * @param str $settings the option settings group
 *
 * @return str
 */
function sdg_get_option($option, $settings = 'sdg_settings') {
    return sdg_get_field($option, 'option');
}

/**
 * Echo site option value
 *
 * @param str $option the option name
 * @param str $settings the option settings group
 *
 * @return  void
 */
function sdg_option($option, $settings = 'sdg_settings') {
    echo sdg_get_field($option, 'option');
}

function sdg_get_acf_img($key, $id = false, $size = 'full') {
    $default = get_stylesheet_directory_uri() . '/assets/img/spacer.gif';
    $img = sdg_get_field($key, $id, $default);
    if ( is_string($img) ) {
        return $img;
    } elseif ( is_int($img) ) {
        return $img;
    } else {
        return get_array_item(get_array_item($img, 'sizes'), $size, $default);
    }
}