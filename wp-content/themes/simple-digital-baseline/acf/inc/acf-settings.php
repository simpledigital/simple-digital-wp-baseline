<?php
/**
 * Customize Advanced Custom Fields Settings
 *
 * @author sdg Branding
 */

/**
 * Change ACF auto-export directory
 *
 * @return string
 */
function sdg_acf_auto_export_dir($path) {
    $path = get_stylesheet_directory() . '/components/acf/acf-json';
    return $path;
}

add_filter('acf/settings/save_json', 'sdg_acf_auto_export_dir');
