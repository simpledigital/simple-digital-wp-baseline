<?php
/**
 * Load Shortcut/alias ACF Functions
 *
 * @author sdg Branding
 */

/**
 * Alias of `sdg_get_field( ... )`
 *
 * @param str $key the custom field key
 * @param int $id the post id
 * @param mixed $default what to return if there's no custom field value
 * @return void
 */
if (!function_exists('grabfield')):
    function grabfield( $key, $id=false, $default='' ) {
        return sdg_get_field($key, $id, $default);
    }
endif;