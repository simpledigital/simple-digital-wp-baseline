<?php
/**
* sdg Branding functions and definitions
*
*
* @package sdg Branding
* @subpackage sdg-child
* @since 1.1
*/

function my_theme_enqueue_styles() {
    $parent_style = 'style-css';

    wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css');
    wp_enqueue_style(
        'app-style',
        get_stylesheet_directory_uri() . '/assets/css/app.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

$whereami = dirname( __FILE__ );

/* INCLUDE COMMON FUNCTIONS */
require_once $whereami . '/inc/sdg.common.php';

/* SHORTEN SOME FUNCTIONS */
require_once $whereami . '/inc/sdg.aliases.php';

/* INCLUDE CMS/FRAMEWORK UTILITY FUNCTIONS */
require_once $whereami . '/inc/sdg.cms.utility.php';

/* DEFINE GLOBAL VARIABLES */
require_once $whereami . '/inc/sdg.globals.php';

/* INCLUDE THEME OPTIONS */
require_once $whereami . '/inc/sdg.theme.options.php';

/* MAKE CUSTOM STYLES AVAILABLE TO MCE */
require_once $whereami . '/inc/sdg.custom.mce.php';

/* TAXONOMY DEFINITION FUNCTIONS */
require_once $whereami . '/inc/sdg.taxonomies.php';

/* POSTS TYPES DEFINITION FUNCTIONS */
require_once $whereami . '/inc/sdg.post-types.php';

/* CUSTOM USER ROLES */
require_once $whereami . '/inc/sdg.user-roles.php';

/* NAVIGATION FUNCTIONS */
require_once $whereami . '/inc/sdg.navigation.php';

/* VC CUSTOMIZATION DEFINITION */
require_once $whereami . '/inc/sdg.jscomposer.php';

/* SHORTCODES DEFINITION */
require_once $whereami . '/inc/sdg.shortcodes.php';

/* WIDGETS DEFINITION */
require_once $whereami . '/inc/sdg.widgets.php';

/* INCLUDE ADMIN INTERFACE */
require_once $whereami . '/inc/sdg.admin.php';

/* INCLUDE STYLES & SCRIPTS */
require_once $whereami . '/inc/sdg.enqueue.php';

/* INCLUDE HOOKS */
require_once $whereami . '/inc/sdg.hooks.php';

/* INCLUDE HOOKS */
require_once $whereami . '/inc/sdg.intranet.php';

/* POSTS TYPES DEFINITION FUNCTIONS */
require_once $whereami . '/acf/acf.php';

/* TRIBE EVENTS */
require_once $whereami . '/inc/sdg.plugins.php';

/* INCLUDE HOOKS */
require_once $whereami . '/inc/sdg.app.custom.php';

/* INCLUDE DEV METHODS */
require_once $whereami . '/inc/sdg.class.wp-sdg.php';

/* INCLUDE DWQA (discussion_boards) FUNCTIONS */
require_once $whereami . '/inc/sdg.dwqa.php';