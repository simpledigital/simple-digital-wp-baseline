<?php
/**
 * Events template for Tribe Events Calendar
 */

get_header();

ob_start();

// Get our posts
if ( have_posts() ) :
	// Start the Loop
	while ( have_posts() ) : the_post();
		get_template_part( 'content', get_post_type() );
	endwhile;
	// Show numeric pagination vs. default prev/next
	sdg_numeric_posts_nav();
else : ?>

	<?php if ( current_user_can( 'edit_posts' ) ) :
		// Show a different message to a logged-in user who can add posts.
	?>
		<header class="entry-header">
			<h1 class="entry-title"><?php _e( 'No posts to display' ); ?></h1>
		</header>

		<div class="entry-content">
			<p><?php printf( __( 'Ready to publish your first post? <a href="%s">Get started here</a>.' ), admin_url( 'post-new.php' ) ); ?></p>
		</div><!-- .entry-content -->

	<?php else :
		// Show the default message to everyone else.
	?>
		<header class="entry-header">
			<h1 class="entry-title"><?php _e( 'Nothing Found' ); ?></h1>
		</header>

		<div class="entry-content">
			<p><?php _e( 'Apologies, but no results were found. Perhaps searching will help find a related post.' ); ?></p>
			<?php get_search_form(); ?>
		</div><!-- .entry-content -->
	<?php endif; // end current_user_can() check ?>

	</article><!-- #post-0 -->

<?php endif; // have_posts()?

global $post;

$loop = ob_get_contents();
ob_end_clean();

$page_for_events_id = 831;

if( $page_for_events_id != 0 ):
	$post = get_page($page_for_events_id);
	setup_postdata($post);
	$page_css = get_vc_css($page_for_events_id);

	// output our page with styles with loop in place
	echo $page_css;
	echo str_replace('[[content]]', $loop, apply_filters( 'the_content', get_the_content() ) );
	rewind_posts();
else:
	echo $loop;
endif;

get_footer();