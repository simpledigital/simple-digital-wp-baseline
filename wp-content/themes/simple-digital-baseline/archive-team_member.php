<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, sdg already
 * has tag.php for Tag archives, category.php for Category archives, and
 * author.php for Author archives.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package sdg
 * @subpackage sdg
 * @since sdg 1.0
 */


get_header();

global $sdg_opts, $sdg, $AppGlobals;

get_template_part('parts/page-header');

// get our surrounding content ?>
<div class="content blog">
	<div class="vc_row wpb_row vc_row-fluid">
		<div class="vc_col-sm-12 wpb_column vc_column_container ">
			<div class="wpb_wrapper">
				<div class="wpb_text_column wpb_content_element">
					<div class="wpb_wrapper">
						<h2><?php
							if ( is_day() ) :
								printf( __( 'Daily Archives: %s' ), '<span>' . get_the_date() . '</span>' );
							elseif ( is_month() ) :
								printf( __( 'Monthly Archives: %s' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format' ) ) . '</span>' );
							elseif ( is_year() ) :
								printf( __( 'Yearly Archives: %s' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format' ) ) . '</span>' );
							else :
								_e( 'Archives' );
							endif;
						?></h2>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php

include_once('loop.php');

?>
</div> <!-- .content -->
<?php get_footer(); ?>