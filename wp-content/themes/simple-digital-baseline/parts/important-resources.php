<?php
/**
 * Important Resources
 *
 * @package sdg
 */

global $sdg;?>

<section class="module photo-banner banner white-color wrapper">
    <div class="row container">
        <div class="col">

            <h2 class="text-center white-color pad-bottom">Important Resources</h2>

            <div class="row box pad-top more ">
                <div class="col medium-6 pad-right">
                    <h6 class="min-push strong"><a class="underline-border" href="/wp-content/uploads/2018/04/Benefits-Contact-Info.pdf" target="_blank">Benefits Contact Information</a></h6>
                    <p>If you have any questions about benefits information you can find contact information for appropriate representatives.</p><br/>
                    <h6 class="min-push strong"><a class="underline-border" href="/wp-content/uploads/2018/04/Marketing-Time-Tracking-Cheat-Sheet-1.pdf" target="_blank">Marketing Information</a></h6>
                    <p>Project set-up information, templates, business card request, apparel and more.</p><br/>
                    <h6 class="min-push strong"><a class="underline-border" href="/wp-content/uploads/2018/04/Drawing-Standards.pdf" target="_blank">Drawing Standards</a></h6>
                    <p>M+A’s guidelines for how our documents are prepared and organized, important to insure consistency and clarity.</p>
                </div>
                <div class="col medium-6 pad-left">
                    <h6 class="min-push strong"><a class="underline-border" href="/resources/resource-scheduling/">Resource Calendar</a></h6>
                    <p>Live feed of conference room calendars and availability of company owned resources.</p><br/>
                    <h6 class="min-push strong"><a class="underline-border" href="/resources/education-presentation-archive/" target="_blank">Presentation Archive</a></h6>
                    <p>A repository of past presentations related to various capacities and function areas.</p><br/>
                    <h6 class="min-push strong"><a class="underline-border" href="/wp-content/uploads/2018/04/Printing-Resources.pdf" target="_blank">Printing Resources</a></h6>
                    <p>Information regarding printing protocol and company practices. </p>
                </div>
            </div> <br/>

            <div class="row text-center banner pad-top pad-bottom">
                <a href="/resources/" class="button text-center outline secondary block-center fat">See All Resources</a>
            </div>

        </div>
    </div>
</section>