<?php
/**
 * Post Meta Header
 *
 * @author sdg
 */

?>

<header class="row pad-bottom  post-header">
    <div class="col tiny-4 small-4 medium-3 large-2 pad less author-info ">
        <div class="mask clear pad-bottom ">
            <figure class="clear">
                <img class="" src="<?php echo get_avatar_url(get_the_author_ID(), 'thumbnail', get_stylesheet_directory_uri() . '/assets/img/spacer.gif'); ?>" alt="">
            </figure><br/>
        </div>
        <h6 class="alt-style text-center light-weight decrease">
            <?php get_template_part('parts/snippets/by-author'); ?>
        </h6>
    </div>
    <div class="col tiny-8 small-8 medium-9 large-9 publish-info">
        <h2 class="the-title alt-style title-case min-push"><?php sdg_the_title(); ?></h2>
        <div class="row">
            <div class="col medium-7 small-12">
                <?php get_template_part('parts/snippets/date-and-time'); ?>
            </div>
            <div class="col medium-5 small-12">
                <?php get_template_part('parts/snippets/read-time'); ?>
            </div>
        </div>
    </div>
</header>