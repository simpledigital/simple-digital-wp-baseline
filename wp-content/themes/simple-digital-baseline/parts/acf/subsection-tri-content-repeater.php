<?php
/**
 * Page Subsection Loop: Tri-Content
 *
 * @author sdg
 */

global $sdg, $post_id_override;

$id = get_array_item($sdg, 'acf_fid');

$subsections = sdg_get_field('content_subsections', $id);

foreach ($subsections as $subsection): ?>
    <div class="banner row skinny">
        <div class="col medium-3 small-4 tiny-12">
            <figure class="medium">
                <img src="<?php echo grab(grab(grab($subsection, 'subsection_thumb'), 'sizes'), '400-square'); ?>" alt="">
            </figure>
            <p class="mobile-only"><br/></p>
        </div>
        <div class="col medium-9 small-8 tiny-12">
            <h4 class="primary-color"><?php echo grab($subsection, 'subsection_title'); ?></h4>
            <p><?php echo grab($subsection, 'subsection_copy'); ?></p>
        </div>
    </div> <?php
endforeach;