<?php
/**
 * Flexible Content: Text Columns
 *
 * @author sdg
 */
global $sdg;

while ( have_rows('acf_flexible_page_builder') ) : the_row();
    $sdg['acf_data'] = array();
    $sdg['acf_data']['row_layout'] = str_replace_custom('_', '-', get_row_layout());

    get_template_part('parts/acf/flexible-content-' . $sdg['acf_data']['row_layout']);

endwhile;
