<?php
/**
 * Flexible Content: Text Columns
 *
 * @author sdg
 */
global $sdg;

$i_t_first_column = get_sub_field('first_column');
$i_t_first_size = get_sub_field('first_column_size');
$i_t_second_column = get_sub_field('second_column');
$i_t_second_size = get_sub_field('second_column_size');
$i_t_class = get_sub_field('repeater_row_class'); ?>


<section class="<?php echo $sdg['acf_data']['row_layout'] . ' ' . $i_t_class; ?>">
    <article class="wrapper">
        <div class="row">
            <div class="col medium-<?php echo $i_t_first_size;?> r-l-image">
                <?php echo $i_t_first_column; ?>
            </div>
            <div class="col medium-<?php echo $i_t_second_size;?>">
                <div class="second-text-wrap">
                    <?php echo $i_t_second_column; ?>
                </div>
            </div>
        </div>
    </article>
</section>