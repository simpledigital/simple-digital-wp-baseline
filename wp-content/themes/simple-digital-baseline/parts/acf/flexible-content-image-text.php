<?php
/**
 * Flexible Content: Image Text Columns
 *
 * @author sdg
 */
global $sdg;

$i_t_first_column = get_sub_field('first_column');
$i_t_left_size = get_sub_field('first_column_size');
$i_t_second_column = get_sub_field('second_column');
$i_t_right_size = get_sub_field('second_column_size');
$i_t_class = get_sub_field('repeater_row_class'); ?>


<section class="<?php echo $sdg['acf_data']['row_layout']; ?> <?php if ($i_t_class):echo $i_t_class; endif;?>">
    <div class="wrapper">
        <div class="row">
            <div class="col col-md-<?php echo $i_t_left_size;?> r-l-image"><?php echo wp_get_attachment_image( $i_t_first_column, 'full' ); ?></div>
            <div class="col col-md-<?php echo $i_t_right_size;?>"><div class="right-text-wrap"><?php echo $i_t_second_column; ?></div></div>
        </div>
    </div>
</section>