<?php
/**
 * Page Subsection Loop: Link List
 *
 * @author sdg
 */

global $sdg, $post_id_override;

$id = get_array_item($sdg, 'acf_fid');

$subsections = sdg_get_field('content_subsections', $id); ?>

<div> <?php
    foreach ($subsections as $subsection): ?>
        <div class="banner row condense full">
            <div class="col medium-12"> <?php
                $link_or_links = grab($subsection, grab($subsection, 'link_type'));
                $childlinks = false;
                $linkclass = 'underline';
                if ( is_string($link_or_links) || is_int($link_or_links) || (is_array($link_or_links) && count($link_or_links) == 1 ) ) {
                    if (is_int($link_or_links)) { $link_or_links = array($link_or_links); }
                    $linkhref = is_string($link_or_links) ? $link_or_links : wp_get_attachment_url(get_array_first_item($link_or_links));
                } else {
                    $linkclass = 'dont-underline trigger';
                    $linkhref = '#' . clean_slug(grab($subsection, 'subsection_title'));
                    $childlinks = $link_or_links; ?>
                    <div class="collapsible-content"> <?php
                } ?>
                <a href="<?php echo $linkhref; ?>" class="primary-color <?php echo $linkclass ?>" target="_blank"> <?php if (is_array($childlinks)): ?> <i class="fa fa-caret-down"></i> <?php endif;
                     echo grab($subsection, 'subsection_title'); ?>
                </a> <?php

                if ( is_array($childlinks) ): ?>
                        <ul class="content row banner condense" style="display: block;"> <?php
                            foreach ($childlinks as $child):
                                $finds = array('.pdf', '.jpg', '.png', '_', '-');
                                $replaces = array('', '', '', ' ', ' ');
                                //var_log($child); ?>
                                <li class="banner row condense">
                                    <h6><a href="<?php echo wp_get_attachment_url($child); ?>" class="underline pad-left more inline-block" target="_blank"><?php echo trim(ucwords(str_replace_all($finds, $replaces, basename ( wp_get_attachment_url( $child ) )))); ?></a></h6>
                                </li> <?php
                            endforeach; ?>
                        </ul>
                    </div> <!-- .collapsible-content --> <?php
                endif; ?>
            </div>
        </div> <?php
    endforeach; ?>
</div>