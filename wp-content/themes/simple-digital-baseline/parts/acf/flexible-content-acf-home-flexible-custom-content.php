<?php
/**
 * Flexible Content: Custom HTML
 *
 * @author sdg
 */
global $sdg;

$html = sdg_get_field('custom_acf_row_html'); ?>


<section class="<?php echo grab(grab($sdg, 'acf-data'), 'row-layout'); ?>">
    <?php echo $html; ?>
</section>