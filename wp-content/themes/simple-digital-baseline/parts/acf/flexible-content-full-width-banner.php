<?php
/**
 * Flexible Content: Full Width Banner
 *
 * @author sdg
 */
global $sdg;


$acf_banner_type = sdg_get_field('banner_type');
$acf_banner_bg_color = sdg_get_field('banner_background_color');
$acf_box_bg_color = sdg_get_field('box_background_color', false, '#ffffff');
$acf_slightly_transparent = sdg_get_field('slightly_transparent');
$acf_box_text_color = sdg_get_field('box_text_color');
$acf_header_text = sdg_get_field('header_text');
$acf_body_text = sdg_get_field('body_text');
$acf_cta_button_text = sdg_get_field('cta_button_text');
$acf_cta_button_url = sdg_get_field('cta_button_url');
$acf_bg_image = sdg_get_field('background_image');

$cta_button = ( $acf_cta_button_text !== '' ) ? ('<div class="wrapper container text-center"><a href="' . $acf_cta_button_url . '" class="button">' . $acf_cta_button_text . '</a></div>') : '';

// $banner_style = contains('image', $acf_banner_type) ? 'background-image: url("' .  esc_url(wp_get_attachment_image_url($acf_bg_image, 'full')) . '");' : 'background-color: ' . $acf_banner_bg_color . ';';
$banner_style = contains('image', $acf_banner_type) ? 'background-image: url(\'' .  $acf_bg_image . '\');' : 'background-color: ' . $acf_banner_bg_color . ';';

$banner_id = 'banner-' . uniqid();

if ($acf_slightly_transparent == true):
    list($r, $g, $b) = sscanf($acf_box_bg_color, "#%02x%02x%02x");
    $acf_box_bg_color = 'rgba(' . $r . ', ' . $g . ', ' . $b . ', 0.85)';
endif;

$box_style = ($acf_banner_type === 'background-image no-box') ? 'display:none;' : 'background-color: ' . $acf_box_bg_color . ';';
// $acf_first_column = get_sub_field('first_column');
// $acf_left_size = get_sub_field('first_column_size');
// $acf_second_column = get_sub_field('second_column');
// $acf_right_size = get_sub_field('second_column_size');
// $acf_class = get_sub_field('repeater_row_class');
?>

<section id="<?php echo $banner_id; ?>" class="banner <?php echo $sdg['acf_data']['row_layout']; ?> <?php echo $acf_banner_type; ?>" style="<?php echo $banner_style; ?>">
    <style type="text/css" media="screen">
        #<?php echo $banner_id; ?> .box * { color: <?php echo $acf_box_text_color; ?>; background-color: }
        #<?php echo $banner_id; ?> .box {background-color: <?php echo $acf_box_bg_color; ?>;
    </style>
    <article class="wrapper">
        <div class="row box" style="<?php echo $box_style; ?>">
            <div class="col medium-12"> <?php
                sdg_field_wrap('header_text', 'h3');
                sdg_field('body_text');
                echo $cta_button; ?>
            </div>
        </div>
    </article>
</section>
