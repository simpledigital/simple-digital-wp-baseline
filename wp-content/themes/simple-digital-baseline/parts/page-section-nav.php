<?php
/**
 * Page Section Nav
 *
 * @author sdg
 */

global $sdg, $post_id_override, $sdg_id;

$page_sections = sdg_get_field('include_sections', $sdg_id);

if ($page_sections): ?>
    <nav class="anchor-nav">
        <ul> <?php
        foreach ($page_sections as $id): ?>
            <li class="mask">
                <a class="anchor-link" href="#<?php echo get_the_slug($id); ?>">
                    <figure>
                        <h6 class="stripe"><?php echo get_the_title($id); ?></h6>
                        <span class="circle" style="background-image:url('<?php echo get_the_post_thumbnail_url($id, '400-square'); ?>');"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/spacer.gif'; ?>" alt="" width="300" height="300" /></span>

                    </figure>
                </a>
            </li><?php
        endforeach; ?>
        </ul>
    </nav> <?php
endif;