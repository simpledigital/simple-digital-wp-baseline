<?php
/**
 * Events Content
 *
 * @author sdg
 */

global $in_past_events;

if ( $in_past_events == 'true' ) {
    $EID = '140';
} else {
    $EID = get_the_ID();
}

// $event_host = get_event_host($EID);
$event_meta = get_post_meta($EID);
$EID = get_array_item($event_meta, 'ID');
$vid = get_array_item($event_meta, '_EventVenueID');
$date = split_string_custom(sdg_get_event_start_date_meta($event_meta, 'M j'), ' ');
$time = sdg_get_event_start_date_meta($event_meta, 'ga'); ?>

<article class="card equal">
    <figure>
        <a href="<?php the_permalink($EID); ?>" class="thumbnail post-thumbnail">
            <span class="emblem">
                <time>
                    <strong class="uppercase h5">
                        <?php echo get_array_item($date, 0); ?><br/><?php echo get_array_item($date, 1); ?>
                    </strong>
                </time>
            </span>
            <span class="frame">
                <img src="<?php echo sdg_get_post_thumbnail_uri(get_the_id(), '300-thumb'); ?>" alt="Event Featured Image" />
            </span>
        </a>
    </figure>

    <figcaption>
        <h5 class="black-color strong pull-up"> <?echo get_the_title($EID); ?> </h5>
        <h6 class="black-color italic pull-up">
            <?php echo trim($time); ?> <span class="pipe">&nbsp;|&nbsp;</span> <?php echo tribe_get_venue() ?>
        </h6>
    </figcaption> <?php

    $EID = get_array_item($event_meta, 'ID');
    $vid = get_array_item($event_meta, '_EventVenueID');
    $event_URL = get_array_item($event_meta, '_EventURL');

    if ( is_single() ):  ?>
        <br/>
        <h6> Location </h6>
        <p> <br/>
            <a class="button small" href="<?echo tribe_get_map_link($eid, false);?>" target="_blank">View Map</a>
        </p> <br/>
        <?php echo sdg_get_the_content($EID);
    else: ?>
        <p class="equal-inner"><?php echo strip_tags(get_the_excerpt($EID)); ?></p><?php
    endif; ?>
    <a class="cta" href="<?php the_permalink($EID); ?>">Read More</a>

</article>
