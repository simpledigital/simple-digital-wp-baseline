<?php
/**
 * Login View
 *
 * @author sdg
 */

?>

<div class="header-wrapper">
    <div class="wpb_wrapper level_0">
        <div class="row wpb_row vc_inner row-fluid">
            <div class="col vc_column_container medium-1">&nbsp;</div>
            <div class="col vc_column_container medium-10 logo">
                <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo sdg_get_site_logo(); ?>" /></a></h1>
            </div>
            <div class="col vc_column_container medium-1">&nbsp;</div>
        </div>

        <div class="row wpb_row vc_inner row-fluid">
            <div class="col vc_column_container medium-1">
                &nbsp;
            </div>
            <div class="col vc_column_container medium-10">
                <h2 class="text-center push-down less"><?php sdg_tagline(); ?></h2>
                <p class="push-up push-down"> <?php
                    if ( contains('password', get_current_page_url()) ): ?>
                        &nbsp; <?php
                    else: ?>
                        Welcome to the M+A Architects employee intranet. To begin, please log in. <?php
                    endif; ?>
                </p>
            </div>
            <div class="col vc_column_container medium-1">
                &nbsp;
            </div>
        </div>
    </div>
</div> <?php
