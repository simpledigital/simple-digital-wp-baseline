<?php
/**
* The Social Media Nav
*
* @package sdg
*/
?>

<nav class="social">
    <ul>
        <li class="footer-logo">
            <a href="/">
                <img alt="M+A Emblem Logo" src="<?php echo get_stylesheet_directory_uri() . '/assets/img/emblem-logo.png'; ?>"/>
            </a>
        </li> <?php

        $social_links = sdg_get_option('social_media_links');
        foreach($social_links as $slink):
            $network = grab($slink, 'social_media_network'); ?>
            <li class="<?php echo clean_slug($network); ?>">
                <a class="icon-social" href="<?php array_item($slink, 'social_media_profile_url'); ?>" target="_blank">
                    <i class="fa fa-<?php array_item($slink, 'fa_icon_suffix'); ?>"> </i>
                    <span class="show-for-sr">
                        <?php echo $network; ?>
                    </span>
                </a>
            </li> <?php
        endforeach; ?>
    </ul>
</nav>
