<?php
/**
 * Search Filter Nav
 *
 * @author sdg
 */

global $sdg, $current_loop, $sdg_id;

$tax_args = array('hide_empty' => false );

$tax_args = sdg_get_field('taxonomy_filters', $sdg_id, grab($sdg, 'current_tax_args'));

$filter_taxonomies = grabfield('taxonomy_filters', $sdg_id, get_object_taxonomies(grab($sdg, 'current_post_type')));

$filter_terms = get_terms( array(
    'taxonomy' => $filter_taxonomies,
    'hide_empty' => false,
) );

$sdg['current_filter_taxonomies'] = $filter_taxonomies;
$sdg['current_filter_terms'] = $filter_terms;

$count = 0; ?>

<div class="filterFilters filter-nav">
    <div class="row no-gutter full-large">
        <div class="col"> <?php

            foreach($filter_taxonomies as $filter_tax):
                $tax_obj = get_taxonomy_labels(get_taxonomy($filter_tax));

                /* Get array of terms */
                $tax_term = get_terms($filter_tax, $tax_args );
                $tax_name = get_array_item($tax_obj, 'singular_name');

                /* Get current URL query parameter value */
                    $filter_val = get_qv($filter_tax); ?>

                    <div id="multi-select-filter-by-<?php echo $filter_tax; ?>" class="filter-sub-module filter-query">

                        <h6 class="show-for-sr">
                            <label for="<?php echo $filter_tax; ?>-sort"><strong><?php echo $tax_name; ?></strong></label>
                        </h6>

                        <div class="filter-group">

                            <nav id="filter-panel-<?php echo $count; ?>" class="filter-group" data-filter-by="<?php echo $filter_tax; ?>" name="<?php echo $filter_tax; ?>">

                                <!-- Clear all filters link -->
                                <a href="#clear-all-filters-trigger" id="clear-all-filters-trigger" data-filter="*" name="clear-all-filters-trigger" value="clear-all-filters-trigger" class="active actual-trigger">All</a> <?php

                                /* Iterate through the terms */
                                foreach ($tax_term as $term) :
                                    if ( $term->name !== 'Uncategorized'):
                                        $current = ($filter_val === $term->slug) ? 'selected="selected"' : '';
                                        $link_class = ($filter_val === $term->slug) ? 'active actual-trigger' : 'actual-trigger'; ?>

                                        <a href="#<?php echo $term->slug; ?>/" class="<?php echo $link_class; ?>" value="<?php echo $term->slug; ?>" <?php echo $current; ?> data-<?php echo $filter_tax; ?>-id="<?php echo $term->id; ?>" name="<?php echo $term->slug; ?>">
                                            <?php echo $term->name; ?>
                                        </a> <?php
                                    endif;
                                endforeach; ?>

                            </nav>
                        </div>
                    </div><?php
            endforeach; ?>

        </div>
    </div>
</div>