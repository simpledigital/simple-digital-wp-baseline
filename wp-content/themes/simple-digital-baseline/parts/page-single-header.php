<?php
/**
 * Page Header Region
 *
 * @author sdg
 */

global $sdg, $post_id_override;

$class = '';

if ( !empty(sdg_get_field('profile_page_header_image') )):
    $featured_img = grab(grab(sdg_get_field('profile_page_header_image'), 'sizes'), 'large-slider');
else:
    $featured_img = has_post_thumbnail() ? get_the_post_thumbnail_url(get_the_ID(), 'large-slider') : '';
endif;
?>

<section class="wrapper hero-alt underlay hero-section <?= $class; ?>"<?php echo ($featured_img !== '') ? (' style="background-image:url(\'' . $featured_img . '\');" ') : ''; ?>>

    <div class="hero">
        <div class="row no-gutters">
            <div class="col">
                <h1 class="large"><?php sdg_the_title(); ?></h1>
                <h3 class="pull-up light-weight"><?php sdg_field('subtitle'); ?></h3>
                <h3 class="light-weight pad-top decrease">
                    <?php sdg_field('job_title'); ?><br/>
                    <?php echo get_the_title(sdg_get_field('home_office_location')); ?>
                </h3>
                <br/><br/>
                <a href="/working-at-ma/employee-directory/" class="button large tertiary">< Return to Directory</a>
            </div>
        </div>
    </div>

</section> <?php
