<?php
/**
 * Events Module
 *
 * @author sdg
 */

global $post;

$classes = '';

if ( is_single() ):
    $posts_count = 1;
    $classes = '';
    $arg_extra_index = 'post__in';
    $arg_extra_val = array(get_the_id());
elseif ( is_front_page()) :
    $posts_count = 3;
    $arg_extra_index = 'order';
    $arg_extra_val = 'ASC';
else :
    $posts_count = -1;
    $arg_extra_index = 'order';
    $arg_extra_val = 'ASC';
endif; ?>

<section class="module container content wrapper alt-bg">

    <h2>Upcoming Events</h2><?php

    $args = array('post_type' => 'tribe_events', 'posts_per_page' => 3 );
    $all_events = new WP_Query($args); ?>

    <div class="row equal-heights"> <?php
        $x = 0;
        while($all_events->have_posts()) : $all_events->the_post();
            $event_meta = get_post_meta(get_the_ID());
            if ( is_single() ):
                get_template_part('parts/events-content-single');
            else: ?>
                <div class="equal card col medium-4"> <?php
                    get_template_part('parts/events-content'); ?>
                </div> <?php
            endif;
            $x += 1;
        endwhile; ?>
    </div>

    <div class="wrapper container text-center">
        <a href="<?php echo '/events/'; ?>" class="button text-center outline block-center fat">See All Events</a>
    </div>

</section>