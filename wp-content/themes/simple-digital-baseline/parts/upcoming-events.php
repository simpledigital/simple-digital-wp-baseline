<?php
/**
 * Events Module
 *
 * @author sdg
 */

global $post, $sdg, $sdg_id;

$classes = '';

if ( is_single() ):
    $posts_count = 1;
    $classes = '';
    $arg_extra_index = 'post__in';
    $arg_extra_val = array(get_the_id());
elseif ( is_front_page()) :
    $posts_count = 3;
    $arg_extra_index = 'order';
    $arg_extra_val = 'ASC';
else :
    $posts_count = -1;
    $arg_extra_index = 'order';
    $arg_extra_val = 'ASC';
endif; ?>

<h5 class="alt-style title-case increase">Upcoming Events</h5>
<nav class="related-items"> <?php
    $args = array('post_type' => 'tribe_events', 'posts_per_page' => 3, 'post__not_in' => array($sdg_id) );
    $upcoming_events = new WP_Query($args); ?>

    <ul class=" equal-heights equal-heights-mobile"> <?php
        foreach($upcoming_events->posts as $item): ?>
            <li class="row equal"> <?php
                $id = grab($item, 'ID');
                $event_meta = get_post_meta($id);
                // var_log($event_meta);
                $EID = get_array_item($event_meta, 'ID');
                $vid = get_array_item($event_meta, '_EventVenueID');
                $date = split_string_custom(sdg_get_event_start_date_meta($event_meta, 'M j'), ' ');
                $time = sdg_get_event_start_date_meta($event_meta, 'ga'); ?>
                <figure class="col small-4">
                    <a href="<?php the_permalink($EID); ?>" class="thumbnail post-thumbnail">
                        <span class="emblem">
                            <time>
                                <strong class="uppercase h5">
                                    <?php echo get_array_item($date, 0); ?><br/><?php echo get_array_item($date, 1); ?>
                                </strong>
                            </time>
                        </span>
                    </a>
                </figure>
                <figcaption class="col small-8">
                    <h5 class="line-overflow min-push alt-style title-case"><a href="<?php echo sdg_get_the_permalink($id); ?>"><?php echo get_the_title($id); ?></a></h5>
                    <p class="primary-color light-weight italic pull-up">
                        <?php echo trim($time); ?> <span class="pipe">&nbsp;|&nbsp;</span> <?php echo tribe_get_venue($id) ?>
                    </p>
                </figcaption>
            </li><?php
        endforeach; ?>
    </ul>
</nav>