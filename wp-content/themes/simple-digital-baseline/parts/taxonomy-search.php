<?php
/**
 * Taxonomy Search
 *
 * @author sdg
 */

global $sdg, $sdg_id;
?>

<div class="search-wrap">
    <form id="local-nav-search-form" action="<?php get_current_page_path(); ?>" class="filter-search filter-posts-form">
        <div class="filter-module text-filter small-screens">
            <div>
                <label for="local-nav-search-form-text-input" class="filter-label show-for-sr"><?php echo grab(grab($sdg, 'current_term'), 'name'); ?></label>
                <div class="input">
                    <input type="text" id="local-nav-search-form-text-input" name="local-nav-search-form-text-input" placeholder="Search <?php echo grab(grab($sdg, 'current_term'), 'name'); ?>..." class="search filter-search" value="<?php echo get_qv('search'); ?>" />
                </div>
            </div>
        </div>
    </form>
</div>