<?php
/**
 * Global Nav Links
 *
 * @author sdg
 */
?>

<nav class="image-nav wrapper">

    <div class="text-center row inner equal-heights equal-heights">

        <div class="col medium-2 small-6 equal">
            <a href="/about-ma/employee-directory/" class="icon">
                <img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/icon-employees.png'; ?>" alt="" />
                <span class="primary-color h6 clear normal-weight">
                    Employee Directory
                </span>
            </a>
        </div>
        <div class="col medium-2 small-6 equal">
            <a href="/discussion-board/" class="icon">
                <img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/icon-discussion-board.png'; ?>" alt="" />
                <span class="primary-color h6 clear normal-weight">
                    Discussion Board
                </span>
            </a>
        </div>
        <div class="col medium-2 small-6 equal">
            <a href="/resources/it-help-desk/" class="icon">
                <img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/icon-it-help.png'; ?>" alt="" />
                    <span class="primary-color h6 clear normal-weight">
                        IT Support
                    </span>
            </a>
        </div>
        <div class="col medium-2 small-6 equal">
            <a href="/resources/human-resources/" class="icon">
                <img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/icon-hr.png'; ?>" alt="" />
                <span class="primary-color h6 clear normal-weight">
                    Human Resources
                </span>
            </a>
        </div>
        <div class="col medium-2 small-6 equal">
            <a href="/events/" class="icon">
                <img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/icon-calendar.png'; ?>" alt="" />
                <span class="primary-color h6 clear normal-weight">
                    Events Calendar
                </span>
            </a>
        </div>
        <div class="col medium-2 small-6 equal">
            <a href="/wp-content/uploads/2018/04/Project-Set-Up-Requests-and-Links.pdf" target="_blank" class="icon">
                <img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/icon-projects.png'; ?>" alt="" />
                <span class="primary-color h6 clear normal-weight">
                    Project Setup
                </span>
            </a>
        </div>
    </div><br/>

</nav>
