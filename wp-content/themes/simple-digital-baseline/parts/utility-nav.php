<?php
/**
 * Utility Nav
 *
 * @package sdg
 */

$col_class = '';

?>
<div class="bar">
    <nav class="fixed-nav row"> <?php
        if ( is_user_logged_in() ): ?>
            <div class="col medium-8 large-9">
                <div class="text-right search-wrap">
                    <?php get_search_form(); ?>
                </div>
            </div> <?php
        endif; ?>
        <div class="col medium-4 large-3 end">
            <ul>
                <li><a href="tel:+<?php echo str_replace_all(array('-', '.', '(', ')'), '', sdg_get_option('global_options_phone')); ?>" target="_blank" class="button fat-top support-button"><strong class="title-case">Easy IT:</strong> <?php echo sdg_get_option('global_options_phone'); ?> </a></li>
            </ul>
        </div>
    </nav>
</div>