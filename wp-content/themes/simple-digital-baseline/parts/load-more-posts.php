<?php
/**
 * Load More Posts
 *
 * @author sdg
 */

global $sdg, $sdg_id, $current_loop;

$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;

if ( intval(get_current_page_path_last()) > 0 ):
    $paged = intval(get_current_page_path_last());
endif;

$sdg['loop_args']['paged'] = $paged;

$next_posts = new WP_Query(grab($sdg, 'loop_args'));
// $next_posts['paged'] =  ($next_posts['paged']+1);

if ( $next_posts->post_count > 0 ) : ?>
    <div class="text-center wrapper">
        <?php sdg_pagination_btn('button outline load-more-trigger fat large', $paged+1); ?>
    </div> <?php
endif;
