<?php
/**
 * Team Member Archive Loop
 *
 * @author sdg
 */

global $sdg, $current_loop;

if ( sdg_get_field('is_leadership') ): ?>

    <article class="banner wrapper condense">
        <div class="row full">
            <div class="col medium-3 mask">
                <a href="<?php the_permalink(); ?>"><figure class="image"><img src="<?php echo grab(grab(sdg_get_field('headshot_image'), 'sizes'), '400-square'); ?>" alt="" /></figure></a>
            </div>
            <div class="col medium-9">
                <h4 class="primary-color caption"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4> <?php
                $partner_caption = grab(grabfield('leadership_fields'), 'leader_caption');
                if ( !empty($partner_caption) ): ?>
                    <h5 class="sub-caption normal-weight"><?php echo strip_tags($partner_caption, '<em><i><span><strong>'); ?></h5> <?php
                endif; ?>
                <div class="row">
                    <div class="col">
                        <p class="summary full"><?php echo strip_tags(get_the_excerpt()); ?></p>
                    </div>
                </div>
                <div class="row flex flex-desktop bottom">
                    <div class="col medium-9 small-12">
                        <br/>
                        <?php $year = grab(grabfield('leadership_fields'), 'partner_year');
                        if (!empty($year)): ?>
                            <h5 class="primary-color normal-weight">Became a M+A Partner in <?php echo $year; ?></h5> <?php
                        endif; ?>
                    </div>
                    <div class="col medium-3 small-12">
                        <a href="<?php the_permalink(); ?>" class="button outline full-width">See Full Bio</a>
                    </div>
                </div>

            </div>
        </div>
    </article> <hr class="inner" /> <?php
endif;
