<?php
/**
 * Events Loop Content
 *
 * @author sdg
 */

global $sdg, $sdg_id;

$EID = get_the_ID();

$event_meta = get_post_meta($EID);
$EID = get_array_item($event_meta, 'ID');
$vid = get_array_item($event_meta, '_EventVenueID');
$date = split_string_custom(sdg_get_event_start_date_meta($event_meta, 'M j'), ' ');
$time = sdg_get_event_start_date_meta($event_meta, 'ga');
?>

<article class="row wrapper skinny slim banner end-section condense">
    <div class="row full">
        <div class="col small-2 medium-1 expand">
            <figure class="thumbnail post-thumbnail">
                <span class="emblem raise-up">
                    <time>
                        <strong class="uppercase h5">
                            <?php echo get_array_item($date, 0); ?><br/><?php echo get_array_item($date, 1); ?>
                        </strong>
                    </time>
                </span>
            </figure>
            &nbsp;
        </div>
        <div class="col medium-5 small-10">
            <h3 class="medium-weight pull-up"><?php echo sdg_get_the_title(); ?></h3>
            <h5 class="black-color italic pull-up"><?php echo trim($time); ?> <span class="pipe">&nbsp;|&nbsp;</span> <?php echo tribe_get_venue() ?></h5>
            <p class="pad-top less"><?php echo strip_tags(get_the_excerpt()); ?></p>
            <div class="row">
                <div class="col">
                    <a href="<?php the_permalink(); ?>" class="button outline read-more">Read More</a>
                </div>
            </div>
        </div>
        <div class="col medium-6 end small-12 desktop-only">
            <a href="<?php the_permalink(); ?>" class="image pad-left"><figure class="photo shadow"><?php echo get_the_post_thumbnail(null, 'large-slider'); ?></figure></a>
        </div>
    </div>
</article>
