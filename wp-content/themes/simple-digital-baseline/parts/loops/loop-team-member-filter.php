<?php
/**
 * Team Member Archive Loop
 *
 * @author sdg
 */

global $sdg, $current_loop;

$str_class = '';
$args = array(
    'taxonomy' => 'work_sector',
    'orderby' => 'name',
    'order'   => 'ASC'
);
$cats = get_categories($args);
$categories = get_the_terms( get_the_id(), 'work_sector' );
if ( ! $categories || is_wp_error( $categories ) ) { $categories = array(); }
$categories = array_values( $categories );
$terms = sdg_get_a_posts_custom_terms_class($post->ID, 'work_sector');
$featured_img = grab(grab(sdg_get_field('headshot_image'), 'sizes'), '400-square');; ?>

<li class="filter-item-target grid-item active <?php echo $terms; ?>">
    <a class="filter-item-target-link" href="<?php the_permalink(); ?>">
        <div class="headshot-holder">
            <span class="show-for-sr"><?php echo $terms; ?></span>
            <div class="headshot">
                <figure class="image">
                    <img src="<?php echo $featured_img; ?>" alt="" />
                </figure>
            </div>
            <div class="text">
                <h6 class="name uppercase normal-weight min-push"><?php sdg_field_wrap('member_name', 'a', '', array('href' => get_permalink())); ?></h6>
                <h6 class="secondary-color decrease">
                    <?php sdg_field_wrap('job_title', 'div', '', array('class' => 'pad-bottom minimum medium-weight')); ?>
                    <div class="light-weight italic"><?php echo get_the_title(sdg_get_field('home_office_location')); ?></div>
                </h6>
            </div>
        </div>
    </a>
</li>
