<?php
/**
 * Team Member Archive Loop
 *
 * @author sdg
 */

global $sdg, $current_loop;

$str_class = '';
$args = array(
    'taxonomy' => 'resource_category',
    'orderby' => 'menu_order',
    'order'   => 'ASC',
    'parent'  => 0
);

$cats = get_categories($args);

?>
<section class="wrapper pad-top less">
    <div class="overflow-visible">
        <nav class="row circle-nav">
            <div class="row"> <?php
                $i = 0;
                $class = 'medium-3';
                foreach ($cats as $category) :
                    $thumbnail = get_field('custom_taxonomy_image', $category->taxonomy . '_' . $category->term_id); ?>
                    <div class="mask col <?php echo $class; ?>">
                        <a class="circle-link" href="<?php echo get_category_link($category->term_id); ?>">
                            <figure>
                                <img src="<?php echo grab(grab($thumbnail, 'sizes'), '400-square', get_stylesheet_directory_uri() . '/assets/img/spacer.gif'); ?>" alt="" />
                            </figure>
                        </a>
                        <h5 class="under"><?php echo $category->cat_name; ?></h5>
                    </div> <?php
                    if ( $i === 3 ): ?> </div> <div style="position: relative;" class="row" style=""> <?php $class = 'medium-4'; endif; $i +=1;
                endforeach; ?>
            </div>
        </nav>
    </div>
</section>