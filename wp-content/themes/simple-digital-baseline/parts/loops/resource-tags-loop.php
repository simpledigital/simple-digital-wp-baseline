<?php
/**
 * Resource Categories Loop
 *
 * @author sdg
 */

$term = get_queried_object();
$cat_label = $term->slug;

$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
$query_args = array(
    'post_type' => 'resource',
    'resource_category' => $cat_label,
    'orderby' => 'term_order',
    'posts_per_page' => -1,
    'order' => 'DESC',
);

$resources_query = new WP_Query( $query_args ); ?>

<section id="custom-content-container" class="wrapper section-loop alt-bg begin-section collapsible-content multi-level">
    <article id="featured-nav-resources" class="contain interior bg-white">
        <div> <?php
            $i=0;

            $resources = array();
            $tag_children = array();

            while($resources_query->have_posts()) : $resources_query->the_post();
                $id = get_the_id();
                $post_tags = get_the_terms( $id, 'resource_tag' );

                foreach($post_tags as $tag => $term) {
                    if($term->parent !== 0) {
                        $par = get_term($term->parent)->name;
                        if (!isset($resources[$par])) {
                            $resources[$par] = array();
                        }
                        if (!isset($resources[$par][$term->name])) {
                            $resources[$par][$term->name] = array();
                        }
                        $resources[$par][$term->name][$id] = get_the_title($id);

                        unset($post_tags[$tag]);
                    } else {
                        if (!isset($resources[$term->name])) {
                            $resources[$term->name] = array();
                            unset($post_tags[$tag]);
                        }
                        $resources[$term->name][$id] = get_the_title($id);
                    }
                }
                $i++;
            endwhile;
// var_log($resources);
            foreach($resources as $parent => $child):
                if (!is_array($child)) {
                    $type = 'download resource underline';
                    $href = sdg_get_field('file_download', $parent, sdg_get_field('resource_link', $parent));
                    $extras = ' target="_blank"';
                    $title = $child;
                    $class = 'child open';
                } else {
                    $type = 'trigger tag';
                    $href = '#' . clean_slug($parent);
                    $extras = ' class="white-color"';
                    $title = $parent;
                    $class = 'parent bg-primary white-color open'; ?>

                    <div class="<?php echo $class; ?> <?php echo $type; ?> banner row condense full filter-item-target trigger ">
                        <div class="col medium-12">
                            <a href="<?php echo $href; ?>" <?php echo $extras; ?>>
                                <?php echo $title ?>
                            </a>
                        </div>
                    </div>
                    <section class="content" style="display:block;"> <?php
                        foreach($child as $par => $kid):
                            if (!is_array($kid)) {
                                $type = 'download resource underline';
                                $href = sdg_get_field('file_download', $par, sdg_get_field('resource_link', $par));
                                $extras = ' target="_blank"';
                                $title = $kid;
                                $class = 'child open';
                            } else {
                                $type = 'trigger tag';
                                $href = '#' . clean_slug($par);
                                $extras = '';
                                $title = $par;
                                $class = 'parent sub-parent open'; ?>
                                <div class="<?php echo $class; ?> <?php echo $type; ?> banner row condense full filter-item-target trigger">
                                    <div class="col medium-12">
                                        <a href="<?php echo $href; ?>" <?php echo $extras; ?>>
                                            <?php echo $title ?>
                                        </a>
                                    </div>
                                </div>
                                <section class="content" style="display:block;"> <?php
                                    foreach($kid as $id => $r):
                                        $type = 'download resource underline';
                                        $href = sdg_get_field('file_download', $id, sdg_get_field('resource_link', $id));
                                        $extras = ' target="_blank"';
                                        $title = $r;
                                        $class = 'child open'; ?>
                                        <div class="<?php echo $class; ?> <?php echo $type; ?> banner row condense full filter-item-target">
                                            <div class="col medium-12">
                                                <a href="<?php echo $href; ?>" <?php echo $extras; ?>>
                                                    <?php echo $title ?>
                                                </a>
                                            </div>
                                        </div> <?php
                                    endforeach; ?>
                                </section> <?php
                            }
                        endforeach; ?>
                    </section> <?php
                }
            endforeach; ?>
        </div>
    </article>
</section>