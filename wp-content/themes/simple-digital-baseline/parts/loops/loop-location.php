<?php
/**
 * Team Member Archive Loop
 *
 * @author sdg
 */

global $sdg, $current_loop; ?>

<article class="banner condense link-container">
    <div class="row">
        <div class="col">
            <div class="full-row flex figure cover link blur" style="background-image:url('<?php echo get_the_post_thumbnail_url(false, 'full'); ?>');">
                <div class="col medium-5 large-4 box extra">
                    <div class="block">
                        <h1 class="white-color normal-weight"><?php the_title(); ?></h1>
                        <?php $location_parts = parse_location_address(sdg_get_field('location_full_address')); ?>
                        <p>
                            <?php echo grab($location_parts, 'street', ''); ?><br/>
                            <?php echo grab($location_parts, 'city', ''); ?>, <?php echo grab($location_parts, 'state', ''); ?> <?php echo grab($location_parts, 'zip', ''); ?>
                        </p>
                        <p>
                            Phone: <?php sdg_field('location_phone_number'); ?><br/>
                            Fax: <?php sdg_field('location_fax_number'); ?><br/>
                            <a href="mailto:<?php sdg_field('location_email'); ?>"><?php sdg_field('location_email'); ?></a>
                            <?php $btns = sdg_get_field('location_resources'); ?>
                        </p>
                        <div class="row btn-group pad-top">
                            <?php foreach($btns as $btn): ?>
                                <div class="col medium-6">
                                    <a href="<?php echo $btn['file_download']; ?>" target="_blank" class="small button outline secondary push-down"><?php echo $btn['resource_name']; ?></a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <a href="<?php the_permalink(); ?>" class="col medium-7 large-7"></a>
            </div>
        </div>
    </div>
</article>