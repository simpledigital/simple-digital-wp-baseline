<?php
/**
 * Resource Loop Query
 *
 * @author sdg
 */

global $sdg, $current_loop;

$str_class = '';
$args = array(
    'taxonomy' => 'resource_category',
    // 'orderby' => 'menu_item',
    'order'   => 'ASC',
    'parent'  => 0
);

$cats = get_categories($args);
$categories = get_the_terms( get_the_id(), 'resource_category' );

if ( ! $categories || is_wp_error( $categories ) ) { $categories = array(); }
