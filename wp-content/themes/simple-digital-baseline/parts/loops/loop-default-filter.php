<?php
/**
 * Team Member Archive Loop
 *
 * @author sdg
 */

global $sdg, $current_loop, $sdg_id;

$post_type = grab($sdg, 'current_post_type', get_post_type());

$str_class = '';
$args = array(
    'taxonomy' => grabfield('taxonomy_filters', $sdg_id, get_post_type_taxonomies($post_type)),
    'posts_per_page' => grabfield('post_limit', $sdg_id, -1),
    'display_type' => 'paged',
    'orderby' => implode(',', grabfield('orderby', $sdg_id, array('name'))),
    'order' => 'ASC'
);

$cats = get_categories($args);
$categories = get_the_terms( get_the_id(), 'work_sector' );
if ( ! $categories || is_wp_error( $categories ) ) { $categories = array(); }
$categories = array_values( $categories );
$terms = sdg_get_a_posts_custom_terms_class($post->ID, 'work_sector');
$featured_img = grab(grab(sdg_get_field('headshot_image'), 'sizes'), '400-square');; ?>

<li class="filter-item-target grid-item equal active <?php echo $terms; ?>">
    <a class="filter-item-target-link" href="<?php the_permalink(); ?>">
        <div class="headshot-holder">
            <span class="show-for-sr"><?php echo $terms; ?></span>
            <div class="headshot">
                <figure class="image">
                    <img src="<?php echo $featured_img; ?>" alt="" />
                </figure>
            </div>
            <div class="text">
                <h4 class="name"><?php sdg_field_wrap('member_name', 'a'); ?></h4>
                <h6 class="secondary-color">
                    <?php sdg_field_wrap('job_title', 'span', '<br/>'); ?>
                    <span class="light-weight italic"><?php echo get_the_title(sdg_get_field('home_office_location')); ?></span>
                </h6>
            </div>
        </div>
    </a>
</li>
