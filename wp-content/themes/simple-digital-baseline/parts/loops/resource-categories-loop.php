<?php
/**
 * Resource Categories Loop
 *
 * @author sdg
 */

$term = get_queried_object();
$cat_label = $term->slug;

// var_dump($term);
$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
$query_args = array(
    'post_type' => 'resource',
    'resource_category' => $cat_label,
    'orderby' => 'term_order',
    'posts_per_page' => -1,
    'order' => 'ASC',
);

$resources_query = new WP_Query( $query_args ); ?>

<section id="custom-content-container" class="wrapper alt-bg begin-section">
    <article id="featured-nav-resources" class="contain interior bg-white">
        <div> <?php
            $i=0;
            while($resources_query->have_posts()) : $resources_query->the_post();
                $id = get_the_id(); ?>
                <div class="banner row condense full filter-item-target fade-in active actual-link">
                    <div class="col medium-12"> <?php
                        $linkhref = sdg_get_field('file_download', $id, sdg_get_field('resource_link', $id));
                        $linkclass = 'underline';
                        ?>
                        <a href="<?php echo $linkhref; ?>" class="primary-color <?php echo $linkclass ?>" target="_blank">
                            <?php echo sdg_the_title($id); ?>
                        </a>
                    </div>
                </div> <?php
                $i++;
            endwhile; ?>
        </div>
    </article>
</section>*