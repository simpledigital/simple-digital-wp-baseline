<?php
/**
 * Generic Archive Loop
 *
 * @author sdg
 */

global $sdg, $current_loop; ?>

<article class="wrapper">
    <div class="end-section row pad-bottom">
        <div class="col medium-3">
            <?php echo get_the_post_thumbnail(); ?>
        </div>
        <div class="col medium-9">
            <h4 class="primary-color"><?php the_title(); ?></h4>
            <p><?php echo strip_tags(get_the_excerpt()); ?></p>
        </div>
    </div>
</article>