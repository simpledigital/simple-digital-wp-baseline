<?php
/**
 * Team Member Archive Loop
 *
 * @author sdg
 */

global $sdg, $current_loop;

$str_class = '';
$args = array(
    'taxonomy' => 'categories',
    'orderby' => 'name',
    'order'   => 'ASC',
    'posts_per_page' => -1
);
get_template_part('parts/post-categories');

?>


