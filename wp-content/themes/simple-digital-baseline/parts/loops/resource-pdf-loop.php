<?php
/**
 * Resource PDF Loop
 *
 * @author sdg
 */

$term = get_queried_object();
$cat_label = $term->slug;

// var_dump($term);
$paged = ( get_qv('page') ) ? get_qv('page') : 1;

$query_args = array(
    'post_type' => 'resource',
    'resource_category' => $cat_label,
    'orderby' => 'menu_order',
    'posts_per_page' => 3,
    'paged' => $paged,
    'order' => 'ASC',
);

$resources_query = new WP_Query( $query_args ); ?>

<section id="custom-content-container" class="wrapper section-loop alt-bg begin-section">
    <article id="featured-nav-resources" class="contain interior bg-white">
        <div>
            <div class="row filter-item-target"> <?php
                $i=0;

                while($resources_query->have_posts()) : $resources_query->the_post();
                    $id = get_the_id(); ?>
                    <div class="col medium-12"> <?php
                        $pdf = sdg_get_field('file_download', $id); ?>
                        <div class="pdf-container box bg-white drop-shadow">
                            <h2 class="primary-color"><?php echo sdg_the_title($id); ?></h3>
                            <?php echo do_shortcode('[pdf width="1000px"]' . $pdf . '[/pdf]'); ?>
                        </div>
                    </div> <?php
                    $i++;
                endwhile; ?>
            </div>
        </div>
    </article> <?php

    $paged = ( get_qv('page') ) ? get_qv('page') : 1;

    if ( intval(get_current_page_path_last()) > 0 ):
        $paged = intval(get_current_page_path_last());
    endif;

    $query_args['paged'] = ($paged +1);

    $next_posts = new WP_Query($query_args);
    // $next_posts['paged'] =  ($next_posts['paged']+1);

    if ( $next_posts->post_count > 0 ) : ?>
        <div class="text-center wrapper">
            <a href="<?php echo get_current_page_path() . '?page=' . ($paged+1) ?>" class="button fat outline">Load More Posts</a>
        </div> <?php
    endif; ?>
</section>