<?php
/**
 * Blog Posts Module
 *
 * @author sdg
 */

global $sdg_opts, $sdg, $AppGlobals, $sdg_id;

$paged = ( get_qv('page') ) ? get_qv('page') : 1;
$post_type = grab($sdg, 'current_post_type');
$sdg['current_post_type'] = $post_type;

if ( intval(get_current_page_path_last()) > 0 ):
    $paged = intval(get_current_page_path_last());
endif;

$section_class = '';
$tax_args = array('hide_empty' => false );
$tax_args = grab($sdg, 'current_tax_args');

$filter_taxonomies = grabfield('current_filter_taxonomies', $sdg_id);
$filter_terms = grab($sdg, 'current_filter_terms');

wp_reset_query();
$args = array(
    'post_type' => $post_type,
    'posts_per_page' => grabfield('post_limit', $sdg_id, 6),
    'display_type' => 'paged',
    'orderby' => implode(',', grabfield('orderby', $sdg_id, 'name')),
    'order' => 'ASC'
);
$sdg['loop_args'] = $args;
//var_log($terms); ?>
<div id="post-module" class="filter-grid load-more-wrapper posts-module card-group offset<?= $section_class; ?>">

    <div class="load-more-content"> <?php
        $x = -1; ?>

        <div class="row equal-heights"> <?php
            while (have_posts() ): the_post();
                $terms = sdg_get_a_posts_custom_terms_class($post->ID, 'category'); ?>
                <article class="col medium-4 filter-item-target active <?php echo $terms; ?>"> <?php
                    get_template_part('parts/post-image'); ?>
                    <div class="pad-top row">
                        <div class="col"><?php get_template_part('parts/post-content'); ?></div>
                    </div>
                </article> <?php

                if ( ($x%3) == 1 ):
                    echo '</div> <div class="row equal-heights">';
                endif;
                $x += 1;
            endwhile; ?>
        </div> <?php
        wp_reset_query();
        ?>
    </div>
    <?php get_template_part('parts/load-more-posts'); ?>
</div> <?php
