<?php
/**
 * Page Section Loop
 *
 * @author sdg
 */

global $sdg, $post_id_override;

$page_sections = sdg_get_field('include_sections');

if ($page_sections): ?>
    <section class="wrapper alt-bg begin-section"> <?php
        foreach ($page_sections as $id):
            $sdg['acf_fid'] = $id; ?>
            <article id="<?php echo get_the_slug($id); ?>" class="contain interior">
                <figure class="banner" style="background-image:url('<?php echo get_the_post_thumbnail_url($id, 'large-banner'); ?>');">
                    <h2 class="stripe inline"><?php echo get_the_title($id); ?></h2>
                </figure>
                <?php get_template_part('parts/acf/subsection-' . sdg_get_field('subsection_style', $id) . '-repeater'); ?>
            </article> <?php
        endforeach; ?>
    </section> <?php
endif;