<?php
/**
 * Team Member Archive Loop
 *
 * @author sdg
 */

global $sdg, $current_loop;

$str_class = '';

$subsections = sdg_get_featured_posts(-1, 'resource', 'featured');
// var_log($subsections);
?>
<section class="wrapper section-loop alt-bg begin-section">
    <article id="featured-nav-resources" class="contain interior bg-white">
        <div> <?php
            foreach ($subsections->posts as $subsection):
                $id = get_array_item($subsection, 'ID'); ?>
                <div class="banner row condense full">
                    <div class="col medium-12"> <?php
                        $linkhref = sdg_get_field('file_download', $id, sdg_get_field('resource_link', $id));
                        $linkclass = 'underline'; ?>
                        <a href="<?php echo $linkhref; ?>" class="primary-color <?php echo $linkclass ?>" target="_blank">
                            <?php echo sdg_the_title($id); ?>
                        </a>
                    </div>
                </div> <?php
            endforeach; ?>
        </div>
    </article>
</section>