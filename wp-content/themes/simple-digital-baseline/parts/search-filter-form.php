<?php
/**
 * Search Filter Form
 *
 * @author sdg
 */

global $sdg, $current_loop, $sdg_id;

$suffix = (!empty(grab($sdg,'current_post_type'))) ? '-' . str_replace_custom('_', '-', grab($sdg,'current_post_type')) : '-default';


// var_dump($filter_taxonomies);
// var_dump($filter_terms);

$tax_args = array('hide_empty' => false );
$sdg['current_tax_args'] = $tax_args; ?>

<div class="search-wrap">
    <form id="local-nav-search-form" action="<?php sdg_the_permalink($sdg_id); ?>" class="filter-search filter-posts-form">
        <div class="filter-module text-filter small-screens">
            <div>
                <label for="local-nav-search-form-text-input" class="filter-label show-for-sr"><?php echo get_the_title($sdg_id); ?></label>
                <div class="input">
                    <input type="text" id="local-nav-search-form-text-input" name="local-nav-search-form-text-input" placeholder="Search <?php echo get_the_title($sdg_id); ?>..." class="search filter-search" value="<?php echo get_qv('s'); ?>" />
                </div>
            </div>
        </div>
    </form>
</div><?php

wp_reset_postdata();
