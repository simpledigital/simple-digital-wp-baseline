<?php
/**
 * Page Header Events
 *
 * @author sdg
 */

global $sdg, $sdg_id;

$class = '';
$featured_img = has_post_thumbnail() ? get_the_post_thumbnail_url($sdg_id, 'full') : ''; ?>

<section class="wrapper hero-section <?= $class; ?>"<?php echo ($featured_img !== '') ? (' style="background-image:url(\'' . $featured_img . '\');" ') : ''; ?>>
    <div class="container">
        <div class="row no-gutters">
            <div class="col medium-10">
                <h2><?php sdg_the_title(); ?></h2>
            </div>
            <div class="col medium-2 text-right">
                <a href="<?php sdg_the_permalink(get_id_by_slug('submit-an-event')); ?>" class="button large outline">Submit an Event</a><br/><br/>
            </div>
        </div>
        <div class="row">
            <div class="col medium-12">
                <p class="full"> <?php echo strip_tags(get_the_excerpt()); ?> </p>
            </div>
        </div>
        </div>
    </div>

</section>
