<?php
/**
 * Hero For Author Pages
 *
 * @author sdg
 */

?>

<section class="hero-section hero-alt expand">
    <div class="hero super-hero">
        <div class="one-up">
            <article>
                <h6>Author Archives</h6>
                <h1> <?php echo get_the_author(); ?> </h1>
                <?php echo wpautop(get_the_author_meta('description')); ?>
            </article>
        </div>
    </div>
</section>
