<?php
/**
 * Filters Module
 *
 * @author sdg
 */

global $sdg, $sdg_id;

?>

<div id="filter-filter" class="wrapper"> <?php

    get_template_part('parts/page-header-filters');

    echo sdg_get_the_content($sdg_id, '<section><article class="wrapper"><div class="row"><div class="col">', '</div></div></article></section>');

    wp_reset_query();

    $args = array(
        'post_type' => grab($sdg, 'current_post_type', toggle_word_singular_plural(get_current_page_path_last())),
        'posts_per_page' => -1,
        'display_type' => 'paged',
        'orderby' => 'menu_title',
        'order' => 'ASC'
    );
    $GLOBALS['current_loop'] = new WP_Query( $args );

    get_template_part('inc/views/filter-archive-loop');?>
</div>