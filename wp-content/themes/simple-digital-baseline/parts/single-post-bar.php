<?php
/**
 * Single Post Bar
 *
 * @author sdg
 */

global $sdg, $sdg_id;
?>

<section class="post-bar">
    <div class="row flex flex-desktop text-center-mobile">
        <div class="col medium-10">
            <a href="mailto:<?php sdg_field('email'); ?>"> <i class="fa fa-envelope-o"></i> &nbsp; <?php sdg_field('email'); ?> </a>
            <strong class="pad-left pad-right less desktop-only">|</strong> <span class="mobile-only"> &nbsp;<br/><br/></span>
            <a class="phone direct-dial" href="tel:+<?php sdg_field('phone'); ?>"> <?php sdg_field_wrap('phone', 'span'); ?></a> <span class="extension"> <em>ext.</em> <?php sdg_field('phone_extension'); ?>
        </div> <?php
        $user_assoc = sdg_get_field('intranet_user_login_association');
        if ( sdg_user_has_access($user_assoc) ): ?>
            <div class="col medium-2">
                <span class="mobile-only"> &nbsp;<br/></span>
                <a href="/wp-admin/post.php?post=<?php echo get_the_ID(); ?>&action=edit" class="text-right edit-profile text-center-mobile"><?php echo sdg_user_owns_object($user_assoc) ? 'Edit My Profile' : 'Edit Profile'; ?></a>
            </div> <?php
        endif; ?>
    </div>
</section> <?php
