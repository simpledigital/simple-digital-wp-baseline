<?php
/**
 * Related Posts Nav
 *
 * @author sdg
 */

global $sdg, $sdg_id; ?>

<h4 class="alt-style decrease">Related Articles</h4>
<nav class="related-items">
    <?php $related_posts = sdg_get_related_posts_alt($sdg_id); ?>
    <ul class=" equal-heights equal-heights-mobile"> <?php
        foreach($related_posts->posts as $item): ?>
            <li class="row equal"> <?php
                $id = grab($item, 'ID'); ?>
                <figure class="col small-4"><img src="<?php echo get_the_post_thumbnail_url($id, '400-square'); ?>" alt="" class="thumbnail" /></figure>
                <figcaption class="col small-8">
                    <h5 class="line-overflow min-push"><a href="<?php echo sdg_get_the_permalink($id); ?>"><?php echo get_the_title($id); ?></a></h5>
                    <h6 class="alt-style soft"><?php echo strip_tags(sdg_get_the_categories($id, 'uncategorized', false, '', 1)); ?></h6>
                </figcaption>
            </li><?php
        endforeach; ?>
    </ul>
</nav>