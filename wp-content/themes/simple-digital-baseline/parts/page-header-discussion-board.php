<?php
/**
 * Page Header Region
 *
 * @author sdg
 */

global $sdg, $post_id_override;

$class = '';

$featured_img = has_post_thumbnail() ? get_the_post_thumbnail_url(get_the_ID(),'full') : ''; ?>

<section class="wrapper hero-section <?= $class; ?>"<?php echo ($featured_img !== '') ? (' style="background-image:url(\'' . $featured_img . '\');" ') : ''; ?>>

    <div class="container breadcrumb-nav">
        <div class="row no-gutters">
            <div class="col medium-12">
				<?php if( !is_single() ): ?>
					<h2><?php sdg_the_title(); ?></h2>
				<?php else: ?>
					<a href="discussion-board">Discussion Board</a> > <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				<?php endif; ?>
            </div>
        </div>
    </div>
    <div class="container">
    <?php
        the_content(); ?>
	</div>
	<div class="container discussion-board">
		<?php
		while ( have_posts() ) : the_post();
			get_template_part('parts/acf/flexible-content-loop');
		endwhile; ?>
	</div>
</section> <?php
