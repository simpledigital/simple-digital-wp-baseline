<?php
/**
 * Local Navigation for Related Pages
 *
 * @author sdg
 */
$nav_html = sdg_nav_children_and_parent();

if ( $nav_html !== null ): ?>

    <aside>
        <nav class="local-nav-list">
            <h2>Also See:</h2>
            <ul> <?php
            echo $nav_html; ?>

            </ul>
        </nav>
    </aside> <?php

endif; ?>