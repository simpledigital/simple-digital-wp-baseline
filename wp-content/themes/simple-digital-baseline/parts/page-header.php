<?php
/**
 * Page Header Region
 *
 * @author sdg
 */

global $sdg;

$class = '';
$featured_img = has_post_thumbnail() ? get_the_post_thumbnail_url(get_the_ID(),'full') : ''; ?>

<section class="wrapper hero-section <?= $class; ?>"<?php echo ($featured_img !== '') ? (' style="background-image:url(\'' . $featured_img . '\');" ') : ''; ?>>

    <div class="container">
        <div class="row no-gutters">
            <div class="col medium-9">
                <h2><?php sdg_the_title(); ?></h2> <?php
                the_excerpt();
                get_template_part('parts/page-section-nav'); ?>
            </div>
            <div class="col medium-3"> <?php
                get_template_part('parts/nav-related-pages'); ?>
            </div>
        </div>
    </div>

</section> <?php
