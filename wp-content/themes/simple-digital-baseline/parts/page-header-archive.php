<?php
/**
 * Page Header Archive
 *
 * @author sdg
 */

global $sdg, $sdg_id;

$id = $sdg_id;

$featured_img = has_post_thumbnail($id) ? get_the_post_thumbnail_url($id, 'full') : ''; ?>

<section class="wrapper hero-section"<?php echo ($featured_img !== '') ? (' style="background-image:url(\'' . $featured_img . '\');" ') : ''; ?>>
    <div class="container">
        <div class="row no-gutters">
            <div class="col medium-12">
                <h2><?php sdg_the_title($id); ?></h2> <?php
                the_excerpt($id); ?>
            </div>
        </div>
    </div>
</section> <?php
