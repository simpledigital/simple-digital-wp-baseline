<?php
/**
 * Read Time
 *
 * @author sdg
 */

?>

<span class="date uppercase small light-weight">
    <span class="icon h4 pad-right less">
        <i class="fa fa-clock-o primary-color"></i>
    </span>
    <?php echo sdg_estimated_reading_time(); ?> Read
</span>