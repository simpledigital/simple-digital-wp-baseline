<?php
/**
 * Date & Time
 *
 * @author sdg
 */
?>

<span class="date uppercase small light-weight">
    <span class="icon h4 pad-right less"><i class="fa fa-calendar primary-color "></i></span> <?php the_date('F d, Y', '<date>', '</date>'); ?>
        <span class="desktop-only"><?php get_template_part('parts/snippets/by-author'); ?></span>
</span>