<?php
/**
 * Back Button
 *
 * @author sdg
 */

$back_href = str_replace_custom('/event/', '/events/', get_parent_permalink());
$back_href = ( get_post_type() === 'post' ) ? $back_href . 'news/' : $back_href;

?>

<div class="wrapper condense more pad-bottom"><a href="<?php echo $back_href; ?>" class="button large outline">&lt; Back To <?php echo sdg_get_post_type_label('name'); ?></a></div>