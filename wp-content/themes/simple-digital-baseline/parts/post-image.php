<?php
/**
 * Post Featured Image
 *
 * @author sdg
 */

if (get_the_post_thumbnail()):
    $img_url = wp_get_attachment_image(get_post_thumbnail_id(), '400-square');
else:
    $img_url = '<img src="//dummyimage.com/400x400/000/fff" alt="" />';
endif;

if ($img_url): ?>
    <figure class="square">
        <a class="image-link" href="<?php the_permalink();?>"><?php echo $img_url; ?></a>
    </figure> <?php
endif;