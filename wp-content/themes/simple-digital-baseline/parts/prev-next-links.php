<?php
/**
 * Prev/Next Page Links
 *
 * @author sdg
 */

?>

<nav class="bar post-bar">
    <div class="row">
        <div class="col medium-8 medium-centered"><?php
            echo sdg_post_nav_links(); ?>
        </div>
    </div>
</nav>
