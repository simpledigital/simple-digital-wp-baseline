<?php
/**
 * Page Header Region
 *
 * @author sdg
 */

global $sdg, $sdg_id;

$sdg['current_post_type'] = sdg_get_field('choose_post_type', $sdg_id);

$featured_img = has_post_thumbnail($sdg_id) ? get_the_post_thumbnail_url($sdg_id, 'full') : '';
$show_search = sdg_get_field('include_filter_search', $sdg_id);

?>

<section class="wrapper hero-section end-section"<?php echo ($featured_img !== '') ? (' style="background-image:url(\'' . $featured_img . '\');" ') : ''; ?>>
    <div class="container">
        <div class="row">
            <div class="col <?php echo ($show_search === true) ? 'large-6' : ''; ?>">
                <h2><?php sdg_the_title($sdg_id); ?></h2> <?php
                echo get_the_excerpt($sdg_id);
                get_template_part('parts/page-section-nav'); ?>
            </div> <?php
            if ($show_search === true): ?>
                <div class="col large-5 end"> <?php
                    get_template_part('parts/search-filter-form'); ?>
                </div> <?php
            endif; ?>
        </div>
    </div>

</section> <?php
