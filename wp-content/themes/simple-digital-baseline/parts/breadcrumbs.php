<?php
/**
 * Breadcrumbs
 *
 * @author sdg
 */

global $sdg;

?>

<nav id="breadcrumb-nav" class="breadcrumb-nav breadcrumbs bar <?php echo grab($sdg, 'bread_class'); ?>">
    <?php sdg_nav_breadcrumbs(false); ?>
</nav>
