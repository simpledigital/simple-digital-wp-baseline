<?php
/**
 * Page Header Region
 *
 * @author sdg
 */

global $sdg, $sdg_id, $current_term;

$term = $sdg['current_term'];
$cat_label = $term->slug;

$id = get_array_item($term, 'term_ID');
$slug = get_array_item($term, 'slug');
?>

<section class="wrapper hero-section <?php echo $slug; ?>">

    <div class="container">
        <div class="row no-gutters">
            <div class="col medium-9">
                <h2><?php echo grab($term, 'name'); ?></h2>
                <p><?php echo strip_tags(get_the_archive_description($id)); ?></p><?php

                get_template_part('parts/taxonomy-search'); ?>
            </div>
            <div class="col medium-3">


                <aside>
                    <nav class="local-nav-list">
                        <h2>Also See:</h2>
                        <ul> <?php
                            $term_args = array(
                                'hide_empty' => false,
                                'post__not_in'    => $id
                            );
                            $show_cats = get_terms($term->taxonomy, $term_args);

                             foreach ($show_cats as $cat) {
                                $activeClass = '';
                                if($term->term_id == $cat->term_id) { ?>
                                     <li class="active show-for-sr"><?php echo $cat->name; ?></li>
                                <?php } else { ?>
                                    <li><a href="<?php echo get_site_url() . '/resources/' . $cat->slug . '/'; ?>"><?php echo $cat->name; ?></a></li>
                                <?php } ?>
                             <?php } ?>
                        </ul>
                    </nav>


                </aside>
            </div>
        </div>
    </div>

</section> <?php
