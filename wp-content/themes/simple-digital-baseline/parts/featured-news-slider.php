<?php
/**
* Flexible Banners Loop
*
* @author sdg
*/


$subsections = sdg_get_featured_posts(4, 'post', 'featured'); ?>

<div id="featured-news-posts-slider" class="wp1s-slider-wrapper wp1s-slider-wrapper-12383  wp1s-pager-padding transparent">
    <div class="wp1s-main-wrapper">
        <div class="wp1s-slider-container wp1s-pager-type-1 wp1s-arrow-type-5 wp1s-pagination">
            <ul class="wp1s-bxslider" data-id="12383" data-auto="false" data-speed="700" data-pause="30000" data-transition="horizontal" data-controls="true" data-responsive="true" data-pager="thumbnail"> <?php
                $slide_ids = array();
                $i = 0;
                foreach ($subsections->posts as $subsection):
                    $id = get_array_item($subsection, 'ID');
                    $slide_ids []= $id; ?>
                    <li>
                        <img src="<?php echo get_the_post_thumbnail_url($id, 'medium-slider'); ?>">
                        <div class="wp1s-caption-wrapper wp1s-caption-type-4 wp1s-caption-middleright">
                            <h1 class="wp1s-caption-title"><?php echo sdg_get_the_title($id); ?></h1>
                            <h2 class="wp1s-caption-content"><?php echo strip_tags(get_the_excerpt($id)); ?></h2>
                            <a href="<?php sdg_the_permalink($id); ?>" class="wps1-readmore-button btn">Read More</a>
                        </div>
                    </li> <?php
                endforeach; ?>
            </ul>
        </div>
    </div>
    <div class="wp1s-thumbnail-wrapper wp1s-thumbnail-type-3 wp1s-thb-arrow-type-3">
        <!-- The thumbnails -->
        <ul class="wp1s-bxslider-pager" data-id="12383" id="wp1s-pager-12383" data-count="<?php echo count($slide_ids); ?>"> <?php
            foreach($slide_ids as $id): ?>
                <li>
                    <a data-slide-index="<?php echo $i; ?>" href="">
                        <div class="wps-thumb-overlay" data-href="<?php sdg_the_permalink($id); ?>"></div>
                        <img src="<?php echo get_the_post_thumbnail_url($id, 'slideshow-thumb'); ?>" />
                    </a>
                </li> <?php
                $i += 1;
            endforeach; ?>
        </ul>
    </div>
</div>
