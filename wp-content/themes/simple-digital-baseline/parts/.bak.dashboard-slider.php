<?php
/**
* Flexible Banners Loop
*
* @author sdg
*/

$banner_class = 'simple-banner';
global $post;

$repeater_items = sdg_get_repeater('flexible_banner_content');

if (have_rows('flexible_banner_content')):

    while (have_rows('flexible_banner_content')): the_row();

        $flexible_layout = get_row_layout();
        $banner_header = get_sub_field('header_text');
        $banner_copy = get_sub_field('main_copy');
        $banner_cta1 = get_sub_field('cta_1_text');
        $banner_cta2 = get_sub_field('cta_2_text');
        $banner_img = get_sub_field('graphic'); ?>

        <div class="row ct-header slide tablex item" data-background="<?php echo $banner_img; ?>" >
            <div class="col medium-8">
                &nbsp;
            </div>
            <div class="col medium-4 overlay">
                <h2> <?= $banner_header; ?></h2> <?php
                sdg_field_wrap('main_copy', 'p');
                sdg_field_wrap('cta_1_text', 'p', '', array(
                    'href'  => get_sub_field('cta_1_link'),
                    'class' => 'button',
                )); ?>
            </div>
        </div>

        <?php
    endwhile; ?>
    <?php
endif;