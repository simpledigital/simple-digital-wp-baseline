<?php
/**
 * Post Content
 *
 * @author sdg
 */

if ( is_single() ):

    the_content();

else: ?>

    <div class="inner equal pad-bottom">
        <?php get_template_part('parts/post-category'); ?>
        <h4 class="equal-inner alt-style soft title-case">
            <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
        </h4> <?php
endif;

if ( get_post_type() === 'post' ): ?>
    <p style="display:none !important;">
        <time><?php the_time('m.d.Y');?></time>
    </p><?php
endif;

if ( !is_single() ): ?>

        <div class="equal-other-inner">
            <p><?php echo sdg_get_first_paragraph(); ?> <a class="inline read-more" href="<?php the_permalink(); ?>">more</a></p>
        </div>

        <p class="bottom">
            <?php get_template_part('parts/snippets/date-and-time'); ?>
        </p>
    </div> <!-- .inner --> <?php

endif;
