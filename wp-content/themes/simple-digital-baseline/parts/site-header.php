<?php
/**
 * Global Site Header
 *
 * @package sdg
 */

global $sdg;

?>

<header id="header" class="site-header">
    <?php get_template_part('parts/utility-nav'); ?>
    <?php if( $sdg['logged-in'] ): ?>
        <?php get_template_part('parts/primary-nav'); ?>
    <?php endif; ?>
</header><!-- .site-header -->