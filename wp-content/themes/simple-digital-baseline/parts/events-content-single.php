<?php
/**
* Events Content Single
*
* @package sdg
*/

// $event_host = get_event_host(get_the_ID());
$event_meta = get_post_meta(get_the_ID());
$thumb_size = array(300, 300);

$eid = get_array_item($event_meta, 'ID');

$vid = get_array_item($event_meta, '_EventVenueID');

$event_URL = get_array_item($event_meta, '_EventURL');

?>

<aside>
    <figure>
        <a href="<?= $event_URL; ?>">
            <img src="<?php the_post_thumbnail_uri(get_the_id(), $thumb_size); ?>" alt="<?php the_title(get_the_id()); ?> - Event Featured Image" />
        </a>
    </figure>
</aside>

<article>
    <h5><?php sdg_field('event_extra_info_header_text_primary'); ?></h5>
    <h6><strong class="main-copy"><?php sdg_field('event_extra_info_header_text_secondary'); ?></strong></h6>
    <strong class="highlight"><?php sdg_field('event_extra_info_header_text_tertiary'); ?></strong><br/><br/>

    <h6>Location:</h6>
    <h6 class="condensed">
        <span>
            <?=str_replace('<a href', '<a target="_blank" href', tribe_get_venue_website_link($vid, tribe_get_venue($vid)));?>
        </span>
    </h6>
    <p class="condensed location-info">
        <small>
            <a class="main-copy" href="<?=tribe_get_map_link($eid, false);?>" target="_blank">View Map</a>
        </small>
    </p> <br/>

    <p>
        <a class="button secondary scroll-to" href="#agenda-section"> Agenda </a>
        <?php if ( $event_URL[0] != '' ) { ?>
            <a class="button" href="<?= $event_URL; ?>" target="_blank"> Register </a>
        <?php } ?>
    </p>

</article>

</section>

<section class="inner">
    <article class="the-content">
        <?php the_content(); ?>
    </article>
</section>
