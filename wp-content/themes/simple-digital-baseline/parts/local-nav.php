<?php
/**
 * Local Navigation
 *
 * @author sdg
 */

$current_pid = get_the_id();

if (wp_get_post_parent_id($current_pid)):
    if ( isset($post->post_parent) && $post->$post_parent ):
        $post_parent = $post->post_parent;
    else:
        $post_parent = get_the_id();
    endif;

    $args = array(
        'post_type' => 'page',
        'posts_per_page' => -1,
        'orderby' => 'menu_order',
        'post_parent' => $post_parent,
        'order' => 'ASC'
    );

    $siblings = new WP_Query($args);

    if ($siblings->post_count > 1): ?>

        <nav class="local-nav">
            <ul> <?php
                while ($siblings->have_posts()) :
                    $siblings->the_post();
                    $nav_class = ( $post->ID === $current_pid ) ? 'class="current"' : ''; ?>
                    <li <?php echo $nav_class; ?>>
                        <span>&nbsp;</span>
                        <a href="<?= get_the_permalink(); ?>">
                            <?php echo str_replace('Performance Analytics and Diagnostics', 'Performance Analytics', str_replace('Software Defined Mobility', 'SDM', get_the_title()));?>
                        </a>
                    </li> <?php
                endwhile; wp_reset_postdata(); ?>
            </ul>
        </nav> <?php
    endif;
endif;