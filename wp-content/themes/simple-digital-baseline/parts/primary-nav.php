<?php
/**
* Primary Nav
*
* @package sdg
*/

global $sdg;

?>
<div id="primary-navigation" class="header-navigation">
    <div class="logo-mobile-wrapper">
        <div class="row no-gutter flex flex-desktop">
            <div class="col large-4 logo-container">
                <div class="row flex">
                    <div class="col small-4 medium-1 large-3 equal the-logo">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" <?php /* style="background-image:url('<?php echo sdg_get_site_logo(); ?>');" */ ?> rel="home" class="logo">
                            <img class="logo-img" src="<?php echo sdg_get_site_logo(); ?>" alt="Matter Logo" />
                        </a>
                    </div>
                    <div class="col small-6 medium-10 large-9 equal the-tagline"><h1 class="tagline"><a href="/"><?php sdg_tagline() ?></a></h1></div>
                    <div class="col small-2 medium-1 large-1 equal the-mobile-menu">
                        <a href="#primary-nav-container" class="mobile-trigger nav-trigger"><span class="show-for-sr">Menu</span><i class="fa fa-bars"></i></a>
                    </div>
                </div>
            </div>
            <div class="col large-8">
                <div class="nav-wrapper">
                    <nav id="primary-nav-container" class="primary-nav" role="navigation">
                        <ul>
                            <?php primary_navigation(); ?>
                        </ul>
                    </nav><!-- #primary-nav -->
                </div>
            </div>
        </div>
    </div>
</div>