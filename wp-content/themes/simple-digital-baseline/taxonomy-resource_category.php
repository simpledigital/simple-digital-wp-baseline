<?php
/**
 * The template for displaying Resource Category pages.
 *
 * @author sdg
 */

get_header();
$term = get_queried_object();
$sdg['current_term'] = $term;

?>

<div class="filterFilters">

    <nav id="breadcrumb-nav" class="breadcrumb-nav breadcrumbs">
        <ul>
            <li><a href="/resources/"><?php echo 'Resources' ?></a></li>
            <li><?php echo $term->name ?></li>
        </ul>
    </nav><?php

    get_template_part('parts/taxonomy-header');

    if ( contains('uman', $term->name )):
        get_template_part('parts/loops/resource-tags-loop');
    elseif(  contains('Presentation', $term->name)):
        get_template_part('parts/loops/resource-pdf-loop');
    elseif(  contains('Scheduling', $term->name)):
        get_template_part('parts/loops/resource-calendar-loop');
    else:
        get_template_part('parts/loops/resource-categories-loop');
    endif; ?>

</div>
<script>
    window.onload=function() {
        sdg.Filters.initiateTextSearchFilter();
        window.setTimeout(function() {
            $('.filter-item-target').addClass('active');
            $('#primary-navigation a[href*="/resources/"]').parents('li').addClass('current-page-ancestor');
        }, 1);
};</script> <?php

get_footer();