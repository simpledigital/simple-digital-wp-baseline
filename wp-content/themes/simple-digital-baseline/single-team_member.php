<?php
/**
 * Single Team Member Page
 *
 * @author sdg
 */

get_header();

global $sdg_opts, $sdg, $AppGlobals;

get_template_part('parts/page-single-team-member-header');

get_template_part('parts/single-post-bar');

while ( have_posts() ) : the_post();
    $detail_fields = sdg_get_field('additional_employee_details');
    $detail_fields_two = sdg_get_field('additional_employee_details_two');
    $year = sdg_get_field('start_date');
    if ( !(intval($year) > 999 && intval($year) < 10000) ):
        $year = grab(date_parse($year), 'year');
    endif;
    $finds = array(
        'pto', 'house what would you', 'play hard what', 'go to\'s', 'Fun facts'
    );
    $replaces = array(
        'PTO', 'house that you would', 'play hard - what', 'go-to\'s', 'Fun Facts'
    );
    // $year = (empty($year))   ?>
    <section class="wrapper">
        <div class="row full no-gutter">
            <div class="col medium-6 pad-bottom more">
                <h2 class="underline section-header"><?php sdg_field('tagline', false, 'Work Hard'); ?></h2> <?php
                // $fun_facts = '';
                if (!empty($year)): ?>
                    <h4 class="primary-color">Year Started at M+A: <?php echo $year; ?></h4> <?php
                endif;
                foreach ($detail_fields as $f => $field):
                    if ( is_array($field) ):
                        $field_str = '<ul>';
                        foreach($field as $term):
                            // var_log($field);
                            $term_name = get_term_by( 'id', absint($term), str_replace_custom('expertise', 'service_area', $f) );
                            $field_str .= '<li>' . grab($term_name, 'name') . '</li>';
                        endforeach;
                        $field_str .= '</ul>';
                        $field = $field_str;
                    endif;
                    if (!empty(strip_tags(trim($field)))):
                        ?><h4 class="pad-top less"><?php echo str_replace('and', '&amp;', unclean_slug($f)); ?></h4>
                        <div class="list"><?php
                            echo $field; ?>
                        </div> <?php
                    endif;
                endforeach; ?>
            </div>
            <div class="col medium-6 pad-bottom more">
                <h2 class="underline section-header"><?php sdg_field('tagline_two', false, 'Play Hard'); ?></h2>
                <article><br/> <?php
                    foreach ($detail_fields_two as $f => $field):
                        if ( is_array($field) ):
                            $field_str = '<ul>';
                            foreach($field as $term):
                                $term_name = get_term_by( 'id', absint($field), str_replace_custom('expertise', 'service_area', $f) );
                                $field_str .= '<li>' . grab($term_name, 'name') . '</li>';
                            endforeach;
                            $field_str .= '</ul>';
                        endif;
                        if (!empty(strip_tags(trim($field)))):
                            ?><h5 class="pad-top strong less"><?php echo str_replace_all($finds, $replaces, str_ucsentence(strtolower(unclean_slug($f)))); ?></h5>
                            <div class="list"><?php
                                echo $field; ?>
                            </div> <?php
                        endif;
                    endforeach;
                    // sdg_field('biography');
                    if ( !empty($fun_facts)): ?>
                        <h4 class="pad-top less">Fun Facts</h4>
                        <p><?php echo $field; ?></p> <?php
                    endif; ?>
                </article>
            </div>
        </div>
    </section> <?php

endwhile;

get_footer();
