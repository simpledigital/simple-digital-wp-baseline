jQuery(function() {
	jQuery(document)
		.ready(function(){
			 // change question status
			 $('#dwqa-question-category').on('change', function(e){
                var t = $(this),
					nonce = t.data('nonce'),
					post = t.data('post'),
					category = t.val(),
					data = {
					action: 'dwqa-update-category',
					post: post,
					nonce: nonce,
					category: category
				};

                $.ajax({
					url: dwqa.ajax_url,
					type: 'POST',
					dataType: 'json',
					data: data,

					success: function(data) {
						if ( data.success == false ) {
							alert( data.data.message );
						} else {
							window.location.reload();
						}
					}
				})
			});
		})
});