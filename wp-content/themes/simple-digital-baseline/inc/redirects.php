<?php
/**
 * Redirects
 *
 * @author sdg
 */

global $post;
$GLOBALS['sdg_id'] = $post->ID;
$field = get_post_meta($post->ID, 'redirect_single', true);
if($field) {
    wp_redirect(clean_url($field), 301);
}