<?php
/**
 * Functions for /wp-admin
 *
 * @author   sdg Branding
 * @package sdg Branding
 * @subpackage sdg-child
 * @since    1.0
 */

add_filter('gform_enable_field_label_visibility_settings', '__return_true');

/**
 * Use theme logo on login screens
 */
function sdg_login_logo() {?>
    <style type="text/css">
    	body.login{
    		background-color:#fff;
    	}
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/login-logo.png);
		width:320px;
		background-size: 320px auto;
		background-repeat: no-repeat;
        	padding-bottom: 20px;
        }
        .wp-core-ui #loginform .button-primary{
			background: rgba(13, 91, 137, 1);
			border:none;
			-webkit-box-shadow: none;
			box-shadow: none;
			color: #fff;
			text-decoration: none;
			text-shadow: none;
			border-radius:0;
		}
    </style>
<?php }

/**
 * Add the style select menu to the TinyMCE editor
 *
 * @param array $buttons Buttons for this row of the TinyMCE toolbar
 * @return array
 */
function sdg_add_style_select_to_tinymce($buttons = array()) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}

/**
 * Hide admin menus we don't need
 *
 * @global $menu
 * @return void
 */
function sdg_remove_admin_menus() {
    global $menu;
    $restricted = array(__('TML'), __('Theme Options'));
    end($menu);
    while (prev($menu)) {
        $value = explode(' ', $menu[key($menu)][0]);
        if (in_array(null != $value['0'] ? $value[0] : '', $restricted)) {
            unset($menu[key($menu)]);
        }
    }
    // global $wp_admin_bar;

    /* Remove Clutter Menus */
    // $wp_admin_bar->remove_menu('menu-posts-wp1slider');

    return;
}

/**
 * Remove the "Text Color" TinyMCE button
 *
 * @param array $buttons Buttons for this row of the TinyMCE toolbar
 * @return array
 */
function sdg_remove_forecolor_button($buttons = array()) {
    if ($key = array_search('forecolor', $buttons)) {
        unset($buttons[$key]);
    }
    return $buttons;
}

/**
 * Add custom admin JS/CSS
 *
 * @return  void
 */
// function sdg_add_custom_admin_styles() {
//     echo '<link type="text/css" rel="stylesheet" href="' . get_stylesheet_directory_uri() . '/assets/css/admin.css" />';
//     echo '<script type="text/javascript" src="' . get_stylesheet_directory_uri() . '/assets/js/admin.js" async ></script>';
// }

/* Only hide if user is developer */
// if ( get_current_user_id() === 1) {
//     show_admin_bar( false );
// }

/** Custom WP Admin Menu Pages/Sub-Pages/Shortcuts */

# Add "Edit Home Page" submenu item to "Pages" nav in WP admin
function sdg_add_template_submenus() {
    /* Add "Edit Home Page" menu item to "Pages" nav */
    $front_page_id = get_option('page_on_front');

    add_pages_page('Edit Front Page', 'Edit Front Page', 'edit_pages', 'post.php?post=' . $front_page_id . '&action=edit#edit-home-page');

    /**
     * Customize & Cleanup plugin admin sub-menus
     */
    /* 1.) ACF Import/Export Menu Item */
    add_submenu_page('tools.php', 'Import/Export ACF', 'Import/Export ACF', 'manage_options', 'edit.php?post_type=acf-field-group&page=acf-settings-tools#acf-import-export');
    /* 2.) Move Yucky Plugins to Sub-Menus */
    add_submenu_page("upload.php", "Sliders", "Sliders", "manage_options", "edit.php?post_type=wp1slider");

    /* Add menu item(s) to edit the page content associated with custom post types */
    $my_custom_post_types = array(
        'tribe_events' => 'events',
        'post'         => 'news',
        'resource'     => 'resources');

    foreach ($my_custom_post_types as $my_post_type => $p) {
        $my_pg_id = get_id_by_slug($p);
        // js_log($my_post_type);
        // js_log($p);
        if (null !== $my_pg_id) {
            $suffix = ( $my_post_type == 'post') ? 'post.php' : 'edit.php?post_type=' . $my_post_type;
            add_submenu_page('' . $suffix, 'Edit ' . strtotitlecase($p) . ' Page', 'Edit ' . strtotitlecase($p) . ' Page', 'edit_pages', 'post.php?post=' . $my_pg_id . '&action=edit#edit-page-content-' . $p);
        }
    }
}

/**
 * Hide Yoast For Developers
 *
 * @return void
 */
function sdg_hide_yoast_profile() {
    if (contains_any(array('.local', 'local.', '.dev', 'dev.', 'localhost'), get_server_domain())) {
        echo '<style> form#your-profile h3#wordpress-seo, form#your-profile h3#wordpress-seo + table {display: none; } </style>';
    }
}

/**
 * Add Logout Button To Sidebar
 *
 * @return void
 */

function sdg_logout_menu_item() {
    add_menu_page('', 'Logout', 'manage_options', 'logout', '__return_false', 'dashicons-external', 99999999);
}

/**
 * Redirect logouts
 *
 * @void
 */
function sdg_redirect_loggingout() {
    if (isset($_GET['page']) && 'logout' == $_GET['page']) {
        wp_redirect(wp_logout_url());
        exit();
    }
}

/**
 * Move Admin Bar to Footer
 *
 * @return void
 */
function sdg_fb_move_admin_bar() {?>
    <style type="text/css">
        body.admin-bar {
            margin-top: -32px;
            padding-bottom: 32px;
        }
        body.admin-bar #wphead {
            padding-top: 0;
        }
        body.admin-bar #footer {
            padding-bottom: 28px;
        }
        #wpadminbar {
            top: auto !important;
            bottom: 0;
        }
        #wpadminbar .quicklinks .menupop ul {
            bottom: 28px;
        }
    </style> <?php
}
/**
 * Default Show Admin Bar to False
 *
 * @param  int $user_ID
 * @return void
 */
function sdg_trash_public_admin_bar($user_ID) {
    update_user_meta($user_ID, 'show_admin_bar_front', 'false');
}

/**
 * Move WP Admin Menu Items (Pages to top)
 *
 * @return void
 */
function sdg_change_post_links() {
    global $menu;
    $menu[6] = $menu[5];
    $menu[5] = $menu[20];
    unset($menu[20]);
}

function additional_admin_color_schemes() {
    //Get the theme directory
    $theme_dir = get_stylesheet_directory();
    $primary_color = '#D54B2A';
    $secondary_color = '#CBCCCC';
    $tertiary_color = '#000000';
    $quaternary_color = '#ffffff';

    //Ocean
    wp_admin_css_color('ocean', __('Ocean'),
        get_stylesheet_directory_uri() . '/inc/admin-colors/ocean/colors.min.css',
        array($primary_color, $secondary_color, $primary_color, $quaternary_color, $primary_color)
    );
}

function set_default_admin_color($user_id) {
  $args = array(
    'ID' => $user_id,
    'admin_color' => 'flat'
  );
  wp_update_user( $args );
}
add_action('user_register', 'set_default_admin_color');

add_action('admin_init', 'additional_admin_color_schemes');

/** Add Filters/Actions */
// add_action('admin_menu', 'sdg_change_post_links');
add_action('user_register', 'sdg_trash_public_admin_bar');
add_action('wp_head', 'sdg_fb_move_admin_bar'); /* Admin Bar to bottom On Front-End */
// add_action('admin_head', 'sdg_fb_move_admin_bar'); /* Admin Bar to bottom On Backend */
add_action('admin_menu', 'sdg_logout_menu_item');
add_action('admin_menu', 'sdg_remove_admin_menus');
add_action('after_setup_theme', 'sdg_redirect_loggingout');
// add_action('admin_init', 'sdg_add_admin_logout_link');
add_filter('mce_buttons_2', 'sdg_add_style_select_to_tinymce');
add_filter('mce_buttons_2', 'sdg_remove_forecolor_button');
// add_action('admin_head', 'sdg_add_custom_admin_styles');
add_action('admin_menu', 'sdg_add_template_submenus');
add_action('admin_head', 'sdg_hide_yoast_profile');

add_action('login_enqueue_scripts', 'sdg_login_logo');
add_filter('tribe_ce_event_submission_login_form', 'my_custom_login_form', 10, 2);