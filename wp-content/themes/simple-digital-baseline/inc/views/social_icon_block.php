<?php

global $sdg;

?>
<div class="social-icon-block">
	<ul class="icons">
		<?php if( ($sdg['twitter']) ): ?><li class="twitter"><a href="<?php echo $sdg['twitter']; ?>" target="_blank"><i class="fa fa-twitter fa-lg"></i></a></li><?php endif; ?>
		<?php if( ($sdg['facebook']) ): ?><li class="facebook"><a href="<?php echo $sdg['facebook']; ?>" target="_blank"><i class="fa fa-facebook fa-lg"></i></a></li><?php endif; ?>
		<?php if( ($sdg['linkedin']) ): ?><li class="linkedin"><a href="<?php echo $sdg['linkedin']; ?>" target="_blank"><i class="fa fa-linkedin fa-lg"></i></a></li><?php endif; ?>
		<?php if( ($sdg['youtube']) ): ?><li class="youtube"><a href="<?php echo $sdg['youtube']; ?>" target="_blank"><i class="fa fa-youtube fa-lg"></i></a></li><?php endif; ?>
	</ul>
</div>
