<div class="vc_wp_search wpb_content_element resource_search">
    <div class="widget widget_search">
        <form role="search" method="get" id="searchform" class="searchform" action="">
	        <div>
		        <input type="text" value="" name="s" id="s" placeholder="Search">
	        </div>
        </form>
    </div>
    <div class="results_message"></div>
</div>
<?php if( $resource_list_id != '' ): ?>
<script type="text/javascript">
jQuery(document)
    .ready(function(){

    })
    .on('keyup','.resource_search #searchform #s',function(){
        var term = jQuery(this).val();
        filter_resources(term);
    })
    .on('submit','.resource_search #searchform',function(){
        return false;
    });
function filter_resources(term){
    console.log(term);
    jQuery('.results_message').html("");
    if( term == ''){
        jQuery("<?php $resource_list_id; ?> .resource_list li").show();
    } else {
        jQuery(".resource_list li").hide();
        jQuery(".resource_list li:contains("+term+")").show();
        if( !jQuery(".resource_list li").is(":visible") ){
            jQuery('.results_message').html("No results found.");
        }
    }
}
</script>
<?php endif; ?>