<?php

global $sdg; ?>

<div class="program_accordion">
    <div class="vc_tta-container" data-vc-action="collapse">
        <div class="vc_general vc_tta vc_tta-accordion vc_tta-color-grey vc_tta-style-classic vc_tta-shape-rounded vc_tta-o-shape-group vc_tta-controls-align-left">
            <div class="vc_tta-panels-container">
                <div class="vc_tta-panels">
                    <?php foreach( $cats as $cat ): ?>
                    <div class="vc_tta-panel" id="<?php echo $cat->term_id; ?>" data-vc-content=".vc_tta-panel-body">
                        <div class="vc_tta-panel-heading">
                            <h4 class="vc_tta-panel-title vc_tta-controls-icon-position-left"><a href="#<?php echo $cat->term_id; ?>" data-vc-accordion="" data-vc-container=".vc_tta-container"><span class="vc_tta-title-text"><?php echo $cat->name; ?></span><i class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a></h4>
                        </div>
                        <div class="vc_tta-panel-body">
                            <?php if( $cat->programs->have_posts() ): ?>
                            <div class="vc_tta-container" data-vc-action="">
                                <div class="vc_general vc_tta vc_tta-accordion vc_tta-color-grey vc_tta-style-classic vc_tta-shape-rounded vc_tta-o-shape-group vc_tta-controls-align-left">
                                    <div class="vc_tta-panels-container">
                                        <div class="vc_tta-panels">
                                        <?php while ( $cat->programs->have_posts() ) : $cat->programs->the_post();
                                            $post_image_full = get_the_post_thumbnail_url( get_the_ID(), 'full');
                                            $post_image_large = get_the_post_thumbnail_url( get_the_ID(), 'large');
                                            $post_image_medium = get_the_post_thumbnail_url( get_the_ID(), 'medium');
                                            ?>
                                            <div class="vc_tta-panel" id="program_<?php echo get_the_ID(); ?>" data-vc-content=".vc_tta-panel-body">
                                                <div class="vc_tta-panel-heading">
                                                    <h4 class="vc_tta-panel-title vc_tta-controls-icon-position-left"><a href="#program_<?php echo get_the_ID(); ?>" data-vc-accordion="" data-vc-container=".vc_tta-container"><span class="vc_tta-title-text"><?php echo get_the_title(); ?></span><i class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a></h4>
                                                </div>
                                                <div class="vc_tta-panel-body">
                                                    <div class="vc_row wpb_row vc_row-fluid edgtf-section">
                                                        <div class="wpb_column vc_column_container vc_col-sm-5">
                                                            <div class="vc_column-inner">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_single_image wpb_content_element vc_align_center">
                                                                        <figure class="wpb_wrapper vc_figure">
                                                                            <div class="vc_single_image-wrapper vc_box_border_grey">
                                                                                <img width="736" height="388" src="<?php echo $post_image_full; ?>" class="vc_single_image-img attachment-full" alt="" srcset="<?php echo $post_image_full; ?> 736w, <?php echo $post_image_medium; ?> 300w, <?php echo $post_image_large; ?> 624w" sizes="(max-width: 736px) 100vw, 736px">
                                                                            </div>
                                                                        </figure>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-7">
                                                            <div class="vc_column-inner ">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element">
                                                                        <div class="wpb_wrapper">
                                                                            <h4 class="p1"><?php the_title(); ?></h4>
                                                                            <p class="p1"><?php echo get_the_content(); ?></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endwhile; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php else: ?>
                            No programs
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>