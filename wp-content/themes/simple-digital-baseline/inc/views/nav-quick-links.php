<?php
/**
 * Quick Links Nav
 *
 * @author sdg Branding
 */
?>

<?php if( has_nav_menu( 'quick-links' ) ): ?>
    <div class="quick_links_wrapper">
        <div class="wrapper">
            <div class="row">
                <div class="col span_4">
                    &nbsp;
                </div>
                <div class="col span_8">
                    <div class="quick-links">
                        <div class="menu"> <?php
                            wp_nav_menu(array(
                                'theme_location' => 'quick-links',
                                'menu_class' => 'nav-menu',
                                'link_before' => '<span>',
                                'link_after' => '</span>')
                            ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>