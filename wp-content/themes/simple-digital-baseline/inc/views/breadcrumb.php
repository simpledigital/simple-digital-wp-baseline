<?php
/**
 * Breadcrumbs
 *
 * @author sdg Branding
 */

if(count($crumbs) > 0): ?>
<div class="breadcrumb">
    <ul><?php
        $count = 0;
        foreach($crumbs as $crumb): ?>
        <li class="<?php echo $crumb['selected_style']; ?>">
            <a href="<?php echo $crumb['permalink']; ?>"><?php echo $crumb['post_title']; ?></a>
        </li><?php
        endforeach; ?>
    </ul>
</div><?php
endif; ?>