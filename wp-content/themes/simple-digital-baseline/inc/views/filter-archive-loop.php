<?php
/**
 * Filter Archive Loop
 *
 * @author sdg
 */

global $sdg, $current_loop, $sdg_id;

$suffix = (!empty($sdg['current_post_type'])) ? '-' . str_replace_custom('_', '-', grab($sdg, 'current_post_type')) : '-default'; ?>

<section class="wrapper alt-bg begin-section">
    <?php get_template_part('parts/search-filter-nav'); ?>
    <div class="filter-grid contain loop-container custom-loop-container alt-bg begin-section row container<?php echo $suffix; ?>">
        <?php if( $current_loop->have_posts() ): ?>

            <div class="grid-sizer"></div>

            <ul class="filter-grid clear col medium-12 equal-heights">

                <?php while( $current_loop->have_posts() ) : $current_loop->the_post(); ?>

                    <?php get_template_part('parts/loops/loop' . $suffix . '-filter') ?>

                <?php endwhile; ?>

            </ul>

        <?php else: ?>

            <p>No <?php echo sdg_get_post_type_label('name', grab($sdg, 'current_post_type')); ?> to show.</p>

        <?php endif; ?>

    </div>

</section>