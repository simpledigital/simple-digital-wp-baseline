<div class="recent_posts <?php echo strtolower($display_style); ?> <?php echo $text_color; ?>">
<?php if( $title != '' ): ?><h3><span class=""><?php echo $title; ?></span></h3><?php endif; ?>
	<ul><?php
	if( is_multisite() && $site_blog_id != '' ):
		switch_to_blog( $site_blog_id );
	endif;
	foreach( $events as $event ) :
		setup_postdata( $event ); ?>
		<li><?php
		if( $show_image ):
			if( has_post_thumbnail( $event->ID ) ):
				$post_image = get_the_post_thumbnail( $event->ID, 'full');
			else:
				$post_image = '<img src="' . get_stylesheet_directory_uri() . '/images/blog-placeholder.png" />';
			endif;
		endif; // show_image
		if( $display_style == 'Columns' ): ?>
			<div class="recent_post_details">
				<div class="recent_post_title">
					<h3><a href="<?php echo get_post_permalink( $event->ID ); ?>"><?php echo $event->post_title; ?></a></h3>
				</div>
				<div class="recent_post_excerpt">
					<p><?php echo sdg_wp_trim_excerpt( $event->post_content ); ?></p>
					<p><?php echo tribe_event_format_date($event->EventStartDate, true);?></p>
				</div>
				<a href="<?php echo get_post_permalink( $event->ID ); ?>" rel="<?php echo $event->ID; ?>" class="blue-button"><?php echo $more_link_text; ?></a>
			</div><?php
		else: // li	st ?>
			<div class="wpb_wrapper">
				<div class="vc_row wpb_row">
					<div class="wpb_column vc_column_container vc_col-sm-2">
						<div class="event-date">
							<?php echo date( 'M j',strtotime( $event->EventStartDate ) ); ?>
						</div>
					</div>
					<div class="wpb_column vc_column_container vc_col-sm-10">
						<div class="event-title">
								<h3><a href="<?php echo get_the_permalink( $event->ID ); ?>"><?php echo $event->post_title; ?></a></h3>
						</div>
						<div class="event-details">
							<?php echo date( 'g:i A',strtotime( $event->EventStartDate ) ); ?> <?php if(date( 'g:i A',strtotime( $event->EventStartDate ))  != date( 'g:i a',strtotime( $event->EventEndDate ) )): ?> - <?php echo date( 'g:i a',strtotime( $event->EventEndDate ) ); ?> <?php endif; ?> <?php if( tribe_get_venue( $event->ID ) ):?><div class="location">@<?php echo tribe_get_venue( $event->ID ); ?></div><?php endif; ?>
						</div>
					</div>
				</div>
			</div><?php
		endif; ?>
		</li><?php
		wp_reset_postdata();
	endforeach;
	if( is_multisite() && $site_blog_id != '' ):
		restore_current_blog();
	endif; ?>
	</ul>
</div>