<?php
/**
 * Generic Archive Loop
 *
 * @author sdg
 */

global $sdg, $current_loop;

$suffix = (!empty($sdg['current_post_type'])) ? '-' . str_replace_custom('_', '-', $sdg['current_post_type']) : '-default';
$tax_suffix = str_replace_custom('_', '-', $sdg['current_post_type']) . '-taxonomies';

get_template_part('parts/loops/loop-' . $tax_suffix);

get_template_part('parts/loops/featured' . $suffix . '-nav'); ?>

<section class="wrapper section-loop alt-bg begin-section loop-container custom-loop-container alt-bg begin-section container<?php echo $suffix; ?>">
    <?php if( $current_loop->have_posts() ): ?>

        <?php while( $current_loop->have_posts() ) : $current_loop->the_post(); ?>
            <?php get_template_part('parts/loops/loop' . $suffix) ?>
        <?php endwhile; ?>

    <?php else: ?>
        <p>No <?php echo sdg_get_post_type_label('name', grab($sdg, 'current_post_type')); ?> to show.</p>
    <?php endif; ?>
</div>