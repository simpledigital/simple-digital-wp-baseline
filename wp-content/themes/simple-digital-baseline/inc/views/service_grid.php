<div class="container hidden-xs hidden-sm">
	<table class="ghn facilities default_bg rowPaddingContent">
		<thead>
			<tr>
				<th class="vertical-middle"><span class="title">OHIO</span></th>
				<?php
				foreach($field['choices'] as $choice):
				?>
					<th class="check alt centered vertical-middle"><?php echo $choice  ?></th>
				<?php
				endforeach;
				?>
			</tr>
		</thead>
		<tbody>
			<?php while ( $location_loop->have_posts() ) : $location_loop->the_post(); ?>
			<tr>
				<td class="vertical-middle">
					<a class="ghn" href="<?php echo get_the_permalink() ; ?>">
					<h3 class="ghn"><?php echo get_the_title();?></h3></a>
				</td>
				<?php
				foreach($field['choices'] as $choice):
				?>
				<td class="vertical-middle alt">
					<?php 
						$field_array = get_field('locations_services_grid'); 
						if(!is_array($field_array)) $field_array = array();
						if(in_array($choice, $field_array)): ?>
							<img alt="Present" height="13" src="https://www.greystonehealth.com/wp-content/themes/greystonehealth/images/services/health-centers/facilities_check.png" width="17">
						<?php else: ?>
							&nbsp;
						<?php endif; ?>
				</td>
				<?php
				endforeach;
				?>
			
			</tr>
		<?php endwhile; ?>
			<tr class="spacer">
				<td colspan="12">&nbsp;</td>
			</tr>
		</tbody>
	</table>
	<table class="ghn facilities">
		<thead>
			<tr>
				<th colspan="12">&nbsp;</th>
			</tr>
		</thead>
	</table>
</div>

<div class="container hidden-md hidden-lg">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-centered">
			<span class="span-bs-facilities-title">OHIO</span>
		</div>
	</div>
	<?php while ( $location_loop->have_posts() ) : $location_loop->the_post(); ?>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-centered">
			<a class="ghn" href="<?php echo get_the_permalink() ; ?>">
			<h2 class="ghn cursor-pointer"><?php echo get_the_title();?></h2></a><br>
			<p class="ghn">
				<?php 
					$field_array = get_field('locations_services_grid'); 
					if(!is_array($field_array)) $field_array = array();
					echo implode(", ", $field_array); ?>Rehab, Alzheimer's Care, Dialysis Care, Hospice Care, Memory Enhancement Unit, Respite Care, Private Suites, Short Term Rehabilitation, Orthopedic &amp; Cardiac Rehab
			</p>
		</div>
	</div>
	<?php endwhile; ?>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-centered">
			<span class="span-bs-facilities-title"></span>
		</div>
	</div>
</div>