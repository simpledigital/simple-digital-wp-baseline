<div class="vc_row wpb_row vc_inner vc_row-fluid fives">
	<?php while ( $location_loop->have_posts() ) : $location_loop->the_post(); ?>
		<div class="wpb_column vc_column_container vc_col-sm-2">
			<div class="vc_column-inner">
				<div class="wpb_wrapper">
					<div class="vc_btn3-container image-button vc_btn3-center with-description location-button" style="background: url(<?php if ( has_post_thumbnail() ): the_post_thumbnail_url(); endif;?>) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;">
						<a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-custom vc_btn3-block" href="<?php the_permalink(); ?>" style="color:#ffffff;" title=""><span class="location-wrapper" style="background-color:<?php echo $color; ?>;"><span class="location-title"><?php echo get_the_title(); ?></span><?php echo get_field('location_city') . ', ' . get_field('location_state'); ?></span></a>
					</div>
				</div>
			</div>
		</div>
		<?php endwhile; ?>
</div>