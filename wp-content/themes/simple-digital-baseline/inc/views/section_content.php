<div class="section <?php echo $css_classes; ?>">
<?php while ( $section_loop->have_posts() ) : $section_loop->the_post(); 
	$section_css = get_post_meta(get_the_ID(), '_wpb_shortcodes_custom_css', true);
	echo '<style>' . $section_css . '</style>';
	echo apply_filters( 'the_content',get_the_content() );
endwhile; ?>
</div>