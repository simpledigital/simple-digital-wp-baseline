<div class="recent_posts <?php echo $font_color; ?>">
<?php if( $title != '' ): ?><h3><?php echo $title; ?></h3><?php endif; ?>
	<ul>
<?php while ( $blog_loop->have_posts() ) : $blog_loop->the_post(); ?>
		<li>
			<div class="recent_post_image">
				<a href="<?php echo get_permalink( get_option('page_for_posts') ); ?>">
				 <img src="<?php if ( has_post_thumbnail() ): the_post_thumbnail_url(); endif;?>"/>
				</a>
			</div>
			<div class="recent_post_excerpt">
				<span class="recent_post_date"><?php echo date('M j',strtotime(get_the_date())); ?></span>
				<h4><a href="<?php echo get_permalink( get_option('page_for_posts') ); ?>"><?php echo get_the_title(); ?></a></h4>
				<?php echo the_excerpt(); ?>
				<a href="<?php echo get_permalink( get_option('page_for_posts') ); ?>" class="button blue-button">Read More</a>
			</div>
		</li>
<?php endwhile; ?>
	</ul>
</div>