<?php

global $sdg;

switch( $display_style ):
    case 'accordion': ?>
    <div class="resource_accordion">
        <div class="vc_tta-container" data-vc-action="collapse">
            <div class="vc_general vc_tta vc_tta-accordion vc_tta-color-grey vc_tta-style-classic vc_tta-shape-rounded vc_tta-o-shape-group vc_tta-controls-align-left">
                <div class="vc_tta-panels-container">
                    <div class="vc_tta-panels">
                        <?php foreach( $cats as $cat ): ?>
                        <div class="vc_tta-panel" id="<?php echo $cat->term_id; ?>" data-vc-content=".vc_tta-panel-body">
                            <div class="vc_tta-panel-heading">
                                <h4 class="vc_tta-panel-title vc_tta-controls-icon-position-left"><a href="#<?php echo $cat->term_id; ?>" data-vc-accordion="" data-vc-container=".vc_tta-container"><span class="vc_tta-title-text"><?php echo $cat->name; ?></span><i class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a></h4>
                            </div>
                            <div class="vc_tta-panel-body">
                                <?php if( $cat->resources->have_posts() ): ?>
                                <ul>
                                    <?php while ( $cat->resources->have_posts() ) : $cat->resources->the_post();
                                    $link = ( !empty( get_field('file') ) ? get_field('file')['url'] : ( !empty(get_field('url')) ? get_field('url') : '' ) );
                                    if( $link != '' ):
                                    ?>
                                    <li><a href="<?php echo $link; ?>" target="_blank">
                                        <span><?php the_title(); ?></span>
                                        <?php echo get_the_content(); ?>
                                    </a></li>
                                    <?php
                                    endif;
                                endwhile; ?>
                                </ul>
                                <?php else: ?>
                                No documents
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    break;
    default: // all other display styles
        if( $display_style == 'categorized' ):
            if( !empty( $cats ) ): ?>
            <div class="page_list icons">
                <ul class="inline">
                    <li><a href="#" rel="all" class="button orange-button"><span class="s1">Show All</span></a></li><?php
            foreach( $cats as $cat): ?>
                    <li><a href="#" rel="<?php echo $cat->slug; ?>" class="button orange-button"><span class="s1"><?php echo $cat->name; ?></span></a></li><?php
            endforeach;?>
                </ul>
            </div><?php
            endif;
        endif;?>
        <div class="resource_list <?php echo $display_style; ?>">
            <?php if( $resource_loop->have_posts() ): ?>
            <ul >
                <?php while ( $resource_loop->have_posts() ) : $resource_loop->the_post();
                $terms = get_the_terms(get_the_ID(),'resource_category');
                $term_arr = array();
                foreach( $terms as $term ): $term_arr[] = $term->slug . ' '; endforeach;
                // Because of some wackiness, we're using the actual meta field names: field_5a314baf18c0d - file, field_5a395e8b761ac - url
                $link = ( !empty( get_field('field_5a314baf18c0d', get_the_ID() ) ) ? get_field('field_5a314baf18c0d', get_the_ID() )['url'] : ( !empty(get_field('field_5a395e8b761ac', get_the_ID() )) ? get_field('field_5a395e8b761ac', get_the_ID() ) : '' ) );
                if( $link != '' ):
                ?>
                <li class="<?php echo implode(' ',$term_arr); ?>"><a href="<?php echo $link; ?>" target="_blank" class="<?php echo ( $display_style == 'icons' ? 'button orange-button' : '' ); ?>">
                    <span><?php the_title(); ?></span>
                    <?php //echo get_the_content(); ?>
                </a></li>
                <?php
                endif;
                endwhile; ?>
                <?php if( $display_style == 'categorized' ): ?>
                <li class="none">No documents.</li>
                <?php endif; ?>
            </ul>
        <?php else: ?>
        <p>No resources to show.</p>
        <?php endif; ?>
        </div>
        <?php if( $display_style == 'categorized' ): ?>
        <script type="text/javascript">
            jQuery(document)
            .on('ready',function(){
                jQuery('.resource_list li.none').hide();
                <?php if( $display_style == 'categorized' ): ?>
                var hash = window.location.hash.replace('#','');
                if( hash.length ){
                    select_clicked(hash);
                }
                <?php endif; ?>
            })
            .on('click','.icons a',function(e){
                e.preventDefault();
                var target = jQuery(this).attr('rel');
                if( target == 'all'){
                    jQuery('.icons li a').removeClass('active');
                    jQuery('.resource_list li').show();
                    jQuery('.resource_list li.none').hide();
                } else {
                    select_clicked(target);
                }
            });
            function select_clicked(target){
                var source = jQuery('a[rel="'+target+'"]');
                jQuery('.icons li a').removeClass('active');
                jQuery(source).addClass('active');
                jQuery('.resource_list li').hide();
                if( jQuery('.resource_list li.' + target).length ){
                    jQuery('.resource_list li.' + target).show();
                } else {
                    jQuery('.resource_list li.none').show();
                }
            }
        </script>
        <?php endif;
    break;
endswitch;