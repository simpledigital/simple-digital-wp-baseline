<?php

global $sdg;

?>
<div class="page_list <?php echo $display_style; ?>">
    <?php if( $page_loop->have_posts() ): ?>
    <div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-equal-height vc_row-flex">
        <?php while ( $page_loop->have_posts() ) : $page_loop->the_post(); ?>
        <div class="wpb_column vc_column_container vc_col-sm-3">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <a href="<?php the_permalink(); ?>" class="button orange-button"><span class="s1"><?php the_title(); ?></span></a>
                </div>
            </div>
        </div>
        <?php endwhile; ?>
    </div>
    <?php endif; ?>
</div>
