<?php

global $sdg; ?>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/ju/dt-1.10.16/datatables.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/ju/dt-1.10.16/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

<div class="team_member_list">
<?php if( $team_member_loop->have_posts() ): ?>
    <table id="team_members" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Job Title</th>
                <th>Program</th>
                <th>Office</th>
                <th>Phone</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
            <?php while( $team_member_loop->have_posts() ) : $team_member_loop->the_post(); ?>
            <tr>
                <td><?php echo get_field('field_5a4674443e420', get_the_ID()); //first_name ?></td>
                <td><?php echo get_field('field_5a4674963e421', get_the_ID()); //last_name ?></td>
                <td><?php echo get_field('field_5a4674b33e422', get_the_ID()); //job_title ?></td>
                <td><?php echo get_field('field_5a4674d53e423', get_the_ID()); //program ?></td>
                <td><?php echo get_field('field_5a4674e53e424', get_the_ID()); //office ?></td>
                <td><?php echo get_field('field_5a4674fc3e425', get_the_ID()); //phone ?></td>
                <td><?php echo get_field('field_5a4675103e426', get_the_ID()); //email ?></td>
            </tr>
            <?php endwhile; ?>
        </tbody>
    </table>
<?php else: ?>
<p>No team members to show.</p>
<?php endif; ?>
</div>

<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery('#team_members').DataTable({
        "paging": false,
        "columnDefs": [
            { "width": "12%", "targets": 0 }
        ],
        language: {
            searchPlaceholder: "Search..."
        },
    });
});
</script>