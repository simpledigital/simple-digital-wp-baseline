<div class="career-accordion">
	<?php
		$acc_header = '<span class=\'career-header\'><span class=\'career-position\'>Title</span><span class=\'career-location\'>Facility</span><span class=\'career-citystate\'>Location</span><span class=\'career-position-type\'>Position Type</span><span class=\'career-action\'>&nbsp;</span></span>';
		$acc_start = '[vc_tta_accordion style="flat" shape="square" c_icon="" active_section="0" collapsible_all="true"]';
		$acc_sections = "";
		while ( $careers->have_posts() ) : $careers->the_post();
			$this_title = get_the_title();
			$this_position_type = get_field('career_position_type');
			$this_id = get_the_ID() ;
			$this_content = get_the_content();
			$loc_title = get_the_title(get_field('career_location'));
			$loc_city = get_field('location_city', get_field('career_location'));
			$loc_state = get_field('location_state', get_field('career_location'));
		
			$acc_sections .= '[vc_tta_section title="<span class=\'career-section\'>';
			$acc_sections .= '<span class=\'career-position\'>' . $this_title. '</span>';
			$acc_sections .= '<span class=\'career-location\'>' . $loc_title . '</span>';
			$acc_sections .= '<span class=\'career-citystate\'>' . $loc_city . ', ' . $loc_state . '</span>';
			$acc_sections .= '<span class=\'career-position-type\'>' . $this_position_type . '</span>';
			$acc_sections .= '<span class=\'career-action\'>LEARN MORE</span>';
			$acc_sections .= '</span>" tab_id="acc_career_' . $this_id . '"][vc_column_text]';
			$acc_sections .= $this_content;
			$acc_sections .= '[/vc_column_text][/vc_tta_section]';
		endwhile;
	
		$acc_end = '[/vc_tta_accordion]';

		echo do_shortcode( $acc_header . $acc_start . $acc_sections . $acc_end );
	?>
</div>