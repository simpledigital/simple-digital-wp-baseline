<div class="vc_btn3-container image-button vc_btn3-center with-description" style="background: url(<?php echo $image; ?>) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;">
	<a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-custom vc_btn3-block" href="#" onclick="javascript:return false;" style="color:#ffffff; background-color:<?php echo $color; ?>" title="">
		<?php if( !empty( $title ) ): ?><span class="title"><?php echo $title; ?></span><?php endif; ?><?php if( !empty( $description ) ): echo $description; endif; ?>
	</a>
</div>