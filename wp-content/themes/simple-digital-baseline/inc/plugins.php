<?php
/**
 * HTTP API: WP_Http class
 *
 * @package WordPress
 * @subpackage HTTP
 * @since 2.7.0
 */

/**
 * Plugin Helper Functions
 */

/** TRIBE EVENTS */

/**
 * Get Event Start Date
 *
 * @param  int $post_id
 * @param  str $date_format
 * @return str
 */
function sdg_get_event_start_date($post_id = -1, $date_format = 'M d, Y') {
    if (-1 === $post_id) {
        $post_id = get_the_ID();
    }
    $post_meta = get_post_meta($post_id);
    $meta_item = grab($post_meta, '_EventStartDate');
    $date = date_create($meta_item);
    return date_format($date, $date_format);
}

function sdg_get_event_start_date_meta($meta, $date_format = 'M d, Y') {
    $post_meta = $meta;
    $meta_item = get_array_first_item(grab($post_meta, '_EventStartDate'));
    $date = date_create($meta_item);
    return date_format($date, $date_format);
}


/**
 * Shortcut for `echo sdg_get_event_start_date( ... )`, accepts the same arguments
 */
function sdg_the_event_start_date($post_id = -1, $date_format = 'M d, Y') {
    echo sdg_get_event_start_date($post_id, $date_format);
}

/**
 * Get Event Start Time
 *
 * @param  int $post_id
 * @param  str $date_format
 * @return str
 */
function sdg_get_event_start_time($post_id = -1, $date_format = 'H:i A') {
    if (-1 === $post_id) {
        $post_id = get_the_ID();
    }
    $post_meta = get_post_meta($post_id);
    $meta_item = grab($post_meta, '_EventStartDate');
    $date = date_create($meta_item);
    return date_format($date, $date_format);
}

/**
 * Get Event End Time
 *
 * @param  int $post_id
 * @param  str $date_format
 * @return str
 */
function sdg_get_event_end_time($post_id = -1, $date_format = 'H:i A') {
    if (-1 === $post_id) {
        $post_id = get_the_ID();
    }
    $post_meta = get_post_meta($post_id);
    $meta_item = grab($post_meta, '_EventEndDate');
    $date = date_create($meta_item);
    return date_format($date, $date_format);
}

/**
 * Shortcut for `echo sdg_get_event_start_time( ... )`, accepts the same arguments
 */
function sdg_the_event_start_time($post_id = -1, $date_format = 'H:i A') {
    echo sdg_get_event_start_time($post_id, $date_format);
}

/** END TRIBE EVENTS **/


/** REDUX SETUP **/

if (!function_exists('redux_init') && class_exists('ReduxFramework')) {
    /**
     * Initialize Redux Modifications
     *
     * @return void
     */
    function redux_init() {
        /**
         * Most of your editing will be done in this section.
         *
         * Here you can override default values, uncomment args and change their values.
         * No $args are required, but they can be overridden if needed.
         */

        $args = array();
        $tabs = array();

        ob_start();

        $ct = wp_get_theme();
        $theme_data = $ct;
        $item_name = $theme_data->get('Name');
        $tags = $ct->Tags;
        $screenshot = $ct->get_screenshot();
        $class = $screenshot ? 'has-screenshot' : '';

        $customize_title = sprintf(__('Customize &#8220;%s&#8221;', 'redux-framework-demo'), $ct->display('Name')); ?>

        <div id="current-theme" class="<?php echo esc_attr($class); ?>"> <?php
            if ($screenshot): ?> <?php
                if (current_user_can('edit_theme_options')): ?>
                    <a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize" title="<?php echo esc_attr($customize_title); ?>">
                        <img src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview');?>" />
                    </a> <?php
                endif; ?>
                <img class="hide-if-customize" src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview');?>" /> <?php
            endif; ?>

            <h4> <?php echo $ct->display('Name'); ?> </h4>

            <div>
                <ul class="theme-info">
                    <li><?php printf(__('By %s', 'redux-framework-demo'), $ct->display('Author'));?></li>
                    <li><?php printf(__('Version %s', 'redux-framework-demo'), $ct->display('Version'));?></li>
                    <li><?php echo '<strong>' . __('Tags', 'redux-framework-demo') . ':</strong> '; ?><?php printf($ct->display('Tags'));?></li>
                </ul>
                <p class="theme-description"><?php echo $ct->display('Description'); ?></p> <?php
                if ($ct->parent()) {
                    printf(' <p class="howto">' . __('This <a href="%1$s">child theme</a> requires its parent theme, %2$s.') . '</p>', __('http://codex.wordpress.org/Child_Themes', 'redux-framework-demo'), $ct->parent()->display('Name'));
                } ?>
            </div>
        </div> <?php

        $item_info = ob_get_contents();

        ob_end_clean();

        $sampleHTML = '';

        if (file_exists(dirname(__FILE__) . '/info-html.html')) {
            /** @global WP_Filesystem_Direct $wp_filesystem  */
            global $wp_filesystem;
            if (empty($wp_filesystem)) {
                require_once ABSPATH . '/wp-admin/includes/file.php';
                WP_Filesystem();
            }
            $sampleHTML = $wp_filesystem->get_contents(dirname(__FILE__) . '/info-html.html');
        }

        // BEGIN Sample Config

        // Setting dev mode to true allows you to view the class settings/info in the panel.
        // Default: true
        global $AppGlobals;
        if ( isset($AppGlobals['DEV_MODE']) ) {
            $args['dev_mode'] = $AppGlobals['DEV_MODE'];
        } else {
            $args['dev_mode'] = false;
        }

        // Set the icon for the dev mode tab.
        // If $args['icon_type'] = 'image', this should be the path to the icon.
        // If $args['icon_type'] = 'iconfont', this should be the icon name.
        // Default: info-sign
        //$args['dev_mode_icon'] = 'info-sign';

        // Set the class for the dev mode tab icon.
        // This is ignored unless $args['icon_type'] = 'iconfont'
        // Default: null
        $args['dev_mode_icon_class'] = 'icon-large';

        // Set a custom option name. Don't forget to replace spaces with underscores!
        $args['opt_name'] = 'sdg';

        // Setting system info to true allows you to view info useful for debugging.
        // Default: false
        $args['system_info'] = true;

        // Set the icon for the system info tab.
        // If $args['icon_type'] = 'image', this should be the path to the icon.
        // If $args['icon_type'] = 'iconfont', this should be the icon name.
        // Default: info-sign
        //$args['system_info_icon'] = 'info-sign';

        // Set the class for the system info tab icon.
        // This is ignored unless $args['icon_type'] = 'iconfont'
        // Default: null
        //$args['system_info_icon_class'] = 'icon-large';

        $theme = wp_get_theme();

        $args['display_name'] = $theme->get('Name');
        //$args['database'] = "theme_mods_expanded";
        $args['display_version'] = $theme->get('Version');

        // Define the starting tab for the option panel.
        // Default: '0';
        //$args['last_tab'] = '0';

        // Define the option panel stylesheet. Options are 'standard', 'custom', and 'none'
        // If only minor tweaks are needed, set to 'custom' and override the necessary styles through the included custom.css stylesheet.
        // If replacing the stylesheet, set to 'none' and don't forget to enqueue another stylesheet!
        // Default: 'standard'
        //$args['admin_stylesheet'] = 'standard';

        // Setup custom links in the footer for share icons
        $args['share_icons']['twitter'] = array(
            'link'  => 'http://twitter.com/sdgbranding',
            'title' => 'Follow me on Twitter',
            'img'   => ReduxFramework::$_url . 'assets/img/social/Twitter.png',
        );

        // Enable the import/export feature.
        // Default: true
        //$args['show_import_export'] = false;

        // Set the icon for the import/export tab.
        // If $args['icon_type'] = 'image', this should be the path to the icon.
        // If $args['icon_type'] = 'iconfont', this should be the icon name.
        // Default: refresh
        //$args['import_icon'] = 'refresh';

        // Set the class for the import/export tab icon.
        // This is ignored unless $args['icon_type'] = 'iconfont'
        // Default: null
        $args['import_icon_class'] = 'icon-large';

        /**
         * Set default icon class for all sections and tabs
         * @since 3.0.9
         */
        $args['default_icon_class'] = 'icon-large';

        // load google fonts
        //$args['async_typography'] = true;
        // Set a custom menu icon.
        //$args['menu_icon'] = '';

        // Set a custom title for the options page.
        // Default: Options
        $args['menu_title'] = __('Theme Options', 'redux-framework-demo');

        // Set a custom page title for the options page.
        // Default: Options
        $args['page_title'] = __('Theme Options', 'redux-framework-demo');

        // Set a custom page slug for options page (wp-admin/themes.php?page=***).
        // Default: redux_options
        $args['page_slug'] = 'higher_options';

        $args['default_show'] = true;
        $args['default_mark'] = '*';

        $args['google_api_key'] = 'AIzaSyCbflp7JZF9dQoxn3IoV4Q8FtKda2vl7iM';
        // Set a custom page capability.
        // Default: manage_options
        //$args['page_cap'] = 'manage_options';

        // Set the menu type. Set to "menu" for a top level menu, or "submenu" to add below an existing item.
        // Default: menu
        //$args['page_type'] = 'submenu';

        // Set the parent menu.
        // Default: themes.php
        // A list of available parent menus is available at http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        //$args['page_parent'] = 'options-general.php';

        // Set a custom page location. This allows you to place your menu where you want in the menu order.
        // Must be unique or it will override other items!
        // Default: null
        //$args['page_position'] = null;

        // Set a custom page icon class (used to override the page icon next to heading)
        //$args['page_icon'] = 'icon-themes';

        // Set the icon type. Set to "iconfont" for Elusive Icon, or "image" for traditional.
        // Redux no longer ships with standard icons!
        // Default: iconfont
        //$args['icon_type'] = 'image';

        // Disable the panel sections showing as submenu items.
        // Default: true
        //$args['allow_sub_menu'] = false;

        // Set ANY custom page help tabs, displayed using the new help tab API. Tabs are shown in order of definition.
        $args['help_tabs'][] = array(
            'id'      => 'redux-opts-1',
            'title'   => __('Theme Information 1', 'redux-framework-demo'),
            'content' => __('<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo'),
        );
        $args['help_tabs'][] = array(
            'id'      => 'redux-opts-2',
            'title'   => __('Theme Information 2', 'redux-framework-demo'),
            'content' => __('<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo'),
        );

        // Set the help sidebar for the options page.
        $args['help_sidebar'] = __('<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo');

        $sections = array();

        //Background Patterns Reader
        $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
        $sample_patterns_url = ReduxFramework::$_url . '../sample/patterns/';
        $sample_patterns = array();

        if (is_dir($sample_patterns_path)):

            if ($sample_patterns_dir = opendir($sample_patterns_path)):
                $sample_patterns = array();

                while (($sample_patterns_file = readdir($sample_patterns_dir)) !== false) {
                    if (stristr($sample_patterns_file, '.png') !== false || stristr($sample_patterns_file, '.jpg') !== false) {
                        $name = explode(".", $sample_patterns_file);
                        $name = str_replace('.' . end($name), '', $sample_patterns_file);
                        $sample_patterns[] = array('alt' => $name, 'img' => $sample_patterns_url . $sample_patterns_file);
                    }
                }
            endif;
        endif;

        $sections[] = array(
            'title'  => __('General settings', 'nhp-opts'),
            'desc'   => __('<p class="description">General theme settings</p>', 'nhp-opts'),
            'icon'   => 'el-icon-cogs',
            //Lets leave this as a blank section, no options just some intro text set above.
            'fields' => array(
                array(
                    'id'       => 'site-logo',
                    'type'     => 'media',
                    'title'    => __('Site Logo', 'nhp-opts'),
                    'compiler' => 'true',
                    'mode'     => false,
                    // Can be set to false to allow any media type, or can also be set to any mime type.
                    'desc'     => __('', 'nhp-opts'),
                    'subtitle' => __('', 'nhp-opts'),
                ),
                array(
                    'id'    => 'enable_safe_button',
                    'type'  => 'checkbox',
                    'value' => '1',
                    'title' => __('Enable Safe Button', 'nhp-opts'),
                ),
            ),
        );

        $sections[] = array(
            'title'  => __('Style Colors', 'nhp-opts'),
            'desc'   => __('<p class="description">Set style colors</p>', 'nhp-opts'),
            'icon'   => 'el-icon-css',
            //Lets leave this as a blank section, no options just some intro text set above.
            'fields' => array(
                array(
                    'id'       => 'scss-color-1',
                    'type'     => 'color_rgba',
                    'title'    => __('Color 1', 'nhp-opts'),
                    'compiler' => 'true',
                    'mode'     => false,
                    // Can be set to false to allow any media type, or can also be set to any mime type.
                    'desc'     => __('', 'nhp-opts'),
                    'subtitle' => __('', 'nhp-opts'),
                ),
                array(
                    'id'       => 'scss-color-2',
                    'type'     => 'color_rgba',
                    'title'    => __('Color 2', 'nhp-opts'),
                    'compiler' => 'true',
                    'mode'     => false,
                    // Can be set to false to allow any media type, or can also be set to any mime type.
                    'desc'     => __('', 'nhp-opts'),
                    'subtitle' => __('', 'nhp-opts'),
                ),
                array(
                    'id'       => 'scss-color-3',
                    'type'     => 'color_rgba',
                    'title'    => __('Color 3', 'nhp-opts'),
                    'compiler' => 'true',
                    'mode'     => false,
                    // Can be set to false to allow any media type, or can also be set to any mime type.
                    'desc'     => __('', 'nhp-opts'),
                    'subtitle' => __('', 'nhp-opts'),
                ),
                array(
                    'id'       => 'scss-color-4',
                    'type'     => 'color_rgba',
                    'title'    => __('Color 4', 'nhp-opts'),
                    'compiler' => 'true',
                    'mode'     => false,
                    // Can be set to false to allow any media type, or can also be set to any mime type.
                    'desc'     => __('', 'nhp-opts'),
                    'subtitle' => __('', 'nhp-opts'),
                ),
                array(
                    'id'       => 'scss-color-5',
                    'type'     => 'color_rgba',
                    'title'    => __('Color 5', 'nhp-opts'),
                    'compiler' => 'true',
                    'mode'     => false,
                    // Can be set to false to allow any media type, or can also be set to any mime type.
                    'desc'     => __('', 'nhp-opts'),
                    'subtitle' => __('', 'nhp-opts'),
                ),
                array(
                    'id'       => 'scss-color-6',
                    'type'     => 'color_rgba',
                    'title'    => __('Color 6', 'nhp-opts'),
                    'compiler' => 'true',
                    'mode'     => false,
                    // Can be set to false to allow any media type, or can also be set to any mime type.
                    'desc'     => __('', 'nhp-opts'),
                    'subtitle' => __('', 'nhp-opts'),
                ),
                array(
                    'id'       => 'scss-color-7',
                    'type'     => 'color_rgba',
                    'title'    => __('Color 7', 'nhp-opts'),
                    'compiler' => 'true',
                    'mode'     => false,
                    // Can be set to false to allow any media type, or can also be set to any mime type.
                    'desc'     => __('', 'nhp-opts'),
                    'subtitle' => __('', 'nhp-opts'),
                ),
                array(
                    'id'       => 'scss-color-8',
                    'type'     => 'color_rgba',
                    'title'    => __('Color 8', 'nhp-opts'),
                    'compiler' => 'true',
                    'mode'     => false,
                    // Can be set to false to allow any media type, or can also be set to any mime type.
                    'desc'     => __('', 'nhp-opts'),
                    'subtitle' => __('', 'nhp-opts'),
                ),
                array(
                    'id'       => 'scss-color-9',
                    'type'     => 'color_rgba',
                    'title'    => __('Color 9', 'nhp-opts'),
                    'compiler' => 'true',
                    'mode'     => false,
                    // Can be set to false to allow any media type, or can also be set to any mime type.
                    'desc'     => __('', 'nhp-opts'),
                    'subtitle' => __('', 'nhp-opts'),
                ),
                array(
                    'id'       => 'scss-color-10',
                    'type'     => 'color_rgba',
                    'title'    => __('Color 10', 'nhp-opts'),
                    'compiler' => 'true',
                    'mode'     => false,
                    // Can be set to false to allow any media type, or can also be set to any mime type.
                    'desc'     => __('', 'nhp-opts'),
                    'subtitle' => __('', 'nhp-opts'),
                ),
            ));
        $sections[] = array(
            'title'  => __('Style Fonts', 'nhp-opts'),
            'desc'   => __('<p class="description">Set style fonts</p>', 'nhp-opts'),
            'icon'   => 'el-icon-fontsize',
            //Lets leave this as a blank section, no options just some intro text set above.
            'fields' => array(
                array(
                    'id'          => 'scss-font-1',
                    'type'        => 'typography',
                    'title'       => __('Font 1', 'nhp-opts'),
                    'compiler'    => 'true',
                    'font-backup' => true,
                    'font-style'  => false,
                    'font-weight' => false,
                    'font-size'   => false,
                    'font-family' => true,
                    'line-height' => false,
                    'text-align'  => false,
                    'color'       => false,
                    'google'      => true,
                    'all_styles'  => true,
                    'subsets'     => false,
                    //'mode'     => false,
                    // Can be set to false to allow any media type, or can also be set to any mime type.
                    'desc'        => __('', 'nhp-opts'),
                    'subtitle'    => __('', 'nhp-opts'),
                ),
                array(
                    'id'          => 'scss-font-2',
                    'type'        => 'typography',
                    'title'       => __('Font 2', 'nhp-opts'),
                    'compiler'    => 'true',
                    'font-backup' => true,
                    'font-style'  => false,
                    'font-weight' => false,
                    'font-size'   => false,
                    'font-family' => true,
                    'line-height' => false,
                    'text-align'  => false,
                    'color'       => false,
                    'google'      => true,
                    'all_styles'  => true,
                    'subsets'     => false,
                    //'mode'     => false,
                    // Can be set to false to allow any media type, or can also be set to any mime type.
                    'desc'        => __('', 'nhp-opts'),
                    'subtitle'    => __('', 'nhp-opts'),
                ),
                array(
                    'id'          => 'scss-font-3',
                    'type'        => 'typography',
                    'title'       => __('Font 3', 'nhp-opts'),
                    'compiler'    => 'true',
                    'font-backup' => true,
                    'font-style'  => false,
                    'font-weight' => false,
                    'font-size'   => false,
                    'font-family' => true,
                    'line-height' => false,
                    'text-align'  => false,
                    'color'       => false,
                    'google'      => true,
                    'all_styles'  => true,
                    'subsets'     => false,
                    //'mode'     => false,
                    // Can be set to false to allow any media type, or can also be set to any mime type.
                    'desc'        => __('', 'nhp-opts'),
                    'subtitle'    => __('', 'nhp-opts'),
                ),
                array(
                    'id'          => 'scss-font-4',
                    'type'        => 'typography',
                    'title'       => __('Font 4', 'nhp-opts'),
                    'compiler'    => 'true',
                    'font-backup' => true,
                    'font-style'  => false,
                    'font-weight' => false,
                    'font-size'   => false,
                    'font-family' => true,
                    'line-height' => false,
                    'text-align'  => false,
                    'color'       => false,
                    'google'      => true,
                    'all_styles'  => true,
                    'subsets'     => false,
                    //'mode'     => false,
                    // Can be set to false to allow any media type, or can also be set to any mime type.
                    'desc'        => __('', 'nhp-opts'),
                    'subtitle'    => __('', 'nhp-opts'),
                ),
                array(
                    'id'          => 'scss-font-5',
                    'type'        => 'typography',
                    'title'       => __('Font 5', 'nhp-opts'),
                    'compiler'    => 'true',
                    'font-backup' => true,
                    'font-style'  => false,
                    'font-weight' => false,
                    'font-size'   => false,
                    'font-family' => true,
                    'line-height' => false,
                    'text-align'  => false,
                    'color'       => false,
                    'google'      => true,
                    'all_styles'  => true,
                    'subsets'     => false,
                    //'mode'     => false,
                    // Can be set to false to allow any media type, or can also be set to any mime type.
                    'desc'        => __('', 'nhp-opts'),
                    'subtitle'    => __('', 'nhp-opts'),
                ),
            ),
        );
        $sections[] = array(
            'icon'   => 'el-icon-bullhorn',
            'title'  => __('Contact info', 'nhp-opts'),
            'desc'   => __('<p class="description">Complete contact info</p>', 'nhp-opts'),
            'fields' => array(
                array(
                    'id'       => 'twitter',
                    'type'     => 'text',
                    'title'    => __('Twitter url', 'nhp-opts'),
                    'validate' => 'url',
                ),
                array(
                    'id'       => 'facebook',
                    'type'     => 'text',
                    'title'    => __('Facebook url', 'nhp-opts'),
                    'validate' => 'url',
                ),
                array(
                    'id'       => 'pinterest',
                    'type'     => 'text',
                    'title'    => __('Pinterest url', 'nhp-opts'),
                    'validate' => 'url',
                ),
                array(
                    'id'       => 'youtube',
                    'type'     => 'text',
                    'title'    => __('YouTube url', 'nhp-opts'),
                    'validate' => 'url',
                ),
                array(
                    'id'       => 'vimeo',
                    'type'     => 'text',
                    'title'    => __('Vimeo url', 'nhp-opts'),
                    'validate' => 'url',
                ),
                array(
                    'id'       => 'linkedin',
                    'type'     => 'text',
                    'title'    => __('LinkedIn url', 'nhp-opts'),
                    'validate' => 'url',
                ),
                array(
                    'id'       => 'google',
                    'type'     => 'text',
                    'title'    => __('Google url', 'nhp-opts'),
                    'validate' => 'url',
                ),
                array(
                    'id'       => 'instagram',
                    'type'     => 'text',
                    'title'    => __('Instagram url', 'nhp-opts'),
                    'validate' => 'url',
                ),
                array(
                    'id'       => 'rss',
                    'type'     => 'text',
                    'title'    => __('RSS url', 'nhp-opts'),
                    'validate' => 'url',
                ),
                array(
                    'id'    => 'org_email',
                    'type'  => 'text',
                    'title' => __('Email', 'nhp-opts'),
                ),
                array(
                    'id'    => 'org_phone',
                    'type'  => 'text',
                    'title' => __('Phone', 'nhp-opts'),
                ),
                array(
                    'id'    => 'org_address_1',
                    'type'  => 'text',
                    'title' => __('Address 1', 'nhp-opts'),
                ),
                array(
                    'id'    => 'org_address_2',
                    'type'  => 'text',
                    'title' => __('Address 2', 'nhp-opts'),
                ),
                array(
                    'id'    => 'org_address_3',
                    'type'  => 'text',
                    'title' => __('Address 3', 'nhp-opts'),
                ),
            ),
        );

        $sections[] = array(
            'type' => 'divide',
        );

        $sections[] = array(
            'icon'   => 'el-icon-info-sign',
            'title'  => __('Theme Information', 'redux-framework-demo'),
            'desc'   => __('<p class="description">This is the Description. Again HTML is allowed</p>', 'redux-framework-demo'),
            'fields' => array(
                array(
                    'id'      => 'raw_new_info',
                    'type'    => 'raw',
                    'content' => $item_info,
                ),
            ),
        );

        if (file_exists(trailingslashit(dirname(__FILE__)) . 'README.html')) {
            $tabs['docs'] = array(
                'icon'       => 'el-icon-book',
                'icon_class' => 'icon-large',
                'title'      => __('Documentation', 'redux-framework-demo'),
                'content'    => nl2br(file_get_contents(trailingslashit(dirname(__FILE__)) . 'README.html')),
            );
        }

        global $ReduxFramework;
        // $ReduxFramework = new ReduxFramework($sections, $args, $tabs);

        // END Sample Config
    }
}

/**
 * Remove all things related to the Redux Demo mode.
 */
if (!function_exists('redux_remove_demo_options')) {
    function redux_remove_demo_options() {
        // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
        remove_action('admin_notices', array(ReduxFrameworkPlugin::get_instance(), 'admin_notices'));

        // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
        if (class_exists('ReduxFrameworkPlugin')) {
            remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::get_instance(), 'plugin_meta_demo_mode_link'), null, 2);
        }
    }
    //add_action('init', 'redux_remove_demo_options');
}

/** END REDUX SETUP */

add_action('init', 'redux_init');