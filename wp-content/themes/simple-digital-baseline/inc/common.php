<?php
/**
 * Common Functions
 *
 * Functions that PHP should have built in (these should all work without wordpress)
 *
 * @author Simple Digital
 */

/*
 * Displays formatted array output
 */
if(!function_exists('pprint_r')){
    function pprint_r($array, $dump = true){
        echo "<pre>";
        if($dump) var_dump($array);
        if(!$dump) print_r($array);
        echo "</pre>";
    }
}

if(!function_exists('time_ago')){
function time_ago($tm,$rcs = 0) {
   $cur_tm = time(); $dif = $cur_tm-$tm;
   $pds = array('second','minute','hour','day','week','month','year','decade');
   $lngh = array(1,60,3600,86400,604800,2630880,31570560,315705600);
   for($v = sizeof($lngh)-1; ($v >= 0)&&(($no = $dif/$lngh[$v])<=1); $v--); if($v < 0) $v = 0; $_tm = $cur_tm-($dif%$lngh[$v]);

   $no = floor($no); if($no <> 1) $pds[$v] .='s'; $x=sprintf("%d %s ",$no,$pds[$v]);
   if(($rcs == 1)&&($v >= 1)&&(($cur_tm-$_tm) > 0)) $x .= time_ago($_tm);
   return $x;
}
}

function wpse_allowedtags() {
    // Add custom tags to this string
    return '<script>,<style>,<em>,<i>,<ul>,<ol>,<li>';
}

if ( !function_exists( 'sdg_wp_trim_excerpt' ) ) :
    /**
     * Automatic Excerpt Trim
     *
     * @since  Edition 1.0
     * @return void
     */
    function sdg_wp_trim_excerpt($text) {
        $raw_excerpt = $text;
        if ( '' == $text ) {

            if ( $count >= $excerpt_word_count ) {

                $text = get_the_content('');

                remove_shortcode('sdg_sub_menu');
                $text = do_shortcode( $text );
                $text = strip_shortcodes( $text );

                $text = apply_filters('the_content', $text);

                $text = preg_replace('[\[sdg_sub_menu.*\]]','',$text);
                $text = str_replace(']]>', ']]&gt;', $text);


                $text = str_replace(']]>', ']]&gt;', $text);
                $text = strip_tags($text, wpse_allowedtags()); /*IF you need to allow just certain tags. Delete if all tags are allowed */

                //Set the excerpt word count and only break after sentence is complete.
                    $excerpt_word_count = 45;
                    $excerpt_length = apply_filters('excerpt_length', $excerpt_word_count);
                    $tokens = array();
                    $excerptOutput = '';
                    $count = 0;

                    // Divide the string into tokens; HTML tags, or words, followed by any whitespace
                    preg_match_all('/(<[^>]+>|[^<>\s]+)\s*/u', $text, $tokens);

                    foreach ($tokens[0] as $token) {

                        if ($count >= $excerpt_length && preg_match('/[\,\;\?\.\!]\s*$/uS', $token)) {
                        // Limit reached, continue until , ; ? . or ! occur at the end
                            $excerptOutput .= trim($token);
                            break;
                        }

                        // Add words to complete sentence
                        $count++;

                        // Append what's left of the token
                        $excerptOutput .= $token;
                    }

                $text = trim(force_balance_tags($excerptOutput));

//                  $excerpt_end = ' <a href="'. esc_url( get_permalink() ) . '">' . '&nbsp;&raquo;&nbsp;' . sprintf(__( 'Read more about: %s &nbsp;&raquo;', 'wpse' ), get_the_title()) . '</a>';
//                  $excerpt_more = apply_filters('excerpt_more', ' ' . $excerpt_end);
                    $excerpt_more = ' ...';
                    //$pos = strrpos($text, '</');
                    //if ($pos !== false)
                    // Inside last HTML tag
                    //$text = substr_replace($text, $excerpt_end, $pos, 0); /* Add read more next to last word */
                    //else
                    // After the content
                    $text .= $excerpt_more; /*Add read more in new paragraph */

            }

            return $text;

        }
        return apply_filters('sdg_wp_trim_excerpt', $text, $raw_excerpt);
    }
    if( is_search() ):
        remove_filter('get_the_excerpt', 'wp_trim_excerpt');
        add_filter('get_the_excerpt', 'sdg_wp_trim_excerpt');
    endif;
endif;


/**
 * Find a value in an array of objects
 */
function in_object_array($objects, $key, $value) {
    $return = false;
    foreach($objects as $object) {
        $objVars = get_object_vars($object);
        if (isset($objVars[$key]) && $objVars[$key] == $value) {
           $return = true;
        }
    }
    return $return;
}

/**
* Converts phone numbers to the formatting standard
*
* @param   String   $num   A unformatted phone number
* @return  String   Returns the formatted phone number
*/
function format_phone($num){
    $num = preg_replace('/[^0-9]/', '', $num);

    $len = strlen($num);
    if($len == 7)
    $num = preg_replace('/([0-9]{3})([0-9]{4})/', '$1-$2', $num);
    elseif($len == 10)
    $num = preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3', $num);

    return $num;
}

/*
function get_terms_by_post_type( $taxonomies, $post_types ) {
    global $wpdb;

    $query = $wpdb->prepare(
        "SELECT t.*, COUNT(*) from $wpdb->terms AS t
        INNER JOIN $wpdb->term_taxonomy AS tt ON t.term_id = tt.term_id
        INNER JOIN $wpdb->term_relationships AS r ON r.term_taxonomy_id = tt.term_taxonomy_id
        INNER JOIN $wpdb->posts AS p ON p.ID = r.object_id
        WHERE p.post_type IN('%s') AND tt.taxonomy IN('%s')
        GROUP BY t.term_id",
        join( "', '", $post_types ),
        join( "', '", $taxonomies )
    );

    $results = $wpdb->get_results( $query );

    return $results;
}
*/

/**
 * Check for a blog page
 */
if(!function_exists('is_blog')):
function is_blog () {
    return ( is_search() || is_archive() || is_author() || is_category() || is_home() || is_single() || is_tag()) && 'post' == get_post_type();
}
endif;

//TODO - remove if not multisite
class WP_Query_Multisite extends WP_Query{
    var $args;

    function __construct( $args = array() ) {
        $this->args = $args;
        $this->parse_multisite_args();
        $this->add_filters();
        $this->query($args);
        $this->remove_filters();

    }

    function parse_multisite_args() {
        global $wpdb;

        $site_IDs = $wpdb->get_col( "select blog_id from $wpdb->blogs" );

        if ( isset( $this->args['sites']['sites__not_in'] ) )
            foreach($site_IDs as $key => $site_ID )
                if (in_array($site_ID, $this->args['sites']['sites__not_in']) ) unset($site_IDs[$key]);

        if ( isset( $this->args['sites']['sites__in'] ) )
            foreach($site_IDs as $key => $site_ID )
                if ( ! in_array($site_ID, $this->args['sites']['sites__in']) )
                    unset($site_IDs[$key]);


        $site_IDs = array_values($site_IDs);
        $this->sites_to_query = $site_IDs;
    }

    function add_filters() {

            add_filter('posts_request', array(&$this, 'create_and_unionize_select_statements') );
            add_filter('posts_fields', array(&$this, 'add_site_ID_to_posts_fields') );
            add_action('the_post', array(&$this, 'switch_to_blog_while_in_loop'));
            add_action('loop_end', array(&$this, 'restore_current_blog_after_loop'));

    }
    function remove_filters() {
            remove_filter('posts_request', array(&$this, 'create_and_unionize_select_statements') );
            remove_filter('posts_fields', array(&$this, 'add_site_ID_to_posts_fields') );

    }

    function create_and_unionize_select_statements( $sql ) {
        global $wpdb;

        $root_site_db_prefix = $wpdb->prefix;

        $page = isset( $this->args['paged'] ) ? $this->args['paged'] : 1;
        $posts_per_page = isset( $this->args['posts_per_page'] ) ? $this->args['posts_per_page'] : 10;
        $s = ( isset( $this->args['s'] ) ) ? $this->args['s'] : false;

        foreach ($this->sites_to_query as $key => $site_ID) :

            switch_to_blog( $site_ID );

            $new_sql_select = str_replace($root_site_db_prefix, $wpdb->prefix, $sql);
            $new_sql_select = preg_replace("/ LIMIT ([0-9]+), ".$posts_per_page."/", "", $new_sql_select);
            $new_sql_select = str_replace("SQL_CALC_FOUND_ROWS ", "", $new_sql_select);
            $new_sql_select = str_replace("# AS site_ID", "'$site_ID' AS site_ID", $new_sql_select);
            $new_sql_select = preg_replace( '/ORDER BY ([A-Za-z0-9_.]+)/', "", $new_sql_select);
            $new_sql_select = str_replace(array("DESC", "ASC"), "", $new_sql_select);

            if( $s ){
                $new_sql_select = str_replace("LIKE '%{$s}%' , wp_posts.post_date", "", $new_sql_select); //main site id
                $new_sql_select = str_replace("LIKE '%{$s}%' , wp_{$site_ID}_posts.post_date", "", $new_sql_select);  // all other sites
            }

            $new_sql_selects[] = $new_sql_select;
            restore_current_blog();

        endforeach;

        if ( $posts_per_page > 0 ) {
            $skip = ( $page * $posts_per_page ) - $posts_per_page;
            $limit = "LIMIT $skip, $posts_per_page";
        } else {
            $limit = '';
        }
        $orderby = "tables.post_date DESC";
        $new_sql = "SELECT SQL_CALC_FOUND_ROWS tables.* FROM ( " . implode(" UNION ", $new_sql_selects) . ") tables ORDER BY $orderby " . $limit;

        return $new_sql;
    }

    function add_site_ID_to_posts_fields( $sql ) {
        $sql_statements[] = $sql;
        $sql_statements[] = "# AS site_ID";
        return implode(', ', $sql_statements);
    }

    function switch_to_blog_while_in_loop( $post ) {
        global $blog_id;
        if($post->site_ID && $blog_id != $post->site_ID )
            switch_to_blog($post->site_ID);
        else
            restore_current_blog();
    }

    function restore_current_blog_after_loop() {
        restore_current_blog();
    }
}


if(!function_exists('curl_get_contents')):
function curl_get_contents($url, $ignoreSSL = false){
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    if($ignoreSSL):
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    endif;

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}
endif;


/**
 * Convert RGBA to HEX
 */
function rgb2hex($rgb){
    if( !empty( $rgb ) ):
        preg_match('#\((.*?)\)#', $rgb, $match);
        $rgb = explode(',',$match[1]);
        return '#' . sprintf('%02x', $rgb[0]) . sprintf('%02x', $rgb[1]) . sprintf('%02x', $rgb[2]);
    else:
        return '';
    endif;
}

function get_vc_css($postid, $withtags = true){
    $css = "";

    if($withtags) $css = "<style>";
    $css .= get_post_meta($postid, '_wpb_shortcodes_custom_css', true);
    if($withtags) $css .= "</style>";

    return $css;
}

if (!function_exists('get_query_var')) {
    function get_query_var($query_var, $defaul_val) {
        $val = get_qv($query_var, $default_val);
        return $val;
    }
}

if (!function_exists('set_query_var')) {
    function set_query_var($query_var, $defaul_val) {
        $val = get_qv($query_var, $default_val);
        $GLOBALS[$query_var] = $val;
    }
}

if (!function_exists('object_to_array')) {
    /**
     * Convert a PHP object to an array
     *
     * @param [object] $obj
     * @return arr
     */
    function object_to_array($obj) {
        return (array) $obj;
    }
}

if (!function_exists('index_of')) {
    function index_of($haystack, $needle, $offset = 0) {
        return strpos($haystack, $needle, $offset);
    }
}

if (!function_exists('is_odd')) {
    /**
     * Check if integer is odd number
     *
     * @param int $num
     * @return bool
     */
    function is_odd($num) {
        return ( ($num % 2) === 1);
    }
}

if (!function_exists('is_even')) {
    /**
     * Check if integer is even number
     *
     * @param int $num
     * @return bool
     */
    function is_even($num) {
        return ( ($num % 2) === 0);
    }
}

/**
 * Determine wordpress base path when wordpress core is not loaded
 *
 * @return str
 */
function sdg_get_wordpress_base_path() {
    $dir = dirname(__FILE__);
    do {
        /* Check for wp-config.php (it is possible to check for other files here) */
        if (file_exists($dir . '/wp-config.php')) {
            return $dir;
        }
    } while ($dir = realpath("$dir/.."));
    return null;
}

/**
 * Determine wordpress theme path when core is not loaded
 *
 * @return str
 */
function sdg_get_wordpress_theme_path() {
    $theme_path = sdg_get_wordpress_base_path() . '/wp-content/themes';
    /** Loop through/register all ACF field groups in specified directory */
    if(file_exists($theme_path)) {
        $dir = new DirectoryIterator($theme_path);
        foreach ($dir as $fileinfo) {
            if (!$fileinfo->isDot()) {
                $filename = $fileinfo->getFilename();
                if (strpos($filename, '.') !== 0 && is_dir($fileinfo->getPathname())) {
                    return $fileinfo->getPathname();
                }
            }
        }
    }
}

/**
 * Generate Font Awesome Icon
 *
 * @param str $class fontawesome icon class string (e.g. 'fa-check')
 * @return void
 */
function fa_icon($class) {
    $icon = '<i class="icon fa ' . $class . '"></i>';
    echo $icon;
}

/**
 * Clean a string - remove spaces & special characters (replace with '-')
 *
 * @param str $string
 * @return str
 */
function clean_slug($string) {
    $string = strtolower(str_replace(' ', '-', $string)); // Replaces all spaces with hyphens.
    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

/**
 * Try to turn URL-friendly string into page title-friendly string
 * (e.g. 'this-page' => 'This Page')
 *
 * @param  str $string   [description]
 * @return str
 */
function unclean_slug($string) {
    $patterns = array(
        '/_/',
        '/-/',
    );
    $string = preg_replace($patterns, ' ', $string);
    $string = ucwords(strtolower($string));
    $string = str_replace(array('And', 'Or'), array('and', 'or'), $string);
    return $string;
}

/**
 * Log a value in JavaScript function (default is console.log)
 *
 * @param mixed $value value to log
 * @param str $log_type type of log (e.g. 'alert')
 * @return void
 */
function js_log($value, $log_type = 'console.log') {
    $data = null;
    $jseval = false;
    if (!$value):
        return false;
    endif;
    $isevaled = false;
    $type = ($data || gettype($data)) ? 'Type: ' . gettype($data) : '';
    if ($jseval && (is_array($data) || is_object($data))):
        $data = 'eval(' . preg_replace('#[\s\r\n\t\0\x0B]+#', '', json_encode($data)) . ')';
        $isevaled = true;
    else:
        $data = json_encode($data);
    endif;
    $data = $data ? $data : '';
    $search_array = array("#'#", '#""#', "#''#", "#\n#", "#\r\n#");
    $replace_array = array('"', '', '', '\\n', '\\n');
    $data = preg_replace($search_array, $replace_array, $data);
    $data = ltrim(rtrim($data, '"'), '"');
    $data = $isevaled ? $data : ("'" === $data[0]) ? $data : "'" . $data . "'";
    $wrapping = '';
    $js_string = $wrapping . $log_type . "('" . $value . "');" . $wrapping;
    $js_string = '<script>' . $js_string . '</script>';
    echo $js_string;
}

/**
 * Output a nicer, cleaner var_dump using HTML <pre> tag
 *
 * @param mixed $var variable to var_dump in pre tags
 * @return void
 */
function var_log($var) {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}

/**
 * Get current page server path
 *
 * @return str
 */
function get_current_page_path() {
    $path = get_array_item($_SERVER, 'REQUEST_URI');
    $path = str_replace_custom('?', '', str_replace_custom(get_array_item($_SERVER, 'QUERY_STRING'), '', $path));
    return $path;
}

/**
 * Get last item of current server page path
 * (e.g.get_current_page_path_last('http://site.co/dad/kid/'); // <= returns '/kid/'
 *
 * @return str
 */
function get_current_page_path_last() {
    $path = get_current_page_path();
    $path = str_replace_custom('?', '', str_replace_custom(get_array_item($_SERVER, 'QUERY_STRING'), '', $path));
    $split_str = explode('/', $path);
    $split_str = array_reverse(array_filter($split_str));
    if (isset($split_str[0])) {
        return $split_str[0];
    }
}

/**
 * Get last item of current server page path
 * (e.g.get_current_page_path_last('http://site.co/dad/kid/'); // <= returns '/kid/'
 *
 * @return str
 */
function get_current_page_path_first() {
    $path = get_current_page_path();
    $path = str_replace_custom('?', '', str_replace_custom(get_array_item($_SERVER, 'QUERY_STRING'), '', $path));
    $split_str = explode('/', $path);
    $split_str = array_filter($split_str);
    if (isset($split_str[1])) {
        return $split_str[1];
    }
}

/**
 * Get server domain name
 *
 * @param bool $protocol whether to return protocol with domain str
 * @return str
 */
function get_server_domain($protocol = false) {
    $domain = (($protocol) ? get_current_document_protocol($_SERVER["PROTOCOL"]) . '://' : '') . trim($_SERVER["SERVER_NAME"]);
    return $domain;
}

/**
 * Get current page protocol
 *
 * @param $suffix - wether to return '://' with the protocol
 * @return str
 */
function get_current_document_protocol($suffix = false) {
    $protocol = (isset($_SERVER['HTTPS'])) ? 'https' : 'http';
    return ($suffix ? ($protocol . '://') : $protocol);
}

/**
 * Get page server http referrer
 *
 * @return str
 */
function get_page_referrer() {
    if (isset($_SERVER["HTTP_REFERER"])):
        return $_SERVER["HTTP_REFERER"];
    endif;
}

/**
 * Get current page URL
 *
 * @return str
 */
function get_current_page_url() {
    $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, strpos($_SERVER["SERVER_PROTOCOL"], '/'))) . '://';
    return $protocol . trim($_SERVER["SERVER_NAME"]) . trim($_SERVER["REQUEST_URI"]);
}

/**
 * Check if item is last item in array
 *
 * @param  arr $array
 * @param  str/int $item the key or index in the array to check
 * @return bool
 */
function is_last_item($array, $item) {
    $array_keys = array_keys($array);
    $last_key = end($array_keys);
    return ($last_key === $item);
}

/**
 * Get array/object item without breaking if it doesn't exist
 *
 * @param arr/obj $array
 * @param int/str $key the key or index in the array to get
 * @param mixed $fallback what to return if there's nothing
 * @return mixed
 */
function get_array_item($array, $key = 0, $fallback = false) {
    if (!is_array($array)) {
        $array = (array) $array;
    }
    if (isset($array[$key])) {
        $final_val = $array[$key];
    } else {
        $final_val = $fallback;
    }
    return $final_val;
}

/**
 * Shortcut for `echo get_array_item( ... )` - accepts the same arguments
 *
 * @return void
 */
function array_item($array, $key = 0, $multi = false, $fallback = false) {
    echo get_array_item($array, $key, $multi, $fallback);
}

/**
 * Get first item in array without breaking if it doesn't exist
 *
 * @param arr/obj $array
 * @param mixed $fallback what to return if there's nothing
 * @return mixed
 */
function get_array_first_item($array, $fallback = false) {
    return get_array_item($array, 0, $fallback);
}

/**
 * Shortcut for `echo get_array_first_item( ... )` - accepts the same arguments
 *
 * @return void
 */
function array_first_item($array) {
    echo get_array_first_item($array);
}

/**
 * Get last item in array without breaking if it doesn't exist
 *
 * @param arr/obj $array
 * @param mixed $fallback what to return if there's nothing
 * @return mixed
 */
function get_array_last_item($array, $fallback = false) {
    if (!is_array($array)) {
        $array = (array) $array;
    }
    $length = count($array);
    if (count($length) > 0 && isset($array[$length - 1])) {
        return $array[$length - 1];
    } else {
        return $fallback;
    }
}

/**
 * Get URL query var
 *
 * @param str $query_var name of query var to get/check
 * @param mixed $default_val what to return if query var is not already set
 * @return mixed
 */
function get_qv($query_var, $default_val = null) {
    global $argv;
    if (isset($argv) && null !== $argv) {
        foreach ($argv as $param => $val) {
            $param = (explode('=', $val));
            if (contains('=', $val) && get_array_item($param, 0) === $query_var) {
                $_query_var_val = get_array_item($param, 1);
            }
        }
    } else {
        if (isset($_GET[$query_var])) {
            $_query_var_val = $_GET[$query_var];
        } elseif (isset($_POST[$query_var])) {
            $_query_var_val = $_POST[$query_var];
        } else {
            $_query_var_val = $default_val;
        }
    }
    if (!isset($_query_var_val)) {
        $_query_var_val = $default_val;
    }
    return $_query_var_val;
}

/**
 * Get URL query var or set to a default value
 *
 * @param $query_var name of query var to get/check
 * @param mixed $default_val what to return if query var is not already set
 * @param bool $allow_empty allow empty string?
 * @return str
 */
function get_set_query_var($query_var, $default_val = '', $allow_empty = true, $set = true) {
    if (isset($_GET[$query_var]) && '' !== $_GET[$query_var]) {
        $_query_var_val = $_GET[$query_var];
    } elseif (isset($_POST[$query_var]) && '' !== $_POST[$query_var]) {
        $_query_var_val = $_POST[$query_var];
    } else {
        $_query_var_val = $default_val;
    }

    if (!$allow_empty && ('' === $_query_var_val || !isset($_query_var_val) || !$_query_var_val)) {
        $_query_var_val = false;
    }

    if ($set) {
        set_query_var($query_var, $_query_var_val);
    }
    return $_query_var_val;
}

/**
 * Check if query variable is empty
 *
 * @param str $var_key
 * @param bool $allow_empty
 * @return bool
 */
function query_var_empty($var_key) {
    $qv = get_set_query_var($var_key, '', true, false);
    $is_empty = ('' === $qv) ? true : false;
    return $is_empty;
}

/**
 * Echo line break(s)
 *
 * @return void
 */
function br($num_times = 1) {
    echo str_repeat('<br/>', $num_times);
}

/**
 * Does opposite of isset
 *
 * @param $var variable to test
 * @return bool
 */
function isntset($var) {
    if (isset($var)) {
        return false;
    } else {
        return false;
    }
}

/**
 * Check if string contains substring
 *
 * @param $substring
 * @param $string
 * @return bool
 */
function contains($substring, $string, $strict = true) {
    if (false === $strict) {
        $pos = strpos(strtolower($string), strtolower($substring));
    } else {
        $pos = strpos($string, $substring);
    }
    if (false === $pos) {
        return false;
    } else {
        return true;
    }
}

/**
 * Check if string contains any substrings passed in as an array
 *
 * @param $substrings array An array contain each substring to search for
 * @param $string str - the string to be searched
 * @return bool
 * @uses contains()
 */
function contains_any($substrings, $string, $return_string = false) {
    $contains = false;
    if (is_array($substrings)) {
        foreach ($substrings as $s => $substr) {
            if (contains($substr, $string)) {
                if ($return_string) {
                    return $substrings[$s];
                } else {
                    return true;
                }
            }
        }
    } elseif (is_string($substrings)) {
        $contains = contains($substrings, $string);
    }
    return $contains;
}

/**
 * Get most frequent value(s) in array
 *
 * @param $arr
 * @return arr
 */
function sdg_get_most_frequent_array_values($arr) {
    $counted = array_count_values($arr);
    arsort($counted);
    return (key($counted));
}

/**
 * Get formatted date from timestamp
 *
 * @param $timestamp
 * @param $format
 * @return str
 */
function get_date_from_timestamp($timestamp, $format = 'm/d/Y') {
    return date($format, intval($timestamp));
}

/**
 * Split string into two lines as HTML
 *
 * @param $string str
 * @param $lines int
 * @return str
 */
function str_word_wrap($string, $lines = 2) {
    return nl2br(wordwrap($string, (strlen($string) / ($lines+0.2)) + (ceil(($lines+0.2) / 2) + 2)));
}

/**
 * Split string into two lines as HTML
 *
 * @param $string str
 * @param $center [float]
 * @return str
 */
function sdg_split_str($string, $center = 0.4, $split_str = '<br/>') {
    $length2 = strlen($string) * $center;
    $tmp = explode(' ', $string);
    $index = 0;
    $result = array(0 => '', 1 => '');
    $return_str = '';
    foreach ($tmp as $word) {
        if (!$index && strlen($result[0]) > $length2) {
            $index++;
        }
        $result[$index] .= $word . ' ';
    }
    return implode($split_str, $result);
}

/**
 * Custom str_replace function for limiting number of replacements
 *
 * @param $find str
 * @param $replace str
 * @param $subject str
 * @param $limit int
 * @param &$count int
 * @return str
 */
function str_replace_custom($find, $replacement, $subject, $limit = 0, &$count = 0) {
    if (0 == $limit) {
        return str_replace($find, $replacement, $subject, $count);
    }
    $ptn = '/' . preg_quote($find, '/') . '/';
    return preg_replace($ptn, $replacement, $subject, $limit, $count);
}

/**
 * Get the file path/dir from which a script/function was initially executed
 *
 * @param bool $include_filename include/exclude filename in the return string
 * @return str
 */
function get_function_origin_path($include_filename = true) {
    $bt = debug_backtrace();
    array_shift($bt);
    if (array_key_exists(0, $bt) && array_key_exists('file', $bt[0])) {
        $file_path = $bt[0]['file'];
        if (false === $include_filename) {
            $file_path = str_replace(basename($file_path), '', $file_path);
        }
    } else {
        $file_path = null;
    }
    return $file_path;
}

/**
 * Loop through a directory for existing PHP files
 *
 * @param str folder_name
 * @param [bool] require_here
 * @return void
 */
function require_all_here($folder_name, $require_here = true) {
    $folder_name = get_function_origin_path(false) . $folder_name;
    $all_files = array();
    if ($handle = opendir($folder_name)):
        while (false !== ($file = readdir($handle))):
            $current_file = $folder_name . '/' . $file;
            if (is_file($current_file) && strpos($current_file, '.php') !== false):
                $all_files[] = $current_file;
                if (true === $require_here):
                    require_once $current_file;
                endif;
            endif;
        endwhile;
        closedir($handle);
    endif;
    return $all_files;
}

/**
 * Check if multi-array key exists
 *
 * @param mixed $needle the key you want to check for
 * @param mixed $haystack the array you want to search
 * @return bool
 */
function multi_array_key_exists($needle, $haystack) {
    foreach ($haystack as $key => $value):
        if ($needle == $key) {
            return true;
        }
        if (is_array($value)):
            if (multi_array_key_exists($needle, $value) == true) {
                return true;
            } else {
                continue;
            }
        endif;
    endforeach;
    return false;
}

/**
 * Flatten an array
 *
 * @param arr $array
 * @param [string/int] $key
 * @return arr
 */
function array_flatten($array, $key = 0) {
    $return = array();
    if (is_object($array)) {
        $array = (array) $array;
    }
    foreach ($array as $v => $value) {
        if (isset($value[$key])) {
            $return[] = $value[$key];
        }
    }
    return $return;
}

/**
 * Return a string or array of strings replacing all instances
 * of a substring with provided replacement strings
 *
 * @param arr $find_array an array of all substrings to find
 * @param arr $replacements an array of all strings to replace with correlated string in $find_array
 * @param arr/str $subject
 * @return arr/str
 */
function str_replace_all($find_array, $replacements, $subject) {
    foreach ($find_array as $i => $value):
        if (is_array($replacements)):
            $subject = str_replace($find_array[$i], $replacements[$i], $subject);
        else:
            $subject = str_replace($find_array[$i], $replacements, $subject);
        endif;
    endforeach;
    return $subject;
}

/**
 * Return the values in an array as string
 *
 * @param arr $array
 * @param str $separator
 * @param [string/int] $sub_key
 * @return str
 */
function echo_array_values($array, $separator = ' ', $sub_key = null) {
    $return_string = '';
    foreach ($array as $key => $val) {
        if (is_object($val)) {
            $val = object_to_array($val);
        }
        if (null !== $sub_key) {
            $val = get_array_item($val, $sub_key);
        }
        $return_string .= ($val . $separator);
    }
    return trim($return_string);
}

/**
 * Trim/Remove empty array items (e.g. null or empty string values)
 *
 * @param arr $array
 * @return arr
 */
function trim_empty_array_items($array) {
    $trimmed_array = array_filter($array, create_function('$value', 'return $value !== "";'));
    return $trimmed_array;
}

/**
 * Trim/Remove empty array items (e.g. null or empty string values)
 *
 * @param arr $array
 * @return arr
 */
function filter_out_false_array_items($array) {
    $trimmed_array = array_filter($array, create_function('$value', 'return $value !== false;'));
    return $trimmed_array;
}

/**
 * Split string returning array or last item
 *
 * @param str $string
 * @param str $split where to split the string
 * @param bool $last
 * @return arr/str
 */
function split_string_custom($string, $split, $last = false) {
    $array = explode($split, $string);
    if ($last) {
        if (is_array($array) && isset($array[count($array) - 1])) {
            return $array[count($array) - 1];
        }
    } else {
        return $array;
    }
}

/**
 * Return an array as string
 *
 * @param arr $array
 * @param str $separator
 * @param [string/int] $sub_key
 * @return str
 */
function get_array_as_string($array, $separator = ' ', $sub_key = null) {
    $return_string = '';
    foreach ($array as $key => $val) {
        if (is_object($val)) {
            $val = object_to_array($val);
        }
        if (null !== $sub_key) {
            $val = get_array_item($val, $sub_key);
        }
        $return_string .= ($val . $separator);
    }
    return trim($return_string);
}

/**
 * Set Cookie w/ JavaScript by injecting inline script element
 *
 * @param  $name
 * @param  $value
 * @return cookie
 */
function js_set_cookie($name, $value = null) {
    $js_script = 'function createCookie(name, value, days) {var expires; if (days) {var date = new Date(); date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000)); expires = "; expires=" + date.toGMTString(); } else {expires = ""; } document.cookie = name + "=" + value + expires + "; path=/"; } function getCookie(c_name) {if (document.cookie.length > 0) {c_start = document.cookie.indexOf(c_name + "="); if (c_start != -1) {c_start = c_start + c_name.length + 1; c_end = document.cookie.indexOf(";", c_start); if (c_end == -1) {c_end = document.cookie.length; } return unescape(document.cookie.substring(c_start, c_end)); } } return null; } function setCookie(c_name, value, exdays) {var exdate = new Date(); exdate.setDate(exdate.getDate() + exdays); var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString()); document.cookie = c_name + "=" + c_value; }';
    if (null !== $value) {
        $js_script .= 'setCookie("' . $name . '", "' . $value . '");';
    }
    $js_string = '<script id="my-tmp-script-id">' . $js_script . '</script>';
    echo $js_string;
    return get_array_item($_COOKIE, $name);
}

/**
 * Validate a US zip code
 * @param $zip_code
 *
 * @return bool
 */
function validate_zip_code($zip_code) {
    if (preg_match("/^([0-9]{5})(-[0-9]{4})?$/i", $zip_code)):
        return true;
    else:
        return false;
    endif;
}

/**
 * Log to file on server - Optionally define global PHP variable $user_log_file
 *
 * Example:
 * $GLOBALS['user_log_file'] = '/var/log/userlog.log';
 *
 * @param  $msg str the log message to write
 * @param  $filename str the log file path on server
 * @return void
 */
function sdg_log_to_file($msg, $filename = null) {
    global $user_log_file;
    if (null !== $filename):
        $fd = fopen($filename, "a");
    elseif (isset($user_log_file)):
        $fd = fopen($user_log_file, "a");
    endif;
    $str = "[" . date("Y/m/d h:i:s", strtotime('Now')) . "] " . $msg;
    fwrite($fd, $str . "\n");
    fclose($fd);
}

/**
 * Send log message to email address - Optionally define global PHP variable $user_log_email
 *
 * Example:
 * $GLOBALS['user_log_email'] = '/var/log/userlog.log';
 *
 * @param  $msg str the log message
 * @param  $address str email address to send to
 * @return void
 */
function sdg_log_to_mail($msg, $address = null) {
    global $user_log_email;
    $str = "[" . date("Y/m/d h:i:s", strtotime('Now')) . "] " . $msg;
    $e = "/^[-+\\.0-9=a-z_]+@([-0-9a-z]+\\.)+([0-9a-z]){2,4}$/i";
    if (null !== $address):
        $valid_email = (preg_match("/^([_a-z0-9\-]+)(\.[_a-z0-9\-]+)*@([a-z0-9\-]{2,}\.)*([a-z]{2,4})(,([_a-z0-9\-]+)(\.[_a-z0-9\-]+)*@([a-z0-9\-]{2,}\.)*([a-z]{2,4}))*$/", $address)) ? true : false;
        if ($valid_email):
            mail($address, "Log message", $str);
            log_to_file('Sending log email(s) to ' . $address . '.');
        else:
            log_to_file('Skipping email log: email(s) invalid.');
        endif;
    elseif (isset($user_log_email)):
        $valid_email = (preg_match("/^([_a-z0-9\-]+)(\.[_a-z0-9\-]+)*@([a-z0-9\-]{2,}\.)*([a-z]{2,4})(,([_a-z0-9\-]+)(\.[_a-z0-9\-]+)*@([a-z0-9\-]{2,}\.)*([a-z]{2,4}))*$/", $user_log_email)) ? true : false;
        if ($valid_email):
            mail($user_log_email, "Log message", $str);
            log_to_file('Sending log email(s) to ' . $user_log_email . '.');
        else:
            log_to_file('Skipping email log: email(s) invalid.');
        endif;
    else:
        log_to_file('Skipping email log: no email addresses defined. (define $GLOBALS["user_log_email"] in "wp-config.php" to enable).');
    endif;
}
/**
 * Check Remote File URL Exists
 *
 * @param  [str] $url
 * @return bool
 */
function check_remote_file_url_exists($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_NOBODY, 1);
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    if (curl_exec($ch) !== FALSE) {
        return true;
    } else {
        return false;
    }
}

/**
 * Split HTML string into elements
 *
 * @param $string
 * @return str
 */
function sdg_split_els($string, $elem = 'div') {
    return trim_empty_array_items(explode('</' . $elem . '>', $string));
}

/**
 * Split HTML string into paragraphs
 *
 * @param $string
 * @return str
 */
function sdg_split_paragraphs($string, $elem = 'div') {
    return sdg_split_els($string, $elem);
}

/**
 * Get image count in directory
 *
 * @param  [str] $directory
 * @return int
 */
function get_img_count_in_dir($directory) {
    $files = glob("$directory{*.jpg,*.JPG,*jpeg, *JPEG,*.png,*.gif,*.svg,*.SVG}", GLOB_BRACE);
    if (false !== $files) {
        $filecount = count($files);
        return $filecount;
    } else {
        return 0;
    }
}

/**
 * Validate a US phone number
 *
 * @param $phone_num
 * @return bool
 */
function validate_phone_number($phone_num) {
    $regex = "/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i";
    $valid = (preg_match($regex, $phone_num) ? 'valid' : 'invalid');
    return $valid;
}

/**
 * Format phone number
 *
 * @param  [str] $country
 * @param  [str] $phone
 * @return [str]
 */
function format_phone_number($country, $phone) {
    $function = 'format_' . $country . 'phone_number';
    if (function_exists($function)) {
        return $function($phone);
    }
    return $phone;
}

/**
 * Format US phone number
 *
 * @param  [str] $country
 * @param  [str] $phone
 * @return [str]
 */
function format_us_phone_number($phone, $html_compliant = false) {
    if (!isset($phone{3})) {return '';}
    $phone = preg_replace("/[^0-9]/", "", $phone);
    $length = strlen($phone);
    switch ($length) {
    case 7:
        $formatted = preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
        break;
    case 10:
        $formatted = preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
        break;
    case 11:
        $formatted = preg_replace("/([0-9]{1})([0-9]{3})([0-9]{3})([0-9]{4})/", "$1($2) $3-$4", $phone);
        break;
    default:
        $formatted = $phone;
        break;
    }
    if ($html_compliant) {
        $formatted = str_replace_custom(' ', '', $formatted);
    }
    return $formatted;
}

/**
 * Check if file is image
 *
 * @param  [str] $path
 * @return bool
 */
function sdg_file_is_valid_image($path) {
    $size = @getimagesize($path);
    return !empty($size);
}

/**
 * Check if file exists or just suppress the error
 *
 * @param  [str] $path - the file path
 * @return void
 */
function sdg_require($path) {
    if( file_exists($path) ) {
        require_once $path;
    }
}

/**
 * Split HTML into pieces
 *
 * @param  [str] $content html as string
 * @param  int $pieces how many pieces to split into
 * @return arr
 */
function sdg_split_html_content($content, $pieces = 2) {
    $half = (int)ceil(count($words = str_word_count($content, 1)) / $pieces);
    $string1 = implode(' ', array_slice($words, 0, $half));
    $string2 = implode(' ', array_slice($words, $half));
    return array($string1, $string2);
}

/**
 * Find the position of the Xth occurrence of a substring in a string
 *
 * @param $haystack
 * @param $needle
 * @param $number integer > 0
 * @return int
 */
function strpos_x($haystack, $needle, $number = 1){
    if ( $number == 1 ){
        $return = strpos($haystack, $needle);
    } elseif ( $number > 1 ) {
        $return = strpos($haystack, $needle, strposX($haystack, $needle, $number - 1) + strlen($needle));
    } else{
        $return = 'Error: Value for parameter $number is out of range';
    }
    return $return;
}

/**
 * Toggle a word between singular & plural form
 *
 * @param  [str] $word
 * @return [str]
 */
function toggle_word_singular_plural($word) {
    return (preg_match('~s$~i', $word) > 0) ? rtrim($word, 's') : sprintf('%ss', $word);
}

/**
 * Get array keys & values as string
 *
 * @param  array $array array with keys & values
 * @param  str $wrap string to wrap values in
 * @param  str $kvsep string to separate key w/ value
 * @param  str $arrsep string to separate each array as string
 * @return arr
 */
function sdg_get_arr_str($array, $wrap='"', $kvsep='=', $arrsep=' ') {
    $GLOBALS['sdg_tmp']['wrap'] = $wrap;
    $GLOBALS['sdg_tmp']['kvsep'] = $kvsep;
    $output = implode($wrap.$arrsep, array_map(
        function ($v, $k) {
            global $sdg_tmp;
            if (is_array($v)) {
                return $k.'[]='.implode('&'.$k.'[]=', $v);
            } else {
                return $k.$sdg_tmp['kvsep'].$sdg_tmp['wrap'].$v;
            }
        },
        $array,
        array_keys($array)
    ));
    unset($sdg_tmp);
    return $output;
}

/**
 * Validates value as URL (according to » http://www.faqs.org/rfcs/rfc2396)
 *
 * @param  str $url
 * @return bool
 */
function sdg_validate_url($url) {
    if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
        return false;
    } else {
        return true;
    }
}

/**
 * Get scaffolding column class/size based on number of columns
 *
 * @param  mixed $cols provide columns (array/object) or count (int) of columns
 * @param  bool $as_word return number as word (e.g. '2' as 'two')
 * @param  int $max maximum number of columns per row
 * @return str
 */
function sdg_get_scaffold_class($cols, $as_word = true, $max = 4) {
    $cols = $pillar_count = is_numeric($cols) ? intval($cols) : count($cols);

    if ( $pillar_count > 12 ) {
        $pillar_count = (($pillar_count % 12) === 0) ? ($pillar_count / 12) : ($pillar_count % 12);
    }
    if ( $pillar_count > $max ) {
        for ($i = $max; $i > 0; $i--) {
            if ( ($pillar_count % $i) === 0 ) {
                $pillar_count = $pillar_count / $i;
                $i = 0;
            }
         }
    }
    if ( $pillar_count >= 5 ) {
        if ( $pillar_count % 2 === 0 ) {
            $pillar_count = intval($pillar_count / 2);
        } else {
            $pillar_count = intval(($pillar_count + 1) / 2);
        }
    }

    if ( $as_word ) {
        if ( class_exists('NumberFormatter') ) {
            $return_count = new NumberFormatter("en", NumberFormatter::SPELLOUT);
            $return_count = $return_count->format($pillar_count);
        } else {
            $return_count = 'two';
        }
    } else {
        $return_count = (12/intval($pillar_count));
    }
    return $return_count;
}

/**
 * Simple division without breaking on 0 denominator
 *
 * @param  int $numerator top fraction
 * @param  int $denominator bottom fraction
 * @param  mixed $fallback returned if denominator is 0
 * @return float
 */
function sdg_divide($numerator, $denominator, $fallback = null) {
    $fallback = (null === $fallback) ? $numerator : $fallback;
    if ( $denominator === 0 ) {
      return $fallback;
    }
    return $numerator/$denominator;
}

/**
 * Get the time/date properly
 *
 * @param  str $format
 * @return str
 */
function sdg_get_time($format = 'Y') {
    $timezone_format = _x($format, 'timezone date format');
    return date_i18n( $timezone_format, false, true );
}

/**
 * Shortcut for `echo sdg_get_time( ... )` - accepts the same arguments
 *
 * @return void
 */
function sdg_time($format = 'Y') {
    echo sdg_get_time($format);
}

/**
 * Title Case
 */
function sdg_to_title_case($string) {
    return ucwords($string);
}

if (!function_exists('strtotitlecase')):
    function strtotitlecase($str) {
        return sdg_to_title_case($str);
    }
endif;

/**
 * Parse US address into parts
 *
 * @param string $address
 * @param string $return - what parts to return
 * @return array
 */
function parse_location_address($address, $return = 'all') {
    preg_match("/(.+), (\w+), (\w+) (\w+)/", $address, $matches);
    list($original, $street, $city, $state, $zip) = $matches;
    return array(
        'street'  => $street,
        'city' => $city,
        'state' => $state,
        'zip'   => $zip
    );
}

/**
 * Try to format string like sentence
 *
 * @param string $str - sentence or phrase as string
 * @return string
 */
function str_ucsentence($str) {
    if ($str) { // input
        $str = preg_replace('/'.chr(32).chr(32).'+/', chr(32), $str); // recursively replaces all double spaces with a space
        if (($x = substr($str, 0, 10)) && ($x == strtoupper($x))) $str = strtolower($str); // sample of first 10 chars is ALLCAPS so convert    $str to lowercase; if always done then any proper capitals would be lost
            $na = array('. ', '! ', '? '); // punctuation needles
            foreach ($na as $n) { // each punctuation needle
                if (strpos($str, $n) !== false) { // punctuation needle found
                    $sa = explode($n, $str); // split
                    foreach ($sa as $s) $ca[] = ucfirst($s); // capitalize
                    $str = implode($n, $ca); // replace $str with rebuilt version
                    unset($ca); //  clear for next loop
                }
            }
        return ucfirst(trim($str)); // capitalize first letter in case no punctuation needles found
    }
}

/**
 * Convert dashed string to CamelCase
 *
 * @param  string $string
 * @param  boolean $capitalize_first
 * @return string
 */
function dashed_to_camel_case($string, $capitalize_first = true) {
    $str = str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));
    if (!$capitalize_first) {
        $str[0] = strtolower($str[0]);
    }
    return $str;
}
