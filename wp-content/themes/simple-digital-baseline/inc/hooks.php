<?php
/**
 * Custom Hooks & Filters
 *
 * @package sdg Branding
 * @subpackage sdg-child
 * @since 1.0
 */

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class CustomHooks{

 	// Register all hook and filter functions here
    private $hooks = array( 'facebook_featured_image',
    						'add_query_vars_filter',
    						'gravity_form_confirmation',
						);

	public function __construct() {
		// setup hooks using the registered functions from this class
		foreach( $this->hooks as $hook ):
			$this->{$hook}();
		endforeach;
	}

	/**
	 * Enable the confirmation anchor on all forms
	 */
	function gravity_form_confirmation(){
		add_filter( 'gform_confirmation_anchor', '__return_true' );
	}

	/**
	 * Facebook Featured Image and Open Graph Meta Tags
	 *  - Use featured image as the facebook Open Graph image
	 *  - Based on plugin by Ryan S. Cowles (http://www.ryanscowles.com)
	 */
	function facebook_featured_image(){

		add_filter('language_attributes', array( $this, 'add_og_xml_ns' ) );
		add_filter('language_attributes', array( $this, 'add_fb_xml_ns' ) );
		add_action('wp_head', array( $this, 'fbogmeta_header' ) );

	}

	// Allow for Facebooks's markup language
	function add_og_xml_ns($content) {
		return ' xmlns:og="http://ogp.me/ns#" ' . $content;
	}

	function add_fb_xml_ns($content) {
		return ' xmlns:fb="https://www.facebook.com/2008/fbml" ' . $content;
	}

	// Set your Open Graph Meta Tags
	function fbogmeta_header() {
		global $post;
		setup_postdata( $post );
	  if (is_single()) {
		?>
			<!-- Open Graph Meta Tags for Facebook and LinkedIn Sharing !-->
			<meta property="og:title" content="<?php the_title(); ?>"/>
			<meta property="og:description" content="<?php echo strip_tags(get_the_excerpt($post->ID)); ?>" />
			<meta property="og:url" content="<?php the_permalink(); ?>"/>
			<?php $fb_image = wp_get_attachment_image_src(get_post_thumbnail_id( get_the_ID() ), 'thumbnail'); ?>
			<?php if ($fb_image) : ?>
				<meta property="og:image" content="<?php echo $fb_image[0]; ?>" />
				<?php endif; ?>
			<meta property="og:type" content="<?php
				if (is_single() || is_page()) { echo "article"; } else { echo "website";} ?>"
			/>
			<meta property="og:site_name" content="<?php bloginfo('name'); ?>"/>
			<!-- End Open Graph Meta Tags !-->
		<?php
	  }
	}

	/* End: Facebook Featured Image */

	/**
	 * Add query var for donation form dynamism
	 */
	function add_query_vars_filter(){
		add_filter( 'query_vars', array( $this, '_add_query_vars_filter' ) );
	}

	function _add_query_vars_filter( $vars ){
		//$vars[] = "dr"; // donation restriction (matches name in options list)
		//$vars[] = "da"; // donation amount
		//$vars[] = "df"; // donation frequency
		return $vars;
	}

}

// init
$sdg_customhooks = new CustomHooks();

// Add query variables for digestion
function sdg_add_query_vars_filter( $vars ){
  $vars[] = "locationqqq";
  return $vars;
}
add_filter( 'query_vars', 'sdg_add_query_vars_filter' );

add_action('init','add_location_id');
function add_location_id()
{
    add_rewrite_rule(
        'schedule-a-tour/(\d*)$',
        'index.php?location_id=$matches[1]',
        'top'
    );
}

function wpdev_nav_classes( $classes, $item ) {
    if( ( is_post_type_archive( 'location' ) || is_singular( 'location' ) )
        && $item->title == 'News' ){
        $classes = array_diff( $classes, array( 'current_page_parent' ) );
    }
    return $classes;
}
add_filter( 'nav_menu_css_class', 'wpdev_nav_classes', 10, 2 );


/**
 * Performs login checks
 */
function css_check_login(){
	/* list of allowed page names */
	$this_page = grab(grab_first(grab(grab($GLOBALS, 'wp_query'), 'posts'), 0), 'post_name');
	// var_log($this_page);
	$allowed = array( 'login', 'register', 'logout', 'lostpassword', 'resetpassword', 'resetpass' );
	if( !is_user_logged_in() && !in_array( $this_page, $allowed ) ):
		/* perform login check and redirect if necessary */
		if( !is_user_logged_in() && !in_array( $this_page, $allowed ) ):
			wp_redirect('/login');
			exit();
		endif;
	endif;
}
// add_action( 'wp', 'css_check_login' );


/**
 * Performs login checks
 */
// function css_check_login(){
// 	// only check non-admin pages
// 	if( !is_admin() ):
// 		$this_page = grab(grab_first(grab(grab($GLOBALS, 'wp_query'), 'posts'), 0), 'post_name');

// 		if( $this_page != 'logout' ):
// 			// list of allowed page names
// 			$allowed = array( 'login', 'register', 'logout', 'lostpassword', 'resetpassword', 'resetpass' );
// 			// perform login check and redirect if necessary
// 			if( !is_user_logged_in() && !in_array( $this_page, $allowed ) ):
// 				wp_redirect('/login');
// 				exit();
// 			else:
// 				if( is_user_logged_in() && in_array( $this_page, $allowed ) ):
// 					wp_redirect('/home');
// 					exit();
// 				endif;
// 			endif;
// 		endif;
// 	endif;
// }
// add_action( 'wp', 'css_check_login' );

/**
 * Hide admin bar from certain user roles
 */
function css_hide_admin_bar( $show ) {
	if( is_user_logged_in() ):
		$user_data = get_userdata( get_current_user_id() );
		// Hide admin bar for all non admins and "cssintranet" user
		if ( !is_super_admin( get_current_user_id() ) || $user_data->data->user_login == 'cssintranet' ) :
			return false;
		endif;
	endif;
	return $show;
}
add_filter( 'show_admin_bar', 'css_hide_admin_bar' );