<?php
/*
 * Editor tweaks
 */

// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}

// Add additional editor buttons
function enable_more_buttons($buttons) {
    $buttons[] = 'hr';

  /*
  Repeat with any other buttons you want to add, e.g.
      $buttons[] = 'fontselect';
      $buttons[] = 'sup';
  */

    return $buttons;
}
add_filter("mce_buttons", "enable_more_buttons");

// Register our callback to the appropriate filter
add_filter('mce_buttons_2', 'my_mce_buttons_2');

// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats( $init_array ) {
    // Define the style_formats array
    $style_formats = array(
        array(
            'title' => 'Hint Text',
            'inline' => 'span',
            'classes' => 'hint soft',
            'wrapper' => true,
        ),
        array(
            'title' => 'Brand Color Text',
            'inline' => 'span',
            'classes' => 'primary-color',
            'wrapper' => true,
        ),
        array(
            'title' => 'Largest Text',
            'inline' => 'span',
            'classes' => 'large-size',
            'wrapper' => true,
        ),
        array(
            'title' => 'Larger Text',
            'inline' => 'span',
            'classes' => 'larger',
            'wrapper' => true,
        ),
        array(
            'title' => 'Large Text',
            'inline' => 'span',
            'classes' => 'large',
            'wrapper' => true,
        ),
        array(
            'title' => 'Smaller Text',
            'block' => 'span',
            'classes' => 'smaller',
            'wrapper' => true,
        ),
        array(
            'title' => 'Smallest Text',
            'block' => 'span',
            'classes' => 'smallest',
            'wrapper' => true,
        ),
        array(
            'title' => 'Uppercase Text',
            'block' => 'span',
            'classes' => 'uppercase',
            'wrapper' => true,
        ),
        array(
            'title' => 'Lowercase Text',
            'block' => 'span',
            'classes' => 'lowercase',
            'wrapper' => true,
        ),
        array(
            'title' => 'Theme Button',
            'selector' => 'a',
            'classes' => 'button outline',
            'wrapper' => false,
        ),
        array(
            'title' => 'Secondary Button',
            'selector' => 'a',
            'classes' => 'button tertiary-button ',
            'wrapper' => false,
        ),
    );
    /* Insert the array, JSON ENCODED, into 'style_formats' */
    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;
}

function my_theme_add_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}

/* Attach callback to 'tiny_mce_before_init' */
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );
add_action( 'init', 'my_theme_add_editor_styles' );
