<?php
/**
 * Custom Functions Unique to This Website/App
 *
 * @author sdg
 */

/**
 * Auto Logout after 2 hours
 *
 * @param  integer $seconds seconds
 * @return void
 */
function sdg_auto_logout_enable($seconds = 900) {
	if ( isset($_SESSION) ) {
		echo "<script>alert('15 Minutes over!');</script>";
		if(time() - $_SESSION['timestamp'] > $seconds) { //subtract new timestamp from the old one
			echo"<script>alert('15 Minutes over!');</script>";
			unset($_SESSION['username'], $_SESSION['password'], $_SESSION['timestamp']);
			$_SESSION['logged_in'] = false;
		header("Location: " . index.php); //redirect to index.php
		exit;
		} else {
			$_SESSION['timestamp'] = time(); //set new timestamp
		}
	}
}

sdg_auto_logout_enable(3600);

/* Remove auto register ACF fields from WP CLI plugin */
remove_action('plugins_loaded', 'acf_wpcli_register_groups');
