<?php
/**
 * Global Variables
 *
 * @package sdg Branding
 * @subpackage sdg-child
 * @since 1.1
 */

$GLOBALS['AppGlobals'] = array();

/** Check server domain for type of dev environment */
if ( isset($_SERVER) && isset($_SERVER['SERVER_NAME']) ):
    $_ENV['DEV_MODE'] = !(strpos($_SERVER['SERVER_NAME'], 'matter.ma-architects.com') !== false);
    $_ENV['LOCAL_DEV_MODE'] = (strpos($_SERVER['SERVER_NAME'], '.local') !== false) || (strpos($_SERVER['SERVER_NAME'], '.dev') !== false);
else:
    $_ENV['DEV_MODE'] = false;
    $_ENV['LOCAL_DEV_MODE'] = false;
endif;
global $AppGlobals;

$DEV_MODE = $_ENV['DEV_MODE'];
$AppGlobals['DEV_MODE'] = true;

/** Debug tools */
$AppGlobals['user_log_file'] = ABSPATH . '/cement.log';

/** User status */
$AppGlobals['logged-in'] = (function_exists('is_user_logged_in') && is_user_logged_in());
$AppGlobals['login-page'] = is_login_page();

/** Global WP property overrides */
$GLOBALS['title_override'] = '';
$GLOBALS['excerpt_override'] = '';


