<?php
/**
 * Aliases/Fallbacks for PHP functions
 *
 * @package sdg Branding
 * @subpackage sdg-child
 * @since 1.1
 */

if ( !function_exists('get_query_var') ) {
    function get_query_var($query_var, $defaul_val) {
        $val = get_qv($query_var, $default_val);
        return $val;
    }
}

if ( !function_exists('set_query_var') ) {
    function set_query_var($query_var, $defaul_val) {
        $val = get_qv($query_var, $default_val);
        $GLOBALS[$query_var] = $val;
    }
}

if ( !function_exists('grab') ) {
    function grab($array, $key = 0, $fallback = false) {
        return get_array_item($array, $key, $fallback);
    }
}

if ( !function_exists('grab_first') ) {
    function grab_first($array, $fallback = false) {
        return get_array_first_item($array, $fallback);
    }
}