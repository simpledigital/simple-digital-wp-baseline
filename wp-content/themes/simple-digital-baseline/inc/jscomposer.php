<?php
/**
 * Visual Composer Configuration
 *
 * @author   sdg Branding
 * @package sdg Branding
 * @subpackage sdg-child
 * @since    1.0
 */

// maps
if(!function_exists('sdg_vc_custom_map_func')):
	function sdg_vc_custom_map_func(){

		if (function_exists('vc_remove_element')):

			// Map all shortcodes to Visual Composer

			vc_map(array(
				"name" => __("Breadcrumb") ,
				"description" => __("Displays breadcrumb navigation for the current page.") ,
				"base" => "sdg_breadcrumb",
				"class" => "",
				"icon" => "sdgicon",
				"category" => __('Content') ,
				"show_settings_on_create" => false,
				"params" => array(
				)
			));

			vc_map(array(
				"name" => __("Section") ,
				"description" => __("Displays content of a section within your page.") ,
				"base" => "sdg_section_content",
				"class" => "",
				"icon" => "sdgicon",
				"category" => __('Content') ,
				"params" => array(
					array(
						"type" => "dropdown",
						"holder" => "div",
						"class" => "",
						"heading" => __("Choose a Section") ,
						"param_name" => "section_id",
						"value" => __(get_all_sections() ) ,
						"description" => __("Choose a section.")
					) ,
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __("CSS classes") ,
						"param_name" => "css_classes",
						"value" => __("") ,
						"description" => __("Enter additional CSS classes to apply (separate multiples with spaces).")
					) ,
				)
			));

			vc_map(array(
				"name" => __("Page List") ,
				"description" => __("Displays links to list of pages filtered by certain criteria.") ,
				"base" => "sdg_page_list",
				"class" => "",
				"icon" => "sdgicon",
				"category" => __('Content') ,
				"params" => array(
					array(
						"type" => "dropdown",
						"holder" => "div",
						"class" => "",
						"heading" => __("Choose a Parent Page") ,
						"param_name" => "parent_page",
						"value" => __(get_posts_of_type('page') ) ,
						"description" => __("Choose a page.")
					) ,
					array(
						"type" => "dropdown",
						"holder" => "div",
						"class" => "",
						"heading" => __("Style of display") ,
						"param_name" => "display_style",
						"value" => __( array( 'Select' => '', 'Icon List' => 'icons', 'Simple List' => 'simple' ) ) ,
						"description" => __("Choose how you want your list to look.")
					),
				)
			));

			vc_map(array(
				"name" => __("Social Icon Block") ,
				"description" => __("Displays icons linking to social media accounts as defined under the 'Theme Options'.") ,
				"base" => "sdg_social_icon_block",
				"class" => "",
				"icon" => "sdgicon",
				"category" => __('Content') ,
				"params" => array(
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __("Content") ,
						"param_name" => "title",
						"value" => __("") ,
						"description" => __("Enter a title (optional).")
					)
				)
			));

			vc_map(array(
				"name" => __("Program List") ,
				"description" => __("Displays a list of programs.") ,
				"base" => "sdg_program_list",
				"class" => "",
				"icon" => "sdgicon",
				"category" => __('Content') ,
				"params" => array(
					array(
						"type" => "dropdown",
						"holder" => "div",
						"class" => "",
						"heading" => __("Choose Parent Category") ,
						"param_name" => "program_parent_category_id",
						"value" => __(get_post_taxonomy('program_category') ) ,
						"description" => __("Choose a category.")
					) ,
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __("Number to show") ,
						"param_name" => "limit",
						"value" => __("") ,
						"description" => __("Enter a number (blank for all).")
					),
				)
			));

						vc_map(array(
				"name" => __("Program List") ,
				"description" => __("Displays a list of programs.") ,
				"base" => "sdg_program_list",
				"class" => "",
				"icon" => "sdgicon",
				"category" => __('Content') ,
				"params" => array(
					array(
						"type" => "dropdown",
						"holder" => "div",
						"class" => "",
						"heading" => __("Choose Parent Category") ,
						"param_name" => "program_parent_category_id",
						"value" => __(get_post_taxonomy('program_category') ) ,
						"description" => __("Choose a category.")
					) ,
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __("Number to show") ,
						"param_name" => "limit",
						"value" => __("") ,
						"description" => __("Enter a number (blank for all).")
					),
				)
			));

			vc_map(array(
				"name" => __("Resource Search Box") ,
				"description" => __("Displays a search box to filter resource items on the page.") ,
				"base" => "sdg_resource_search",
				"class" => "",
				"icon" => "sdgicon",
				"category" => __('Content') ,
				"params" => array(
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __("ID of resource list to filter") ,
						"param_name" => "resource_list_id",
						"value" => __("") ,
						"description" => __("")
					),
				)
			));

			vc_map(array(
				"name" => __("Resource List") ,
				"description" => __("Displays a list of resources.") ,
				"base" => "sdg_resource_list",
				"class" => "",
				"icon" => "sdgicon",
				"category" => __('Content') ,
				"params" => array(
					array(
						"type" => "dropdown",
						"holder" => "div",
						"class" => "",
						"heading" => __("Filter by Category") ,
						"param_name" => "resource_category_id",
						"value" => __(get_post_taxonomy('resource_category') ) ,
						"description" => __("Choose a category.")
					) ,
					array(
						"type" => "dropdown",
						"holder" => "div",
						"class" => "",
						"heading" => __("Choose Parent Category") ,
						"param_name" => "resource_parent_category_id",
						"value" => __(get_post_taxonomy('resource_category') ) ,
						"description" => __("Choose a category.")
					) ,
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __("Number to show") ,
						"param_name" => "limit",
						"value" => __("") ,
						"description" => __("Enter a number (blank for all).")
					),
					array(
						"type" => "dropdown",
						"holder" => "div",
						"class" => "",
						"heading" => __("Style of display") ,
						"param_name" => "display_style",
						"value" => __( array( 'Select' => '', 'Simple List' => 'simple', 'Icon List' => 'icons', 'Categorized List' => 'categorized', 'Accordion' => 'accordion', 'Paged List' => 'paged' ) ) ,
						"description" => __("Choose how you want your list to look.")
					),
				)
			));

			vc_map(array(
				"name" => __("Team Member List") ,
				"description" => __("Displays a list of team members, filtered by certain criteria.") ,
				"base" => "sdg_team_member_list",
				"class" => "",
				"icon" => "sdgicon",
				"category" => __('Content') ,
				"params" => array(
					array(
						"type" => "textfield",
						"holder" => "div",
						"class" => "",
						"heading" => __("Number to show") ,
						"param_name" => "limit",
						"value" => __("") ,
						"description" => __("Enter a number (blank for all).")
					),
				)
			));

		endif; // vc_remove_element function check

	}
	add_action('sdg_vc_custom_map','sdg_vc_custom_map_func');
endif;

/**
 * Change path for overridden templates
 */
if(function_exists('vc_set_shortcodes_templates_dir')) {
	$dir = get_stylesheet_directory() . '/vc-templates';
	vc_set_shortcodes_templates_dir( $dir );
}

/*** Row ***/
if ( ! function_exists('sdg_vc_row_map') ) {
	/**
	 * Map VC Row
	 * Hooks on vc_after_init action
	 */
	function sdg_vc_row_map() {
		vc_add_param('vc_row', array(
			'type' => 'dropdown',
			'class' => '',
			'heading' => 'Content Width',
			'param_name' => 'content_width',
			'value' => array(
				'In Grid' => 'grid',
				'Full Width' => 'full',
			)
		));

		vc_add_param('vc_row', array(
			'type' => 'dropdown',
			'class' => '',
			'heading' => 'Page Header Style',
			'param_name' => 'page_header_style',
			'value' => array(
				'Not a Page Header' => '',
				'Normal' => 'page-header',
				'Hero' => 'hero',
			)
		));

	}

	add_action('vc_after_init', 'sdg_vc_row_map');
}


// TODO - move this to sdg.tribe.php
function get_multisite_event_params(){
	$params = array();

	$params[] = array(
					 "type" => "textfield",
					 "holder" => "div",
					 "class" => "",
					 "heading" => __("Title"),
					 "param_name" => "title",
					 "value" => __(""),
					 "description" => __("Enter a title (not required).")
				  );

	if( is_multisite() ):
		$all_sites = get_all_sites();

		$params[] = array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Choose a Site"),
			"param_name" => "site_blog_id",
			"value" => __( $all_sites),
			"description" => __("Choose a site.")
		);

		foreach( $all_sites as $blog_name => $blog_id ):
			$params[] = array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Event Venue"),
				"param_name" => "event_venue_" . $blog_id,
				"value" => __( get_event_venues( $blog_id )),
				"dependency" => array( 'element' => 'site_blog_id',
							'value' => $blog_id
						),
				"description" => __("Choose a venue filter. Leave blank for all.")
			);
			$params[] = array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Event Organizer"),
				"param_name" => "event_organizer_" . $blog_id,
				"value" => __( get_event_organizers( $blog_id )),
				"dependency" => array( 'element' => 'site_blog_id',
							'value' => $blog_id
						),
				"description" => __("Choose an organizer filter. Leave blank for all.")
			);
			$params[] = array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Event Tags"),
				"param_name" => "post_tag_" . $blog_id,
				"value" => __( get_post_tags( $blog_id )),
				"dependency" => array( 'element' => 'site_blog_id',
							'value' => $blog_id
						),
				"description" => __("Choose a tag filter.")
			);
			$params[] = array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => __("Event Category"),
				"param_name" => "event_category_" . $blog_id,
				"value" => __( get_event_categories( $blog_id )),
				"dependency" => array( 'element' => 'site_blog_id',
							'value' => $blog_id
						),
				"description" => __("Choose a category filter. Leave blank for all.")
			);
			$specific_ids = get_recent_events( $blog_id );
			if( !empty( $specific_ids ) ):
				$params[] = array(
					"type" => "checkbox",
					"holder" => "div",
					"class" => "",
					"heading" => __("Choose Specific Posts"),
					"param_name" => "specific_ids_" . $blog_id,
					"value" => __( $specific_ids),
					"dependency" => array( 'element' => 'site_blog_id',
								'value' => $blog_id
							),
					"description" => __( "Select from 15 most recent events", "neomed")
				);
			endif;
		endforeach;
	else:
		$params[] = array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Event Venue"),
			"param_name" => "event_venue",
			"value" => __( get_event_venues()),
			"description" => __("Choose a venue filter. Leave blank for all.")
		);
		$params[] = array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Event Organizer"),
			"param_name" => "event_organizer",
			"value" => __( get_event_organizers()),
			"description" => __("Choose an organizer filter. Leave blank for all.")
		);
		$params[] = array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Event Tags"),
			"param_name" => "post_tag",
			"value" => __( get_post_tags()),
			"description" => __("Choose a tag filter.")
		);
		$params[] = array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Event Category"),
			"param_name" => "event_category",
			"value" => __( get_event_categories()),
			"description" => __("Choose a category filter. Leave blank for all.")
		);
		$params[] = array(
			"type" => "checkbox",
			"holder" => "div",
			"class" => "",
			"heading" => __("Choose Specific Events"),
			"param_name" => "specific_ids",
			"value" => __( get_recent_events()),
			"description" => __( "Select from 15 most recent events", "neomed")
		);
	endif;

	$params[] = array(
				 "type" => "textfield",
				 "holder" => "div",
				 "class" => "",
				 "heading" => __("Number to show"),
				 "param_name" => "limit",
				 "value" => __("1"),
				 "description" => __("Enter the number of posts to show.")
			  );
	$params[] = array(
				 "type" => "dropdown",
				 "holder" => "div",
				 "class" => "",
				 "heading" => __("Display Style"),
				 "param_name" => "display_style",
				 "value" => __( array('Columns' => '','List' => 'List')),
				 "description" => __("Choose a style to show the events.")
			  );
	$params[] = array(
					"type" => "checkbox",
					"holder" => "div",
					"class" => "",
					"heading" => __("Show image"),
					"param_name" => "show_image",
					"value" => 1,
					"std" => true,
					"description" => __( "Check to show featured images of the posts", "neomed")
				);
	$params[] = array(
				 "type" => "dropdown",
				 "holder" => "div",
				 "class" => "",
				 "heading" => __("Text Color"),
				 "param_name" => "text_color",
				 "value" => __( array('Choose a Color' => '','White' => 'white', 'Black' => 'black')),
			  );
	$params[] = array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __("'View All' link text"),
					"param_name" => "more_link_text",
					"value" => __(""),
					"description" => __("Text to show in the link to view all events.")
				);

	return $params;

}

/**
 * Get event venues
 */
 function get_event_venues( $blog_id = '' ){

	if( is_multisite() && $blog_id != '' ) switch_to_blog( $blog_id );

	$venues = array('Select' => '');

	// get all venues from tribe events
	$_venues = tribe_get_venues();

	foreach( $_venues as $_venue ):
		$venues[$_venue->post_title] = $_venue->ID;
	endforeach;

	if( is_multisite() && $blog_id != '' ) restore_current_blog();

	return $venues;
}

/**
 * Get event organizers
 */
 function get_event_organizers( $blog_id = '' ){

	if( is_multisite() && $blog_id != '' ) switch_to_blog( $blog_id );

	$organizers = array('Select' => '');

	// get all organizers from tribe events
	$_organizers = tribe_get_organizers();

	foreach( $_organizers as $_organizer ):
		$organizers[$_organizer->post_title] = $_organizer->ID;
	endforeach;

	if( is_multisite() && $blog_id != '' ) restore_current_blog();

	return $organizers;
}

/**
 * Get post tags
 */
 function get_post_tags( $blog_id = '' ){
	 return get_post_taxonomy('post_tag', true, $blog_id );
}

/**
 * Get event categories
 */
 function get_event_categories( $blog_id = '' ){
	 return get_post_taxonomy('tribe_events_cat', true, $blog_id);
}

/**
 * Get recent events
 */
function get_recent_events( $blog_id = '' ){

	return get_posts_of_type( 'tribe_events', 'Event', 'date', 'DESC', 15, false, $blog_id );

}

/**
 * Get custom taxonomy
 */
function get_post_taxonomy($thisTaxonomy, $select = true, $blog_id = ''){

	if( is_multisite() && $blog_id != '' ) switch_to_blog( $blog_id );

	$cats = array();
	$cats = get_terms( array(
		'taxonomy' => $thisTaxonomy,
		'hide_empty' => false,
	) );
	$cats = simplify_array($cats,'name','term_id');
	if( $select ):
		$cats = array('Select' => '') + $cats;
	endif;

	if( is_multisite() && $blog_id != '' ) restore_current_blog();

	return $cats;
}

/**
 * Get posts of a certain type
 */
function get_posts_of_type( $post_type = 'post', $post_title = 'Post', $order_by = 'menu_order', $order = 'ASC', $limit = -1, $show_blank = true, $blog_id = '' ){

	if( $blog_id != '' ) switch_to_blog( $blog_id );

	$posts = array();
	$post_loop = new WP_Query( array(
		'post_type' => $post_type,
		'posts_per_page' => $limit,
		'orderby'=> $order_by,
		'order'=> $order,
	) );

	if( $post_loop->have_posts() ):
		while ( $post_loop->have_posts() ) : $post_loop->the_post();
			$posts[get_the_title() . ( $show_blank ? '' : '<br />' )] = get_the_ID();
		endwhile;
	endif;

	if( $show_blank ):
		$posts = array( 'No ' . $post_title . ' Selected' => '' ) + $posts;
	endif;

	if( $blog_id != '' ) restore_current_blog();

	return $posts;

}

/**
* Get all sites
*/
function get_all_sites(){

	$return = false;
	if( is_multisite() ):
		$return['Select a Site'] = '';
		$sites = get_sites();
		foreach( $sites as $site ):
			$subsite_id = get_object_vars($site)["blog_id"];
			$subsite_name = get_blog_details($subsite_id)->blogname;
			$return[ $subsite_name ] = $subsite_id;
		endforeach;
	endif;
	return $return;
}

function simplify_array( $array, $key_name, $value_name ){
	$return = array();
	foreach($array as $k => $v){
		if( is_object( $v ) ):
			$return[$v->{$key_name}] = $v->{$value_name};
		endif;
	}
	return $return;
}

function get_all_theme_colors( $include_blank = false ){
	global $sdg;
	// Our theme color names
	$names = array( '#0d5b89' => 'Blue',
					'#38c7de' => 'Cyan',
					'#a3c245' => 'Green',
					'#e77933' => 'Orange',
					'#ea2874' => 'Pink',
					'#fed83b' => 'Yellow',
					'#f4f4f1' => 'Off-White',
					);
	$return = array();

	if( $include_blank ) $return['Select a Color'] = '';
	for($i=1;$i<9;$i++){
		if( $sdg['scss-color-' . $i]['rgba'] != '' ):
			${'color$i'} = rgb2hex($sdg['scss-color-' . $i]['rgba']);
			$return[$names[${'color$i'}]] = ${'color$i'};
		endif;
	}
	return $return;
}