<?php
/**
 * Theme Options
 *
 * @author sdg
 */

/**
 * Sets up theme defaults and registers the various WordPress features that
 * sdg supports.
 *
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_editor_style() To add a Visual Editor stylesheet.
 * @uses add_theme_support() To add support for post thumbnails, automatic feed links,
 *     custom background, and post formats.
 * @uses register_nav_menu() To add support for navigation menus.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since sdg 1.0
 */
function sdg_setup_theme_options() {
    /*
     * Makes sdg available for translation.
     *
     * Translations can be added to the /languages/ directory.
     * If you're building a theme based on sdg, use a find and replace
     * to change 'sdg' to the name of your theme in all the template files.
     */
    load_theme_textdomain('sdg', get_template_directory() . '/languages');

    /* Adding Excerpts to Pages in WordPress */
    add_post_type_support('page', 'excerpt');

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support('title-tag');

    // This theme styles the visual editor with editor-style.css to match the theme style.
    add_editor_style();

    // Adds RSS feed links to <head> for posts and comments.
    add_theme_support('automatic-feed-links');

    // This theme supports a variety of post formats.
    add_theme_support('post-formats', array('aside', 'image', 'link', 'quote', 'status'));

    // This theme uses wp_nav_menu() in one location.
    register_nav_menu('primary', __('Primary Menu', 'sdg'));
    register_nav_menu('top', __('Top Menu', 'sdg'));
    register_nav_menu('quick-links', __('Quick Links Menu', 'sdg'));
    register_nav_menu('footer', __('Footer Menu', 'pangea'));
    register_nav_menu('mobile', __('Mobile Menu', 'pangea'));

    /*
     * This theme supports custom background color and image, and here
     * we also set up the default background color.
     */
    add_theme_support(
        'custom-background', array(
            'default-color' => 'e6e6e6',
       )
   );

    // This theme uses a custom image size for featured images, displayed on "standard" posts.
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(624, 9999); // Unlimited height, soft crop
}

function wp_scss_set_variables() {
    global $sdg;
    // echo 'doing variables!';
    // pprint_r($sdg);
    $variables = array(
        'color1'  => $sdg['scss-color-1']['rgba'],
        'color2'  => $sdg['scss-color-2']['rgba'],
        'color3'  => $sdg['scss-color-3']['rgba'],
        'color4'  => $sdg['scss-color-4']['rgba'],
        'color5'  => $sdg['scss-color-5']['rgba'],
        'color6'  => $sdg['scss-color-6']['rgba'],
        'color7'  => $sdg['scss-color-7']['rgba'],
        'color8'  => $sdg['scss-color-8']['rgba'],
        'color9'  => $sdg['scss-color-9']['rgba'],
        'color10' => $sdg['scss-color-10']['rgba'],
        'font1'   => $sdg['scss-font-1']['font-family'] . ',' . $sdg['scss-font-1']['font-backup'],
        'font2'   => $sdg['scss-font-2']['font-family'] . ',' . $sdg['scss-font-2']['font-backup'],
        'font3'   => $sdg['scss-font-3']['font-family'] . ',' . $sdg['scss-font-3']['font-backup'],
        'font4'   => $sdg['scss-font-4']['font-family'] . ',' . $sdg['scss-font-4']['font-backup'],
        'font5'   => $sdg['scss-font-5']['font-family'] . ',' . $sdg['scss-font-5']['font-backup'],
   );
    return $variables;
    // exit();
}

/**
 * Get Site Logo
 *
 * @return str
 */
function sdg_get_site_logo() {
    global $sdg;
    if (isset($sdg['site-logo']) && isset($sdg['site-logo']['url'])) {
        return $sdg['site-logo']['url'];
    } else {
        return sdg_get_field('global_site_logo', get_stylesheet_directory_uri() . '/assets/img/logo.png');
    }
}

/**
 * Modify the default "Excerpt" label in the backend for clarity
 *
 * @param  str $translation
 * @param  str $original
 * @return str
 */
function sdg_modify_excerpt_label($translation, $original) {
    if ('Excerpt' == $original) {
        return __('Excerpt/Summary');
    } elseif (false !== strpos($original, 'Excerpts are optional hand-crafted summaries of your')) {
        return __(' When provided, the summary is used in feeds and on landing pages instead of the main content for brevity.');
    }
    return $translation;
}


/**
 * Add the wp-editor back into WordPress after it was removed in 4.2.2.
 *
 * @param $post
 * @return void
 */
function sdg_fix_no_editor_on_posts_page($post) {
    if ($post->ID != get_option('page_for_posts')) {
        return;
    }
    // body_class(array_push(get_body_class(), 'page-for-posts-admin-id'));
    remove_action('edit_form_after_title', '_wp_posts_page_notice');
    add_post_type_support('post', array('title', 'editor', 'excerpt', 'thumbnail', 'page-attributes'));
}

/**
 * Customize Backend-End Body Classes
 *
 * @param  $classes
 * @return str
 */
function sdg_backend_body_classes($classes) {
    $extra_classes = array();
    if ($post->ID == get_option('page_for_posts')) {
        $extra_classes [] = 'page-for-posts-wp-admin';
    }
    $extra_classes [] = ('admin-slug-' . get_the_slug_or(get_qv('post'), 'non-post'));
    return $classes . ' ' . implode(' ', $extra_classes);
}

/**
 * Customize Front-End Body Classes
 *
 * @param  $classes
 * @return array
 */
function sdg_frontend_body_classes($classes) {
    global $sdg_id;
    $classes[] = 'intranet-frontent slug-' . get_the_slug($sdg_id);
    return $classes;
}

/**
 * Customize Thumbnails
 *
 * @return void
 */
function sdg_custom_theme_thumbnails() {
    add_image_size('300-thumb', 300); /* 300 pixels wide (and unlimited height) */
    add_image_size('slideshow-thumb', 520, 360, true); /* 520 wide by 360 height, cropped */
    add_image_size('400-square', 400, 400, array('center', 'center'));
    add_image_size('large-slider', 1200, 550, true);
    add_image_size('medium-slider', 1200, 720, true);
    add_image_size('large-banner', 1200, 250, true);
}

/**
 * Show these sizes when inserting into post
 *
 * @param  array $sizes
 * @return array
 */
function sdg_show_custom_theme_thumbnail_sizes($sizes) {
    $sizes['300-thumb'] = __( '300px wide (and unlimited height)', 'sdg');
    $sizes['slideshow-thumb'] = __( '520px by 360px (Landscape Thumbnail)', 'sdg');
    $sizes['400-square'] = __( '400px by 400px (Image used for square images)', 'sdg');
    $sizes['large-slider'] = __( 'Large Banner Image (1200px by 550px)', 'sdg');
    $sizes['medium-slider'] = __( 'Large Slideshow Image (1200px by 720px)', 'sdg');
    $sizes['large-banner'] = __( 'Short Banner Image (1200px by 250px)', 'sdg');
    return $sizes;
}

/** Add/Register Filters & Actions */
add_action(
    'admin_menu', function () {
        remove_meta_box('postexcerpt', 'post', 'normal');
    }, 999
);
add_action('edit_form_after_title', 'sdg_fix_no_editor_on_posts_page', 0);
add_filter('body_class', 'sdg_frontend_body_classes');
add_filter('admin_body_class', 'sdg_backend_body_classes');
add_action('edit_form_after_title', 'post_excerpt_meta_box');
add_filter('gettext', 'sdg_modify_excerpt_label', 10, 2);
add_action('after_setup_theme', 'sdg_setup_theme_options');
add_filter('wp_scss_variables', 'wp_scss_set_variables');
add_action('after_setup_theme', 'sdg_custom_theme_thumbnails');
// add_filter('image_size_names_choose', 'sdg_show_custom_theme_thumbnail_sizes');
