<?php
/**
 * Enque Scripts & Styles
 *
 * @author sdg
 */

/**
 * Enqueues scripts and styles for front-end.
 *
 * @since sdg 1.0
 */
function sdg_scripts_styles() {
    global $wp_styles;

    // extra css + js
    if (!is_admin()) {
        wp_register_script('app', get_stylesheet_directory_uri() . '/assets/js/app.js', array('jquery'), null, true);
        wp_register_script('lib', get_stylesheet_directory_uri() . '/assets/js/lib.js', array('jquery'), null, true);
        wp_register_script('modernizr', get_stylesheet_directory_uri() . '/assets/js/modernizr.js', null, null, false);
        wp_enqueue_style('responsive-parent', get_template_directory_uri() . '/css/responsive.css', array('style-css'));
        wp_enqueue_style('responsive-child', get_stylesheet_directory_uri() . '/responsive.css', array('responsive-parent'));
        wp_enqueue_style('font-awesome', '//netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css');
        //wp_enqueue_style('font-awesome', '//use.fontawesome.com/251dd6e53c.css');

        // wp_enqueue_style('app-styles', get_stylesheet_directory_uri() . '/assets/css/app.css', array('app-styles'));

        // wp_enqueue_script('headroom-jquery-js', get_stylesheet_directory_uri() . '/js/jQuery.headroom.min.js', array('jquery'));
        // wp_enqueue_script('headroom-js', get_stylesheet_directory_uri() . '/js/headroom.min.js', array('headroom-jquery-js'));
        // wp_enqueue_script('dotdotdot', get_stylesheet_directory_uri() . '/js/jquery.dotdotdot.min.js', array('jquery'));

        // wp_enqueue_script('custom-js', get_stylesheet_directory_uri() . '/js/custom.js', array('jquery'));

		wp_enqueue_script( 'wpb_composer_front_js' );
		wp_enqueue_style( 'js_composer_front' );
		wp_enqueue_style( 'js_composer_custom_css' );
		// wp_enqueue_script( 'vendor-scripts', get_stylesheet_directory_uri() . '/assets/js/lib.js', array( 'jquery' ));
		// wp_enqueue_script( 'app-scripts', get_stylesheet_directory_uri() . '/assets/js/app.js', array( 'jquery') );
		wp_enqueue_script( 'dwqa-custom', get_stylesheet_directory_uri() . '/js/dw-custom.js', array('jquery'), $script_version, true );
		wp_enqueue_style( 'mast-style', get_stylesheet_directory_uri() . '/mast.css' );


	}

    if (! is_admin() && ! is_login_page()) {
        // wp_enqueue_style('styles');
        wp_enqueue_script('modernizr');
        // wp_enqueue_script('gmaps');
        wp_enqueue_script('app');
        wp_enqueue_script('lib');
    }

    /*
     * Loads our main stylesheet.
     */
    wp_enqueue_style('style-css', get_stylesheet_uri());

    /*
     * Loads the Internet Explorer specific stylesheet.
     */
    wp_enqueue_style('sdg-ie', get_template_directory_uri() . '/css/ie.css', array('style-css'), '20121010');
    $wp_styles->add_data('sdg-ie', 'conditional', 'lt IE 9');
}

function sdg_scripts_styles_admin() {
    // wp_enqueue_style('sdg-vc', get_stylesheet_directory_uri() . '/css/vc_sdg.css');
    // wp_enqueue_style('wp-admin-styles', get_stylesheet_directory_uri() . '/assets/css/admin.css');
}

/**
 * Add custom admin JS/CSS
 *
 * @return  void
 */
function add_custom_admin_css_js() {
    echo '<link type="text/css" rel="stylesheet" href="' . get_stylesheet_directory_uri() . '/assets/css/admin.css" />';
    echo '<script type="text/javascript" src="' . get_stylesheet_directory_uri() . '/assets/js/admin.js" async ></script>';
}

function replace_core_jquery_version() {
    wp_deregister_script( 'jquery' );
    // Change the URL if you want to load a local copy of jQuery from your own server.
    wp_register_script( 'jquery', "https://code.jquery.com/jquery-2.0.0.min.js", array(), '2.0.0' );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );

/** Register/add filters & actions */
add_action('wp_enqueue_scripts', 'sdg_scripts_styles');
add_action('admin_enqueue_scripts', 'sdg_scripts_styles_admin');
add_action('admin_head', 'add_custom_admin_css_js');
