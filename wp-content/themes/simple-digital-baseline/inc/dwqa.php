<?php

add_filter('dwqa_get_user_badge','mast_dwqa_get_user_badge', 10, 2);
function mast_dwqa_get_user_badge($badges, $user_id){
    $badges['staff'] = get_field( 'title', 'user_' . $user_id );
    return $badges;
}

function mast_editor_settings($settings) {
    $settings['quicktags'] = false;
    return $settings;
}

add_action( 'wp_ajax_dwqa-update-category', 'dwqa_update_category'  );
function dwqa_update_category() {
    if ( ! isset( $_POST['nonce'] ) ) {
        wp_send_json_error( array( 'message' => __( 'Are you cheating huh?', 'dwqa' ) ) );
    }
    check_ajax_referer( '_dwqa_update_category_nonce', 'nonce' );

    if ( ! isset( $_POST['post'] ) ) {
        wp_send_json_error( array( 'message' => __( 'Missing post ID', 'dwqa' ) ) );
    }

    global $current_user;
    $post_author = get_post_field( 'post_author', esc_html( $_POST['post'] ) );
    if ( dwqa_current_user_can( 'edit_question' ) || $current_user->ID == $post_author ) {
        if ( isset( $_POST['category'] ) ) {
            $update = wp_set_post_terms( intval( $_POST['post'] ), esc_html( $_POST['category'] ), 'dwqa-question_category');
            
            if ( $update ) {
                wp_send_json_success( array( 'ID' => $update ) );
            } else {
                wp_send_json_error(  array(
                    'message'   => __( 'Post does not exist','dwqa' )
                ) );
            }
        } else {
            wp_send_json_error( array(
                'message'   => __( 'Invalid post status','dwqa' )
            ) );
        }
    } else {
        wp_send_json_error( array(
            'message'   => __( 'You do not have permission to edit question', 'dwqa' )
        ) );
    }	
}

/**
 * Rename Plugin Menu Items
 * 
 * Renames certain plugin menu items to match client spec
 */
function mast_rename_plugin_menus() {
    global $menu;
	global $submenu;
	
// 	pprint_r($submenu);
    // Define your changes here
    $updates = array(
        "Questions" => array(
            'name' => 'Discussion Board',
        ),
        "edit.php?post_type=dwqa-question" => array(
            'name' => 'Topics',
            'add_new' => 'Add New Topic',
            'category' => 'Topic Category',
            'tag' => 'Topic Tags',
            'answers' => 'Replies',
        )
    );

    foreach ( $menu as $k => $props ) {

        // Check for new values
        $new_values = ( isset( $updates[ $props[0] ] ) ) ? $updates[ $props[0] ] : false;
        if ( ! $new_values ) continue;

        // Change menu name
        $menu[$k][0] = $new_values['name'];

    }

    foreach ( $submenu as $k => $props ) {

        // Check for new values
        $new_values = ( isset( $updates[ $k ] ) ) ? $updates[ $k ] : false;
        if ( ! $new_values ) continue;

        // Change menu name
        $submenu[$k][5][0] = $new_values['name'];
        $submenu[$k][10][0] = $new_values['add_new'];
        $submenu[$k][15][0] = $new_values['category'];
        $submenu[$k][16][0] = $new_values['tag'];
        $submenu[$k][17][0] = $new_values['answers'];

    }

}
add_action( 'admin_init', 'mast_rename_plugin_menus' );

// Remove search form from the before_questions 
remove_action( 'dwqa_before_questions_archive', 'dwqa_search_form', 11 );

// Remove setting question status to answered
remove_action( 'dwqa_add_answer','dwqa_auto_change_question_status', 11 );