<?php
/**
 * Intranet Tools
 *
 * @author sdg
 */

/**
 * Change Dashboard Title
 *
 * @return void
 */
function change_dashboard_title( $admin_title ) {
    global $current_screen;
    if( $current_screen->id != 'dashboard' ) {
        return $admin_title;
    }

    $change_title = 'Admin Panel';
    $admin_title = str_replace( __( 'Dashboard' ) , $change_title , $admin_title );
    return $admin_title;
}

// $request_order = apply_filters( 'http_api_transports', array( 'curl', 'streams' ), $args, $url );


/**
 * Custom login form
 *
 * @return void
 */
function my_custom_login_form($post_type = null, $network_name = '') {
    $post_type = ($post_type === null) ? get_post_type() : $post_type;
    echo "<h3>" . origet_title_case($post_type) . " Submission Form</h3>" .
    "<p>You must be a member of the " . $network_name . " Network to sign in and submit an " . $post_type . ". Please fill in your username and password below.</p>";
}

$sdg['logged-in'] = is_user_logged_in();
$sdg['login-page'] = is_login_page();
$bclass = $sdg['logged-in'] ? '' : 'login-page';

/**
 * Load WP Template
 *
 * @return void
 */
function sdg_load_wordpress_template($template) {
    global $wp_query;
    // if we have a 404 status
    if ($wp_query->is_404) {
        // set status of 404 to false
        $wp_query->is_404 = false;
        $wp_query->is_archive = true;
    }
    // change the header to 200 OK
    header("HTTP/1.1 200 OK");
    //load our template
    include($template);
    exit;
}


/**
 * Template redirect
 *
 * @return void
 */
function sdg_tpl_redirect() {
    global $wp, $sdg;
    // var_log('hi');
    if ($wp->request == 'some-custom-url-that-does-not-exist') {
        sdg_load_wordpress_template(TEMPLATEPATH . '/super-custom-template.php');
    }
}


/**
 * Redirect login page
 *
 * @return void
 */
function redirect_login_page() {
    $login_page = home_url('/login/');
    $page_viewed = basename($_SERVER['REQUEST_URI']);

    if ('GET' == $_SERVER['REQUEST_METHOD']) {
        if ("wp-login.php" == $page_viewed) {
            wp_redirect($login_page);
            exit;
        } else if ('wp-login.php?action=lostpassword' == $page_viewed) {
            wp_redirect($login_page . '?action=lostpassword');
            exit;
        }
    }
}


/**
 * Failed Login
 *
 * @return void
 */
function login_failed() {
    $login_page = home_url('/login/');
    wp_redirect($login_page . '?login=failed');
    exit;
}

/**
 * Verify Username Password
 *
 * @param  mixed $user     [description]
 * @param  str $username [description]
 * @param  str $password [description]
 * @return void
 */
function verify_username_password($user, $username, $password) {
    $login_page = home_url('/login/');
    if ("" == $username || "" == $password) {
        wp_redirect($login_page . "?login=empty");
        exit;
    }
}

/**
 * Lost password link
 *
 * @return void
 */
function add_lost_password_link() {
    return '<a href="' . site_url() . '/lostpassword/">Lost Password?</a>';
}

/**
 * Login page redirect after logout
 *
 * @return void
 */
function logout_page() {
    $login_page = home_url('/login/');
    wp_redirect($login_page . "?login=false");
    exit;
}

/**
 * Check if current user has permissions
 *
 * @param  int $id user id
 * @return bool
 */
function sdg_user_owns_object($obj_id, $uid = false) {
    $uid = !$uid ? get_current_user_id() : $uid;
    $allowed_roles = array('administrator');
    return ( $uid == $obj_id );
}

/**
 * Check if current user has permissions
 *
 * @param  int $id user id
 * @return bool
 */
function sdg_user_has_access($obj_id, $uid = false) {
    $uid = !$uid ? get_current_user_id() : $uid;
    $allowed_roles = array('administrator');
    $user = get_user_by('id', $uid);

    if( ( $uid == $obj_id ) || array_intersect($allowed_roles, $user->roles ) ) {
        return true;
    } else {
        return false;
    }
}

/**
 * Login screen redirects
 *
 * @return void
 */
function sdg_intranet_redirect() {
    $path_arr = explode('/', trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'));

    $dashboard = array_shift($path_arr) == '/';

    // If the User is not logged in and they land on submit, update, or your story then make them login.
    // if (!is_user_logged_in()) {
    //     // 1457 login page
    //     // 350 home page

    //     if ($dashboard) {
    //         parse_str($_SERVER['QUERY_STRING'], $query_arr);
    //         if (array_key_exists('sid', $query_arr)) {
    //             if (!my_house_sid_check()) {
    //                 wp_redirect(get_permalink('350'));
    //                 exit;
    //             }
    //         } else if ((isset($_GET["user"])) && (get_user_meta($_GET["user"], 'user_associations_share_my_house', true) === 'true')) {
    //         } else {
    //             wp_redirect(get_permalink('1457'));
    //             exit;
    //         }
    //     }

    // }

    if (is_user_logged_in()) {
        // if (is_page(get_id_by_slug('')) || is_page('1139')) {
        //     if (is_page('1139')) {
        //         wp_redirect("/my-house/#/logged-in/activated");
        //         exit;
        //     } else {
        //         wp_redirect("/my-house/");
        //         exit;
        //     }
        // }
        if (is_login_page()) {
            wp_redirect(get_home_url());
        }
        // if ($dashboard) {
        //     parse_str($_SERVER['QUERY_STRING'], $query_arr);
        //     if (array_key_exists('sid', $query_arr)) {
        //         if (!my_house_sid_check()) {
        //             wp_redirect(get_post_type_archive_link('rooms'));
        //             exit;
        //         }

        //     }
        // }
    }
}


function sdg_intranet_loginout_link( $items, $args ) {
    if ($args->theme_location =='primary') {
        if (is_user_logged_in()) {
            $items .= '<li class="right"><a href="'. wp_logout_url() .'">'. __("Log Out") .'</a></li>';
        } else {
            $items .= '<li class="right"><a href="'. wp_login_url(get_permalink()) .'">'. __("Log In") .'</a></li>';
        }
    }
    return $items;
}

add_filter( 'wp_nav_menu_items', 'sdg_intranet_loginout_link', 10, 2 );
add_action('init', 'redirect_login_page');
add_action('template_redirect', 'sdg_intranet_redirect');
add_action('wp_login_failed', 'login_failed');
add_filter('authenticate', 'verify_username_password', 1, 3);
add_action('login_form_middle', 'add_lost_password_link');
add_action('wp_logout', 'logout_page');
add_action( 'admin_title' , 'change_dashboard_title' );

// add_action('template_redirect', 'sdg_tpl_redirect');
// var_log($sdg['login-page']);
// if ( !$sdg['login-page'] && !$sdg['logged-in'] ):
//     echo '<script>document.location="/login/";</script>';
// endif;