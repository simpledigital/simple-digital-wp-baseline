<?php
/**
 * Custom Shortcodes for use with/without Visual Composer
 *
 * @author   sdg Branding
 * @package sdg Branding
 * @subpackage sdg-child
 * @since    1.0
 */

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class CustomShortcodes{

	// Register all shortcodes here
    public $shortcodes = array(
									'breadcrumb',
									'resource_list',
									'events_list',
									'page_list',
									'latest_blog',
									'program_list',
									'resource_search',
    								'section_content',
									'social_icon_block',
									'team_member_list',
    							);

	public function __construct() {
		$this->_init();
	}

	/**
	 * Handles dynamic creation of shortcode functions and inclusion of associated process and views
	 */
    function __call($func, $params ){
		//pprint_r($func);
		//pprint_r($params);

		$whereami = dirname( __FILE__ );

        if(in_array($func, $this->shortcodes)){

			$atts = isset( $params[0] ) ? $params[0] : null;
			$content = isset( $params[1] ) ? $params[1] : null;
        	$return = '';

			if(!function_exists('sdg_sc_'.$func)):
				include($whereami . '/process/' . $func . '.php');
				ob_start();
				include($whereami . '/views/' . $func . '.php');
				$return .= ob_get_contents();
				ob_end_clean();
			else:
				$run_function = "sdg_sc_" . $func;
				$return .= $run_function($atts, $content);
			endif;
			return $return;
        }
    }

	/**
	 * Initialization method
	 */
	function _init(){
		// adds shortcodes based on functions that start with sdg_sc_
		$found_codes = get_defined_functions()['user'];
		foreach (array_keys($found_codes) as $key):
			if(substr($found_codes[$key],0,9) == 'sdg_sc_') array_push($this->shortcodes, str_replace('sdg_sc_','',$found_codes[$key]));
		endforeach;

		// add shortcodes from this class
		foreach($this->shortcodes as $shortcode):
			add_shortcode('sdg_' . $shortcode, array(
				$this,
				$shortcode
			));
		endforeach;
	}
}

// init
function setup_custom_shortcodes(){
	$sdg_shortcodes = new CustomShortcodes();
	do_action('sdg_vc_custom_map');
}
add_action('init','setup_custom_shortcodes');

//add_shortcode('sdg_display_user_role','sdg_display_user_role');
function sdg_sc_display_user_role( $atts, $content=null ){
	extract(shortcode_atts(array(
		'returnstyle' => '',
	), $atts));
	$cur_user = wp_get_current_user();
	switch(strtolower($returnstyle)){
		case '':
		case 'csv':
			return ucwords(implode(",",$cur_user->roles));
			break;
		case 'array':
			return $cur_user->roles;
			break;
		case 'filtered':
			return ucwords(implode(",",array_filter($cur_user->roles, function($a){
					$a = strtolower($a);
					if($a != 'administrator' && $a != 'editor' && $a != 'subscriber' && $a != 'author' && $a !='contributor' && $a != 'subscriber'){
						return $a;
					}
				})));
			break;
	}
}


//shortcode to display user's full name (First Last)
add_shortcode('sdg_display_user_fullname', 'sdg_display_user_fullname');
function sdg_display_user_fullname(){
	return trim(get_the_author_meta('first_name') . " " . get_the_author_meta('last_name'));
}

add_shortcode('sdg_display_location_list', 'sdg_display_location_list');
function sdg_display_location_list($atts){
	extract(shortcode_atts(array(
		'css' => '',
		'links' => false,
		'select' => false,
		'ul' => true,
		'placeholder' => 'Choose a location...',
	), $atts));
	if($ul) $list = '<ul class="' . $css . '">';
	if($select) $list = '<select class="' . $css . '"';
	if($select && $links) $list.= ' onchange="window.location = this.value" ';
	if($select) $list.= '>';

	if($select && $placeholder) $list .= "<option value=''>" . $placeholder . "</option>";
	$locations_loop = new WP_Query( array(
	'post_type' => 'location',
	'post_status' => 'publish',
	'orderby' => 'menu_order',
	'order' => 'ASC',
	) );


	foreach($locations_loop->posts as $location_loop):
//		pprint_r($location_loop);
		if($ul) $list .= '<li>';
		if($select) $list .= '<option value="' . get_the_permalink($location_loop->ID) . '">';
		if($links && !$select) $list .= '<a href="' . get_the_permalink($location_loop->ID) . '">';
		$list .= get_field('location_shortname', $location_loop->ID);
		if($links && !$select) $list .= '</a>';
		if($ul) $list .= '</li>';
		if($select) $list .= '</option>';
	endforeach;
	if($ul) $list .= '</ul>';
	if($select) $list .= '</select>';
	return $list;
	//return trim(get_the_author_meta('first_name') . " " . get_the_author_meta('last_name'));
}