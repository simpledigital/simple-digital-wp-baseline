<?php
/**
 * Functions meant to extend core CMS/Framework functionality
 *
 * @package sdg Branding
 * @subpackage sdg-child
 * @since 1.1
 */

if (!function_exists('sdg_entry_meta')):
/**
 * Prints HTML with meta information for current post: categories, tags, permalink, author, and date.
 *
 * Create your own sdg_entry_meta() to override in a child theme.
 *
 * @since sdg 1.0
 */
    function sdg_entry_meta() {
        // Translators: used between list items, there is a space after the comma.
        $categories_list = get_the_category_list(__(', ', 'sdg'));

        // Translators: used between list items, there is a space after the comma.
        $tag_list = get_the_tag_list('', __(', ', 'sdg'));

        $date = sprintf('<a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a>',
            esc_url(get_permalink()),
            esc_attr(get_the_time()),
            esc_attr(get_the_date('c')),
            esc_html(get_the_date())
        );

        $author = sprintf('<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
            esc_url(get_author_posts_url(get_the_author_meta('ID'))),
            esc_attr(sprintf(__('View all posts by %s', 'sdg'), get_the_author())),
            get_the_author()
        );

        // Translators: 1 is category, 2 is tag, 3 is the date and 4 is the author's name.
        $utility_text = __('Posted by <span class="by-author">%4$s</span>.', 'sdg');

        printf(
            $utility_text,
            $categories_list,
            $tag_list,
            $date,
            $author
        );
    }
endif;

/**
 * Numeric blog pagination
 */
function sdg_numeric_posts_nav() {
    if (is_singular()) {
        return;
    }

    global $wp_query;

    /** Stop execution if there's only 1 page */
    if ($wp_query->max_num_pages <= 1) {
        return;
    }

    $paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
    $max = intval($wp_query->max_num_pages);

    /** Add current page to the array */
    if ($paged >= 1) {
        $links[] = $paged;
    }

    /** Add the pages around the current page to the array */
    if ($paged >= 3) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if (($paged + 2) <= $max) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<div class="navigation"><ul>' . "\n";

    /** Previous Post Link */
    if (get_previous_posts_link()) {
        printf('<li class="arrow">%s</li>' . "\n", get_previous_posts_link('<i class="fa fa-caret-left" aria-hidden="true"></i>'));
    }

    /** Link to first page, plus ellipses if necessary */
    if (!in_array(1, $links)) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link(1)), '1');

        if (!in_array(2, $links)) {
            echo '<li>…</li>';
        }

    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort($links);
    foreach ((array) $links as $link) {
        $class = $paged == $link ? ' class="active"' : '';
        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($link)), $link);
    }

    /** Link to last page, plus ellipses if necessary */
    if (!in_array($max, $links)) {
        if (!in_array($max - 1, $links)) {
            echo '<li>…</li>' . "\n";
        }

        $class = $paged == $max ? ' class="active"' : '';
        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($max)), $max);
    }

    /** Next Post Link */
    if (get_next_posts_link()) {
        printf('<li class="arrow">%s</li>' . "\n", get_next_posts_link('<i class="fa fa-caret-right" aria-hidden="true"></i>'));
    }

    echo '</ul></div>' . "\n";

}

/**
 * Get all post types
 */
function get_all_post_types($exclude = array()) {
    $types = array();
    $types['Select a Type'] = '';

    $args =

    $output = 'objects'; // names or objects, note names is the default
    $operator = 'and'; // 'and' or 'or'

    $builtin_post_types = get_post_types(array('public' => true, '_builtin' => true), $output, $operator);
    $custom_post_types = get_post_types(array('public' => true, '_builtin' => false), $output, $operator);

    $post_types = array_merge($builtin_post_types, $custom_post_types);
    foreach ($post_types as $k => $v) {
        if (!in_array($v->name, $exclude)) {
            $types[$v->label] = $k;
        } else {
            unset($types[$v]);
        }
    }
    return $types;

}

/**
 * Get all sections
 */
function get_all_sections() {
    $sections = array();
    $section_loop = new WP_Query(array(
        'post_type'      => 'section',
        'posts_per_page' => -1,
        'orderby'        => 'menu_order',
        'order'          => 'ASC',
    ));

    while ($section_loop->have_posts()): $section_loop->the_post();
        $sections[get_the_title()] = get_the_ID();
    endwhile;

    $sections = array('No Section Selected' => '') + $sections;

    return $sections;

}

/**
 * Get all sections
 */
function get_all_locations() {
    $locations = array();
    $location_loop = new WP_Query(array(
        'post_type'      => 'location',
        'posts_per_page' => -1,
        'orderby'        => 'menu_order',
        'order'          => 'ASC',
    ));

    while ($location_loop->have_posts()): $location_loop->the_post();
        $locations[get_the_title() . "<br/>"] = get_the_ID();
    endwhile;

    //$locations = array( 'No Location Selected' => '' ) + $locations;

    return $locations;

}

/**
 * Get all pages
 */
function get_all_pages() {
    $pages = array();
    $page_loop = new WP_Query(array(
        'post_type'      => 'page',
        'posts_per_page' => -1,
        'orderby'        => 'menu_order',
        'order'          => 'ASC',
    ));

    while ($page_loop->have_posts()): $page_loop->the_post();
        $pages[get_the_title()] = get_the_ID();
    endwhile;

    $pages = array('Use all Top Level Pages' => '') + $pages;

    return $pages;

}

/**
 * Get all services
 */
function get_all_services() {
    $pages = array();
    $page_loop = new WP_Query(array(
        'post_type'      => 'service',
        'posts_per_page' => -1,
        'orderby'        => 'menu_order',
        'order'          => 'ASC',
    ));

    while ($page_loop->have_posts()): $page_loop->the_post();
        $pages[get_the_title()] = get_the_ID();
    endwhile;

    $pages = array('No Default or Feed from URL' => '') + $pages;

    return $pages;

}

/**
 * Get all post categories
 */
function get_post_categories($tax = 'category') {
    // Get all specialties of the site
    $cats = array();
    $cats = get_terms($tax, array('hide_empty' => false));
    $cats = simplify_array($cats, 'name', 'term_id');

    $cats = array('Select a Category' => '') + $cats;

    return $cats;

}

/**
 * Check to see if the current page is the login/register page
 * Use this in conjunction with is_admin() to separate the front-end from the back-end of your theme
 *
 * @return bool
 */
if (!function_exists('is_login_page')) {
    function is_login_page($others = array(), $defaults = true) {
        $test_slugs = array('wp-login.php', 'wp-register.php', 'login', 'password', 'lostpassword', 'register');
        $test_slugs = ($defaults) ? array_merge($others, $test_slugs) : $others;
        return (in_array($GLOBALS['pagenow'], $test_slugs) || contains_any($test_slugs, get_current_page_path()));
    }
}

/**
 * Get the post thumbnail from post ID
 *
 * @param $post_id int
 * @param $image_size str
 * @return str
 */
function sdg_get_post_thumbnail_uri($post_id = -1, $image_size = 'full') {
    if (-1 === $post_id) {
        global $post;
        $post_id = $post->ID;
    }
    $thumb_id = get_post_meta($post_id, '_thumbnail_id', true);

    $thumb_url_array = wp_get_attachment_image_src($thumb_id, $image_size, true);
    $thumb_url = $thumb_url_array[0];
    if (contains('wp-includes/images/media/default', $thumb_url) === false) {
        return $thumb_url;
    }
}

/**
 * Get post or page (or whatever) by slug
 *
 * @param  str $page_slug
 * @param  str $type the post type
 * @return str
 */
function get_page_by_slug($page_slug, $object_var = null) {
    global $wpdb;
    $output = OBJECT;
    $return_val = null;
    $page = $wpdb->get_var($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type= %s", $page_slug, 'page'));
    if ($page) {
        $return_val = get_post($page, $output);
    } else {
        $page = $wpdb->get_var($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type= %s", $page_slug, 'post'));
        if ($page) {
            $return_val = get_post($page, $output);
        }
    }
    if (is_object($return_val)) {
        if (null !== $object_var) {
            $return_val = get_array_item((array) $return_val, $object_var);
        }
        return $return_val;
    }
    return null;
}

/**
 * Get post or page ID by slug
 *
 * @param  str $page_slug
 * @param  str $type the post type
 * @return str
 */
function get_id_by_slug($page_slug) {
    return get_page_by_slug($page_slug, 'ID');
}

/**
 * Get post or page (or whatever) permalink by slug
 *
 * @param  str $page_slug
 * @param  str $type the post type
 * @return str
 */
function get_permalink_by_slug($page_slug) {
    return get_permalink(get_id_by_slug($page_slug));
}

function sdg_get_page_id($context) {
    $pid = get_id_by_slug('bold-' . $context);
    $pid = (null === $pid) ? get_the_id() : $pid;
    return $pid;
}

function sdg_get_archive_type($context = null) {
    $context = (null === $context) ? get_the_slug() : $context;
    $context = str_replace('bold-', '', $context);
    if ('blog' === $context || 'articles' === $context) {
        return 'post';
    } else {
        if (post_type_exists($context)) {
            return $context;
        } else {
            return get_post_type(get_id_by_slug($context));
        }
    }
    return null;
}

/**
 * Get the content w/o echo
 *
 * @param int $pg_id
 * @param str $before optionally include content before the main content if it isn't empty
 * @param str $after optionally include content after the main content if it isn't empty
 *
 * @return true
 */
function sdg_get_the_content($pg_id = null, $before = '', $after = '') {
    if (null !== $pg_id) {
        $content = get_post_field('post_content', $pg_id);
    } else {
        $content = apply_filters('the_content', get_the_content(''));
    }
    if (!empty($content)) {
        return ($before . $content . $after);
    }
}

/**
 * Get last item of current page path
 *  - Example Usage:
 *     get_current_page_path_last('http://domain.com/first/child/'); // <= returns '/child/'
 *
 * @return str
 */
function sdg_get_paged() {
    $paged = (get_query_var('page')) ? get_query_var('page') : 1;
    if (intval(get_current_page_path_last()) > 0) {
        $paged = intval(get_current_page_path_last());
    }
    return $paged;
}

/**
 * Shortcut for `echo sdg_get_the_permalink( ... )` - accepts the same arguments
 *
 * @param  $postid
 * @return void
 */
function sdg_the_permalink($postid = null) {
    echo sdg_get_the_permalink($postid);
}

/**
 * Get the value of a specified post type's property
 *
 * @param str $prop - the post type property to get (e.g. 'hierarchical')
 * @param str $posttype - the post type slug
 * @param mixed $fallback - fallback if posttype property doesn't exist
 * @return mixed
 */
function sdg_get_post_type_prop($prop, $posttype = null, $fallback = null) {
    $posttype = (null === $posttype) ? get_post_type() : $posttype;
    if (post_type_exists($posttype)) {
        $post_obj = get_post_type_object($posttype);
        $post_prop = property_exists($post_obj, $prop) ? get_array_item($post_obj, $prop) : $fallback;
        return $post_prop;
    }
}

/**
 * Add user meta
 *
 * @param $var variable to test
 * @return void
 */
function sdg_add_meta($type = 'user', $meta_key = 'sdg_user', $meta_value = -1) {
    if ('user' === $type) {
        add_user_meta($user_id, 'sdg_' . $meta_key, $meta_value);
    } else {
        add_post_meta(get_the_ID(), 'sdg_' . $meta_key, $meta_value);
    }
}

/**
 * Get current page slug
 *
 * @param $post_id int
 * @return string
 */
function get_the_slug($id = false) {
    $id = !$id ? get_the_id() : $id;
    return basename(get_permalink($id));
}


/**
 * Get current page slug
 *
 * @param $post_id int
 * @return string
 */
function get_the_slug_or($id = false, $fallback = false) {
    $id = !$id ? get_the_id() : $id;
    $slugval = basename(get_permalink($id));
    if (empty($slugval) && $fallback !== false) { $slugval = $fallback; }
    return $slugval;
}

/**
 * Get the parent permalink based on the url path
 *
 * @param $id int
 * @return str
 */
function get_parent_permalink($id = false) {
    $id = !$id ? get_the_id() : $id;
    return str_replace(get_the_slug() . '/', '', get_permalink($id));
}

/**
 * Output current page slug
 *
 * @return void
 */
function the_slug($id = false) {
    echo get_the_slud($id);
}

/**
 * Get site root path
 *
 * @return string
 */
function get_site_root_path() {
    $full_path = getcwd();
    $ar = explode("wp-", $full_path);
    return $ar[0];
}

/**
 * Get related posts
 *
 * @param  int $postid
 * @param  int $related_count
 * @param  arr $args
 * @return object
 */
function sdg_get_related_posts_alt($postid = null, $related_count = 3, $args = array(), $taxonomy = 'category') {
    $postid = (null === $postid) ? get_the_ID() : $postid;
    $terms = get_the_terms($postid, $taxonomy);

    if (empty($terms)) {
        $terms = array();
    }

    $term_list = wp_list_pluck($terms, 'slug');

    $related_args = array(
        'post_type'      => 'post',
        'posts_per_page' => $related_count,
        'post_status'    => 'publish',
        'post__not_in'   => array($postid),
        'orderby'        => 'rand',
        'tax_query'      => array(
            array(
                'taxonomy' => $taxonomy,
                'field'    => 'slug',
                'terms'    => $term_list,
            ),
        ),
    );
    return new WP_Query($related_args);
}

/**
 * Print post categories
 *
 * @param  int $postid
 * @param  bool $obj
 * @param  arr $excludecat - category to exclude (defaults to 'uncategorized')
 * @return str || array
 */
function sdg_the_categories($postid = null, $excludecat = 'uncategorized') {
    echo sdg_get_the_categories($postid, $excludecat);
}

/**
 * Get featured posts
 *
 * @param  int $postcount
 * @param  str $posttype
 * @return obj
 */
function sdg_get_featured_posts($postcount = 1, $posttype = 'post', $name = 'featured') {
// $args = array(
//     'post_type' => $posttype,
//     'meta_key'      => $name,
//     // 'meta_value'    => 'yes',
//     'posts_per_page' => $postcount
// );
// return new WP_Query($args);
    $featured = new WP_Query(array(
        'post_type'      => $posttype,
        'meta_query'     => array(
            array(
                'key'     => $name,
                'value'   => '1',
                'compare' => '==',
            ),
        ),
        'posts_per_page' => $postcount,
    ));
    return $featured;
// var_log($featured);

    // $count = count(grab($featured->posts, 'query') ); if (  $count == $postcount ) {
//         return $featured;
//     } else {
//         $other = new WP_Query(array(
//             'post_type'      => $posttype,
//             'meta_query'     => array(
//                 array(
//                     'key'     => $name,
//                     'value'   => '1',
//                     'compare' => '!=',
//                 ),
//             ),
//             'posts_per_page' => ($postcount - $count),
//         ));
// // reset_postdata()
//         $post_ids = array_merge($featured, $other);
// // var_log($post_ids);
//         return new WP_Query(array(
//             'post_type' => $posttype,
//             'post__in'  => $post_ids
//         ));
//     }
}

/**
 * Custom version of built-in "get_the_title"
 *
 * @param  [int] $postid
 * @return str
 */
function sdg_get_the_title($postid = null) {
    $postid = (null === $postid) ? get_the_ID() : $postid;
    $custom_title = sdg_get_field('alternate_page_display_title', $postid, null);
    return (null !== $custom_title) ? $custom_title : get_the_title($postid);
}

/**
 * Shortcut for `echo sdg_get_the_title( ... )` - accepts the same arguments
 *
 * @param  [int] $postid
 * @return str
 */
function sdg_the_title($postid = null) {
    echo sdg_get_the_title($postid);
}

/**
 * Better version of `is_page_template()`
 *
 * @return bool
 */
function sdg_is_page_template($template, $postid = null) {
    $postid = (null === $postid) ? get_the_ID() : $postid;
    $tpl = str_replace_custom('.php', '', get_page_template_slug($postid));
    return (($tpl == $template) || (str_replace_custom('templates/', '', $template) == $tpl));
}

/**
 * Get the permalink, rewriting 'redirect_single' posts
 *
 * @param  int $postid
 * @return str
 */
function sdg_get_the_permalink($postid = null) {
    $postid = (null === $postid) ? get_the_ID() : $postid;
    $post_obj = get_post($postid);
    $post_obj = get_post_type_object($post_obj->post_type);
    if (property_exists($post_obj, 'redirect_single') && true === $post_obj->redirect_single) {
        return '#' . get_the_slug($postid);
    } elseif (property_exists($post_obj, 'redirect_single') && false !== $post_obj->redirect_single) {
        if (sdg_get_field($post_obj->redirect_single) !== '') {
            return sdg_get_field($post_obj->redirect_single);
        } else {
            return get_the_permalink($postid);
        }
    } elseif (sdg_get_field('external_link', $postid) !== '') {
        return sdg_get_field('external_link', $postid);
    } else {
        return get_the_permalink($postid);
    }
}

/**
 * Check for custom post type's posts_per_page_limit setting
 *
 * @param  $posttype - the post type slug
 * @return int
 */
function sdg_get_posts_per_page_limit($posttype = null) {
    $posttype = (null === $posttype) ? get_post_type() : $posttype;
    $post_obj = get_post_type_object($posttype);
    $posts_per_page_limit = property_exists($post_obj, 'posts_per_page_limit') ? $post_obj->posts_per_page_limit : get_option('posts_per_page');
    return $posts_per_page_limit;
}

/**
 * Get the value of a specified post type's label
 *
 * @param  $label - the label slug
 * @param  $posttype - the post type slug
 * @return str
 */
function sdg_get_post_type_label($label, $posttype = null) {
    $posttype = (null === $posttype) ? get_post_type() : $posttype;
    $post_obj = get_post_type_object($posttype);
    $post_prop_val = property_exists($post_obj, 'labels') ? get_array_item($post_obj, 'labels') : null;
    return get_array_item($post_prop_val, $label);
}

function sdg_get_tagline() {
    return get_option('blogdescription');
}

function sdg_tagline() {
    echo get_option('blogdescription');
}

function sdg_is_login_page() {
    return contains_any(array('wp-login.php', 'wp-register.php', 'login', 'password', 'register'), get_current_page_url());
}

/**
 * List Child Pages
 *
 * @return str
 */
function wpb_list_child_pages() {
    global $post;
    if (is_page() && $post->post_parent) {
        $childpages = wp_list_pages('sort_column=menu_order&title_li=&child_of=' . $post->post_parent . '&echo=0');
    } else {
        $childpages = wp_list_pages('sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0');
    }

    if ($childpages) {
        $string = '<ul>' . $childpages . '</ul>';
    }
    return $string;
}

function sdg_nav_children_and_parent() {
    global $post;
    $current_page_parent = ($post->post_parent ? $post->post_parent : $post->ID);
    if ($post->post_parent) {
        $children = wp_list_pages(array(
            'title_li' => '',
            'include'  => $post->post_parent,
            'echo'     => 0,
        ));
        $children .= wp_list_pages(array(
            'title_li' => '',
            'child_of' => $post->post_parent,
            'echo'     => 0,
            'exclude'  => get_the_ID(),
        ));
        $title = get_the_title($post->post_parent);
    } else {
        $children = wp_list_pages(array(
            'title_li' => '',
            'child_of' => $post->ID,
            'echo'     => 0,
            'exclude'  => get_the_ID(),
        ));
    }
    if ($children) {
        return $children;
    }
}

function sdg_get_a_posts_custom_terms_class($post_id = null, $tax = 'category') {
    $str_class = '';
    $terms = wp_get_object_terms($post_id, $tax);
    foreach ($terms as $term) {$str_class .= ' ' . $term->slug;}
    return $str_class;
}

function sdg_get_a_posts_custom_terms($post_id = null, $tax = 'category') {
    $terms = wp_get_object_terms($post_id, $tax);
    return $terms;
}
/**
 * [get_tax_hierarchy_template_part description]
 * @param  string $template
 * @param  string $subfolder
 * @return [type]
 */
function get_tax_hierarchy_template_part($template = '', $subfolder = '') {
    /**
     * Make sure we are actually on a taxonomy archive page, if not, return false
     *
     * Instead of returning false here, you can also set it to return a default template
     * Check the section commented out
     */
    if (!is_tax()) {
        return false;
    }

    // return get_template_part( 'content' ); // Return default template

    // Get the current term object
    $current_term_object = get_queried_object();

    // Check if we have a value for $subfolder, if so, sanitize it and add slashes
    if ($subfolder) {
        $subfolder = '/' . filter_var($subfolder, FILTER_SANITIZE_STRING) . '/';
    }

    /**
     * Check if we have value for $template, if so, sanitize, if not, use the taxonomy name
     * Also append the $subfolder to $template
     */
    if ($template) {
        $template = filter_var($template, FILTER_SANITIZE_STRING);
    } else {
        $template = $current_term_object->taxonomy;
    }
    $template = $subfolder . $template;
    // Check if current term is top level, if so, return template part for parent terms
    if (0 == $current_term_object->parent) {
        return get_template_part($template, 'parent');
    }

    /**
     * If we have reached this section, it means our term is not toplevel
     * We must now determine where in the hierarchy the term is
     */
    $hierarchy = get_ancestors($current_term_object->term_id, $current_term_object->taxonomy);

    // We must now get the size of the array
    $hierarchy_depth = count($hierarchy);

    /**
     * We will set child when the size of the array is one. For any size more
     * than one, we will set grandchild
     *
     * If you are going to have grand-grandchildren which should have its own
     * template, you would need to adjust this section
     */
    $part = (1 == $hierarchy_depth) ? 'child' : 'grandchild';
    // return $hierarchy;
    // Return the correct template part according to hierarchy
    return get_template_part($template, $part);
}

/**
 * Get first paragraph from a WordPress post. Use inside the Loop.
 *
 * @return string
 */
function sdg_get_first_paragraph($postid = null, $limit = 30) {
    $postid = (null === $postid) ? get_the_ID() : $postid;
    $str = wpautop(wp_trim_words(get_the_excerpt($postid), $limit));
    // $str = strip_tags(substr( $str, 0, strpos( $str, '.' ) + 1 ));
    if (contains('.', $str)) {
        $str = strip_tags(substr($str, 0, strpos($str, '.') + 1));
    } elseif (contains('?', $str)) {
        $str = strip_tags(substr($str, 0, strpos($str, '?') + 1));
    } elseif (contains('!', $str)) {
        $str = strip_tags(substr($str, 0, strpos($str, '!') + 1));
    } else {
        $str = strip_tags($str);
    }
    return $str;
}

/**
 * Get first paragraph from a WordPress post. Use inside the Loop.
 *
 * @return string
 */
function sdg_get_featured_paragraph($postid = null, $limit = 25) {
    $postid = (null === $postid) ? get_the_ID() : $postid;
    $str = wpautop(wp_trim_words(sdg_get_the_content($postid), $limit));
    $str = contains('.', $str) ? strip_tags(substr($str, 0, strpos($str, '.') + 1)) : $str;
    return strip_tags($str);
}

/**
 * Get related posts
 *
 * @param  int $postid
 * @param  int $related_count
 * @param  arr $args
 * @return object
 */
function sdg_get_related_posts($postid = null, $related_count = 3, $args = array(), $taxonomy = 'category') {
    $postid = (null === $postid) ? get_the_ID() : $postid;
    $terms = get_the_terms($postid, 'category');

    if (empty($terms)) {
        $terms = array();
    }

    $term_list = wp_list_pluck($terms, 'slug');

    $related_args = array(
        'post_type'      => 'post',
        'posts_per_page' => $related_count,
        'post_status'    => 'publish',
        'post__not_in'   => array($postid),
        'orderby'        => 'rand',
        'tax_query'      => array(
            array(
                'taxonomy' => $taxonomy,
                'field'    => 'slug',
                'terms'    => $term_list,
            ),
        ),
    );
    return new WP_Query($related_args);
}

/**
 * Get post categories
 *
 * @param  int $postid
 * @param  arr $excludecat - category to exclude (defaults to 'uncategorized')
 * @param  bool $obj
 * @return str || array
 */
function sdg_get_the_categories($postid = null, $excludecat = 'uncategorized', $obj = false, $separator = ', ', $limit = false) {
    $postid = (null === $postid) ? get_the_ID() : $postid;
    $cats = get_the_category($postid);
    $returncats = '';
    $i = 1;
    if (get_array_item(get_array_last_item($cats), 'slug') === $excludecat) {
        unset($cats[count($cats) - 1]);
    }

    foreach ($cats as $cat) {
        if ($limit == false || $i <= $limit ) {
            if (isset($cat->slug) && $cat->slug !== $excludecat) {
                $returncats .=
                '<li>' .
                '<a href="' . get_category_link($cat->cat_ID) . '">' . $cat->name;
                if (count($cats) !== $i) {$returncats .= '<span class="separator">' . $separator . ' </span>';}
                $returncats .=
                    '</a> ' .
                    '</li>'
                ;
            } elseif ($obj) {
                unset($cats[($i - 1)]);
            }
            $i += 1;
        }
    }
    return ($obj ? $cats : $returncats);
}

function get_current_post_type() {
    global $post, $typenow, $current_screen;

    // Check to see if a post object exists
    if ($post && $post->post_type) {
        return $post->post_type;
    }

    // Check if the current type is set
    elseif ($typenow) {
        return $typenow;
    }

    // Check to see if the current screen is set
    elseif ($current_screen && $current_screen->post_type) {
        return $current_screen->post_type;
    }

    // Finally make a last ditch effort to check the URL query for type
    elseif (isset($_REQUEST['post_type'])) {
        return sanitize_key($_REQUEST['post_type']);
    }

    return null;
}

function get_post_type_taxonomies($post_type, $return = 'array') {
    return get_object_taxonomies($post_type);
}

function get_post_ancestor_title($post_id = null) {
    $post_id = (null === $post_id) ? get_the_ID() : $post_id;
    $post = get_post($post_id)->post_parent;
    if ( 0 == $post->post_parent ) {
       return get_the_title();
    } else {
       get_post_ancestor_title($post->ID);
    }
}

/**
 * Estimate time required to read the article
 *
 * @return string
 */
function sdg_estimated_reading_time($id = false, $plural = false) {
    $post = get_post($id);
    $words = str_word_count( strip_tags( $post->post_content ) );
    $minutes = floor( $words / 120 );
    $seconds = floor( $words % 120 / ( 120 / 60 ) );
    if ( 1 <= $minutes ) {
        $estimated_time = $minutes . ' minute' . ($minutes == 1 ? '' : ($plural ? 's' : '') );
    } else {
        $estimated_time = $seconds . ' second' . ($seconds == 1 ? '' : ($plural ? 's' : '') );
    }
    return $estimated_time;
}

// add_shortcode('wpb_childpages', 'wpb_list_child_pages');