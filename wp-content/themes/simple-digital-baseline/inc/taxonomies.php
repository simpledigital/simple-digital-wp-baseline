<?php
/**
 * Definition of custom taxonomies
 *
 * @author   sdg Branding
 * @package sdg Branding
 * @subpackage sdg-child
 * @since    1.0
 */

/**
 * Loop through/register all custom taxonomies in specified directory
 *
 * @param str folder_name - name of folder in theme directory to loop through for custom taxonomies
 * @return void
 */
function sdg_register_custom_taxonomies($folder_name = 'post-types/taxonomies') {
    $post_types_folder = get_stylesheet_directory() . '/' . $folder_name;
    if (file_exists($post_types_folder)):
        if ($handle = opendir($post_types_folder)):
            while (false !== ($file = readdir($handle))):
                $custom_tax = $post_types_folder . '/' . $file;
                if (is_file($custom_tax) && strpos($custom_tax, '.php') !== false):
                    require_once $custom_tax;
                endif;
            endwhile;
            closedir($handle);
        endif;
    endif;
}

sdg_register_custom_taxonomies();
