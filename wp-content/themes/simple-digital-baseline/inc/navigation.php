<?php
/**
 * Navigation Functionality
 *
 * @author sdg
 */


/* REGISTER SITE NAV MENUS */
register_nav_menus(array(
    'primary-nav' => 'Primary Navigation',
    'utility-nav' => 'Utility Navigation',
    'footer-nav' => 'Footer Navigation'
));

if ( ! function_exists( 'primary_navigation' ) ) {
    function primary_navigation() {
        wp_nav_menu(array(
            'container' => false,
            'theme_location' => 'primary-nav',
            'container' => false,
            'items_wrap' => '%3$s',
            'sort_order' => 'DESC'
        ));
    }
}

if ( ! function_exists( 'utility_navigation' ) ) {
    function utility_navigation() {
        wp_nav_menu(array(
            'container' => false,
            'theme_location' => 'utility-nav',
             'container' => false,
            'items_wrap' => '%3$s'
        ));
    }
}

if ( ! function_exists( 'footer_navigation' ) ) {
    function footer_navigation() {
        wp_nav_menu(array(
            'container' => false,
            'theme_location' => 'footer-nav',
            'container' => false,
            'items_wrap' => '%3$s'
        ));
    }
}

/**
 * Drop-in numeric pagination for archives, search results, etc.
 *
 * @return str
 */
function sdg_get_pagination() {
  global $wp_query;

    $big = 999999999; // Codex-sanctioned hack for search/archive pagination
    $args = array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'current' => max( 1, get_query_var( 'paged' ) ),
        'format' => '?paged=%#%',
        'end_size' => 3,
        'mid_size' => 2,
        'next_text' => __( 'Next &raquo;', '%Text_Domain%' ),
        'prev_text' => __( '&laquo; Previous', '%Text_Domain%' ),
        'total' => $wp_query->max_num_pages,
        'before_page_number' => sprintf( '<span class="screen-reader-text">%s </span>', __( 'Page', '%Text_Domain%' ) ),
        'type' => 'list'
    );
    return paginate_links( $args );
}


/**
* Create previous/next post links
*
* @return str
*
* @uses get_previous_posts_link()
* @uses get_next_posts_link()
*/

function sdg_post_nav_links($prev = ' Prev ', $next = ' Next ') {
    $nav = '';
    if ( $prev = get_previous_posts_link( __( ' <i class="fa fa-chevron-left"></i> ' . $prev, '%Text_Domain%' ) ) ) {
        $nav .= sprintf( '<span class="prev"> %s </span> ', $prev );
    }
    if ( $next = get_next_posts_link( __( $next . ' <i class="fa fa-chevron-right"></i> ', '%Text_Domain%' ) ) ) {
        $nav .= sprintf( '  <span class="next"> %s </span>', $next );
    }
    return ( $nav ? sprintf( '<nav class="post-nav-links">%s</nav>', $nav ) : '' );
}

/**
 * Prints generic pagination link (next or previous)
 *
 * @param str $label
 * @param srr $dir  direction
 * @param WP_Query|null
 * @return void
 */
function sdg_pagination_link( $label = NULL, $dir = 'next', WP_Query $query = NULL ) {
    if ( is_null( $query ) ) $query = $GLOBALS['wp_query'];
    $max_page = ( int ) $query->max_num_pages;
    // Only one page for the query, do nothing
    if ( $max_page <= 1 ) return;
    $paged = ( int ) $query->get( 'paged' );
    if ( empty( $paged ) ) $paged = 1;
    $target_page = $dir === 'next' ?  $paged + 1 : $paged - 1;
    // If we are in 1st page and required previous or in last page and required next,
    // do nothing
    if ( $target_page < 1 || $target_page > $max_page ) return;
    if ( null === $label )  $label = __( 'Next Page &raquo;' );
    $label = preg_replace( '/&([^#])(?![a-z]{1,8};)/i', '&#038;$1', $label );
    printf( '<a href="%s">%s</a>', get_pagenum_link( $target_page ), $label );
}

function sdg_get_pagination_link($page_num =1) {
    return get_pagenum_link($page_num);
}

function sdg_pagination_btn($class = 'button load-more-posts', $page_num = 1, $label = 'Load More', $dir = 'next' ) { ?>
    <a href="<?php echo sdg_get_pagination_link($page_num); ?>" class="<?php echo $class; ?>"><?php echo $label; ?></a> <?php
}

/**
 * Get Breadcrumb Nav
 *
 * @param  bool $include_home
 * @param  int  $page_id
 * @return str
 */
function sdg_get_breadcrumbs($include_home = false) {
    $crumbs = ($include_home) ? '<a href="'.home_url().'" rel="nofollow">Home</a>' : '';
    if (is_category() || is_single()) {
        $crumbs .= "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        $crumbs .= get_the_category(' &bull; ');
        if (is_single()) {
            $crumbs .= " &nbsp;&nbsp;&#187;&nbsp;&nbsp; ";
            $crumbs .= get_the_title();
        }
    } elseif (is_page()) {
        $crumbs .= "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        $crumbs .= the_title();
    } elseif (is_search()) {
        $crumbs .= "&nbsp;&nbsp;&#187;&nbsp;&nbsp;Search Results for... ";
        $crumbs .= '"<em>' . the_search_query() . '</em>"';
    }
    return $crumbs;
}

/**
 * Shortcut for `echo sdg_get_breadcrumbs( ... )`, accepts the same arguments
 *
 * @return void
 */
function sdg_breadcrumbs($include_home = false) {
    printf(sdg_get_breadcrumbs($include_home));
}


function sdg_nav_breadcrumbs($show_home = true, $delimiter = '') {
    $name = $show_home ? 'Home' : $show_home;
    $currentBefore = '<li class="current">';
    $currentAfter = '</span>';
    if (!is_home() && !is_front_page() || is_paged()) {
        echo '<ul id="crumbs">';
        global $post;
        if ($show_home) {
            $home = get_bloginfo('url');
            echo '<li><a href="' . $home . '">' . $name . '</a></span> ' . $delimiter . ' ';
        }
        if (is_category()) {
            // var_log('CAT');
            global $wp_query;
            $cat_obj = $wp_query->get_queried_object();
            $thisCat = $cat_obj->term_id;
            $thisCat = get_category($thisCat);
            $parentCat = get_category($thisCat->parent);
            if (0 != $thisCat->parent) {
                echo (get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
            }

            echo $currentBefore . 'Archive by category &#39;';
            single_cat_title();
            echo '&#39;' . $currentAfter;
        } elseif (is_day()) {
            echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></span> ' . $delimiter . ' ';
            echo '<li><a href="' . get_month_link(get_the_time('Y'), get_the_time('m')) . '">' . get_the_time('F') . '</a></span> ' . $delimiter . ' ';
            echo $currentBefore . get_the_time('d') . $currentAfter;
        } elseif (is_month()) {
            echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></span> ' . $delimiter . ' ';
            echo $currentBefore . get_the_time('F') . $currentAfter;
        } elseif (is_year()) {
            echo $currentBefore . get_the_time('Y') . $currentAfter;
        } elseif (is_single()) {
            $back_href = str_replace_custom('/event/', '/events/', get_parent_permalink());
            $back_href = ( get_post_type() === 'post' ) ? $back_href . 'news/' : $back_href;
            echo '<li><a href="' . $back_href . '">' . sdg_get_post_type_label('name') . '</a>';
            echo $currentBefore;
            the_title();
            echo $currentAfter;
        } elseif (is_page() && !$post->post_parent) {
            if (!$show_home && $post->post_parent) {
                echo $currentBefore;
                the_title();
                echo $currentAfter;
            }
        } elseif (is_page() && $post->post_parent) {
            $parent_id = $post->post_parent;
            $breadcrumbs = array();
            while ($parent_id) {
                $page = get_page($parent_id);
                if ($parent_id > 0) {

                $breadcrumbs[] = '<li><a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a></span>';
                }
                $parent_id = $page->post_parent;
            }
            $breadcrumbs = array_reverse($breadcrumbs);
            foreach ($breadcrumbs as $crumb) {
                echo $crumb . ' ' . $delimiter . ' ';
            }

            echo $currentBefore;
            the_title();
            echo $currentAfter;
        } elseif (is_search()) {
            echo $currentBefore . 'Search results for &#39;' . get_search_query() . '&#39;' . $currentAfter;
        } elseif (is_tag()) {
            echo $currentBefore . 'Posts tagged &#39;';
            single_tag_title();
            echo '&#39;' . $currentAfter;
        } elseif (is_author()) {
            global $author;
            $userdata = get_userdata($author);
            echo $currentBefore . 'Articles posted by ' . $userdata->display_name . $currentAfter;
        } elseif (is_404()) {
            echo $currentBefore . 'Error 404' . $currentAfter;
        }
        if (get_query_var('paged')) {
            if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author()) {
                echo ' (';
            }

            echo __('Page') . ' ' . get_query_var('paged');
            if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author()) {
                echo ')';
            }

        }
        echo '</ul>';
    }
}
