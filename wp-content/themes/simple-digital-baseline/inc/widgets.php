<?php

/**
 * Registers our main widget area and the front page widget areas.
 *
 * @since sdg 1.0
 */
function sdg_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Blog Sidebar', 'sdg' ),
		'id' => 'blog_sidebar',
		'description' => __( 'Appears on the right side of the blog section', 'sdg' ),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Facebook Content Area', 'sdg' ),
		'id' => 'facebook_area',
		'description' => __( 'Appears in the social content area when used on pages', 'sdg' ),
		'before_widget' => '<div class="facebook_widget">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Twitter Content Area', 'sdg' ),
		'id' => 'twitter_area',
		'description' => __( 'Appears in the social content area when used on pages', 'sdg' ),
		'before_widget' => '<div class="twitter_widget">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Footer Hover Area', 'sdg' ),
		'id' => 'footer_hover_area',
		'description' => __( 'Appears in the lower right corner of every page.', 'sdg' ),
		'before_widget' => '<div class="footer_hover_widget">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );

}
add_action( 'widgets_init', 'sdg_widgets_init' );
