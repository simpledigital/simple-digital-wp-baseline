<?php
/**
 * Register Custom Post-Types & Taxonomies
 *
 * @author sdg
 */

/**
 * Loop through/register all custom post types in specified directory
 *
 * @param str folder_name - name of folder in theme directory to loop through for custom post types
 * @return void
 */
function sdg_register_custom_post_types($folder_name = 'post-types') {
    $post_types_folder = get_stylesheet_directory() . '/' . $folder_name;
    if (file_exists($post_types_folder)):
        if ($handle = opendir($post_types_folder)):
            while (false !== ($file = readdir($handle))):
                $custom_post_type = $post_types_folder . '/' . $file;
                if (is_file($custom_post_type) && strpos($custom_post_type, '.php') !== false):
                    require_once $custom_post_type;
                endif;
            endwhile;
            closedir($handle);
        endif;
    endif;
}

/**
 * Disable single view for specified
 *
 * @param [array] $include_post_types - post types to disable single view for
 * @return void
 */
function sdg_disable_single_cpt_views() {
    $include_post_types = array('location');
    $queried_post_type = get_query_var('post_type');
    $cpts_without_single_views = $include_post_types;
    if (is_single() && in_array($queried_post_type, $cpts_without_single_views)) {
        wp_redirect(home_url(sdg_get_post_type_prop($queried_post_type, 'redirect_permalink')), 301);
        // wp_redirect( home_url( '/' . $queried_post_type . '/' ), 301 );
        exit;
    }
}

/**
 * Display a custom taxonomy dropdown in admin
 *
 * @author Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
function tsm_filter_post_type_by_taxonomy() {
    global $typenow;
    $post_type = 'resource'; // change to your post type
    $taxonomy = 'resource_category'; // change to your taxonomy
    if ($typenow == $post_type) {
        $selected = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
        $info_taxonomy = get_taxonomy($taxonomy);
        wp_dropdown_categories(array(
            'show_option_all' => __("Show All {$info_taxonomy->label}"),
            'taxonomy'        => $taxonomy,
            'name'            => $taxonomy,
            'orderby'         => 'menu_order, name',
            'selected'        => $selected,
            'show_count'      => true,
            'hide_empty'      => false,
        ));
    } ;
}

/**
 * Filter posts by taxonomy in admin
 * @author  Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
function tsm_convert_id_to_term_in_query($query) {
    global $pagenow;
    $post_type = 'resource'; // change to your post type
    $taxonomy = 'resource_category'; // change to your taxonomy
    $q_vars = &$query->query_vars;
    if ('edit.php' == $pagenow && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && 0 != $q_vars[$taxonomy]) {
        $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
        $q_vars[$taxonomy] = $term->slug;
    }
}

function acf_load_location($field) {
    $field['choices'] = array();
    $args = array('post_type' => 'location', 'orderby' => 'menu_order', 'order' => 'ASC');
    $thisQuery = new WP_Query($args);
    while ($thisQuery->have_posts()): $thisQuery->the_post();
        $field['choices'][get_the_id()] = get_the_title();
    endwhile;
    return $field;
}

/* Remove Metabox for taxonomy */
function sdg_baseline_remove_meta_box() {
    remove_meta_box('sectorsdiv', 'work_sector', 'normal');
}

/* Add new taxonomy meta box */
function sdg_baseline_add_meta_box() {
    add_meta_box('sectors_radio_box', 'Sector', 'sdg_baseline_sectors_category_metabox', 'work_sector', 'side', 'core');
}

/* Get taxonomy and terms */
function sdg_baseline_sectors_category_metabox($post) {
    $taxonomy = 'work_sector';
    /* Set up the taxonomy object and get terms */
    $tax = get_taxonomy($taxonomy);
    $terms = get_terms($taxonomy, array('hide_empty' => 0));

    /* Name of the form */
    $name = 'tax_input[' . $taxonomy . ']';

    /* Get current and popular terms */
    $popular = get_terms($taxonomy, array('orderby' => 'count', 'order' => 'DESC', 'number' => 10, 'hierarchical' => false));
    $postterms = get_the_terms($post->ID, $taxonomy);
    $current = ($postterms ? array_pop($postterms) : false);
    $current = ($current ? $current->term_id : 0);
    ?>

    <div id="taxonomy-<?php echo $taxonomy; ?>" class="categorydiv">

        <!-- Display tabs-->
        <ul id="<?php echo $taxonomy; ?>-tabs" class="category-tabs">
            <li class="tabs"><a href="#<?php echo $taxonomy; ?>-all" tabindex="3"><?php echo $tax->labels->all_items; ?></a></li>
            <li class="hide-if-no-js"><a href="#<?php echo $taxonomy; ?>-pop" tabindex="3"><?php _e('Most Used');?></a></li>
        </ul>

        <!-- Display taxonomy terms -->
        <div id="<?php echo $taxonomy; ?>-all" class="tabs-panel">
            <ul id="<?php echo $taxonomy; ?>checklist" class="list:<?php echo $taxonomy ?>t form-no-clear">
                <?php foreach ($terms as $term) {
        $id = $taxonomy . '-' . $term->term_id;
        echo "<li id='$id'><label class='selectit'>";
        echo "<input type='radio' id='in-$id' name='{$name}'" . checked($current, $term->term_id, false) . "value='$term->term_id' />$term->name<br />";
        echo "</label></li>";
    }?>
            </ul>
        </div>

        <!-- Display popular taxonomy terms -->
        <div id="<?php echo $taxonomy; ?>-pop" class="tabs-panel" style="display: none;">
            <ul id="<?php echo $taxonomy; ?>checklist-pop" class="categorychecklist form-no-clear" >
                <?php foreach ($popular as $term) {
        $id = 'popular-' . $taxonomy . '-' . $term->term_id;
        echo "<li id='$id'><label class='selectit'>";
        echo "<input type='radio' id='in-$id'" . checked($current, $term->term_id, false) . "value='$term->term_id' />$term->name<br />";
        echo "</label></li>";
    }?>
            </ul>
        </div>

    </div>
    <?php
}

function myprefix_radiotax_javascript() {
    // wp_register_script('radiotax', get_stylesheet_directory_uri() . '/js/admin/radio-tax.js', array('jquery'), null, true); // We specify true here to tell WordPress this script needs to be loaded in the footer
    // wp_enqueue_script('radiotax');
}


/**
 * Perform a custom redirect for old slugs based on multiple post types.  WordPress
 * by default will only redirect old slugs for a single post type but since
 * the post_type param is an array we had to implement this custom logic.
 * This function will use WP_Query instead of MySQL code like the original does.
 *
 * @link https://github.com/WordPress/WordPress/blob/139387b7e554e7a22975470cca5d2ed6ce4c8eb4/wp-includes/query.php#L4950-L4954
 *
 * @global WP_Query   $wp_query   Global WP_Query instance.
 */
function custom_wp_old_slug_redirect() {
    global $wp_query;
    if ( is_404() && '' !== $wp_query->query_vars['name'] ) {
        $query_args = array(
            'post_type' => get_query_var( 'post_type' ),
            'meta_key' => '_wp_old_slug',
            'meta_value' => $wp_query->query_vars['name'],
            'posts_per_page' => 1,
        );
        $query = new WP_Query( $query_args );
        $posts = $query->get_posts();
        $id = $posts[0]->ID;
        if ( ! $id ) {
            return;
        }
        $link = get_permalink( $id );
        if ( isset( $GLOBALS['wp_query']->query_vars['paged'] ) && $GLOBALS['wp_query']->query_vars['paged'] > 1 ) {
            $link = user_trailingslashit( trailingslashit( $link ) . 'page/' . $GLOBALS['wp_query']->query_vars['paged'] );
        } elseif ( is_embed() ) {
            $link = user_trailingslashit( trailingslashit( $link ) . 'embed' );
        }
        /**
         * Filters the old slug redirect URL.
         *
         * @since 4.4.0
         *
         * @param string $link The redirect URL.
         */
        $link = apply_filters( 'old_slug_redirect_url', $link );
        if ( ! $link ) {
            return;
        }
        wp_redirect( $link, 301 ); // Permanent redirect
        exit;
    }
}

sdg_register_custom_post_types();

/**
 * wp_old_slug_redirect does not work when the post_type query param
 * is an array, therefore we need to use our own.
 */
remove_action( 'template_redirect',  'wp_old_slug_redirect' );
add_action( 'template_redirect',  'custom_wp_old_slug_redirect' );

/* Register all custom post types/taxonomies */

/* Register/add all filters/actions */
add_action('admin_menu', 'sdg_baseline_remove_meta_box');
add_action('add_meta_boxes', 'sdg_baseline_add_meta_box');
add_action('admin_enqueue_scripts', 'myprefix_radiotax_javascript');
add_action('template_redirect', 'sdg_disable_single_cpt_views');
add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy');
add_filter('parse_query', 'tsm_convert_id_to_term_in_query');
add_filter('acf/load_field/name=career_location', 'acf_load_location');
