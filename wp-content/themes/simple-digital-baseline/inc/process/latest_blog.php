<?php

extract( shortcode_atts( array(
		'title' => '',
		'display_type' => 'paged',
		'category' => '',
		'limit' => 1,
		'font_color' => '',
	), $atts ) );

$font_color = $font_color == '' ? '' : $font_color;

$args = array(
	'post_type' => 'post',
	'posts_per_page' => $limit,
	'display_type' => 'paged',
	'orderby' => 'date',
	'order' => 'DESC'
);

if( $category != '' ):
	$args['tax_query'][] = array(
								array(
									'taxonomy'	=> 'category',
									'field'		=> 'term_id',
									'terms'		=> $category,
								),
							);
endif;

$blog_loop = new WP_Query( $args );