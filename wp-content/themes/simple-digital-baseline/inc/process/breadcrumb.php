<?php

global $post;

$_crumbs = explode("/",$_SERVER["REQUEST_URI"]);
$crumbs = array();

if(count($_crumbs) > 0):
    $count = 0;
    foreach($_crumbs as $_crumb):
        $count++;
        if((count($_crumbs > 5) && ($count == 1 || $count == 2 || $count > 4)) || count($_crumbs) <= 5):
            if(!empty($_crumb)):
                $the_slug = strtolower($_crumb);
                $args=array(
                    'name' => $the_slug,
                    'post_type' => 'page',
                    'post_status' => 'publish',
                    'posts_per_page' => 1
                );
                $the_post = get_posts( $args );
                if( $the_post ):
                    $crumb = array();
                    $crumb['selected_style'] = ( $post->ID == $the_post[0]->ID ? ' class="selected"' : '' );
                    $crumb['permalink'] = get_permalink( $the_post[0]->ID );
                    $crumb['post_title'] = $the_post[0]->post_title;
                    $crumbs[] = $crumb;
                endif;
            endif; // empty?
        endif;
    endforeach;
endif;