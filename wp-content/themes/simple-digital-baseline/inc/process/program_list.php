<?php
wp_reset_query();

extract( shortcode_atts( array(
		'program_parent_category_id' => '',
		'limit' => -1,
	), $atts ) );

$args = array(
	'post_type' => 'program',
	'posts_per_page' => $limit,
	'display_type' => 'paged',
	'orderby' => 'post_title',
	'order' => 'ASC'
);

$cats = get_terms( array(
	'taxonomy' => 'program_category',
	'parent' => $program_parent_category_id,
	'hide_empty' => false,
	'order' => 'DESC',
) );

foreach( $cats as $cat ):
	$cat->resources = array();
	$args = array(
		'post_type' => 'program',
		'posts_per_page' => $limit,
		'display_type' => 'paged',
		'orderby' => 'post_title',
		'order' => 'ASC',
		'tax_query' => array(
							array(
								'taxonomy'	=> 'program_category',
								'field'		=> 'term_id',
								'terms'		=> $cat->term_id,
								'include_children' => false,
							),
						),
	);		
	$cat->programs = new WP_Query( $args );
endforeach;


$program_loop = new WP_Query( $args );