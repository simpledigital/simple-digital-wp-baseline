<?php
extract( shortcode_atts( array(
	'image' => '',
	'title' => '',
	'description' => '',
	'color' => '',
	), $atts ) );

if( !empty( $image ) ) $image = wp_get_attachment_url($image);