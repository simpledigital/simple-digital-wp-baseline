<?php
wp_reset_query();

extract( shortcode_atts( array(
        'resource_category_id' => '',
        'resource_parent_category_id' => '',
        'display_style' => 'simple',
        'limit' => -1,
    ), $atts ) );

// data structure depends on display style
switch( $display_style ):
    case 'accordion':
        wp_enqueue_script( 'jquery-ui-accordion' );
        if( $resource_parent_category_id == '' ):
            echo "Parent category required for accordion view.";
            break;
        else:
            $cats = get_terms( array(
                'taxonomy' => 'resource_category',
                'parent' => $resource_parent_category_id,
                'hide_empty' => false,
            ) );

            foreach( $cats as $cat ):
                $cat->resources = array();
                $args = array(
                    'post_type' => 'resource',
                    'posts_per_page' => $limit,
                    'display_type' => 'paged',
                    'orderby' => 'menu_order',
                    'order' => 'ASC',
                    'tax_query' => array(
                                        array(
                                            'taxonomy'  => 'resource_category',
                                            'field'     => 'term_id',
                                            'terms'     => $cat->term_id,
                                            'include_children' => false,
                                        ),
                                    ),
                );
                $cat->resources = new WP_Query( $args );
            endforeach;
        endif;

    break;
    default: // all other display styles

        $args = array(
            'post_type' => 'resource',
            'posts_per_page' => $limit,
            'display_type' => 'paged',
            'orderby' => 'menu_order',
            'order' => 'ASC'
        );

        if( $resource_category_id != '' ):
            $include_children = ( $resource_parent_category_id == '' ? false : true ); // include children if we're using a parent category_id
            $args['tax_query'] = array(
                                        array(
                                            'taxonomy'  => 'resource_category',
                                            'field'     => 'term_id',
                                            'terms'     => $resource_category_id,
                                            'include_children' => $include_children,
                                        ),
                                    );
        endif;
        $resource_loop = new WP_Query( $args );

        if( $resource_parent_category_id != '' ):
            $cats = get_terms( array(
                'taxonomy' => 'resource_category',
                'parent' => $resource_parent_category_id,
                'hide_empty' => false,
            ) );
        endif;
    break;
endswitch;