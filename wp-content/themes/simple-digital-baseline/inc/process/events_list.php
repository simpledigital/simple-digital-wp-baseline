<?php

global $ai1ec_registry;	

extract( shortcode_atts( array(
	'title' => '',
	'specific_ids' => '',
	'limit' => 1,
	'post_tag' => 0,
	'event_category' => 0,
	'event_venue' => 0,
	'event_organizer' => 0,
	'display_style' => 'Columns',
	'show_image' => '',
	'site_blog_id' => '',
	'text_color' => '',
	'more_link_text' => 'Learn More'
	), $atts ) );

if( $site_blog_id != '' ):
	extract( shortcode_atts( array(
		'post_tag_' . $site_blog_id => '',
		'event_category_' . $site_blog_id => '',
		'event_venue_' . $site_blog_id => '',
		'event_organizer_' . $site_blog_id => '',
	), $atts ) );
	$post_tag = ${'post_tag_' . $site_blog_id};
	$event_category = ${'event_category_' . $site_blog_id};
	$event_venue = ${'event_venue_' . $site_blog_id};
	$event_organizer = ${'event_organizer_' . $site_blog_id};
else:
	extract( shortcode_atts( array(
		'post_tag' => '',
		'event_category' => '',
		'event_venue' => '',
		'event_organizer' => '',
	), $atts ) );
endif;

$show_image = ( !$show_image ? false : $show_image );

$events = array();

$event_plugin_slug = false;

// Time.ly All-in-one Events Calendar
if( is_plugin_active('all-in-one-event-calendar/all-in-one-event-calendar.php') ):
	$event_plugin_slug = 'ai1ec_event';
endif;

// Modern Tribe Event Calendar takes precedence because.... why not.
if( is_plugin_active('the-events-calendar/the-events-calendar.php') ):
	$event_plugin_slug = 'tribe_events';
endif;


if( $event_plugin_slug ):

	if( $display_style == 'Columns' ):
		$col_span = round(12/$limit);
	else:
		$col_span = 12;
	endif;

	$return = '';

	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

	switch( $event_plugin_slug ):
		case 'ai1ec_event':
	
			// NEW METHOD (gets recurring instances correctly)
			$filters = array();

			if( $event_category != 0 ):
				$filters = array( 'cat_ids' => array( $event_category ) );
			endif;

			$start = $ai1ec_registry->get( 'date.time', time() );
			$end = $ai1ec_registry->get( 'date.time', strtotime('+3 month') );

			$events = $ai1ec_registry->get( 'model.search' )->get_events_between( $start, $end, $filters );
			$count = 0;

		break;
		case 'tribe_events':
			$all_events = array();

			if( is_multisite() && $site_blog_id != '' ):
				switch_to_blog( $site_blog_id );
			endif;

			$args = array( 'venue' => $event_venue, 'organizer' => $event_organizer, 'posts_per_page' => $limit, 'start_date'   => date('Y-m-d') );
			
			if( !empty( $post_tag ) ):
				$args['tax_query'][] = array(
											'taxonomy' => 'post_tag',
											'field'    => 'term_id',
											'terms'    => $post_tag,
									);
			endif;
			if( !empty( $event_category ) ):
				$args['tax_query'][] = array(
											'taxonomy' => 'tribe_events_cat',
											'field'    => 'term_id',
											'terms'    => $event_category,
									);
			endif;
			
			if( !empty( $specific_ids ) ):
				$args['post__in'] = explode(',',$specific_ids);
			endif;

			wp_reset_postdata();
			
			$events = tribe_get_events( $args );
			
			if( is_multisite() && $site_blog_id != '' ):
				restore_current_blog();
			endif;
					
		break;
		default:
		break;
	endswitch;

endif;