<?php
wp_reset_query();

extract( shortcode_atts( array(
		'limit' => -1,
	), $atts ) );

$args = array(
	'post_type' => 'team_member',
	'posts_per_page' => $limit,
	'display_type' => 'paged',
	'orderby' => 'post_title',
	'order' => 'ASC'
);

$team_member_loop = new WP_Query( $args );