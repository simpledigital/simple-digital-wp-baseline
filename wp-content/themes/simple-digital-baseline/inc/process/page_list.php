<?php

extract( shortcode_atts( array(
        'display_style' => 'icon',
        'parent_page' => 0,
		'limit' => -1,
	), $atts ) );

$args = array(
	'post_type' => 'page',
    'posts_per_page' => $limit,
    'post_parent' => $parent_page,
	'display_type' => 'paged',
	'orderby' => 'menu_order',
	'order' => 'ASC'
);

$page_loop = new WP_Query( $args );