<?php

extract( shortcode_atts( array(
	'section_id' => '',
	'css_classes' => '',
	), $atts ) );

if( $section_id != 0):
	$section_loop = new WP_Query( array(
		'p' => $section_id,
		'post_type' => 'section',
		'posts_per_page' => '1'
	) );
else:
	$return = 'Invalid Post';
endif;