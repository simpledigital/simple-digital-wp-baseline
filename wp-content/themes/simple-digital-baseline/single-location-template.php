<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 *
 * @package sdg
 * @subpackage sdg
 * @since sdg 1.0
 */

get_header(); ?>
<div class="content">
	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'content', 'page' ); ?>
	<?php endwhile; // end of the loop. ?>
</div>
<?php get_footer(); ?>