t<?php
/**
 * Sidebar
 *
 * @author sdg
 */

?>

<h3><a href="<?php echo get_permalink($section_parent->ID); ?>"><?php echo $section_parent->post_title; ?></a></h3>
<?php if($section_pages): ?>
<ul class="sidebar_menu">
<?php foreach($section_pages as $section_page):
    $sub_section_pages = get_pages(array('parent' =>$section_page->ID, 'hierarchical' => 0, 'sort_order' => 'ASC', 'sort_column' => 'menu_order')); ?>
    <li><a href="<?php echo get_permalink($section_page->ID); ?>"<?php if($post->ID == $section_page->ID): ?> class="selected"<?php endif; ?>><?php echo $section_page->post_title; ?></a>
    <?php
    if($sub_section_pages && ($post->ID == $section_page->ID || $post->post_parent == $section_page->ID)    ): ?>
        <ul>
        <?php
        foreach($sub_section_pages as $sub_section_page): ?>
            <li><a href="<?php echo get_permalink($sub_section_page->ID); ?>"<?php if($post->ID == $sub_section_page->ID): ?> class="selected"<?php endif; ?>>> <?php echo $sub_section_page->post_title; ?></a></li>
        <?php endforeach; ?>
        </ul>
    <?php endif; ?>
    </li>
<?php endforeach; ?>
</ul>
<?php endif; ?>
