<?php
/**
 * Single Blog Post Template
 *
 * @package sdg
 */

get_header();

global $sdg_opts, $sdg, $AppGlobals;

$sdg['bread_class'] = 'block transparent-bg flush';

while ( have_posts() ) : the_post(); ?>
    <section class="row wrapper-wrap condense more alternate-bg alt equal-heights">
        <article class="col medium-8 equal">

            <?php get_template_part('parts/breadcrumbs'); ?>

            <?php get_template_part('parts/post-info-header'); ?>

            <div class="row">
                <div class="col medium-12 large-11">
                    <figure class="pad-bottom more image-shadow">
                        <?php echo get_the_post_thumbnail(get_the_id(), 'large-slider'); ?>
                    </figure>
                </div>
            </div>
            <?php the_content(); ?>
        </article>
        <aside class="col medium-4 sidebar equal">

            <?php get_template_part('parts/snippets/back-to-parent-btn'); ?>

            <?php get_template_part('parts/related-items'); ?>

        </aside>
    </section> <?php

endwhile; ?>

<script></script> <?php

get_footer();
