<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package sdg
 * @subpackage sdg
 * @since sdg 1.0
 */

// check for resource links
switch( get_post_type() ):
	case 'resource':
		$permalink = ( !empty( get_field('field_5a314baf18c0d') ) ? get_field('field_5a314baf18c0d')['url'] : ( !empty(get_field('field_5a395e8b761ac')) ? get_field('field_5a395e8b761ac') : '' ) );
		$link_target = ' target="_blank"'; // new window for resource links
	break;
	case 'team_member':
		$permalink = '/team-info';
		$link_target = '';
	break;
	default:
		// normal permalink for other post types
		$permalink = get_the_permalink();
		$link_target = '';
	break;
endswitch; ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
		<div class="featured-post">
			<?php _e( 'Featured post' ); ?>
		</div>
		<?php endif; ?>

		<div class="post-date">Updated: <?php echo get_the_date(); ?></div>
		<h3><a href="<?php echo $permalink; ?>"<?php echo $link_target; ?>><?php the_title(); ?><?php echo ( is_search() ? ' [' . str_replace('_',' ',get_post_type()) . ']' : '' ); ?></a></h3>

		<?php if ( !is_single() ) : // Only display Excerpts for Search and Archive ?>
		<div class="entry-summary">
			<?php the_excerpt(); ?>
			<a href="<?php echo $permalink; ?>"<?php echo $link_target; ?> class="red-outline">Read More</a>
		</div><!-- .entry-summary -->
		<?php endif; ?>

		<?php if( has_post_thumbnail() ): ?>
		<div class="featured-image">
			<a href="<?php echo $permalink; ?>"<?php echo $link_target; ?>><?php the_post_thumbnail( 'large' ); ?></a>
		</div>
		<?php endif; ?>

		<?php if ( is_single() ) : ?>
		<div class="entry-content">
			<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>' ) ); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:' ), 'after' => '</div>' ) ); ?>
		</div><!-- .entry-content -->
		<?php endif; ?>

		<div class="article-lines"></div>

	</article><!-- #post -->
