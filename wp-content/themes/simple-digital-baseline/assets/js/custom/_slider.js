/**
 * Sliders
 */

sdg.slidersGo = function() {

    $('#featured-news-posts-slider').removeClass('transparent');

    $('.wp1s-slider-wrapper').on('click', function(event) {
        if ($(event.target).hasClass('wp1s-main-wrapper')) {
            document.location = $('.wp1s-thumbnail-wrapper').find('.active').find('.wps-thumb-overlay').attr('data-href');
        }
    });
};
