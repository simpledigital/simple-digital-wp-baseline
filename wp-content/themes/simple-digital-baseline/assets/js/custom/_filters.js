/**
 * Filter related JS functions and methods
 *
 * @package Simple Digital Baseline JavaScript
 * @author Simple Digital Marketing
 * @repo https://bitbucket.org:sdgmarketing/sdg-baseline-theme.git
 */

$ = jQuery;

sdg.Filters = sdg.Filters || {};

sdg.Filters.initiateTextSearchFilter = function() {
    // $('#clear-all-filters-trigger').
    $('#local-nav-search-form-text-input').on('keyup keypress', function(event) {
        // $('.actual-trigger').removeClass('anchor-link');
        var searchText = $(this).val().toLowerCase();
        $('.actual-trigger').removeClass('active');
        $('#clear-all-filters-trigger').click();
        if (searchText !== '') {
            $('.filter-item-target').filter(":not(:Contains(" + searchText +
                "))").removeClass('active');
            $('#filter-filter').removeClass('filtering');
            $('.filter-item-target').filter(":Contains(" + searchText + ")").addClass('active');
        } else {
            $('#filter-filter').removeClass('filtering');
            $('.filter-item-target').addClass('active');
        }

        // window.setTimeout(function() {
            // $('#clear-all-filters-trigger').trigger('click');
        // },100);
    });
    sdg.genericTaxFilters();

    // if (document.location.hash.length > 1) {
    //     $('a' + document.location.hash).click();
    // }
};

sdg.genericTaxFilters = function () {

    var trans;

    // if (is_safari) {
    //     trans = 0;
    // } else {
    trans = '0.5s';
    // }

    function getHashFilter() {
        // get filter=filterName
        var matches = location.hash.match(/filter=([^&]+)/i);
        var hashFilter = matches && matches[1];
        return hashFilter && decodeURIComponent(hashFilter);
    }

    // init Isotope
    var $grid = $('.filter-grid');

    // bind filter button click
    var $filterButtonGroup = $('.filterFilters');
    $('.actual-trigger').on('click', function(event) {
        // if ( $(this).is('.actual-trigger') ) {
            event.preventDefault();
            $('.actual-trigger').removeClass('active');
            if ( $(this).attr('id') !== 'clear-all-filters-trigger') {
                $('#filter-filter').addClass('filtering');
                $('#local-nav-search-form-text-input').val('');
                $('.filter-grid').find('.filter-item-target').removeClass('active');

                var filterAttr = $(this).attr('name');
                // console.log(filterAttr);
                // console.log(filterAttr);
                $(this).addClass('active');
                location.hash = filterAttr.replace('clear-all-filters-trigger', '');
                $('.filter-grid').find('.filter-item-target').removeClass('active');
                var activeItems = $('.filter-grid').find('.filter-item-target.' + filterAttr)
                    .addClass('active');
            } else {
                $('#filter-filter').removeClass('filtering');
                $(this).addClass('active').siblings().removeClass('active');
                $('.filter-grid').find('.filter-item-target').addClass('active');
            }
    });

    var isIsotopeInit = false;

    function onHashchange() {
        var hashFilter = getHashFilter();
        if (!hashFilter && isIsotopeInit) {
            return;
        }

        if (hashFilter == "" || hashFilter == "*" || hashFilter == null) {
            hashFilter = "*";
        } else {
            hashFilter = "." + hashFilter;
        }
        isIsotopeInit = true;

        if (hashFilter) {

            var hashfilterclass = "";
            if (hashFilter != "*") {
                hashfilterclass = hashFilter.substring(1, hashFilter.length);
            } else {
                hashfilterclass = "*";
            }
            $filterButtonGroup.find('.active').removeClass('active');
            $filterButtonGroup.find('[data-filter="' + hashfilterclass + '"]').addClass(
                'active');
        }
        // ga('set', 'page', '/filter/' + hashFilter.substr(1));
        // ga('send', 'pageview');
    }

    // $(window).on('hashchange', onHashchange);

    // trigger event handler to init Isotope
    // onHashchange();
};
sdg.filterEmployees = function() {

    function closeSort() {} // on portfolio page
    var navOpen = false;
    var subnav = null;
    var screenH;
    var screenW;
    var halfH;
    var scrollPos = 0;

    var is_chrome = navigator.userAgent.indexOf('Chrome') > -1;
    var is_explorer = navigator.userAgent.indexOf('MSIE') > -1;
    var is_firefox = navigator.userAgent.indexOf('Firefox') > -1;
    var is_safari = navigator.userAgent.indexOf("Safari") > -1;
    var is_opera = navigator.userAgent.toLowerCase().indexOf("op") > -1;
    if ((is_chrome) && (is_safari)) {
        is_safari = false;
    }
    if ((is_chrome) && (is_opera)) {
        is_chrome = false;
    }

    function openSearch() {
        closeSort();
        $('body').css('position', 'fixed');
        $('#search').show();
        $('#search-field').focus();
        $('#search-field').val('');

    }

    function closeSearch() {
        $('body').css('position', 'relative');
        $('#search').hide();
        $('#search .bg').removeAttr('style');
    }

    function openNav() {
        $('.main-nav').addClass('menu-open');
        $('#mobile-nav-icon').addClass('nav-on');
        navOpen = true;
    }

    function closeNav() {
        $('.main-nav').removeClass('menu-open');
        $('#mobile-nav-icon').removeClass('nav-on');
        navOpen = false;
    }

    // $(document).ready(function() {

    //     $(window).resize();

    //     // search
    //     $('#nav-search').click(function() {
    //         openSearch();
    //     });
    //     $('#search-close').click(function() {
    //         closeSearch();
    //     });

    //     var content = $('#search-field').val();
    //     $('#search-field').keyup(function() {
    //         if ($('#search-field').val() != content) {
    //             $("#search-icon").delay(250).hide(0);
    //             $("#circle-loader-search").delay(250).show(0);
    //             content = $('#search-field').val();
    //             $.ajax({
    //                 type: "POST",
    //                 url: "/contextsearch",
    //                 data: "q=" + content,
    //                 success: function(msg) {
    //                     $("#circle-loader-search").hide(0);
    //                     $("#search-icon").show(0);
    //                     $('#search-results .inside').html(msg);
    //                 }
    //             });

    //         }
    //     });

    //     $('body').keydown(function(e) {
    //         if (e.keyCode == 27) { // esc key
    //             closeSearch();
    //             closeSort();
    //         }
    //         /*
    //         if(e.keyCode == 32){    // space key
    //         openSearch();
    //         }
    //         */
    //     });

    //     // nav
    //     $('#header-nav .nav-dropdown').each(function() {
    //         $(this).find('.dropdown-main').click(function() {

    //             if (screenW > 715) {
    //                 if (subnav) {
    //                     subnav.hide();
    //                 }
    //                 subnav = $(this).parent().find('.sub-nav');

    //                 if ($(this).parent().hasClass('nav-open')) {
    //                     $(this).parent().removeClass('nav-open');

    //                     $('#nav-cover').hide();
    //                 } else {
    //                     $('.nav-open').removeClass('nav-open');
    //                     $(this).parent().addClass('nav-open');

    //                     subnav.slideDown(400, 'easeOutExpo');
    //                     $('#nav-cover').show();
    //                 }
    //             } else {

    //                 if ($(this).parent().hasClass('nav-open')) {
    //                     $(this).parent().removeClass('nav-open');
    //                     $(this).parent().find('.sub-nav').stop().slideUp(
    //                         400, 'easeOutExpo');
    //                 } else {
    //                     $('.nav-open .sub-nav').stop().slideUp(400,
    //                         'easeOutExpo');
    //                     $('.nav-open').removeClass('nav-open');
    //                     $(this).parent().find('.sub-nav').stop().slideDown(
    //                         400, 'easeOutExpo');
    //                     $(this).parent().addClass('nav-open');

    //                 }
    //             }
    //         });
    //     });

    //     $('#nav-cover').click(function() {
    //         $('.nav-open').removeClass('nav-open');
    //         subnav.hide();
    //         $(this).hide();
    //     });

    //     $('#mobile-nav-icon').click(function() {
    //         if (navOpen) {
    //             closeNav();
    //         } else {
    //             openNav();
    //         }
    //     });

    //     // init
    //     $('.photo-intro').addClass('active');

    // });

    // $(window).load(function() {
    //     // init
    //     $('.photo-intro .bg').addClass('active');
    //     setTimeout(function() {
    //         $('.photo-intro .text').addClass('active');
    //     }, 1000);
    // });

    // $(window).resize(function() {
    //     screenW = $(window).width();
    //     screenH = $(window).height();
    //     halfH = screenH / 2 - 100;

    //     if (screenW > 715) {
    //         $('#search-results, #sort-open').height(screenH - 116);
    //     } else {
    //         $('#search-results, #sort-open').height(screenH - 88);
    //     }

    //     if (screenW > 715) {
    //         closeNav();

    //         /*$('#mobile-nav-search').removeAttr('style');*/
    //         $('.nav-open').find('.sub-nav').hide();
    //         $('.nav-open').removeClass('nav-open');
    //     }

    // });

    // $(window).scroll(function() {
    //     scrollPos = $(window).scrollTop();
    //     $('#scrollPos').html(scrollPos);

    //     // fade in
    //     $('.fade-in').each(function() {
    //         if ($(this).offset().top < scrollPos + screenH - halfH) {
    //             $(this).addClass('active');
    //         }

    //         if ($(this).offset().top > scrollPos + 1200) {
    //             $(this).removeClass('active');
    //         }

    //     });

    // });

    /*window.onhashchange = function () {
    hash = window.location.hash.substring(1);
    alert(hash);
    $(".filter-grid li").show();
    $(".filter-grid li:not(." + hash + ")").hide();
    grid.masonry("layout");
    }*/

    // $(window).scroll(function() {

    //     // filter in
    //     console.log(scrollPos);
    //     if (scrollPos > 1500) {
    //         $('#filter-filter li').each(function() {
    //             $(this).addClass('active');
    //         });
    //     } else {
    //         if ($('#filter-filter').offset().top < scrollPos + screenH - halfH) {
    //             $('#filter-filter li').each(function(index) {
    //                 var clip = $(this);
    //                 setTimeout(function() {
    //                     clip.addClass('active');
    //                 }, (index * 100));

    //             });
    //         }
    //     }

    // });

};
