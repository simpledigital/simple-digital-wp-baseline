/**
 * Admin Front-End Scripts
 *
 * @author sdg
 */

sdg.adminFrontEnd = function() {
    if ( $('#wpadminbar').length ) {
        $('html').addClass('admin-bar-present');
        var editLink = $('#wp-admin-bar-edit a');
        if ( $('#edit-current-post-link').length ) {
            $('#edit-current-post-link').remove();
        }
        $('body footer').append('<a href="' + editLink.attr('href') + '" id="edit-current-post-link" class="edit-post-link floating-button left active" title="' + editLink.text() + '"> <span class="">' + editLink.text() + '</span> </a>');
    }
};