/**
 * Collapsible Content Javascript
 *
 * @author sdg
 */

sdg.collapsibleContent = function() {
    var containers = $('.collapsible-content'),
        triggers = containers.find('.trigger'),
        activeItem;

    $('.collapsible-content .trigger').on('click', function(event) {
        event.preventDefault();
        var me = $(this);
        // var content = $(this).parents('.collapsible-content').find('.content');
        // if (content.length == 1 ) {
        //     me.parents('.collapsible-content').toggleClass('active').find('.content').slideToggle();
        // } else {
            // console.log('parents');
            // console.log(me.parentsUntil('.parent'));
        me.toggleClass('open').next('.content').slideToggle().toggleClass('open');
        // sdg.equalHeights();
    });
};