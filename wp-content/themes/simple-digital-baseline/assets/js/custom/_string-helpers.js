/**
 * Helper String Methods/Extensions to the JS String Object
 *
 * @param String search
 * @return Bool
 */
String.prototype.contains = function(search) {
    return (this.indexOf(search) !== -1);
};

String.prototype.replaceAll = function(find, replasdg) {
    return this.replace(new RegExp(find, 'g'), replasdg);
};

/**
 * Case conversion helper methods
 *
 * @type String
 * @return String
 */
String.prototype.toTitleCase = function() {
    return this.replace(/\w\S*/g, function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
};

String.prototype.camelCaseToDashed = function() {
    return this.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
};

String.prototype.toProperCase = function() {
    return this.replace(/\w\S*/g, function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
};

String.prototype.toCamelCase = function() {
    return this.replace(/(?:^\w|\-[A-Z]|\b\w)/g, function(letter, index) {
        return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
    }).replace(/\s+/g, '').replace('-', '');
};

String.prototype.toDashedCase = function() {
    return this.toCamelCase().camelCaseToDashed();
};
