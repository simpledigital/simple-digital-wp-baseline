/**
 * General JavaScripture
 */

 // jQuery.expr[':'].focus = function( elem ) {
 //   return elem === document.activeElement && ( elem.type || elem.href );
 // };


sdg.siteSearch = function() {
    sdg.globals.searchClicked = false;
    var searchTriggers = $('.submit-trigger'),
        parentForm, label, inpCheck;

    searchTriggers.on('click', function(event) {
        if ($('.search-form input').is(':focus')) {
            event.preventDefault();
        }
        var me = $(this);
        parentForm = me.parents('form');
        label = me.parents('label');
        if ( $('#' + label.attr('for')).length ) {
            if ( sdg.globals.searchClicked ) {
                inpCheck = $('#' + label.attr('for'));
                if ( inpCheck.val() !== '' ) {
                    parentForm.submit();
                }
            }
        }
        sdg.globals.searchClicked = true;
    });
};