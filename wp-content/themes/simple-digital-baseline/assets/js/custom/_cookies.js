/**
 * Cookie-Related Functions
 *
 * @repo https://bitbucket.org:sdgmarketing/sdg-baseline-theme.git
 * @author sdg
 */

sdg.getDocumentCookies = function () {
    var theCookies = document.cookie.split(';'),
        cookieObj = {},
        tmp, tmpName, tmpVal;
    for (var i = 1; i <= theCookies.length; i++) {
        tmp = theCookies[i - 1].split('=');
        if (tmp[0] !== undefined && tmp[1] !== undefined) {
            tmpName = decodeURIComponent(tmp[0].trim());
            tmpVal = decodeURIComponent(tmp[1].trim());
            if (tmpName.indexOf('[') > -1 && tmpName.indexOf(']') > -1) {
                cookieObj[tmpName.split('[')[0]] = cookieObj[tmpName.split('[')[0]] || {};
                cookieObj[tmpName.split('[')[0]][tmpName.split('[')[1].replace(']', '')] = tmpVal;
            } else {
                cookieObj[tmpName] = tmpVal;
            }
        }
    }
    return cookieObj;
};