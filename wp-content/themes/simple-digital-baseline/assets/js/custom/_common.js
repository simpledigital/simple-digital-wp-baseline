/**
 * Common Javascript Functions
 */

/**
 * Push history state to browser and update page title
 *
 * @param variable name as string
 * @return Bool
 * @uses sdg.pageTitle()
 * @usage pushHistoryState('/new-url/');
 */
sdg.pushHistoryState = function(permalink) {
    if (history.pushState) {
        window.history.pushState(null, permalink.replace('/', ''), permalink);
        $.ajax({
            url: permalink,
            complete: function(data) {
                sdg.pageTitle(permalink);
            }
        });
    }
};

sdg.pushHashState = function(permalink) {
    if (history.pushState) {
        window.history.pushState(null, permalink.replace('/', ''), permalink);
    }
};

/**
 * Check if mobile
 *
 * @param variable name as string
 * @return Bool
 * @uses sdg.pageTitle()
 * @usage pushHistoryState('/new-url/');
 */
sdg.isMobile = function() {
    return ($(window).width() <= sdg.globals.mediumBreak);
};

sdg.getParameterByName = function(name, url) {
    if (typeof url === 'undefined') {
        url = window.location.href;
    }
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) {
        return null;
    }
    if (!results[2]) {
        return '';
    }
    return decodeURIComponent(results[2].replace(/\+/g, " "));
};

sdg.scrollToElement = function(selector, time, overrideselector) {
    if (typeof selector === 'undefined') {
        selector = 'body';
    }
    if (typeof overrideselector === 'undefined') {
        overrideselector = 'html, body';
    }
    if (typeof time === 'undefined') {
        time = 1000;
    }
    if ($(selector).length > 0) {
        $(overrideselector).animate({
            scrollTop: $(selector).offset().top + -120
        }, time);
    }
};

sdg.addIEVersionClasses = function() {
    var ua = window.navigator.userAgent,
        msie = ua.indexOf("MSIE ");
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        $('html').addClass('ie' + (parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)))));
    }
    return false;
};

sdg.equalHeights = function() {
    if (typeof(sdg.equalHeightsInner) === 'function') {
        var equalHeightsInner = new sdg.equalHeightsInner();
    }
    var equalHeights = $('.equal-heights');
    equalHeights.removeClass('equalized');
    windowWidth = $(window).width();
    if (equalHeights.length > 0) {
        equalHeights.each(function() {
            if (windowWidth > sdg.globals.mediumBreak || $(this).hasClass(
                    'equal-heights-all')) {
                var sections = $(this).find('.equal'),
                    largestHeight = 0;
                sections.height('auto');
                sections.each(function() {
                    var h = $(this).innerHeight();
                    if (largestHeight < h) {
                        largestHeight = h;
                    }
                });
                sections.css('height', largestHeight + 'px');
                $(this).addClass('equalized');
            } else {
                $(this).find('.equal').css('height', 'auto');
            }
        });
    }
};

sdg.equalizeHeightsWidths = function() {
    var equalWidths = $('.equal-height-width');
    if (equalWidths.length > 0) {
        equalWidths.each(function() {
            var w = $(this).width();
            $(this).css({
                'height': w + 'px'
            });
        });
    }
};

sdg.equalHeightsInner = function(innerClass) {
    if (typeof innerClass === 'undefined') {
        innerClass = 'equal-inner';
    }
    var equalHeights = $('.equal-heights');
    equalHeights.removeClass('equalized-inner');
    windowWidth = $(window).width();
    if (equalHeights.length > 0) {
        equalHeights.each(function() {
            if (windowWidth > sdg.globals.mediumBreak || $(this).hasClass(
                    'equal-heights-all')) {
                var sections = $(this).find('.' + innerClass),
                    largestHeight = 0;
                sections.height('auto');
                sections.each(function() {
                    var h = $(this).innerHeight();
                    if (largestHeight < h) {
                        largestHeight = h;
                    }
                });
                sections.css('height', largestHeight + 'px');
                $(this).addClass('equalized-inner');
            } else {
                $(this).find('.' + innerClass).css('height', 'auto');
            }
        });
    }
};

sdg.equalHeightsRowless = function() {
    var equalHeights = $('.equal-heights-rowless');
    windowWidth = $(window).width();

    $('.adjusted').removeClass('adjusted');
    equalHeights.find('.equal,.equal-child').css('height', 'auto');
    if (windowWidth > sdg.globals.mediumBreak) {
        if (equalHeights.length > 0) {
            equalHeights.each(function() {
                // $(this).removeClass('end');
                // equalHeights.find('.equal,.equal-child').css('height', null);
                if ($(this).hasClass('four-up')) {
                    $(this).find('.equal:nth-child(4n+1)').addClass('end');
                } else if ($(this).hasClass('three-up')) {
                    $(this).find('.equal:nth-child(3n+1)').addClass('end');
                } else if ($(this).hasClass('two-up')) {
                    $(this).find('.equal:nth-child(2n+1)').addClass('end');
                }
                var sections = $('.equal.end'),
                    largestHeight = 0;
                $.each(sections, function(index) {
                    sections[index] = $(this).nextUntil('.end').andSelf();
                });

                $.each(sections, function(index) {
                    var h = $(this).height();
                    var f = $(this).find('.equal-child').innerHeight();
                    if (largestHeight < h) {
                        largestHeight = h;
                        largestFooter = f;
                    }
                    if ($(this).find('.equal-child').length) {
                        $(this).css('height', largestHeight + 'px');

                    } else {
                        $(this).css('height', largestHeight + 'px');
                    }
                });
                $(this).addClass('adjusted');
            });
        }
    } else {
        equalHeights.find('.equal').css('height', 'auto');
    }
};

/**
 * Return the first relative parent of an element
 *
 * @param child element
 * @return Object
 * @uses jQuery
 */
sdg.closestRelativeParent = function(elem) {
    var rel = false,
        par = elem.parent();
    while (rel === false) {
        rel = (par.css('position') === 'relative' || par.is('body'));
        if (rel !== true) {
            par = par.parent();
        }
    }
    return par;
};

/**
 * Get page title of provided URL
 *
 * @param url
 * @return String
 */
sdg.pageTitle = function(url, update) {
    var result = "";
    update = update || false;
    $.ajax({
        url: url,
        async: false,
        success: function(data) {
            result = data;
        }
    });
    if (update) {
        document.title = result.match(/<(title)>[\s\S]*?<\/\1>/gi)[0].replace('<title>', '').replace(
            '</title>', '');
    }
    return result.match(/<(title)>[\s\S]*?<\/\1>/gi)[0].replace('<title>', '').replace(
        '</title>', '');
};

/**
 * Check if variable is defined
 *
 * @param variable name as string
 * @return Bool
 * @usage isSet('varNameAsString')
 */
sdg.isSet = function(varAsStr) {
    prevItem = window;
    varSplit = varAsStr.split('.');
    x = 0;
    retVal = false;

    if (typeof(window[varSplit[0]]) !== 'undefined') {
        obj = window[varSplit[0]];
    }
    $.each(varSplit, function(i, item) {
        if (typeof(item) === 'undefined') {
            return false;
        } else {
            if (typeof prevItem[item] === 'undefined') {
                return false;
            }
            prevItem = prevItem[item];
            if (i === (varSplit.length - 1)) {
                if (typeof(item) !== 'undefined') {
                    retVal = (typeof(prevItem) !== 'undefined');
                } else {
                    retVal = false;
                }
                return retVal;
            }
        }
    });
    return retVal;
};

/**
 * Count the properties of a JavaScript Object
 *
 * @param  Object
 * @return int
 */
sdg.countProperties = function(obj) {
    var count = 0;
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            ++count;
        }
    }
    return count;
};

/**
 * Find elements w/ duplicate attributes in DOM
 *
 * @param  string
 */
sdg.findDuplicateAttributes = function(attr) {
    $('[' + attr + ']').each(function() {
        var _this = $(this),
            allAttributes = $('[' + attr + '="' + _this.attr(attr) + '"]');
        if (allAttributes.length > 1 && allAttributes[0] == this) {
            console.log(_this);
        }
    });
};

sdg.addJavascript = function(jsname) {
    if (jsname.length > 0) {
        scriptElem = document.createElement('script');
        scriptElem.setAttribute('type', 'text/javascript');
        scriptElem.setAttribute('src', jsname);
        $('body').append(scriptElem);
    }
};

sdg.addCSS = function(cssname) {
    if (cssname.length > 0) {
        $('body').append('<link rel="stylesheet" href="' + cssname + '" />');
    }
};

sdg.equalWidths = function(useSmallest) {
    var compareSizes = function(a, b) {
        return (sdg.isSet('useSmallest') ? useSmallest : false) ? (a > b) : (a < b);
    };
    var equalWidths = $('.equal-widths');
    equalWidths.removeClass('equalized');
    windowWidth = $(window).width();
    if (windowWidth > sdg.globals.mediumBreak) {
        if (equalWidths.length > 0) {
            equalWidths.each(function() {
                var sections = $(this).find('.equal'),
                    useWidth = 0;

                sections.width('auto');

                if (sections.length > 1) {
                    sections.each(function() {
                        var h = $(this).innerWidth();
                        if (compareSizes(useWidth, h)) {
                            useWidth = h;
                        }
                    });
                    sections.css('width', useWidth + 'px');
                }

                $(this).addClass('equalized');
            });
        }
    } else {
        equalWidths.find('.equal').css('width', 'auto');
    }
};

sdg.equalHeightsMobile = function() {

    var equalHeights = $('.equal-heights-mobile');

    windowWidth = $(window).width();
    if (windowWidth <= sdg.globals.mediumBreak) {
        // var desktopToo = $(this).find('.equal')
        if (equalHeights.length > 0) {
            equalHeights.each(function() {
                var sections = $(this).find('.equal'),
                    largest_height = 0;
                $(this).removeClass('equalized');
                sections.height('auto');
                sections.each(function() {
                    var h = $(this).innerHeight();
                    if (largest_height < h) {
                        largest_height = h;
                    }
                });
                equalHeights.css('height', largest_height + 'px');
                $(this).addClass('equalized');
            });
        }
    }
    // else {
    //     if ( !equalHeights.first().hasClass('equal-heights') ) {
    //         equalHeights.find('.equal').css('height', 'auto');
    //     }
    // }
};

sdg.toCamelCase = function(str) {
    return str.toString().replace(/(?:^\w|\-[A-Z]|\b\w)/g, function(letter, index) {
        return index === 0 ? letter.toLowerCase() : letter.toUpperCase();
    }).replace(/\s+/g, '').replace('-', '');
};
