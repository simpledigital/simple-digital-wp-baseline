/**
 * Mobile Javascript Functions
 */

sdg.nagivationTriggers = function () {
    var navTriggers = $('.nav-trigger');

    navTriggers.on('click', function(event) {
        event.stopImmediatePropagation();
        event.preventDefault();
        $(this).toggleClass('active');
        $($(this).attr('href')).toggleClass('active');
    });
};