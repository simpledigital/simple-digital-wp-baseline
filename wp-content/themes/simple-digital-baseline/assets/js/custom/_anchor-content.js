/**
 * Anchor Content
 */

sdg.anchorContent = function() {
    if ($('.anchor-nav').length && ($('.anchor-nav li').length <= 4) && $('.hero-section .container').length) {
        $('.hero-section .container').append($('.anchor-nav').addClass('row moved'));
        var anchorNav = $('nav.anchor-nav.moved ul'),
            anchorChildren = anchorNav.find('>li');
        anchorNav.addClass('contains-' + anchorChildren.length);
    }
    var anchorLinks = $('.anchor-link:not(.actual-trigger):not(.filter-item-target)');

    if (anchorLinks.length) {
        anchorLinks.on('click', function(event) {
            event.preventDefault();
            sdg.scrollToElement($(this).attr('href'));
            $(this).parent('li').addClass('current').siblings().removeClass('current');
            $('.back-to-top').addClass('active');
            sdg.pushHashState($(this).attr('href'));
        });
    }

    $(window).scroll(function() {
        if ($(this).scrollTop() >= 200) {
            $('.back-to-top').addClass('active');
            window.setTimeout(function() {
                $('.back-to-top').addClass('idle');
            }, 2000);
        } else {
            $('.back-to-top').removeClass('active');
            window.setTimeout(function() {
                $('.back-to-top').addClass('idle');
            }, 2000);
        }
    });

    $('.back-to-top').on('click', function(event) {
        event.preventDefault();
        sdg.scrollToElement();
    });

};
