/*global $:true*/
/**
 * Init JavaScript - This script is executed first in the final compiled app.js
 *
 * @package Simple Digital Baseline JavaScript
 * @author Simple Digital Marketing
 * @repo https://bitbucket.org:sdgmarketing/sdg-baseline-theme.git
 */

$ = jQuery;
var sdg = sdg || {};