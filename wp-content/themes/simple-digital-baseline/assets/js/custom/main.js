/**
 * Main JavaScript - This script is executed last in the final compiled app.js
 *
 * @author Simple Digital Marketing
 */

/**
 * On Runtime
 */
// var newsdgProject = newsdgProject || {};
var initGlobals = new sdg.initGlobals();
var initGeneralScripts = new sdg.generalJavaScripture();
// var initiateTextFilters = new sdg.Filters.initiateTextSearchFilter();

/**
 * On Document Ready
 */
$(document).ready(function() {
    var anchorNavInit = new sdg.anchorContent();
    var adminFrontEnd = new sdg.adminFrontEnd();
    var mobileMenu = new sdg.nagivationTriggers();
    var fixedHeaderNav = new sdg.fixedNav();
    var fixActiveNav = new sdg.fixCurrentActiveStates();
    var loadMoreInit = new sdg.infiniteScroll();
    var sliderScripts = new sdg.slidersGo();
    var initAccordions = new sdg.collapsibleContent();
});

/**
 * On Window Load
 */
$(window).on('load', function() {
    var registerActions = new sdg.registerActions();
    $(window).trigger('resize');
});

/**
 * On Window Resize
 */
$(window).on('resize', function() {
    sdg.equalHeights();
    sdg.equalHeightsInner('equal-other-inner');
    sdg.equalHeightsMobile();
});

/**
 * On Window Scroll
 */
$(window).on('scroll', function() {
});
