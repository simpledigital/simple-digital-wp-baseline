// /**
//  * Main JavaScript - This script is executed last in the final compiled app.js
//  *
//  * @author Simple Digital Marketing
//  */

// /**
//  * On Runtime
//  */
// // var newsdgProject = newsdgProject || {};
// var initGlobals = new sdg.initGlobals();
// var initGeneralScripts = new sdg.generalJavaScripture();
// // var initSiteSearch = new sdg.siteSearch();
// // var mobileMenu = new sdg.mobileMenu();
// // var initSlider = new sdg.slidersGo();

// /**
//  * On Document Ready
//  */
// $(document).ready(function() {
//     var adminFrontEnd = new sdg.adminFrontEnd();
//     var mobileMenu = new sdg.nagivationTriggers();
//     var fixedHeaderNav = new sdg.fixedNav();
// });

// /**
//  * On Window Load
//  */
// $(window).on('load', function() {
//     $(window).trigger('resize');
//     // $('.slider-for').slick({
//     //   slidesToShow: 1,
//     //   slidesToScroll: 1,
//     //   arrows: false,
//     //   fade: true,
//     //   asNavFor: '.slider-nav'
//     // });
//     // $('.slider-nav').slick({
//     //   slidesToShow: 3,
//     //   slidesToScroll: 1,
//     //   asNavFor: '.slider-for',
//     //   dots: true,
//     //   focusOnSelect: true
//     // });
// });

// /**
//  * On Window Resize
//  */
// $(window).on('resize', function() {
//     sdg.equalHeights();
//     sdg.equalHeightsMobile();
// });

// /**
//  * On Window Scroll
//  */
// $(window).on('scroll', function() {
// });
