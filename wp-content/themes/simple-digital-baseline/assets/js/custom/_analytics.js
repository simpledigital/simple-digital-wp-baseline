/**
 * Simple Digital Marketing Baseline Theme Analytics-Related Javascript Functions
 *
 * @package Simple Digital Baseline JavaScript
 * @author Simple Digital Marketing
 * @repo https://bitbucket.org:sdgmarketing/sdg-baseline-theme.git
 */

/**
 * Manually set & push analytics tracking info to Google
 *
 * @param child element
 * @uses ga (Google Analytics), pageTitle()
 */
sdg.analyticsTrack = function(path, title) {
    var docTitle = title || pageTitle(path);
    ga('set', { page: path, title: docTitle });
    ga('send', 'pageview');
    if ( document.location.hash === "#test" ) {
        console.log('tracked: ' + path + ' ~ ' + docTitle);
    }
};
