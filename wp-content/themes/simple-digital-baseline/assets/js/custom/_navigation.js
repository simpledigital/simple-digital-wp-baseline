/**
 * Navigation JavaScripture
 */

sdg.fixedNav = function() {
    if ($('#primary-navigation').length) {
        var distance = 150,
            $window = $(window),
            primaryNav = $('#primary-navigation');

        // if ($('.bar').length) {
        //     distance = $('.bar').offset().top + $('.bar').height();
        // } else {

        distance = ($('main>*:first-child').offset().top - $('#primary-navigation').height());
        distanceCollapse = ($('main>*:first-child').offset().top);

        // }

        $window.scroll(function() {
            primaryNav.removeClass('collapsed');
            if ($(window).scrollTop() > distance) {
                // Your div has reached the top
                primaryNav.addClass('slim');
                window.setTimeout(function() {
                    if ($(window).scrollTop() > distanceCollapse) {
                        primaryNav.addClass('collapsed');
                    } else {
                        primaryNav.removeClass('slim');
                        primaryNav.removeClass('collapsed');
                    }
                }, 7000);
            } else {
                primaryNav.removeClass('slim');
                primaryNav.removeClass('collapsed');
            }
        });
    }
};

sdg.fixCurrentActiveStates = function() {
    /* Check that current page item is not set in primary navigation */
    var pageInstances = ['/news/', '/working-at-ma/', '/events/', '/event/', '/resource/', '/resources/'];
    if ($('#primary-navigation .current-menu-item').length === 0) {
        $.each(pageInstances, function() {
            if (document.location.pathname.indexOf(this) > 0) {
                var navItems = $('.menu-item a[href*="/' + this + '"]');
                if (navItems.length) {
                    $.each(navItems, function() {
                        $(this).parent('li').addClass('current-menu-item');
                    });
                }
            }
        });
    }
};

sdg.accordionNavigation = function() {
    /* Check that current page item is not set in primary navigation */
    var babyUl = $('header.main-header nav div > ul > li.menu-item-has-children > ul');

    babyUl.find('>li.menu-item-has-children >a').append(
        '<i class="fa icon submenu-trigger" aria-label="Toggle Sub-Navigation" role="navigation" onclick="$(this).parent(\'a\').toggleClass(\'active\').parent(\'li\').toggleClass(\'active-parent\');"><span class="show-for-sr">Toggle Sub-Navigation</span></i>'
    );

    this.toggleSubNav = function(item) {
        // console.log(this);
        // console.log(item);
        // console.log($(item));
        if (!$(item).parents('li').hasClass('current-menu-item')) {
            $(item).toggleClass('active');
            $(item).parent('li').toggleClass('active-parent');
        } else {
            if ($(item).is('a')) {
                console.log('yes an a, let us go!');
            } else {
                console.log('not an a, prevent all!');
            }
        }
    };

    var me = this;

    babyUl.on('click', '>li>a', function(event) {
        var clickedLink = event.target;
        // if ( $(tar).hasClass('active') && $(tar).is('header.main-header nav div > ul > li.menu-item-has-children > ul >li.menu-item-has-children >a') ) {
        // go to page
        // } else {
        if ($(clickedLink).is('.submenu-trigger') ||
            (
                $(clickedLink).find('>.submenu-trigger').length > 0 && !$(clickedLink).hasClass(
                    'active')) &&
            ($(clickedLink).is(
                'header.main-header nav div > ul > li.menu-item-has-children > ul >li.menu-item-has-children >a'
            ))
        ) {
            console.log($(this));
            if (!$(this).next('ul').find('.current-menu-item').length) {
                event.preventDefault();
            }
            me.toggleSubNav(clickedLink);

        }
    });
};
