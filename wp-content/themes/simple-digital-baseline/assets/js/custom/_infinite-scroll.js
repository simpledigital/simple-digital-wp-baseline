/**
 * Infinite Scroll
 *
 * @author Simple Digital Marketing
 */

sdg.infiniteScroll = function(append) {
    if (typeof append === 'undefined') {
        append = true;
    }
    var loadWrapper = $('.load-more-wrapper'),
        loadTrigger = $('.load-more-trigger'),
        loadContent = $('.load-more-content');

    if (loadTrigger.length > 0) {
        var postsPerPage = loadContent.find('>*>*').length;
        loadTrigger.on('click', function(event) {
            if ($(this).attr('disabled') === 'disabled') {
                event.preventDefault();
                event.stopImmediatePropagation();
                return false;
            } else {

                event.preventDefault();
                $('.loader').addClass('loading');
                var me = $(this),
                    loadHref = me.attr('href'),
                    loadID = me.attr('id'),
                    loadWrapper = $(this).closest('.load-more-wrapper'),
                    itemContent, nextLoad;

                loadWrapper.addClass('loading');

                var ajaxTime = new Date().getTime();

                $.ajax({
                    url: loadHref,
                    context: document.body,
                    success: function(result) {
                        var totalTime = new Date().getTime() - ajaxTime,
                            itemContent = $(result),
                            loadHtml = itemContent.find(
                                '.load-more-content').html()
                            .trim(),
                            nextTrigger = itemContent.find(
                                '.load-more-trigger');

                        me.attr('href', nextTrigger.attr('href'));
                        console.log(loadHtml.length);

                        if (append === true) {
                            loadWrapper.find('.load-more-content').append(
                                itemContent.find('.load-more-content').html()
                            );
                        } else {
                            loadWrapper.html(itemContent);
                        }

                        setTimeout(function() {
                            $('.loader').removeClass('loading');
                            loadWrapper.removeClass('loading');
                            // sdg.infiniteScroll();
                            sdg.equalHeights();
                            sdg.equalHeightsRowless();
                        }, 500);
                        if (loadHtml.length <= 0) {
                            me.hide();
                        } else {
                            me.show();
                        }

                        if (nextTrigger.attr('href') === undefined) {
                            me.hide();
                        }
                        // console.log(postsPerPage);
                        // console.log($('.load-more-content>*>*').length);
                        if (itemContent.find('.load-more-content>*>*').length < 6) {
                            $('.load-more-trigger').fadeOut().attr('disabled', 'disabled').text('No More Posts').attr('onclick', 'void');
                            loadWrapper.removeClass('loading');
                            $('.load-more-trigger').fadeIn();
                        }
                    },
                    error: function(response) {
                        $('.load-more-trigger').fadeOut().attr('disabled', 'disabled').text('No More Posts').attr('onclick', 'void');
                        loadWrapper.removeClass('loading');
                        $('.load-more-trigger').fadeIn();
                    }
                }).done(function() {});
            }
        });
    }
};
