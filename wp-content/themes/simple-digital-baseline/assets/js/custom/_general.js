/**
 * General JavaScripture
 */

sdg.generalJavaScripture = function() {
    var disabledOverlays = $('.disabled-overlay'),
        hostname = new RegExp(location.host);

    // Useful for iframes & interactive embedded content (like maps)
    disabledOverlays.on('click', function(event) {
        $(this).addClass('enabled');
    });


    // Add 'target="_blank"' attribute to all external links
    $('a[href]').each(function() {
        var me = $(this),
            url = me.attr('href');
        if (hostname.test(url)) {
            me.addClass('internal-link');
        } else if (url.slice(0, 1) == '/') {
            me.addClass('internal-link');
        } else if (url.slice(0, 1) == '#') {
            // if ( !me.hasClass('filter-item-target') && !me.hasClass('actual-trigger') ) {
            //     me.addClass('anchor-link');
            // }
        } else if (url.indexOf('javascript') !== 0) {
            me.addClass('external-link').attr('target', '_blank');
        }
        if (me.find('img').length > 0 ) {
            me.addClass('image-link');
        }
    });

    // Remove empty paragraphs
    var elemsToEmpty = ['small', 'p'];
    elemsToEmpty.forEach(function(elem) {
        $(elem).each(function() {
            // if ( $(this).html().length )
            if ($(this).html().replace(/\s|&nbsp;/g, '').length === 0) {
                $(this).remove();
            }
        });
    });

    if ($('body.login-page').length) {
        var actionArea = $('.tml-rememberme-wrap'),
            submitWrap = $('.tml-submit-wrap'),
            actionLinks = $('.tml-action-links'),
            theclass = 'row';
        if (actionArea.length === 0) {
            theclass = 'row';
        } else {
            theclass= 'row';
        }
        // if (actionArea.length === 0) {
            $('.gglcptch').addClass(theclass).find('>*:first-child').addClass('col medium-7').after(
                '<div class="col medium-5 action-links-placeholder">' + '</div></div>');
        // } else {
        //     submitWrap.before(
        //         '<div class="pre-submit-row row"><div class="col medium-7 remember-me-placeholder"> &nbsp;' +
        //         ' </div><div class="col medium-5 action-links-placeholder">' +
        //         '</div></div>');
        // }
        // $('.g-recaptcha').append($('.tml-action-links'));

        $('.tml-rememberme-wrap').after(actionLinks);
        // $('.remember-me-placeholder').append(actionArea);
        $('.tml-login').addClass('row');

        $('#user_login').attr('placeholder', 'Username...');
        $('#user_pass').attr('placeholder', 'Password...');
    }
    if ($('.slider,.wp1s-bxslider').length ) {
        $('main').addClass('overflow-hidden');
    }
    if ( $('#breadcrumb-nav ul:empty').length ) {
        $('#breadcrumb-nav').remove();
        $('body').addClass('no-breadcrumbs');

    }
};
