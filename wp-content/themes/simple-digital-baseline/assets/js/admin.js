/**
 *
 * ADMIN JS
 *
 */

var sdg = sdg || {};
var sdgAdmin = sdgAdmin || {};

/**
 * Only do console.log if it's a development domain
 */

sdgAdmin.devLog = function(logVar) {
    if (document.domain.indexOf('.local') > -1 || document.location.hash === '#test') {
        console.log(logVar);
    }
};


sdg.addJavascriptAdmin = function(jsname) {
    if (jsname.length > 0) {
        scriptElem = document.createElement('script');
        scriptElem.setAttribute('type', 'text/javascript');
        scriptElem.setAttribute('src', jsname);
        jQuery('body').append(scriptElem);
    }
};

sdg.sdgAdminScripts = function() {
    function getThemeDir() {
        var myScript = jQuery('script[src*="wp-content/themes/"]');
        myScript = (myScript[0] !== undefined) ? myScript[0].src.replace(/themes\/(.*?)\/(.*)/g,
            'themes/$1') : null;
        return myScript;
    }

    /* Customize page editor */
    /* Excerpt label unhide */

    /* Global variables */
    var themeDir = getThemeDir();

    /* Load common JS */
    if (themeDir.length > 0) {
        sdg.addJavascriptAdmin(themeDir + '/assets/js/custom/_globals.js');
        sdg.addJavascriptAdmin(themeDir + '/assets/js/custom/_common.js');
        sdg.addJavascriptAdmin(themeDir + '/assets/js/custom/_string-helpers.js');
    }
    if ( $('body').hasClass('options-reading-php') ) {
        $('#posts_per_page').parents('tr').hide();
    }
    wpAdminMenu = $('#adminmenu');
    wpAdminMenu.find('.menu-icon-dashboard[href="index.php"] .wp-menu-name').text('Admin Panel'); //attr('href', '/');
    wpAdminMenu.find('>.wp-first-item:first:first-child').before('<li class="wp-first-item wp-not-current-submenu menu-top menu-top-first menu-icon-homepage menu-top-last opensub" id="menu-homepage-front"><a aria-haspopup="false" class="wp-first-item wp-has-submenu wp-not-current-submenu menu-top menu-top-first menu-icon-multisite menu-top-last" href="/"><div class="wp-menu-image dashicons-before dashicons-admin-multisite"></div><div class="wp-menu-name">View Dashboard</div></a></li>');
    if ( $('body').attr('class').indexOf('post-type-')) {
        var postTypeTitle = $('body').attr('class').split('post-type-');

        if ( postTypeTitle !== undefined && postTypeTitle.length > 0 ) {
            postTypeTitle = postTypeTitle[1];
        }
        if ( postTypeTitle !== undefined && postTypeTitle.length > 0 ) {
            postTypeTitle = postTypeTitle.split(' ')[0];
        }
        if ( postTypeTitle !== undefined && postTypeTitle.length > 0 ) {
            postTypeTitle = postTypeTitle.replaceAll('_', ' ').replace('Tribe', '').toTitleCase();
        }

        if (typeof postTypeTitle !== 'undefined' && postTypeTitle.length > 0 ) {
            jQuery('label[for="excerpt"]').removeClass('screen-reader-text').text(postTypeTitle +
                ' Summary').wrap('<h4 id="excerpt-header-element" class="section-header"></h4>');
            jQuery('#postdivrich').before(
                '<h5 id="main-page-content-header-element" class="section-header">Main Page Content</h5>'
            );
            $('#postexcerpt').remove();
            $('#title-prompt-text').text(postTypeTitle + ' Name/Title');
            $('#title-prompt-text').before('<h4 class="section-header">Name This    ' + postTypeTitle + '</h4>');
        }
        /* Auto-Select ACF Fields for Archive Post Pages */
        var postTypeSel = $('.wp-admin [data-type="post_type_selector"] select'),
            acfFeatured = $('.wp-admin [data-name="featured"]');

        // if ( $('body').hasClass('post-type-page') ) {
        //     postTypeSel.attr('disabled', 'disabled');
        //     postTypeSel.find('option:contains(Posts)').prop('selected', 'selected');
        //     acfFeatured.hide();
        // }
    }

    if ( $('[data-name="resource_embed_code"').length ) {
        $('[data-name="resource_embed_code"').find('textarea').wrap('<code class="code-text-field"></code>');
    }

    if ($('#postdivrich:visible').length == 0) {
        $('#main-page-content-header-element').hide();
    }

    // addJavascriptAdmin('/wp-content/themes/dublincvb/assets/js/custom/_common.js');
    $('body').addClass('admin-post-' + sdg.getParameterByName('post'));
    /* Target homepage specifically */
    // if (sdg.getParameterByName('post') === '4') {
    //     $('.hidden-posts-list').show();
    //     $('body').addClass('admin-home');
    // }
    if ($('body').hasClass('wp-admin')) {
        if ($('body').hasClass('post-type-location')) {
            if ($('#location-acf-geo-latitude input').val() === '' && $(
                    '#location-acf-geo-longitude input').val() === '') {
                if (getGeoCoordinates() === true) {
                    window.setTimeout(function() {
                        $('form#post').submit();
                    }, 1000);
                }
            }
        }
        locationsGeoAddress = $('#location-acf-geo-address input');
        locationsGeoAddress.on('blur', function(event) {
            var getCoords = getGeoCoordinates();
        });

    }

    function getGeoCoordinates() {
        var apiAddress = $('#location-acf-geo-address input').val();

        if (apiAddress !== '') {

            $.getJSON({
                url: 'https://maps.googleapis.com/maps/api/geocode/json',
                data: {
                    sensor: false,
                    address: apiAddress
                },
                success: function(data, textStatus) {
                    userData = data;
                    console.log(textStatus);
                    if (sdg.isSet('userData.results')) {
                        userData = data.results[0];
                        console.log(userData);
                        if (sdg.isSet('userData.address_components')) {
                            var geoData = userData;
                            returnVal = true;
                            if (typeof geoData.geometry.location.lat !==
                                'undefined') {
                                $('#location-acf-geo-latitude input').val(geoData.geometry
                                    .location.lat);
                            } else {
                                returnVal = false;
                            }
                            if (typeof geoData.geometry.location.lng !==
                                'undefined') {
                                $('#location-acf-geo-longitude input').val(geoData.geometry
                                    .location.lng);
                            } else {
                                returnVal = false;
                            }
                            return returnVal;
                        }
                    }
                }
            });

        }
    }

    if ( typeof acf !== 'undefined' ) {
        acf.add_action('append', function($el) {
            // $el will be equivalent to the new element being appended $('tr.row')
            // find a specific field
            sdgAdmin.ACFMethods();
            // do something to $field
        });
    }
};

sdgAdmin.imgTooltipPreview = function() {
    var $ = jQuery;
    /*
     * Url preview script
     * powered by jQuery (http://www.jquery.com)
     *
     * written by Alen Grakalic (http://cssglobe.com)
     *
     * for more info visit http://cssglobe.com/post/1695/easiest-tooltip-and-image-preview-using-jquery
     *
     */

    /* VARS */
    cssClass = '';

    /*CONFIG */
    xOffset = 10;
    yOffset = 30;

    // these 2 variable determine popup's distance from the cursor
    // you might want to adjust to get the right result

    /* END CONFIG */

    function getPosition(element) {
        var xPosition = 0;
        var yPosition = 0;

        while (element) {
            xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
            yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
            element = element.offsetParent;
        }
        return {
            x: xPosition,
            y: yPosition
        };
    }


    if ($("#my-screenshot-tooltip").length === 0) {
        $('body').append("<div id='my-screenshot-tooltip' class='" + cssClass + "'></div>");
    }

    // $('.acf-fields').on({
    //     click: function(e) {
    //         // sdgAdmin.acf.colorPaletteMods();
    //     },
    // }, '[data-event="add-layout"],[data-event="add-row"]');

    $('.acf-fields').on({
        mouseenter: function(e) {
            // sdgAdmin.devLog('Hover');
            // $(".screenshot").hover(function (e) {
            var me = this,
                cssClass = ((window.innerWidth - getPosition(me).x) < (window.innerWidth / 2)) ? 'alt' : '';

            if ($('#my-screenshot-tooltip:empty').length === 0) {
                $("#my-screenshot-tooltip").hide();
                $("#my-screenshot-tooltip").removeClass('loading alt').empty();
                window.setTimeout(function() {
                    me.t = me.title;
                    // me.title = "";
                    var c = (me.t != "") ? "<br/>" + me.t : "";
                    $(me).after($('#my-screenshot-tooltip').addClass(cssClass));
                    $('#my-screenshot-tooltip').fadeIn().addClass('loading').html("<img src='" + me.href + "' alt='url preview' /><strong>" + c + "</strong>");
                }, 500);
            } else {
                me.t = me.title;
                // me.title = "";
                var c = (me.t != "") ? "<br/>" + me.t : "";
                $(me).after($('#my-screenshot-tooltip').addClass(cssClass));
                $('#my-screenshot-tooltip').fadeIn().addClass('loading').html("<img src='" + me.href + "' alt='url preview' /><strong>" + c + "</strong>");
            }
        },

        mouseleave: function(e) {
            window.setTimeout(function() {
                $("#my-screenshot-tooltip").fadeOut('slow', function() {
                    $("#my-screenshot-tooltip").removeClass('loading alt').empty();
                });
            }, 500);
        }
    }, '.screenshot');

};

// sdgAdmin.acf.colorPaletteMods = function() {
//     var $ = jQuery;
//     $('input.wp-color-picker').each(function() {
//         $(this).iris('option', 'palettes', sdg.globals.colorPalette);
//     });
// };
sdgAdmin.acf = sdgAdmin.acf || {};

sdgAdmin.acf.otherMods = function() {
    var $ = jQuery;

    $('.acf-edit-chosen-post').remove();

    function newACFOption(newVal) {
        return function() {
            var $ = jQuery;
            console.log(this);
            pID = this.value;
            if (pID > 0) {
                var parentField = $(this).parent();
                parentField.find('.acf-edit-chosen-post').remove();
                parentField.append('<p> <a class="acf-edit-chosen-post" href="/wp-admin/post.php?post=' + pID + '&action=edit&lang=en" target="_blank">Edit Post</a></p>');
            }
        }
    }

    // alert($('[data-name="the_banner"]').length);
    $('[data-name="include_sections"]').each(function() {
        var me = $(this),
            inp = me.find('select+input:hidden'),
            pID = inp.val();
        inp.on('change', newACFOption($(this).val()));
        if (pID > 0) {
            inp.parent().append('<p> <a class="acf-edit-chosen-post" href="/wp-admin/post.php?post=' + pID + '&action=edit&lang=en" target="_blank">Edit Section</a></p>');
        }
        // .find('input')
    });
};


sdgAdmin.ACFMethods = function() {
    // sdgAdmin.acf.colorPaletteMods();
    sdgAdmin.acf.otherMods();
};

jQuery(document).ready(function() {
    // var initAdminScripts = new sdgAdmin.initAdminScripts();
    // var initAdminGlobalVars = new sdgAdmin.initAdminGlobals();
    var initImgTooltipPreview = new sdgAdmin.imgTooltipPreview();
    sdgAdmin.ACFMethods();
    sdgAdmin.devLog('Loaded Admin Scripts');
});

jQuery(document).on('acf/setup_fields', function(e) {
    sdgAdmin.ACFMethods();
});

window.onload = function() {
    // jQuery(document).ready(function($) {
        // var taxonomy = 'mytaxonomy';
        // jQuery('#' + taxonomy + 'checklist li :radio, #' + taxonomy + 'checklist-pop :radio').live( 'click', function(){
        //     var t = $(this), c = t.is(':checked'), id = t.val();
        //     $('#' + taxonomy + 'checklist li :radio, #' + taxonomy + 'checklist-pop :radio').prop('checked',false);
        //     $('#in-' + taxonomy + '-' + id + ', #in-popular-' + taxonomy + '-' + id).prop( 'checked', c );
        // });
    // });
};

window.setTimeout(function() {
    if (typeof jQuery === 'undefined ') {
        (function() {
            var s = document.createElement('script');
            s.setAttribute('src', '//ajax.googleapis.com/ajax/libs/jquery/1/jquery.js');
            if (typeof jQuery == 'undefined') {
                document.getElementsByTagName('head')[0].appendChild(s);
            }
            jQuery("td.edit select option[value=BN]").attr("selected", "");
        })();
    }
    $ = jQuery;
    var sdgAdminScriptsInit = new sdg.sdgAdminScripts();
}, 500);
