jQuery(function() {
	jQuery(document)
		.ready(function(){
			// fix content padding for header height
			init_header_style();
			jQuery('p.cff-post-text span.cff-text').dotdotdot();

			// change form labels to placeholders in account sidebar login form
			jQuery(".account-sidebar form :input").each(function(index, elem) {
				var eId = jQuery(elem).attr("id");
				var label = null;
				if (eId && (label = jQuery(elem).parents("form").find("label[for="+eId+"]")).length == 1) {
					jQuery(elem).attr("placeholder", jQuery(label).html());
				}
			 });

			 jQuery('.widget_nav_menu').find('li:has(ul)').addClass('parent');

			var sidebar   = jQuery(".sticky > .wpb_wrapper"),
				window    = jQuery(window),
				offset     = sidebar.offset(),
				topPadding = 15;

			window.scroll(function() {
				console.log(window.scrollTop());
				if (window.scrollTop() > offset.top) {
					sidebar.stop().animate({
						marginTop: window.scrollTop() - offset.top + topPadding
					});
				} else {
					sidebar.stop().animate({
						marginTop: 0
					});
				}
			});

			// tosrus popup options
			var popup_options = {keys:{close:true,prev:false,next:false},buttons:{prev:false,next:false}};

			// replace [span] with <span> in Page Builder's tabbed content
			jQuery('.vc_tta-title-text').each(function(){
				jQuery(this).html(jQuery(this).html().replace('[span]','<span>').replace('[/span]','</span>'));
			})

		})
		.on('click','.main-navigation-button a', function(event){
			event.preventDefault();
			jQuery('.mobile-nav .main-navigation').toggleClass('visible');
		})
		.on('click', function(event){
//			jQuery("a.tos-close").trigger("click");
		});

	jQuery(window)
		.on('ready',init_header_style() )
		.on('resize',function(){
			init_header_style();
		});

    /* smooth scroll for anchored links */
    jQuery('a[href*=\\#]:not([href=\\#])').click(function() {
    	/* ignore accordion magic */
    	var acc_attr = jQuery(this).attr('data-vc-accordion');
    	var tab_attr = jQuery(this).attr('data-vc-tabs');
    	if((typeof acc_attr === typeof undefined || acc_attr === false) && (typeof tab_attr === typeof undefined || tab_attr === false)){
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			  var target = jQuery(this.hash);
			  target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
			  if (target.length) {
				jQuery('html,body').animate({
				  scrollTop: target.offset().top ,
				  easing: "swing"
				}, 750);
				return false;
			  }
			}
		}
    });
});

// attach header styles and headroom function
function init_header_style(){
	setTimeout(function(){ // timeout to make sure DOM is actually ready
		if( jQuery(window).width() < 768 ){ var prefix = '.mobile-'; } else { var prefix = '.'; }
		// jQuery('.content').css('padding-top',(jQuery(prefix + 'header-wrapper').height() + 35 ) + 'px');
		// sticky on upscroll
		jQuery(prefix + 'header-wrapper').headroom('destroy').removeData('headroom');
		jQuery(prefix + 'header-wrapper').headroom({tolerance: 20, offset: jQuery(prefix + 'header-wrapper').height(), onPin : function() { jQuery('.mobile-nav .main-navigation').removeClass('visible'); }});
		if( jQuery(window).width() >= 768 ){
			// position sub-menus
			jQuery('.page-header .vc_wp_custommenu').each(function(){
				jQuery(this).css('margin-bottom','-' + (jQuery('.page-header .vc_wp_custommenu ul').height() / 2) + 'px' );
			});
		}
	},200);
}