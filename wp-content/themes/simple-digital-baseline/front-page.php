<?php
/**
 * Template Name: Front Page
 *
 * @author sdg
 */

global $sdg_opts;

get_header();

while ( have_posts() ) : the_post(); ?>

    <section class="end-section content">

        <div class="row more text-center inner full-mobile">
            <?php get_template_part('parts/featured-news-slider'); ?>
        </div>

        <?php get_template_part('parts/global-nav-links'); ?>

    </section>

    <?php get_template_part('parts/events-module' ); ?>

    <?php get_template_part('parts/important-resources');

endwhile; /* end of the loop. */

get_footer();
