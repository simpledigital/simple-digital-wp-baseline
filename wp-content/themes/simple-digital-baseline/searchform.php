<?php
/**
 * Search Form
 *
 * @author sdg
 */

?>

<form role="search" method="get" id="searchform" class="search searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="Search..." />
</form>

<?php /**
<!-- <div class="searchform">
    <form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
        <div>
            <input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="Search" />
        </div>
    </form>
</div> -->

*/ ?>