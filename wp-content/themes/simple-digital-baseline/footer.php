<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content.
 *
 * @package sdg
 * @subpackage sdg
 * @since sdg
 */

global $sdg;

?>
	<!-- </div>  --><!-- .container.main -->

</main> <!-- .page -->

<footer class="site-footer">
    <div class="row full-row">
        <div class="col medium-5">
            <?php get_template_part('parts/social-nav'); ?>
        </div>
        <div class="col medium-7 copyright pad-top">
            <?php sdg_option('footer_contact_info'); ?>
            <p>
                <?php echo strip_tags(sdg_get_field('global_options_copyright')); ?>
            </p>
        </div>
    </div>
    <!-- Return to Top -->
    <a href="javascript:" id="back-to-top-link" class="floating-button right back-to-top idle" title="Back to Top">
        <span class="show-for-sr">Back to Top</span>
    </a>
</footer> <?php

wp_footer(); ?>

<script>
// TODO: Add to theme options
// var $buoop = {vs:{i:9,f:-4,o:-4,s:8,c:-4},api:4};
// function $buo_f(){
//  var e = document.createElement("script");
//  e.src = "//browser-update.org/update.min.js";
//  document.body.appendChild(e);
// };
// try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
// catch(e){window.attachEvent("onload", $buo_f)}
</script> <?php
if (sdg_is_login_page()): ?>
    <script src="<?php echo get_stylesheet_directory_uri(). '/assets/js/app.js'; ?>" type="text/javascript" charset="utf-8" async defer></script> <?php
endif; ?>
</body>
</html>