<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 *
 * @package sdg
 * @subpackage sdg
 * @since sdg 1.0
 */
global $post;
get_header(); ?>
<div class="content">
	<?php
	$template_post = get_posts(array('name' => 'template', 'post_type' => 'location-template', 'post_status' => 'publish',  'numberposts' => 1));
	$template = $template_post[0]->post_content;
	$template = str_replace("{LOCATION_ID}",$post->ID,$template);
	$template = str_replace("{LOCATION_SLUG}",$post->post_name,$template);
	$template = str_replace("{LOCATION_TITLE}",get_the_title(),$template);
	$template = str_replace("{LOCATION_NAME}",get_field('location_shortname'),$template);
	$template = str_replace("{LOCATION_SUMMARY}",get_field('location_summary'),$template);
	$template = str_replace("{LOCATION_ADDRESS}",get_field('location_address'),$template);
	$template = str_replace("{LOCATION_CITY}",get_field('location_city'),$template);
	$template = str_replace("{LOCATION_STATE}",get_field('location_state'),$template);
	$template = str_replace("{LOCATION_ZIP}",get_field('location_zip'),$template);
	$template = str_replace("{LOCATION_PHONE}",get_field('location_phone'),$template);
	$template = str_replace("{LOCATION_FAX}",get_field('location_fax'),$template);
	$template = str_replace("{LOCATION_PHONE}",get_field('location_phone'),$template);
	$template = str_replace("{LOCATION_CAPACITY}",get_field('location_capacity'),$template);
	$template = str_replace("{LOCATION_THUMBNAIL}",get_the_post_thumbnail(),$template);
	$template = str_replace("{LOCATION_SLIDER_CATEGORY}",get_field('location_slider_category'),$template);

	$service_amenity_list = "";
	foreach(get_field('location_services_amenities') as $service_amenity):
		$service_amenity_list .= "<li>".$service_amenity['location_services_amenities_name']."</li>";
	endforeach;
	$template = str_replace("{LOCATION_SERVICES_AMENITIES}",$service_amenity_list,$template);

	$template = (get_field('location_map_pin') != '' ? str_replace("{LOCATION_MAP_PIN}",get_field('location_map_pin'),$template) : str_replace("{LOCATION_MAP_PIN}",get_the_title() . ' ' . get_field('location_city') . ',' . get_field('location_state'),$template)	);

	// get all posts of all similar service locations
	$similar_ids = implode(',',get_field('location_similar_locations'));
	$similar_locations = get_posts( array( 'post_type' => 'location', 'post_status' => 'publish', 'include' => $similar_ids ) );

	// init all locations content
	$template_similar = '<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-equal-height centered">';

	// single location markup
	$_template_similar = '<div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper">
							<div class="wpb_text_column wpb_content_element ">
								<div class="wpb_wrapper">
									<p><div class="vc_btn3-container image-button vc_btn3-center with-description location-button" style="background: url({LOCATION_SIMILAR_BGURL}) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;">
													<a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-custom vc_btn3-block" href="{LOCATION_SIMILAR_URL}" style="color:#ffffff;" title=""><span class="location-wrapper"><span class="location-title">{LOCATION_SIMILAR_TITLE}</span>{LOCATION_SIMILAR_CITYSTATE}</span></a>
												</div></p>
								</div>
							</div>
						</div></div></div>';

	// get data for each location
	foreach( $similar_locations as $this_post):
		$this_similar = $_template_similar;
		$this_similar = str_replace("{LOCATION_SIMILAR_URL}", get_post_permalink($this_post),$this_similar);
		$this_similar = str_replace("{LOCATION_SIMILAR_BGURL}", get_the_post_thumbnail_url($this_post),$this_similar);
		$this_similar = str_replace("{LOCATION_SIMILAR_TITLE}", get_the_title($this_post),$this_similar);
		$this_similar = str_replace("{LOCATION_SIMILAR_CITYSTATE}", get_field("location_city",$this_post->ID) . ',' . get_field("location_state", $this_post->ID),$this_similar);

		$template_similar .= $this_similar;
	endforeach;
	// close the similar location row
	$template_similar .= "</div>";
	// put into location template
	$template = str_replace("{SIMILAR_LOCATIONS}",$template_similar,$template);

	$img_header = get_field('location_header_image');
	$img_services = get_field('location_services_amenities_image');
	$img_similar_bg = get_field('location_similar_image');

	$page_css = get_vc_css($template_post[0]->ID, false);
	// output our page with styles with blog loop in place
	echo '<style>';
	echo $page_css ;
	echo ".location-header{background-image: url($img_header) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}";
	echo ".location-services{background-image: url($img_services) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}";
	echo ".location-service-finder{background-image: url($img_similar_bg) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}";
	echo '</style>';


	echo do_shortcode($template);
	?>
</div>
<?php get_footer(); ?>