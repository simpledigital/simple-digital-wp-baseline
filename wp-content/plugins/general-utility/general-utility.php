<?php
/**
 * @package General Utility
 * @version 1.1
 */
/*
Plugin Name: General Utility
Plugin URI: http://www.hb2web.com/wordpress/plugins/image-fixer
Description: This plugin fixes all kinds of problems!
Author: Jed Haldeman	
Version: 1.1
Author URI: http://www.hb2web.com
*/

ini_set('max_execution_time', 300); //300 seconds = 5 minutes
ini_set('memory_limit','128M');

// Constants
if ( ! defined( 'IMGFIX_PLUGIN_BASENAME' ) )
	define( 'IMGFIX_PLUGIN_BASENAME', plugin_basename( __FILE__ ) );

// name of our plugin
if ( ! defined( 'IMGFIX_PLUGIN_NAME' ) )
	define( 'IMGFIX_PLUGIN_NAME', trim( dirname( IMGFIX_PLUGIN_BASENAME ), '/' ) );

// absolute path of the plugin on the server
if ( ! defined( 'IMGFIX_PLUGIN_DIR' ) )
	define( 'IMGFIX_PLUGIN_DIR', WP_PLUGIN_DIR . '/' . IMGFIX_PLUGIN_NAME );

// URL of the plugin
if ( ! defined( 'IMGFIX_PLUGIN_URL' ) )
	define( 'IMGFIX_PLUGIN_URL', WP_PLUGIN_URL . '/' . IMGFIX_PLUGIN_NAME );

// URL of the plugin images
if ( ! defined( 'IMGFIX_PLUGIN_IMAGE_URL' ) )
	define( 'IMGFIX_PLUGIN_IMAGE_URL', IMGFIX_PLUGIN_URL . '/images' );

// relative path of the plugin  to the root of the site
if( ! defined( 'IMGFIX_PLUGIN_PATH' ) )
	define( 'IMGFIX_PLUGIN_PATH', "/" . str_replace(ABSPATH,'',WP_PLUGIN_DIR) . "/" . IMGFIX_PLUGIN_NAME . "/" );

global $template_path;

//	this function adds the settings page to the Appearance tab
add_action('admin_menu', 'add_image_fixer_menu');

function add_image_fixer_menu() {
	add_submenu_page('options-general.php', 'General Utility', 'General Utility', 'upload_files', 'general-utility', 'general_utility_admin');
}

function general_utility_admin() { 
	
	global $wpdb;

    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    echo '<div class="wrap">';

    if(!empty($_POST)):
    	switch($_POST['action']):

			case 'import_resources':

				if(isset($_FILES['uploaded'])):

					$tmp = $_FILES['uploaded']['tmp_name'];
					echo "uploaded file: " . $tmp . "<br />";
					if(file_exists($tmp)):
						echo "file exists<br />";
					endif;

					$name = "temp.csv";
					$tempdir = "csvtemp/";

					$csvfile = $tmp;

					$fh = fopen($csvfile, 'r');
					$headers = fgetcsv($fh);
					$pages = array();
					
//					pprint_r($headers);

					foreach( $headers as $k => $v ):
						$header_found = (strtolower(trim($v)) == 'filename' ? true : false);
						if( $header_found ) break;
					endforeach;
					if( $header_found === false ):
						echo '<p>ERROR: CSV file needs at least a "post_title" column.</p>';
					else:
					
						while (!feof($fh)):
							$row = fgetcsv($fh);
							if (!empty($row)):
								$obj = new stdClass;
								foreach ($row as $i => $value):
									$key = $headers[$i];
									$obj->$key = $value;
								endforeach;
								$pages[] = $obj;
							endif;
						endwhile;
						fclose($fh);
						$c = 0;

					endif; // post_title check

				endif; // file uploaded check
											
				echo "<h1>Import</h1>";

				$counter = 0;
				if(!empty($pages)):
				
					foreach($pages as $key => $page):

						// pprint_r($page);

						$counter++;
						$post_title = ( $page->title != '#N/A' ? $page->id . ' ' . $page->title : preg_split('~.(?=[^.]*$)~', $page->filename)[0] );
						$post_content = ( $page->description != '#N/A' ? $page->description : '' );
						$filename = sanitize_file_name($page->filename);
						echo $filename . "<br />";
						echo $post_title . "<br />";
						echo "<a href='/wp-content/uploads/2017/12/" . $filename . "'>link</a><br />";
						echo ( file_exists('/var/www/vhosts/origobranding.com/cssintranet.origobranding.com/wp-content/uploads/2017/12/' . $filename)  ? 'true' : 'false' ) . "<br />";
						$file_type = wp_check_filetype( '/var/www/vhosts/origobranding.com/cssintranet.origobranding.com/wp-content/uploads/2017/12/' . $filename )['type'];
						echo $file_type . "<br />";
						// create initial post
						$post = array(
							'import_id'			=> (isset($page->ID) ? ((int) $page->ID) : 0),
							'post_parent'    	=> 0,
							'post_title'     	=> $post_title,
							'post_content'		=> apply_filters('the_content', $post_content),
							'comment_status' 	=> 'closed',
							'ping_status'    	=> 'open',
							'post_author'    	=> get_current_user_id(),
							'post_date'      	=> date("Y-m-d H:i:s"),
							'post_name'      	=> sanitize_title($post_title),
							'post_status'    	=> 'publish',
							'post_type'      	=> 'resource'
						); 
					
						pprint_r($post);

						$post_id = wp_insert_post($post, true);
						if ( is_wp_error( $post_id ) ):
							echo $post_id->get_error_message();
						else:
							echo "inserted '" . ucwords(strtolower($post_title)) . "' : " . $post_id . "<br />";    
							$attach_id = wp_insert_attachment( array('post_title' => $filename, 'post_content' => '', 'post_status' => 'publish', 'post_mime_type' => $file_type ), '/var/www/vhosts/origobranding.com/cssintranet.origobranding.com/wp-content/uploads/2017/12/' . $filename);
							update_field('file',$attach_id,$post_id);
							if( $page->category_id != '#N/A'):
								wp_set_post_terms( $post_id, $page->category_id,'resource_category');
							endif;
						endif;
					endforeach;
				else:
					echo "No pages retrieved<br />";
				endif;

			break; // import_resources

			case 'import_team_members':

				if(isset($_FILES['uploaded'])):

					$tmp = $_FILES['uploaded']['tmp_name'];
					echo "uploaded file: " . $tmp . "<br />";
					if(file_exists($tmp)):
						echo "file exists<br />";
					endif;

					$name = "temp.csv";
					$tempdir = "csvtemp/";

					$csvfile = $tmp;

					$fh = fopen($csvfile, 'r'); 
					$headers = fgetcsv($fh); // field names
					$pages = array(); // will hold our data
					
					foreach( $headers as $k => $v ):
						// quick/dumb check for our field names. check for the first expected one.
						$header_found = (strtolower(trim($v)) == 'first_name' ? true : false);
						if( $header_found ) break;
					endforeach;
					if( $header_found === false ):
						echo '<p>ERROR: CSV file needs at least a "first_name" column.</p>';
					else:
					
						// read csv data into an array of objects
						while (!feof($fh)):
							$row = fgetcsv($fh);
							if (!empty($row)):
								$obj = new stdClass;
								foreach ($row as $i => $value):
									$key = $headers[$i];
									$obj->$key = $value;
								endforeach;
								$pages[] = $obj;
							endif;
						endwhile;
						fclose($fh);
						$c = 0;

					endif; // first_name check

				endif; // file uploaded check
											
				echo "<h1>Import</h1>";

				$counter = 0;
				if(!empty($pages)):
				
					foreach($pages as $key => $page):

						$counter++;
						$post_title = $page->first_name . ' ' . $page->last_name;
						$post_content = '';

						// create initial post
						$post = array(
							'import_id'			=> 0,
							'post_parent'    	=> 0,
							'post_title'     	=> $post_title,
							'post_content'		=> apply_filters('the_content', $post_content),
							'comment_status' 	=> 'closed',
							'ping_status'    	=> 'open',
							'post_author'    	=> get_current_user_id(),
							'post_date'      	=> date("Y-m-d H:i:s"),
							'post_name'      	=> sanitize_title($post_title),
							'post_status'    	=> 'publish',
							'post_type'      	=> 'team_member'
						); 
					
						$post_id = wp_insert_post($post, true);
						if ( is_wp_error( $post_id ) ):
							echo $post_id->get_error_message();
						else:
							echo "inserted '" . ucwords(strtolower($post_title)) . "' : " . $post_id . "<br />";
							//Add all our meta data: first_name, last_name, title, program, office, phone, email
							update_field('first_name',$page->first_name,$post_id);
							update_field('last_name',$page->last_name,$post_id);
							update_field('job_title',$page->job_title,$post_id);
							update_field('program',$page->program,$post_id);
							update_field('office',$page->office,$post_id);
							update_field('phone',$page->phone,$post_id);
							update_field('email',$page->email,$post_id);
						endif;
					endforeach;
				else:
					echo "No team members retrieved<br />";
				endif;

			break; // import_team_members
			
		endswitch;
	else:

    ?>
	<h2>General Utility</h2>

	<p>This plugin does all KINDS of fun stuff.</p>
	    <hr />
	<h3>Import Resources</h3>
	<p>Upload CSV with columns labeled as <strong>"ID", "post_title", "post_content",</strong> and <strong>"template"</strong></p>
	<p><em>(Non-existent columns will be ignored)</em></p>
	<form method="post" enctype="multipart/form-data">
		<input type="hidden" name="action" value="import_resources" />
		<input type="file" name="uploaded" />
		<input type="submit" value="Import Resources" class="button" />
	</form>
	<hr />

	<h3>Import Team Members</h3>
	<p>Upload CSV with columns labeled as <strong>"first_name","last_name","title","program","office","phone","email"</p>
	<p><em>(Non-existent columns will be ignored)</em></p>
	<form method="post" enctype="multipart/form-data">
		<input type="hidden" name="action" value="import_team_members" />
		<input type="file" name="uploaded" />
		<input type="submit" value="Import Team Members" class="button" />
	</form>
	<hr />

	<form method="post">
	    <input type="hidden" name="action" value="make_templates" />
	    <input type="submit" value="Make Templates" class="button" />
	</form>
	<hr />

	<h3>Import Calendar</h3>
	<form method="post" enctype="multipart/form-data">
		<input type="hidden" name="action" value="import_new_calendar" />
		<input type="file" name="uploaded" />
		<input type="submit" value="Import" class="button" />
	</form>
	<hr />

	<h3>Import Posts</h3>
	<form method="post" enctype="multipart/form-data">
		<input type="hidden" name="action" value="import_posts" />
		<input type="file" name="uploaded" />
		<input type="submit" value="Import" class="button" />
	</form>

	<?php
	
    endif;
    echo '</div>';
}

/**
 * from symfony's jobeet tutorial (via StackOverflow article (http://stackoverflow.com/questions/2955251/php-function-to-make-slug-url-string)
 * 
 */
function slugify($string){ 
  return strtolower(preg_replace('/[^A-Za-z0-9-]+/', '-', trim($string)));
}

if(!function_exists('pprint_r')):
	function pprint_r($array){
		echo "<pre>" . print_r($array,true) . "</pre>";
	}
endif;

?>